import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
		View,
		Switch,
} from 'react-native';

export interface Props {
  dataAccount: any;
  edit: Function;
	toggleSwitchAccount: Function;
	toggleSwitchBalance: Function;
}

const CardBanckAccounts: React.FC<Props> = (props: any) => {
  let numberCard = props.dataAccount[0].cardNumber.substring(props.dataAccount[0].cardNumber.length - 4, props.dataAccount[0].cardNumber.length)
  return (
    <>
			<View style={styles(props).containerCard}>
				<View style={styles(props).accountBank}>
					<View style={{backgroundColor: 'white', justifyContent: 'center',alignItems:'center', borderRadius: 3}}>
						<Text style={styles(props).textCardNumber}>{numberCard}</Text>
					</View>
					<View>
						<Text style={styles(props).textCardAccounts}>Checking account, MXN</Text>
					</View>
					<TouchableOpacity onPress={props.edit}>
						<Image style={styles(props).iconEdit} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604997219/app/Crecy%20icons%20new%20dimensions/Crecy_app_icons_edit_pencil_settings_t6wvd1.png'}}></Image>
					</TouchableOpacity>
				</View>
			</View>
			<View style={styles(props).containerSwitch}>
				<View style={{width: '50%', justifyContent: 'center', alignItems: 'center', borderRightColor: 'black'}}>
					<Text style={styles(props).textSwitch}>
						Show the account
					</Text>
					<Switch
						trackColor={{ false: "#767577", true: "#7b3eff" }}
						thumbColor={props.dataAccount[0].account ? "white" : "#f4f3f4"}
						onValueChange={() => props.toggleSwitchAccount(props.dataAccount[0].account, props.dataAccount[0].cardNumber)}
						value={props.dataAccount[0].account}
					/>
				</View>
				<View style={{width: '50%', justifyContent: 'center', alignItems: 'center'}}>
					<Text style={styles(props).textSwitch}>
						Show the balance
					</Text>
					<Switch
						trackColor={{ false: "#767577", true: "#7b3eff" }}
						thumbColor={props.dataAccount[0].balance ? "white" : "#f4f3f4"}
						onValueChange={() => props.toggleSwitchBalance(props.dataAccount[0].balance, props.dataAccount[0].cardNumber)}
						value={props.dataAccount[0].balance}
					/>
				</View>
			</View>
		</>
  );
};

const styles = (props: any) => StyleSheet.create({
  containerCard: {
    backgroundColor: 'red',
		borderRadius: 9,
  },
  accountBank: {
		marginVertical: 20,
		justifyContent: 'space-around',
		flexDirection: 'row',
	},
	containerSwitch: {
		flexDirection: 'row',
		marginVertical: 20,
  },
  textCardNumber: {
    fontFamily: 'Baloo2-Regular',
    color: 'red',
		fontSize: 14,
		marginHorizontal: 5
	},
  textSwitch: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 16,
		color: '#5a6175',
		marginVertical: 5,
  },
  textCardAccounts: {
    fontFamily: 'Baloo2-Bold',
    color: 'white',
		fontSize: 17,
  },
  iconEdit: {
    width: 15,
		height: 15,
		marginTop: 5
  },
  containerInfoCardBalance: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerLinkedAccount: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textLinkedAccount: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 14,
    color: props.color,
  },
  iconLinkedAccount: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: props.color,
    width: 20,
    height: 20,
    borderRadius: 50,
    paddingTop: 2,
    marginLeft: 5,
  },
  numberLinkedAccount: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 14,
    lineHeight: 20,
    color: props.background,
  },
  numberCardBalance: {
    color: props.color,
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 24,
  },
  decimalsCardBalance: {
    color: props.color,
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 20,
  },
  containerUpdatedDate: {
    justifyContent: 'flex-end',
  },
  buttonUpdatedDate: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: 40,
  },
  textUpdatedDate: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 12,
    color: props.color,
  },
  iconUpdatedDate: {
    width: 19,
    height: 23,
    marginLeft: 5,
    marginRight: 10,
  }
});

export default CardBanckAccounts;
