import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';

export interface Props {
  jobType: string;
  selectedJobType: string | null;
  onPress: Function;
}

const JobTypeSelector: React.FC<Props> = (props: any) => {
  const jobtypeImage =
    props.jobType == 'gigWorker'
      ? 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833465/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_uber_rappi_ocupacion_lcvsfr.png'
      : 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833465/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_formal_worker_lk3k6i.png';

  const checkImage =
    props.selectedJobType == props.jobType
      ? 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1606238025/app/Crecy%20icons%20new%20dimensions/Crecy_icons_financial_edu_done_udoxp3.png'
      : 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1605955019/app/Crecy%20icons%20new%20dimensions/Crecy_icons_done_grey_2_hy9grk.png';

  const jobtypeText =
    props.jobType == 'gigWorker'
      ? 'Repartidor o conductor'
      : 'Trabajador formal';

  const jobtypeCaption =
    props.jobType == 'gigWorker'
      ? '(trabajo para plataforma: Uber, Rappi, etc.)'
      : '(trabajo para una empresa - pronto)';

  return (
    <>
      <TouchableOpacity onPress={() => props.onPress(props.jobType)}>
        <View style={styles(props).selectionContainer}>
          <View style={styles(props).checkboxContainer}>
            <Image
              source={{ uri: checkImage }}
              style={styles(props).checkImage}
            />
          </View>
          <View style={styles(props).jobtypeContainer}>
            <Image
              source={{ uri: jobtypeImage }}
              style={styles(props).jobtypeImage}
            />
            <Text style={styles(props).jobtypeText}>{jobtypeText}</Text>
          </View>
          <View style={styles(props).checkboxContainer} />
        </View>
        <View style={styles(props).jobtypeCaption}>
          <Text style={styles(props).jobtypeCaptionText}>{jobtypeCaption}</Text>
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  selectionContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  checkboxContainer: {
    width: 50,
    paddingRight: 20,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  checkImage: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  jobtypeImageContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  jobtypeContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 10,
    paddingHorizontal: 20,
    paddingBottom: 10,
    paddingTop: 30,
    width: 140,
    height: 140,
    backgroundColor: props.jobType == 'gigWorker' ? '#fdd25c' : '#7f47dd',
    opacity: props.jobType == 'gigWorker' ? 1 : 0.3,
  },
  jobtypeText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 10,
    lineHeight: 12,
    color: props.jobType == 'gigWorker' ? '#767471' : 'white',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  jobtypeImage: {
    width: props.jobType == 'gigWorker' ? 80 : 50,
    height: props.jobType == 'gigWorker' ? 50 : 50,
    resizeMode: 'contain',
  },
  imageContainer: {
    flex: 0.4,
  },
  jobtypeCaption: {
    justifyContent: 'center',
    marginTop: 10,
  },
  jobtypeCaptionText: {
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'center',
    fontSize: 14,
    color: '#5d6478',
  },
});

export default JobTypeSelector;
