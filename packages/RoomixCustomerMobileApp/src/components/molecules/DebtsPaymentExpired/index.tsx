// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
} from 'react-native';

export interface Props {
  status: string;
}

const DebtsPaymentExpired: React.FC<Props> = (props) => {


  return (
    <>
      <View style={styles(props).container}>
        <View style={styles(props).containerReference}>
          <Text style={styles(props).textReference}># DE REFERENCIA: {props.loans.reference}</Text>
        </View>
        <View style={styles(props).containerDebt}>
          <View style={styles(props).containerDebtInformation}>
            <Text style={styles(props).textDebtInformation}>
              RETIRO DE {`${(props.loans.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[0]}`} MXN DE LA LINEA DE CRÉDITO
            </Text>
            <View style={styles(props).containerStatusPayment}>
              <Text style={styles(props).textStatus}>
                PAGO VENCIDO
              </Text>
            </View>
          </View>
          <View style={styles(props).containerAmountPayment}>
            <Text style={styles(props).textToPay}>TOTAL A PAGAR:</Text>
            <View style={styles(props).containerCredit}>
              <Text style={styles(props).CreditNumber}>
                {`$${(props.loans.amount + (props.loans.amount * props.loans.rate)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[0]}.`}
              </Text>
              <Text style={styles(props).CreditDecimal}>
                {(props.loans.amount + (props.loans.amount * props.loans.rate)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".").length > 1 ? (props.loans.amount + (props.loans.amount * props.loans.rate)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[1].substring(0,2) : '00'}
              </Text>
            </View>
          </View>
        </View>
        {props.status === 'rejected' ?
          <View style={{marginVertical: 15, paddingBottom: 15}}>
            <View style={styles(props).containerTime}>
              <Image style={styles(props).iconTime} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1616540287/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/Crecy_credit_line_expired_payment_hlhxnc.png'}}></Image>
              <Text style={styles(props).textDebtInformationTime}>TU TARJETA NO PASÓ</Text>
            </View>
            <View style={styles(props).containerButtonTransparent}>
              <Text style={styles(props).TextButtonTime}>
                Por favor, paga tu deuda manuelmente. Recuerda que con cada semana
                retraso a tu deuda se agregan los intereses moratorios 
              </Text>
            </View>
          </View>
          : 
          <View style={{marginVertical: 15, paddingBottom: 15}}>
            <View style={styles(props).containerTime}>
              <Image style={styles(props).iconTime} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1616540287/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/Crecy_credit_line_expired_payment_hlhxnc.png'}}></Image>
              <Text style={styles(props).textDebtInformationTime}>ES URGENTE PAGAR ESTA DEUDA</Text>
            </View>
            <View style={styles(props).containerButtonTransparent}>
              <Text style={styles(props).TextButtonTime}>
                En caso de no pagar tu deuda hoy antes de las 12 de la noche tendrás que pagar un interés moratorio de 1.42% por cada día de retraso. Todos los días de retraso se reportarán a buró de crédito.
              </Text>
            </View>
          </View>
        }
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 5,
    backgroundColor: '#ffffff',
    borderRadius: 12,
    paddingVertical: 5,
    paddingHorizontal: 10
    
  },
  containerReference: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  textReference: {
    fontFamily: 'Avenir-Heavy',
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#ed1e79',
  },
  textReferenceId: {
    fontFamily: 'Avenir-Heavy',
    textTransform: 'uppercase',
    fontSize: 12,
    color: '#ed1e79',
  },
  containerDebt: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingHorizontal: 5,
    marginVertical: 5,
    borderBottomColor: '#f1f1f1',
    borderBottomWidth: 1,
    marginTop: 30,
    paddingBottom: 25
  },
  containerDebtInformation: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: '50%',
  },
  containerStatusPayment: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ed1e79',
    marginTop: 5,
    borderRadius: 3
  },
  textDebtInformation: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 12,
    color: '#5a6175',
    lineHeight: 14
  },
  textDebtInformationTime: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 11,
    color: '#ed1e79',
    lineHeight: 15,
    marginBottom: 10
  },
  textStatus: {
    fontFamily: 'AvenirLTStd-Medium',
    textTransform: 'uppercase',
    fontSize: 11,
    color: '#ffffff',
    marginHorizontal: 4,
    marginVertical: 3
  },
  containerAmountPayment: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    width: '25%',
    marginLeft: 20,
  },
  textToPay: {
    fontFamily: 'AvenirLTStd-Light',
    textTransform: 'uppercase',
    fontSize: 10,
    color: '#5a6175',
  },
  containerCredit: {
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    flexDirection: 'row'
  },
  CreditNumber: {
    fontFamily: 'AvenirLTStd-Medium',
    textTransform: 'uppercase',
    fontSize: 22,
    paddingTop: 0,
    color: '#5a6175',
  },
  CreditDecimal: {
    fontFamily: 'AvenirLTStd-Book',
    textTransform: 'uppercase',
    fontSize: 15,
    paddingTop: 10,
    paddingBottom: 3,
    color: '#5a6175',
  },
  iconTime: {
    width: 20,
    height: 20,
    marginRight: 5,
    marginBottom: 10
  },
  containerTime: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    margin: 0,
  },
  containerButtonTransparent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  TextButtonTime: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    color: '#5a6175',
    textAlign: 'center',
    paddingHorizontal: 35,
  },
});

export default DebtsPaymentExpired;
