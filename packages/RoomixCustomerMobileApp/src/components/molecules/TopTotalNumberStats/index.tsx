// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';
import months from 'src/utils/months';

export interface Props {
  month: any;
  spent: any;
  body: any;
}

const TopTotalNumberStats: React.FC<Props> = (props: any) => {
  const [formBody, setFormBody] = React.useState(props.body)
  React.useEffect(
    React.useCallback(() => {
      console.log('Spent' + props.month)
      setFormBody(props.body)
    }, [props.body])
  );
  return (
    <View style={{marginLeft: 20, marginTop: 10, width: '100%'}}>
      <View style={styles.containerExpenses}>
        <Text style={styles.textMoney}> 
          ${props.spent === '' ? formBody[formBody.length -1].expenses.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[0] : props.spent.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[0]}.
        </Text>
        <Text style={styles.textDecimal}>
          {props.spent === '' ? (formBody[formBody.length -1].expenses.toString().includes('.') ? formBody[formBody.length -1].expenses.toString().split(".")[1].substring(0,2) : '00') : ( props.spent.toString().includes('.') ? props.spent.toString().split(".")[1].substring(0,2) : '00')}
        </Text>
        <View style={styles.containerMonth}>
          <Text style={styles.textMonth}>
            {props.month === '' ? months[formBody[formBody.length -1].month].name : months[props.month].name}
          </Text>
        </View>
      </View>
      <View style={{width: '50%', marginBottom: 15}}>
        {/* <Text style={styles.textDescription}>You´ve spent 34% of what you spent last month</Text> */}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerExpenses: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  textMoney: {
    fontFamily: 'AvenirLTStd-Medium',
    color: '#5a6175',
    fontSize: 26,
  },
  textDecimal: {
    fontFamily: 'AvenirLTStd-Medium',
    color: '#5a6175',
    fontSize: 18,
    marginTop: 7,
    marginRight: 4
  },
  containerMonth: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: '#7b3eff',
    borderRadius: 6,
    marginTop: 1,
    marginLeft: 6
  },
  textMonth: {
    fontFamily: 'Avenir-Black',
    color: 'white',
    fontSize: 13,
    paddingHorizontal: 10,
    paddingVertical: 5
  },
  textDescription: {
    fontFamily: 'Baloo2-Medium',
    color: '#b3b3b3',
    fontSize: 15,
    lineHeight: 18
  },
});

export default TopTotalNumberStats;