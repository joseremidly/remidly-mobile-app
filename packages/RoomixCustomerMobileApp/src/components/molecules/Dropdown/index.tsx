// @ts-nocheck
import React from 'react';
// import {
//   Select,
//   Option,
//   OptionList,
//   updatePosition,
// } from Dropdown;
import Select from 'src/components/atoms/Select'

import {
    StyleSheet,
    Text,
} from 'react-native';

export interface Props {

}

const Dropdown: React.FC<Props> = (props) => {
  const [value, setValue] = React.useState('');

  return (
    <>
      <Select
        defaultValue='52'
        src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599685025/app/crecy_icons_arrow_down_white_mlzooz.png'
      >
      </Select>
    </>
  );
};

const styles = StyleSheet.create({
  textPrimaryStyle: {
    flexDirection: 'row',
    fontFamily: 'Baloo2-ExtraBold',
    textAlign: 'center',
    alignSelf: 'stretch',
    color: '#5a6175',
    fontSize: 33,
    lineHeight: 40,
  },
  textSecondaryStyle: {
    flexDirection: 'row',
    fontFamily: 'Baloo2-ExtraBold',
    textAlign: 'center',
    alignSelf: 'stretch',
    color: '#7b3eff',
    fontSize: 33,
    marginTop: 0,
    lineHeight: 40,
  }
});

export default Dropdown;
