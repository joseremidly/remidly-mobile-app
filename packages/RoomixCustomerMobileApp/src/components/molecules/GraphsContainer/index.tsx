// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { BarChart, PieChart, XAxis } from 'react-native-svg-charts';
import TopSelectorStats from 'src/components/molecules/TopSelectorStats';
import TopTotalNumberStats from 'src/components/molecules/TopTotalNumberStats';
import ExpensesCategory from '../../atoms/ExpensesCategory';
import * as scale from 'd3-scale';
import categories from 'src/utils/categories';
import months from 'src/utils/months';

export interface Props {
  onPress: Function;
  onPressGeneral: Function;
  month: any;
  spent: any;
  categoryName: any;
  spentCategory: any;
  overall: Function;
  category: any;
  circleData: any;
  fillSelectCategory: any;
  fillSelectMonth: any;
  backgroundGeneral: any;
  body: any;
}

const GraphsContainer: React.FC<Props> = (props: any) => {
  const [formBody, setFormBody] = React.useState(props.body)
  
  const dataFilter = (value: any , index: any) => (
    {
      value: parseInt(value.expenses),
      svg: {
        fill: index === props.fillSelectMonth ? props.backgroundGeneral : props.backgroundGeneral+'40',
        onPress: () => props.onPressGeneral(index, value),
      },
    }
  )
  const dataName = (value: any , index: any) => (
    {
      value: parseInt(value.expenses),
      svg: {
        fill: 'black',
        fontFamily: 'Baloo2-Bold',
        fontSize: 14,
        onPress: () => props.onPressGeneral(index, value),
      },
    }
  )
  const Data = (formBody !== [] ? formBody.map(dataFilter) : '')
  const DataName = (formBody !== [] ? formBody.map(dataName) : '')
  const dataBar = (value: any , index: any) => (
    {
      key: index,
      value: value.expenses,
      svg: {
          fill: index === props.fillSelectCategory 
            ?
              `${categories[value.category].background === undefined ? categories['default'].background : categories[value.category].background}`
            : 
              `${categories[value.category].background === undefined ? categories['default'].background : categories[value.category].background}40`,
          onPress: () => props.onPress(index, value),
        },
      arc: { padAngle:  0},
    }
  )
  const dataCircle = (value: any, index: any) => (
    <View style={{marginVertical: 5}} key={index}>
      <ExpensesCategory
        category={categories[value.category].name === undefined ? categories['default'].name : categories[value.category].name}
        transactions={value.transactions}
        percentage={value.percentage*100}
        expenses={value.expenses}
        background={categories[value.category].background === undefined ? categories['default'].background : categories[value.category].background}
        src={categories[value.category].src  === undefined ? categories['default'].src : categories[value.category].src}
      />
    </View>
  )
  var DataBar
  var DataCircle
  if (props.circleData !== null) {
    DataBar = (props.circleData !== null ? props.circleData.map(dataBar) : '')
    DataCircle = (props.circleData !== null ? props.circleData.map(dataCircle) : '')
  }

  React.useEffect(
    React.useCallback(() => {
      setFormBody(props.body)
    }, [props.body])
  );
  return (
    <>
      <View style={styles.containerCard}>
        <TopSelectorStats category={props.category} overall={props.overall} />
        <TopTotalNumberStats month={props.month} spent={props.spent} body={props.body} />
          {props.category === true ?
            <View style={{marginBottom: 15}}>
              <BarChart
                style={{ height: 300 }}
                data={Data}
                gridMin={0}
                yAccessor={( {item }) => item.value}
                spacingInner={.3}
                svg={{ fill: 'rgba(134, 65, 244, 0.8)' }}
                contentInset={{ top: 20, bottom: 20, left: 20, right: 20, }}
              />
              <XAxis
                style={{ marginTop: 2, fontFamily: 'Avenir-Black'}}
                data={ DataName }
                scale={scale.scaleBand}
                formatLabel={ (_, index) => months[formBody[index].month].name }
                labelStyle={ { color: '#5A6175',  fontFamily: 'Avenir-Black'} }
                svg= {{
                  fill: '#5A6175',
                }}
              />
            </View>
          :
          <>
            <View style={{marginBottom: 15, position: 'relative'}}>
              <View style={{zIndex: 2}}> 
                {props.circleData !== null ?
                    <PieChart
                      style={{ height: 330 }}
                      data={DataBar} 
                      outerRadius={'80%'}
                      innerRadius={'59%'}
                    />
                  :
                    <View style={{width: '100%', justifyContent: 'center', alignItems: 'center', marginTop: 30}}>
                      <Text style={styles.textTypeCategoryNull}>
                        No hay registro este mes
                      </Text>
                    </View>
                }
              </View>
              <View style={styles.textCenterGraphs}>
                {props.circleData !== null ?
                    <> 
                      <Text style={styles.textTypeCategory}>
                        {props.categoryName === '' ? categories[props.circleData[0].category].name : categories[props.categoryName].name}
                      </Text>
                      <Text style={styles.textMoneyCategory}>
                        ${props.spentCategory  === '' ? props.circleData[0].expenses : props.spentCategory.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
                      </Text>
                    </>
                  :
                    <>
                      <Text style={styles.textTypeCategory}>
                        No hay ingresos este mes
                      </Text>
                    </>
                }
              </View>
            </View>
            <View style={{marginVertical: 20}} >
              {DataCircle}
            </View>
          </>
          }
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerCard: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 9,
  },
  textMoney: {
    fontFamily: 'AvenirLTStd-Medium',
    color: '#5a6175',
    fontSize: 26,
  },
  textCenterGraphs: {
    width: '100%',
    height: 330,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    zIndex: 0,
  },
  textTypeCategory: {
    width: 200,
    fontFamily: 'Avenir-Heavy',
    color: '#5a6175',
    fontSize: 12,
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  textTypeCategoryNull: {
    fontFamily: 'Baloo2-Medium',
    color: '#5a6175',
    fontSize: 20,
  },
  textMoneyCategory: {
    fontFamily: 'AvenirLTStd-Roman',
    color: '#808080',
    fontSize: 14,
    lineHeight: 20
  },
});

export default GraphsContainer;