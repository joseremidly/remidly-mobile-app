// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { ProgressCircle } from 'react-native-svg-charts';

export interface Props {
  daysToPay: number;
  maxDaysToPay: number;
  stateLoans: any;
  loansData: any;
}

const LeftDaysToPayNewsFeedGraph: React.FC<Props> = (props: any) => {
  const [daysGraph, setDaysGraph] = React.useState((10/100));
  const [daysToPay, setDaysToPay] = React.useState(15);
  const _MS_PER_DAY = 1000 * 60 * 60 * 24;

  const dateDiffInDays = (paymentDueDate) => {
    // Discard the time and time-zone information.
    const a = new Date()
    const b = new Date(paymentDueDate)
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
  }
  const getDaysToPay = (paymentDueDate) => {
    let difference = dateDiffInDays(paymentDueDate)
    setDaysToPay(difference)
    let percent = 100 - (difference * 100) / props.maxDaysToPay
    setDaysGraph(1 - (percent/100))
  }

  // 'active_debts'
  // 'debt_expired'
  // 'only_history'
  // 'empty'
  React.useEffect(() => {
    if (props.stateLoans === 'active_debts' ) {
      getDaysToPay(props.loansData[props.loansData.length - 1].payment_due_date)
    } else {
      getDaysToPay('no_debts')
    }
  }, [])
  return (
    <>
      <View style={styles.containerLeftDaysToPay}>
        <View style={{ zIndex: 0}}>
          <ProgressCircle style={{width: 80, height: 80}} strokeWidth={6} progress={props.stateLoans === 'debt_expired' ? (100/100) : daysGraph} progressColor={props.stateLoans === 'debt_expired' ? '#FF2169' : '#49c0aa'} />
        </View>
          {props.stateLoans === 'debt_expired'
            ? (
              <View style={styles.containerImgAbsolute}>
                <Text style={styles.textButtonStyleExpiredPayment} numberOfLines={0}>Deuda vencida</Text>
              </View>
            ) : (
              <>
                {daysToPay !== daysToPay
                  && (
                    <View style={styles.containerImgAbsolute}>
                      <Text style={styles.textButtonStyleEmptyPayment} numberOfLines={0}>Sin deuda</Text>
                    </View>
                  )}
                {daysToPay === daysToPay
                  && (
                    <View style={styles.containerImgAbsolute1}>
                      <Text style={styles.textButtonStyleNextPayment} numberOfLines={0}>Próximo pago en: {daysToPay} {daysToPay === 1 ? 'día' : 'días'}</Text>
                    </View>
                  )}
              </>
            )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerLeftDaysToPay: {
    position: 'relative',
    marginRight: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerImgAbsolute: {
    position: 'absolute',
    zIndex: 2,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    width: 50,
    height: 33,
  },
  containerImgAbsolute1: {
    position: 'absolute',
    zIndex: 2,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    width: 50,
    height: 40,
  },
  textButtonStyleExpiredPayment: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    fontSize: 10,
    lineHeight: 14,
    textTransform: 'uppercase',
    color: '#ffffff',
  },
  textButtonStyleEmptyPayment: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    fontSize: 11,
    lineHeight: 14,
    textTransform: 'uppercase',
    color: '#ffffff',
  },
  textButtonStyleNextPayment: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    fontSize: 9,
    lineHeight: 12,
    textTransform: 'uppercase',
    color: '#ffffff',
  },
});

export default LeftDaysToPayNewsFeedGraph;
