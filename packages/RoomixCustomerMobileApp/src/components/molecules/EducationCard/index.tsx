// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View,
} from 'react-native';

export interface Props {
  onPress: Function;
  header: String;
  information: String;
  buttonName: String;
  src: String;
  colorCard: String;
  colorText: String;
  colorTextButton: String;
  type: String;
}

const defaultProps: Props = {
  colorText: '#FFFFFF',
}

const EducationCard: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles(props).containerEducationCard}>
        <View style={styles(props)['containerImageEducationCard' + props.type]}>
          <Image style={styles(props).imageEducationCard} source={{uri: props.src}}></Image>
        </View>
        <View style={styles(props).containerInfoTotalEducationCard}>
          <View style={styles(props).containerHeader}>
            <Text style={styles(props).textHeader}>{props.header}</Text>
          </View>
          <View style={styles(props).containerInformation}>
            <Text style={styles(props).textInformation}>
              {props.information}
            </Text>
          </View>
          <TouchableOpacity style={styles(props).buttonStyle} onPress={props.onPress}>
            <Text style={styles(props).textButtonStyle}>{props.buttonName}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  containerEducationCard: {
    width: '100%',
    position: 'relative',
    justifyContent: 'space-between',
    backgroundColor: props.colorCard,
    borderRadius: 9,
    marginVertical: 7,
    paddingTop: 8,
    overflow: 'hidden',
  },
  containerImageEducationCard1: {
    width: 120,
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  containerImageEducationCard2: {
    width: 120,
    position: 'absolute',
    bottom: 0,
    right: 5,
  },
  containerImageEducationCard: {
    width: 120,
    position: 'absolute',
    bottom: 0,
    left: 30,
  },
  imageEducationCard: {
    width: '80%',
    height: 100,
  },
  containerInfoTotalEducationCard: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },
  containerInformation: {
    marginHorizontal: 50,
    marginTop: 10,
  },
  containerHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textHeader: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 20,
    color: props.colorText,
  },
  textInformation: {
    fontFamily: 'Baloo2-Medium',
    textAlign: 'center',
    color: props.colorText,
    fontSize: 14,
    lineHeight: 16,
    textTransform: 'uppercase',
    paddingHorizontal: 20,
  },
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 12,
    backgroundColor: props.colorText,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: props.colorCard,
    borderRadius: 19,
    margin: 10,
    marginBottom: 40,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Medium',
    textAlign: 'center',
    fontSize: 14,
    textTransform: 'uppercase',
    color: props.colorTextButton ? props.colorTextButton : props.colorCard,
  },
});

EducationCard.defaultProps = defaultProps;

export default EducationCard;