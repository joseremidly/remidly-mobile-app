import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    View,
} from 'react-native';
import Budget from '../../atoms/BudgetCategory';

export interface Props {
  data: any;
  more: Function;
  less: Function;
  addCategory: Function;
}

const BudgetGoal: React.FC<Props> = (props) => {
  const DataIteration = (value: any) => (
    <View style={{marginHorizontal: 8}}>
      <Budget
        label={value.label}
        number={value.number}
        src={value.src}
        more={props.more}
        less={props.less}
      ></Budget>
    </View>
  );
  const Data = (props.data !== '' ? props.data.map(DataIteration) : '');
  return (
    <>
      <View style={styles(props).principalContainer}>
        {Data}
        <View style={styles(props).buttonStyle} >
          <Text style={styles(props).textButtonStyle}>ADD BUDGET CATEGORY</Text>
          <TouchableOpacity style={styles(props).containerButtonPress} onPress={props.addCategory}>
            <Text style={styles(props).textMoreButton}>+</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  principalContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    borderRadius: 12,
  },
  buttonStyle: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    margin: 15,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Bold',
    textTransform: 'uppercase',
    fontSize: 12,
    color: '#5a6175',
    marginVertical: 12,
    marginHorizontal: 16
  },
  textMoreButton: {
    fontFamily: 'Baloo2-Bold',
    textTransform: 'uppercase',
    fontSize: 16,
    color: '#b3b3b3',
    marginVertical: 10,
    marginHorizontal: 16
  },
  containerButtonPress: {
    backgroundColor: '#E7E6FF',
    borderRadius: 50,
  },
});

export default BudgetGoal;