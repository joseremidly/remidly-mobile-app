// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Image,
    Text,
		View,
		Dimensions,
    ActivityIndicator,
} from 'react-native';
import Apply from 'src/components/atoms/PaycheckTips';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  data: Any;
  onPress: Function;
  stateService: String;
}

const ApplyPaycheck: React.FC<Props> = (props) => {
	const {width, height} = Dimensions.get('window');
  const DataIteration = (value: any) => (
    <Apply
      key={value.text}
      labelImportan={value.underline}
      label={value.text}
    ></Apply>
  );
  const Data = (props.data !== '' ? props.data.map(DataIteration) : '');
  return (
    <>
      <View style={styles({height}).principalContainer}>
				<View style={{justifyContent: 'center', alignItems: 'center',}}>
					<View style={styles({width}).containerText} >
						<Text style={styles(props).textStyle}>¿Necesitas <Text style={styles(props).textGreenStyle}>algo de efectivo extra</Text> antes de la quincena?</Text>
					</View>
					<View>
            <Image source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604014176/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_advances_case_with_money_in_a_circle_kjf3md.png' }} style={styles({height}).iconUpdatedDate} />
					</View>
					<View style={styles({width}).containerSecondParagraph} >
						<Text style={styles(props).textStyleSubtitle}>Obtén hasta $1000 MXN <Text style={styles(props).textMediumStyle}>adelantado a tu día de pago</Text> </Text>
					</View>
				</View>
				<View style={{justifyContent: 'center', alignItems: 'center', width: '80%'}}>
					{Data}
				</View>
				<View style={styles({height}).containerButton}>
          {props.stateService === 'loading'
            && (
              <ActivityIndicator size='large' color='#7b3eff' animating={true} />
            )}
          {props.stateService !== 'loading'
            && (
              <ButtonMid label='Aplicar' backgroundColor='#49c0aa' secondary={false} onPress={props.onPress}></ButtonMid>
            )}
				</View>
				<View style={styles(props).containerRequeriments}>
					<Image style={styles(props).iconRequeriments} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604014175/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_advances_see_requirements_vlkgq7.png'}} resizeMode='contain'></Image>
					<Text style={styles(props).textRequeriments}>Ver requerimientos</Text>
				</View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  principalContainer: {
		width: '100%',
    justifyContent: 'center',
		alignItems: 'center',
  },
  containerText: {
    justifyContent: 'center',
    alignItems: 'center',
		flexDirection: 'row',
		marginHorizontal: props.width <= 380 ? 30 : 50,
		marginVertical: props.height <= 380 ? 10 : 20,
  },
  containerSecondParagraph: {
    justifyContent: 'center',
    alignItems: 'center',
		flexDirection: 'row',
		marginHorizontal: props.width <= 380 ? 30 : 50,
		marginVertical: props.height <= 380 ? 20 : 40,
  },
  textStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    textTransform: 'uppercase',
    fontSize: 24,
		color: '#5a6175',
		textAlign: 'center',
		lineHeight: 30
  },
  textGreenStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    textTransform: 'uppercase',
    fontSize: 24,
    color: '#49c0aa',
  },
  textStyleSubtitle: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 20,
		color: '#5a6175',
		textAlign: 'center',
  },
  textMediumStyle: {
    fontFamily: 'AvenirLTStd-Light',
    textTransform: 'uppercase',
    fontSize: 18,
    color: '#5a6175',
  },
  textMoreButton: {
    fontFamily: 'Baloo2-Bold',
    textTransform: 'uppercase',
    fontSize: 16,
    color: '#b3b3b3',
    marginVertical: 10,
    marginHorizontal: 16
  },
  containerButtonPress: {
    backgroundColor: '#E7E6FF',
    borderRadius: 50,
	},
	iconUpdatedDate: {
    width: 120,
		height: 120,
		marginVertical: 10,
	},
	containerButton: {
		justifyContent: 'center',
		alignItems: 'center',
		marginVertical: props.height <= 680 ? 30 : 50,
	},
	containerRequeriments: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  textRequeriments: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 15,
    color: '#5a6175',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  iconRequeriments: {
    width: 20,
    height: 20,
    marginRight: 10
  },
});

export default ApplyPaycheck;