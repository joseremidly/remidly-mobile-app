import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';

export interface Props {
  isLessonDone: boolean;
  lessonNumber: number;
  lessonName: string;
  imageURI: string;
  hasLeftMargin: boolean;
}

const LessonCard: React.FC<Props> = (props: any) => {
  const navigation = useNavigation();

  const statusImageURI = props.isLessonDone
    ? 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1606238025/app/Crecy%20icons%20new%20dimensions/Crecy_icons_financial_edu_done_udoxp3.png'
    : 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1605955019/app/Crecy%20icons%20new%20dimensions/Crecy_icons_done_grey_2_hy9grk.png';

  return (
    <>
      <TouchableOpacity onPress={() => navigation.navigate('Story')}>
        <ImageBackground
          source={{ uri: props.imageURI }}
          imageStyle={{ borderRadius: 20 }}
          style={styles(props).imageBG}>
          <View style={styles(props).overlay}>
            <View style={styles(props).titleAndCheck}>
              <Text style={styles(props).lessonNumber}>
                lección {props.lessonNumber}
              </Text>
              <Image
                source={{ uri: statusImageURI }}
                style={styles(props).imageCheck}
              />
            </View>
            <Text style={styles(props).lessonName}>{props.lessonName}</Text>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    </>
  );
};

const styles = (props) =>
  StyleSheet.create({
    imageBG: {
      width: 150,
      height: 180,
      borderRadius: 20,
      marginRight: 10,
      marginLeft: props.hasLeftMargin ? 20 : 0,
    },
    overlay: {
      backgroundColor: 'rgba(0,0,0,0.5)',
      height: '100%',
      borderRadius: 20,
      justifyContent: 'space-between',
    },
    titleAndCheck: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginHorizontal: 12,
      marginTop: 12,
    },
    lessonNumber: {
      color: 'white',
      alignSelf: 'flex-start',
      fontFamily: 'AvenirLTStd-Medium',
      textTransform: 'uppercase',
    },
    lessonName: {
      color: 'white',
      alignSelf: 'center',
      textAlign: 'center',
      fontFamily: 'AvenirLTStd-Medium',
      marginBottom: 12,
    },
    imageCheck: {
      width: 20,
      height: 20,
      resizeMode: 'contain',
    },
  });

export default LessonCard;
