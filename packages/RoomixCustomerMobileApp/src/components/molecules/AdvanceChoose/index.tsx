// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  Modal,
} from 'react-native';
import { WebView } from 'react-native-webview';
import Slider from "@brlja/react-native-slider";
import ButtonMid from 'src/components/atoms/ButtonMid';
import CheckBox from '@react-native-community/checkbox';

export interface Props {
  type: boolean;
  info: string;
  src: string;
  srcChat: string;
  onPress: Function;
  values: any;
  changeValue: Function;
  maxValue: any;
  interest: any;
}

const AdvanceChoose: React.FC<Props> = (props) => {
  const [acceptContract, setAcceptContract] = React.useState(false);
  const [modalActive, setModalActive] = React.useState(false);

  let data, comision, result, restante
  if (props.values >= 10) {
    data = props.values * 10
    comision = data * props.interest
    result = data + comision
    restante = (props.maxValue * 10) - data
  }
  let date = new Date()
  date.setDate(date.getDate() + 15)
  const showModal = () => {
    setModalActive(true)
  }
  return (
    <>
      <View style={styles(props).container}>
        <Modal visible={modalActive}>
          <View style={styles(props).modal}>
            <View style={styles(props).modalContainer}>
              <WebView
                style={{ flex : 1 }}
                source={{uri: 'https://drive.google.com/file/d/1Ug3m2jyVhfO8uxWYWYe7Op3DUtcc1y22/view?usp=sharing'}}
              />
              <View style={styles(props).containerCloseButton}>
                <ButtonMid
                  label="Cerrar"
                  onPress={() => setModalActive(false)}
                  secondary={false}
                />
              </View>      
            </View>
          </View>
        </Modal>
        <View style={styles(props).containerMessageDraw} >
          <View style={styles(props).iconsViewLogo}>
            <Image style={styles(props).iconImgCrecy} source={{uri: props.src}} resizeMode='contain'></Image>
          </View>
          <View style={styles(props).iconsView}>
            <View style={{position: 'relative'}}>
              <View style={styles(props).containerImgAbsolute}>
                <Image style={styles(props).iconButtonStyle} source={{uri: props.srcChat}} resizeMode='contain'></Image>
              </View>
              <View style={{ zIndex: 3, width: 220, height: 80 }}>
                <Text style={styles(props).textNumber}>¿Cuánto dinero deseas retirar?</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles(props).containerSecondPart}>
          <View style={{alignItems: 'flex-start', width: '100%', marginVertical: 15}}>
            <Text style={styles(props).textChooseAdvance}>
              ELIGE EL MONTO
            </Text>
            <Text style={styles(props).textChooseAdvanceInformation}>
              (monto mínimo de: 100MXN)
            </Text>
          </View>
          <View style={{alignItems: 'flex-start', width: '100%', marginVertical: -10}}>
            <Text style={styles(props).textValue}>
              $ {data}
            </Text>
          </View>
          <View style={{width: '100%', marginTop: 10}}>
            <Slider
                trackStyle={styles(props).track}
                thumbStyle={styles(props).thumb}
                minimumValue={10}
                maximumValue={props.maxValue}
                value={props.values}
                onValueChange={props.changeValue}
                minimumTrackTintColor='#7b3eff'
              />
            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <View style={{alignItems: 'flex-start', width: '50%'}}>
                <Text style={styles(props).textMinMax}>$100</Text>
              </View>
              <View style={{alignItems: 'flex-end', width: '50%'}}>
                <Text style={styles(props).textMinMax}>${props.maxValue * 10}</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
          <Image style={styles(props).imgPay} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609789908/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_clendar_grey_g3suwv.png'}}></Image>
          <Text style={styles(props).textChoosePay}> PLAZO: 15 DÍAS</Text>
          <Image style={styles(props).imgPay} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609789908/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_percentage_grey_evsvlo.png'}}></Image>
          <Text style={styles(props).textChoosePay}>INTERÉS: {props.interest * 100}%</Text>
        </View>
        <View style={{flexDirection:'row',width: '100%', marginTop: 15, marginBottom: 0}}>
          <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: -8}}>
            <View style={styles(props).containerCircle}>
              <View style={styles(props).outerCircle}>
                <View style={styles(props).innerCircle} />
              </View>
            </View>
            <Text style={styles(props).textLine}>- - - - - - -</Text>
            <View style={styles(props).containerCircle}>
              <View style={styles(props).outerCircle}>
                <View style={styles(props).innerCircle} />
              </View>
            </View>
            <Text style={styles(props).textLine}>- - - - - - -</Text>
            <View style={styles(props).containerCircle}>
              <View style={styles(props).outerCircle}>
                <View style={styles(props).innerCircle} />
              </View>
            </View>
          </View>
          <View>
            <View style={{ marginVertical: 5 , paddingTop: 8}}>
              <Text style={styles(props).textMoneyHeader}>MONTO DEL RETIRO:</Text>
              <Text style={styles(props).textMoney}>${data}</Text>
            </View>
            <View style={{ marginVertical: 5, paddingTop: 10}}>
              <Text style={styles(props).textMoneyHeader}>MONTO DE INTERESES:</Text>
              <Text style={styles(props).textMoney}>${comision}</Text>
            </View>
            <View style={{ marginVertical: 5, paddingTop: 10}}>
              <Text style={styles(props).textMoneyHeader}>TOTAL A PAGAR:</Text>
              <Text style={styles(props).textMoney}>${result}</Text>
            </View>
          </View>
        </View>
        <View style={{flexDirection: 'row', alignItems:'center', justifyContent: 'center', marginBottom: 0 }}>
          <View style={{width: '50%', alignItems:'center', justifyContent: 'center', paddingLeft: 20, borderRightColor: '#e6e6e6', borderRightWidth: 1, marginBottom: 30, marginVertical: 20}}>
            <Text style={styles(props).textInformationCuenta}>SE DEPOSITARÁ A TU CUENTA BANCARIA:</Text>
            <View style={{flexDirection: 'row', alignItems:'flex-start', justifyContent: 'center', marginVertical:3, marginLeft: -60}}>
              <Image style={styles(props).imgPay} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609789908/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_arrow_velvet_f6hy2z.png'}} resizeMode='contain'></Image>
              <Text style={styles(props).textDepositoDescription}> En 10 min.</Text>
            </View>
          </View>
          <View style={{width: '45%', paddingLeft: 30, }}>
            <Text style={styles(props).textInformationDeposito}>FECHA LÍMITE DE PAGO:</Text>
            <View style={{flexDirection: 'row', alignItems:'flex-start', justifyContent: 'flex-start', marginVertical:3,}}>
              <Image style={styles(props).imgPay} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609789908/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_arrow_red_wtty6p.png'}} resizeMode='contain'></Image>
              <Text style={styles(props).textDepositoDescription}> {date.getDate()}/{date.getMonth() + 1}/{date.getFullYear()}</Text>
            </View>
          </View>
        </View>
        <View style={styles(props).containerButtonGiveMoney}>
          <View style={styles(props).checkboxOptionContainer}>
            <CheckBox
              value={acceptContract}
              onValueChange={(value: boolean) => setAcceptContract(value)}
              style={styles(props).checkbox}
              tintColors={{ true: '#7b3eff' }}
              onCheckColor='#7b3eff'
            />
            <TouchableOpacity onPress={() => showModal()}>
              <Text style={styles(props).labelCheckbox}>Acepto términos y condiciones</Text>
            </TouchableOpacity>
          </View>
          <ButtonMid
            label='Retirar'
            secondary={!acceptContract}
            onPress={() => {acceptContract ? props.onPress() : {}}}
          />
        </View>
        <View style={{marginBottom: 40}}>
          <Text style={styles(props).textResult}>
            DESPUÉS DE HACER ESTE RETIRO TE QUEDARAN ${restante} EN TU LÍNEA DE CRÉDITO
          </Text>
        </View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 5,
    backgroundColor: '#ffffff',
    borderRadius: 12,
    paddingVertical: 5,
    position: 'relative'
  },
  modal: {
    flex : 1,
    justifyContent : 'center',
    alignItems : 'center',
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  modalContainer: {
    backgroundColor : 'white',
    width : '90%',
    height : '70%',
  },
  containerCloseButton: {
    position: 'absolute',
    alignItems: 'center',
    bottom: 20,
    width: '100%'
  },
  track: {
    height: 10,
    borderRadius: 16,
    backgroundColor: '#e6e6e6',
  },
  thumb: {
    width: 40,
    height: 20,
    borderRadius: 18,
    backgroundColor: 'white',
    elevation: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
  containerMessageDraw: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  iconsViewLogo: {
    margin: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconsView: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerImgAbsolute: {
    position: 'absolute',
    top: 0,
    zIndex: 0,
  },
  textNumber: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 18,
    color: 'white',
    lineHeight: 22,
    marginHorizontal: 35,
    paddingTop: 20,
  },
  iconButtonStyle: {
    width: 220,
    height: 80,
  },
  iconImgCrecy: {
    width: 85,
    height: 85,
    marginVertical: 3,
  },
  containerSecondPart: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    paddingHorizontal: 25,
    paddingBottom: 20
  },
  textChooseAdvance: {
    width: '100%',
    flexDirection: 'row',
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'left',
    alignSelf: 'stretch',
    color: '#5a6175',
    fontSize: 14,
  },
  textChooseAdvanceInformation: {
    width: '100%',
    flexDirection: 'row',
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'left',
    alignSelf: 'stretch',
    color: '#5a6175',
    fontSize: 12,
  },
  textChoosePay: {
    flexDirection: 'row',
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'left',
    color: '#5a6175',
    fontSize: 12,
    marginHorizontal: 10
  },
  textMoneyHeader: {
    flexDirection: 'row',
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'left',
    color: '#5a6175',
    fontSize: 12,
    marginHorizontal: 0
  },
  textMoney: {
    flexDirection: 'row',
    fontFamily: 'Avenir-Black',
    textAlign: 'left',
    color: '#5a6175',
    fontSize: 18,
    marginHorizontal: 0
  },
  textLine: {
    fontFamily: 'AvenirLTStd-Medium',
    color: '#5a6175',
    fontSize: 13,
    transform: [{ rotate: "90deg" }]
  },
  textInformationDeposito: {
    flexDirection: 'row',
    fontFamily: 'Avenir-Black',
    textAlign: 'left',
    color: '#5a6175',
    fontSize: 12,
    marginTop: -10
  },
  textInformationCuenta: {
    flexDirection: 'row',
    fontFamily: 'Avenir-Black',
    textAlign: 'left',
    color: '#5a6175',
    fontSize: 12,
  },
  textDepositoDescription: {
    flexDirection: 'row',
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'left',
    color: '#5a6175',
    fontSize: 16,
    marginHorizontal: 0
  },
  textValue: {
    fontFamily: 'Avenir-Black',
    color: '#5a6175',
    fontSize: 40
  },
  textMinMax: {
    fontFamily: 'Avenir-Black',
    color: '#5a6175',
    fontSize: 14
  },
  imgPay: {
    width: 12,
    height: 12,
    marginVertical: 3
  },
  containerCircle: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    marginVertical: 19,
  },
  outerCircle: {
    borderRadius: 40,
    width: 12,
    height: 12,
    backgroundColor: "white",
  },
  innerCircle: {
    borderRadius: 35,
    width: 12,
    height: 12,
    backgroundColor: "#5a6175"
  },
  containerButtonGiveMoney: {
    marginTop: 0,
    marginBottom: 10,
  },
  textResult: {
    fontFamily: 'AvenirLTStd-Light',
    textAlign: 'center',
    color: '#5a6175',
    fontSize: 13,
    marginHorizontal: 40,
    marginTop: 20,
  },
  checkbox: {
  },
  checkboxOptionContainer: {
    marginTop: 0,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  labelCheckbox: {
    textAlign: 'center',
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    color: '#6133E2',
    textDecorationLine: 'underline'
  },
});

export default AdvanceChoose;
