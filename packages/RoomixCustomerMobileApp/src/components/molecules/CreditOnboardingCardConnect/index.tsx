// @ts-nocheck
import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import CreditOnboardingInfo from 'src/components/atoms/CreditOnboardingInfo';

import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  active: boolean;
  showError: boolean;
  headerTextOne: string;
  headerTextTwo: string;
  headerTextHighlighted: string;
  infoText: string;
  infoText2: string;
  imageURI: string;
  goToConnect: string;
  skip: Boolean;
  onSkip: Function;
}

const CreditOnboardingCardConnect: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containter}>
        <Text style={styles.headerText}>
          {props.headerTextOne}
          <Text style={styles.textHighlighted}>
            {' '}{props.headerTextHighlighted}{' '}
          </Text>
          {props.headerTextTwo}
        </Text>
        <CreditOnboardingInfo text={props.infoText} text2={props.infoText2} />
        <Image style={styles.mainImage} source={{ uri: props.imageURI }} />
        <View style={styles.buttonContainer}>
          <ButtonMid
            label='Conectar'
            secondary={props.active}
            onPress={() => props.goToConnect()}
          />
        </View>
        {props.skip
          && (
            <View style={styles.buttonContainerSkip}>
              <ButtonMid
                label='Agregar después'
                secondary={false}
                onPress={() => props.onSkip()}
              />
            </View>
          )}
        {/* <Text style={styles.whyText}>¿Por qué me piden esto?</Text> */}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containter: {
    alignItems: 'center',
  },
  headerText: {
    paddingHorizontal: 30,
    marginVertical: 10,
    textAlign: 'center',
    fontFamily: 'Avenir-Black',
    fontSize: 20,
    lineHeight: 25,
    color: '#5a6175',
  },
  textHighlighted: {
    color: '#7f47dd',
  },
  mainImage: {
    marginTop: 20,
    width: 150,
    height: 150,
    resizeMode: 'contain',
  },
  buttonContainer: {
    marginTop: 40,
  },
  buttonContainerSkip: {
    marginTop: 20,
  },
  whyText: {
    marginTop: 16,
    textAlign: 'center',
    fontFamily: 'AvenirLTStd-Roman',
    color: '#7f47dd',
  },
});

export default CreditOnboardingCardConnect;
