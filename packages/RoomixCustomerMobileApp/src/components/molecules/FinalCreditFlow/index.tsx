// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
} from 'react-native';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  amount: any;
  goToRoute: Function;
  accountNumber: any;
}

const CreditFlow: React.FC = (props: any) => {
  return (
    <>
      <View style={styles.containerCreditFlow}>
        <Text style={styles.titleCrecyLogin}>Retirar el dinero</Text>
        <View style={styles.containerInformationCreditLine}>
          <View style={styles.containerCredit}>
            <Text style={styles.creditNumber}>${props.amount}</Text>
            <Text style={styles.creditDecimal}>{'.00'}</Text>
          </View>
          <View style={styles.buttonConected}>
            <Text style={styles.textButton}>Monto retirado</Text>
          </View>
        </View>
      </View>
      <View style={styles.containerBody}>
        <Image style={styles.containerImgAbsolute} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599533279/app/crecy-login_hlr5pj.png'}} resizeMode='contain'></Image>
        <Text style={styles.crecyHeaderInformation}>¡Listo!</Text>
        <Text style={styles.crecyInformation}>Tu dinero ya está en camino y debe reflejarse en tu cuenta max. en 10 minutos</Text>
        <View style={styles.containerBanksDeposito}>
          <View style={styles.textAccountDeposit}>
            <Text style={styles.crecyDeposito}>DEPOSITARÁ A LA:</Text>
            <Text style={styles.crecyDepositoCuenta}>Cuenta terminación</Text>
          </View>
          <View style={styles.containerBanks}>
            {/* <Image style={styles.imageBanks}  source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604538258/app/Crecy_app_logo_citi_banamex_white_high_resolution_fngo6f.png'}} resizeMode='contain'></Image> */}
            <Text style={styles.textAccountNumber}>{props.accountNumber}</Text>
          </View>
        </View>
        <View style={styles.containerButtonContinue}>
          <ButtonMid
            label='Continuar'
            secondary={false}
            onPress={props.goToRoute}
          />
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerCreditFlow: {
    alignItems: 'center',
    backgroundColor: '#7b3eff',
  },
  containerBody: {
    alignItems: 'center',
    backgroundColor: '#ffffff',
    borderRadius: 17,
    width: '100%',
    flex: 1,
  },
  containerBanks: {
    backgroundColor: '#49c0aa',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 8,
  },
  containerBanksDeposito: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 8,
    marginTop: 50,
  },
  titleCrecyLogin: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 25,
    color: '#ffffff',
    marginBottom: 15,
  },
  crecyHeaderInformation: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 30,
    color: '#5a6175',
    marginTop: 100,
  },
  crecyInformation: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 20,
    lineHeight: 24,
    color: '#5a6175',
    marginTop: 20,
    marginHorizontal: 50,
    textAlign: 'center',
  },
  textAccountDeposit: {
    marginHorizontal: 10,
  },
  crecyDeposito: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 16,
    color: '#5a6175',
    textAlign: 'left',
  },
  crecyDepositoCuenta: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 16,
    color: '#5a6175',
    textAlign: 'left',
    lineHeight: 20
  },
  containerInformationCreditLine: {
    marginVertical: 10,
  },
  creditNumber: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    color: '#ffffff',
    fontSize: 40,
    textTransform: 'uppercase',
  },
  creditDecimal: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    color: '#ffffff',
    fontSize: 24,
    textTransform: 'uppercase',
    paddingTop: 6
  },
  containerCredit: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  containerImgAbsolute: {
    position: 'absolute',
    zIndex: 2,
    width: 100,
    height: 130,
    top: 30,
    left: 0
  },
  imageBanks: {
    width: 60,
    height: 40,
  },
  textButton: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    color: '#ffffff',
    textAlign: 'center',
    textTransform: 'uppercase',
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 3
  },
  textAccountNumber: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    color: '#ffffff',
    textAlign: 'center',
    textTransform: 'uppercase',
    marginVertical: 5,
    marginHorizontal: 15,
  },
  buttonConected: {
    borderRadius: 4,
    backgroundColor: '#49c0aa',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 30
  },
  containerButtonContinue: {
    marginTop: 50
  },
});

export default CreditFlow;