import React from 'react';
import { StyleSheet, Text } from 'react-native';
import StoryCardParagraph from 'src/components/molecules/StoryCardParagraph';


export interface Props {
}

const StoryCardParagraphs: React.FC<Props> = (props: any) => {
  return (
    <>
      {props.paragraphsData.map((paragraphData, index) => {
        const paragraph = paragraphData.content;
        
        return <StoryCardParagraph 
          key={index}
          mainText={paragraph.text.text}
          mainTextColor={paragraph.text.color}
          titleText={paragraph.title?.text}
          titleType={paragraph.title?.type}
          titleColor={paragraph.title?.color}
          titleBackgroundColor={paragraph.title?.backgroundColor}
          iconURI={paragraph.icon?.uri}
        />
      })}
    </>
  );
}

const styles = StyleSheet.create({
});

export default StoryCardParagraphs;
