// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    View,
    Text,
} from 'react-native';

const CreditLineStartBenefits: React.FC<Props> = (props: any) => {
  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>
        ¿ Cualés son 
        <Text style={styles.colorText}> los beneficios</Text>?
      </Text>
      <Text style={styles.informationHeaderText}>Sin búro de crédito</Text>
      <Text style={styles.informationBenefitsText}>Revisamos tus ingresos actuales, no tu historial crediticio</Text>
      <View style={styles.horizontalSeparator} />
      <Text style={styles.informationHeaderText}>Comisión fija para todos</Text>
      <Text style={styles.informationBenefitsText}>Monto fijo de comisión, transparente y sin sorpresas</Text>
      <View style={styles.horizontalSeparator} />
      <Text style={styles.informationHeaderText}>Construye tu historial crediticio</Text>
      <Text style={styles.informationBenefitsText}>Cada vez que pagas tu deuda a tiempo lo reportamos a búro de crédito</Text>
      <View style={styles.horizontalSeparator} />
      <Text style={styles.informationHeaderText}>Sin costo de apertura, sin anualidad</Text>
      <Text style={styles.informationBenefitsText}>Olvídate de la banca tradicional, no tenemos montos ocultos</Text>
      <View style={styles.horizontalSeparator} />
      <Text style={styles.informationHeaderText}>Disponible al instante</Text>
      <Text style={styles.informationBenefitsText}>El dinero que retiras de tu línea de crédito llegará directamente a tu cuenta bancaria en menos de 10 minutos</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 10,
  },
  headerText: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 24,
    color: '#5a6175',
    marginBottom: 10,
    marginHorizontal: 50,
    textAlign: 'center',
    lineHeight: 31
  },
  colorText: {
    color: '#6133E2',
  },
  informationHeaderText: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 18,
    color: '#5a6175',
    marginHorizontal: 30,
    textAlign: 'left',
    marginTop: 10
  },
  informationBenefitsText: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 16,
    color: '#5a6175',
    marginHorizontal: 30,
    textAlign: 'left',
    lineHeight: 20
  },
  horizontalSeparator: {
    marginVertical: 5,
    alignItems: 'center',
    width: '84%',
    marginHorizontal: '8%',
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#e6e6e6',
  },
});

export default CreditLineStartBenefits;
