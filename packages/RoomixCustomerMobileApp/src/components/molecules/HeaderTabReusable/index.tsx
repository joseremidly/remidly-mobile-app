import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    Platform,
    View,
} from 'react-native';

export interface Props {
  navigation: Object;
  src: String;
  title: String;
  size: Number;
}

const HeaderTabReusable: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles(props).containerHeaderTabReusable}>
        <View style={styles(props).containerTitleHeaderTabReusable}>
          <Image style={styles(props).iconTitleHeaderTabReusable} resizeMode='contain' source={{uri: props.src}}></Image>
          <Text style={styles(props).textTitleHeaderTabReusable}>{props.title}</Text>
        </View>
      </View>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  containerHeaderTabReusable: {
    height: Platform.select({
      ios: 150,
      android: 160
    }),
    alignItems: 'center',
    backgroundColor: '#7b3eff',
    width: '100%',
  },
  containerTitleHeaderTabReusable: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15
  },
  iconTitleHeaderTabReusable: {
    width: props.size,
    height: props.size,
    marginRight: 10,
  },
  textTitleHeaderTabReusable: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: '#ffffff',
  },
});

export default HeaderTabReusable;
