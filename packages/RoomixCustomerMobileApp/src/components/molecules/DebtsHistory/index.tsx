// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

export interface Props {
  paymentDate: any;
  amount: number;
  rate: number;
  daysToPay: number;
  loan: any;
  id: any;
  onPressPay: Function;
  onPressDetails: Function;
  goToLatePayment: Function;
}

const DebtsHistory: React.FC<Props> = (props) => {
  return (
    <>
      <View style={styles(props).container}>
        <View style={styles(props).containerReference}>
          <Text style={styles(props).textReference}># DE REFERENCIA: {props.id}</Text>
        </View>
        <View style={styles(props).containerDebt}>
          <View style={styles(props).containerDebtInformation}>
            <Text style={styles(props).textDebtInformation}>
              RETIRO DE {`${(props.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[0]}`} MXN DE LA LINEA DE CRÉDITO
            </Text>
            <View style={styles(props).containerStatusPayment}>
              <Text style={styles(props).textStatus}>
                DEUDA PAGADA
              </Text>
            </View>
          </View>
          <View style={styles(props).containerAmountPayment}>
            <Text style={styles(props).textToPay}>TOTAL PAGADO:</Text>
            <View style={styles(props).containerCredit}>
              <Text style={styles(props).CreditNumber}>
                {`$${(props.amount + (props.amount * props.rate)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[0]}.`}
              </Text>
              <Text style={styles(props).CreditDecimal}>
                {(props.amount + (props.amount * props.rate)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".").length > 1 ? (props.amount + (props.amount * props.rate)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[1].substring(0,2) : '00'}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 5,
    backgroundColor: '#ffffff',
    borderRadius: 12,
    paddingVertical: 5,
    paddingHorizontal: 30,
    marginVertical: 10,
  },
  containerReference: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  textReference: {
    fontFamily: 'Avenir-Heavy',
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#7b3eff',
  },
  containerDebt: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingHorizontal: 5,
    paddingTop: 0,
    marginTop: 30,
    paddingBottom: 12
  },
  containerDebtInformation: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: '55%',
  },
  containerStatusPayment: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#49c0aa',
    marginTop: 5,
    borderRadius: 3
  },
  textDebtInformation: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 12,
    color: '#5a6175',
    lineHeight: 14
  },
  textStatus: {
    fontFamily: 'AvenirLTStd-Medium',
    textTransform: 'uppercase',
    fontSize: 11,
    color: '#ffffff',
    marginHorizontal: 4,
    marginVertical: 3
  },
  containerAmountPayment: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: '35%',
    paddingBottom: 0,
    marginLeft: 40,
    marginBottom: 25
  },
  textToPay: {
    fontFamily: 'AvenirLTStd-Light',
    textTransform: 'uppercase',
    fontSize: 9,
    color: '#5a6175',
  },
  containerCredit: {
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    flexDirection: 'row'
  },
  CreditNumber: {
    fontFamily: 'AvenirLTStd-Medium',
    textTransform: 'uppercase',
    fontSize: 25,
    paddingTop: 0,
    color: '#5a6175',
  },
  CreditDecimal: {
    fontFamily: 'AvenirLTStd-Book',
    textTransform: 'uppercase',
    fontSize: 15,
    paddingTop: 10,
    paddingBottom: 3,
    color: '#5a6175',
  },
});

export default DebtsHistory;
