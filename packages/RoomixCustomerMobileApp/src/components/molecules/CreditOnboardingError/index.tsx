import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

export interface Props {}

const CreditOnboardingError: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.container}>
        <View style={styles.warningContainter}>
          <Image
            style={styles.warningIcon}
            source={{
              uri:
                'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833887/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_alert_v2rptr.png',
            }}
          />
          <Text style={styles.warningText}>Algo salió mal...</Text>
        </View>
        <Text style={styles.text}>
          Intenta otra vez o comunícate con nuestro equipo de soporte
        </Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: 30,
    alignItems: 'center',
  },
  warningContainter: {
    backgroundColor: '#fc2173',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 10,
    borderRadius: 10,
    marginBottom: 10,
  },
  warningIcon: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    marginRight: 16,
  },
  warningText: {
    color: 'white',
    fontFamily: 'AvenirLTStd-Medium',
    textTransform: 'uppercase',
  },
  text: {
    color: '#fc2173',
    textAlign: 'center',
    width: '50%',
    fontFamily: 'AvenirLTStd-Medium',
    fontWeight: 'bold',
  },
});

export default CreditOnboardingError;
