import React from 'react';
import { StyleSheet, View } from 'react-native';

export interface Props {
  totalStories: number;
  currentStory: number;
}

const StoriesNavigator: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles().container}>
        {[...Array(props.totalStories)].map(
          (value: undefined, index: number) => (
            <View
              key={index}
              style={styles(index === props.currentStory).story}
            />
          ),
        )}
      </View>
    </>
  );
};

const styles = (selected) =>
  StyleSheet.create({
    container: {
      marginTop: 16,
      flexDirection: 'row',
      marginHorizontal: 10,
    },
    story: {
      marginHorizontal: 3,
      flex: 1,
      height: 4,
      borderRadius: 2,
      backgroundColor: selected ? '#f5f1fe' : '#b795ff',
    },
  });

export default StoriesNavigator;
