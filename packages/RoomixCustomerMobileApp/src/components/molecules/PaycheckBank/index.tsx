// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Image,
    Text,
		View,
		Dimensions,
		ActivityIndicator,
} from 'react-native';
import NewUserAddBank from 'src/components/organisms/NewUserAddBank';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  stateService: String;
  onPress: Function;
	status: any;
	accounts: any;
}

const PaycheckBank: React.FC<Props> = (props) => {
	const {width, height} = Dimensions.get('window');
  return (
    <>
      <View style={styles(height).principalContainer}>
				{props.accounts.length === 0
          ? (
            <View style={{width: '90%'}}>
              <NewUserAddBank addAccount={() => props.onPress('BelvoLinkAccount')}/>
            </View>
          ) : (
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
							<View style={{marginVertical: 20}}>
								<Image source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604090158/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_add_primary_bank_account_webafd.png' }} style={styles(height).iconUpdatedDate} />
							</View>
							<View style={styles(width).ContainerSecondParagraph} >
								<Text style={styles(props).textStyle}>Tu cuenta bancaria ha sido <Text style={styles(props).textGreenStyle}>configurada exitosamente</Text></Text>
							</View>
						</View>
          )}
				<View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: props.status === false ? '#FBB03B': '#49c0aa', marginVertical: 20, borderRadius: 4, paddingHorizontal: 8}}>
					<Text style={styles(props).textStatus}>
						{props.status === false ? 'Pendiente' : 'Listo'}
					 </Text>
				</View>
				<View style={styles(height).containerButton}>
          {props.stateService === 'loading'
            && (
              <ActivityIndicator size='large' color='#7b3eff' animating={true} />
            )}
          {props.stateService !== 'loading'
            && (
              <ButtonMid label={props.status === false ? 'Iniciar' : 'Continuar'} secondary={!props.status} onPress={props.status ? () => props.onPress('PaycheckSelectAccount') : () => true}></ButtonMid>
            )}
				</View>
				<View style={styles(props).containerRequeriments}>
					<Image style={styles(props).iconRequeriments} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604090158/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_see_requirements_purple_wl2vfs.png'}} resizeMode='contain'></Image>
					<Text style={styles(props).textRequeriments}>Ver requerimientos</Text>
					<Image style={styles(props).iconRequerimentsArrow} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604090158/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_see_requirements_arrow_purple_dtexf8.png'}} resizeMode='contain'></Image>
				</View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  principalContainer: {
		width: '100%',
    justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#ffffff',
		borderRadius: 19
  },
  ContainerSecondParagraph: {
    justifyContent: 'center',
    alignItems: 'center',
		flexDirection: 'row',
		marginHorizontal: props <= 380 ? 40 : 60,
	},
  textGreenStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    textTransform: 'uppercase',
    fontSize: 23,
    color: '#7b3eff',
  },
  textStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    textTransform: 'uppercase',
    fontSize: 23,
		color: '#5a6175',
		textAlign: 'center',
		lineHeight: 27
	},
  textStatus: {
    fontFamily: 'AvenirLTStd-Roman',
    textTransform: 'uppercase',
    fontSize: 13,
		color: 'white',
		textAlign: 'center',
    marginTop: 7,
    marginBottom: 4,
  },
  textStepsKyc: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 16,
		color: '#5a6175',
		textAlign: 'left'
  },
	iconUpdatedDate: {
    width: 100,
    height: 100,
    marginVertical: 20,
	},
	containerButton: {
		justifyContent: 'center',
		alignItems: 'center',
		marginVertical: props <= 680 ? 30 : 50,
	},
	containerRequeriments: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
		flexDirection: 'row',
		marginBottom: 30,
  },
  textRequeriments: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 15,
    color: '#5a6175',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  iconRequeriments: {
    width: 20,
    height: 20,
    marginRight: 10
	},
  iconRequerimentsArrow: {
    width: 15,
    height: 15,
    marginRight: 10
  },
});

export default PaycheckBank;