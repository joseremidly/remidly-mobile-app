// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
} from 'react-native';
import Barcode from "react-native-barcode-builder";
 
export interface Props {
  status: string;
  reference: any;
  loan: any;
}

const DebtsOXXO: React.FC<Props> = (props) => {
  return (
    <View style={styles(props).container}>
      <Image style={styles(props).iconOXXO} resizeMode='contain' source={{uri : 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833310/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_oxxo_pay_y6jnfr.png'}}></Image>
      <View style={styles(props).containerDebtInformation}>
        <Text style={styles(props).textTotalPay}>TOTAL A PAGAR:</Text>
        <Text style={styles(props).textNumberToPay}>${(props.loan.amount + (props.loan.amount * props.loan.rate))}</Text>
        <Text style={styles(props).textTotalComision}>+ $12 DE COMISIÓN</Text>
        <Text style={styles(props).textTotalInstructions}>(se cobra por el OXXO)</Text>
      </View>
      <View style={styles(props).containerDebtReferences}>
        <Text style={styles(props).textReference}>
          # DE REFERENCIA 
        </Text>
        <View style={styles(props).buttonReference}>
          <Text style={styles(props).textNumberReference}>
            {props.reference !== '' ? props.reference.substring(0, 4) + ' ' + props.reference.substring(4, 8) + ' ' + props.reference.substring(8, 12) + ' ' + props.reference.substring(12, 14) : '......'}
          </Text>
        </View>
      </View>
      <View style={styles(props).containerDebtReferences}>
        <Barcode value={props.reference.toString()} format="CODE128" />
        <Text style={styles(props).textReferenceCodeBar}>
          {props.reference}
        </Text>
      </View>
    </View>
  );
};

const styles= (props: any) => StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerDebtInformation: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerDebtReferences: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10
  },
  buttonReference: {
    height: 40,
    minWidth: 160,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: props.status === 'rejected' || props.status === 'expired' ? '#ed1e79' : '#7b3eff',
    borderRadius: 4,
    paddingHorizontal: 15,
  }, 
  iconOXXO: {
    width: 200,
    height: 100,
  },
  textTotalPay: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#5a6175',
    lineHeight: 15,
    marginTop: 15,
    marginBottom: 5,
  },
  textTotalComision: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 12,
    color: '#5a6175',
    lineHeight: 15,
    marginTop: 5,
  },
  textTotalInstructions: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    color: '#5a6175',
    lineHeight: 15,
    marginBottom: 25,
  },
  textReference: {
    fontFamily: 'Avenir-Black',
    fontSize: 12,
    color: '#999999',
    marginBottom: 5
  },
  textReferenceCodeBar: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    color: '#5a6175',
    lineHeight: 15,
    marginTop: 0,
    marginBottom: 20
  },
  textNumberReference: {
    fontFamily: 'Avenir-Black',
    fontSize: 16,
    color: 'white',
  },
  textNumberToPay: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 31,
    color: '#5a6175',
  },
  
});

export default DebtsOXXO;
