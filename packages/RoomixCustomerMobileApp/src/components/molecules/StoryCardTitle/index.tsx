import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

export interface Props {
  iconURI: string | null;
  text: string;
  textColor: string;
}

const StoryCardTitle: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles(props).container}>
        {props.iconURI && (
          <View style={styles(props).imageContainer}>
            <Image
              style={styles(props).image}
              source={{ uri: props.iconURI }}
            />
          </View>
        )}
        <Text style={styles(props).text}>{props.text}</Text>
      </View>
    </>
  );
};

const styles = props =>
  StyleSheet.create({
    container: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      paddingVertical: 20,
    },
    imageContainer: {
      flexDirection: 'row',
      marginRight: 20,
      justifyContent: 'flex-end',
    },
    image: {
      width: 50,
      height: 50,
      resizeMode: 'contain',
    },
    text: {
      fontFamily: 'AvenirLTStd-Medium',
      maxWidth: '70%', // in order to force text wrapping
      // left if image is present, center if no image
      textAlign: props.iconURI ? 'left' : 'center',
      fontWeight: 'bold',
      fontSize: 18,
      color: props.textColor,
    },
  });

export default StoryCardTitle;
