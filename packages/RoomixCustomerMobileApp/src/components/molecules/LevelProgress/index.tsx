import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

export interface Props {
  level: number;
  progress: number;
}

const LevelProgress: React.FC<Props> = (props: any) => {
  const startImage =
    'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1606238025/app/Crecy%20icons%20new%20dimensions/Crecy_icons_financial_edu_star_qvjvsj.png';

  return (
    <>
      <View style={styles(props).contaner}>
        <View style={styles(props).levelInfo}>
          <Image source={{ uri: startImage }} style={styles(props).image} />
          <Text style={styles(props).text}>Nivel {props.level}</Text>
        </View>
        <View style={styles(props).progressBarBackground}>
          <View style={styles(props).progressBarFill} />
        </View>
      </View>
    </>
  );
};

const styles = (props) =>
  StyleSheet.create({
    contaner: {
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: 30,
      width: '100%',
    },
    levelInfo: {
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: 8,
    },
    image: {
      width: 30,
      height: 30,
      resizeMode: 'contain',
      marginRight: 6,
    },
    text: {
      fontSize: 20,
      fontFamily: 'AvenirLTStd-Medium',
      fontWeight: 'bold',
      color: '#ffffff',
    },
    progressBarBackground: {
      width: '20%',
      height: 10,
      backgroundColor: '#a578ff',
    },
    progressBarFill: {
      width: props.progress ? props.progress + '%' : 0,
      height: 10,
      backgroundColor: '#fff',
    },
  });

export default LevelProgress;
