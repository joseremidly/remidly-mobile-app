// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

export interface Props {
  creditAmount: any;
  availableAmount: any;
}

const AmountAvailableCreditNewsFeed: React.FC<Props> = (props: any) => {
  return (
    <View style={styles.containerCreditInformation}>
      <View style={styles.containerCreditNumber}>
        <Text style={styles.creditNumber}>${props.availableAmount}</Text>
        <Text style={styles.creditDecimal}>{'.00'}</Text>
      </View>
      <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginTop: -5}}>
        <View style={styles.containerLeftDays}>
          <Text style={styles.textLeftDays}>Quedan</Text>
        </View>
        <Text style={styles.textCreditAmount}>de ${props.creditAmount}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerCreditInformation: {
    marginRight: 30,
  },
  containerCreditNumber: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    marginTop: -4
  },
  creditNumber: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    color: '#ffffff',
    fontSize: 30,
  },
  creditDecimal: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    color: '#ffffff',
    fontSize: 18,
    marginBottom: 5
  },
  containerLeftDays: {
    borderRadius: 3,
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 3,
    backgroundColor: '#49c0aa',
    alignItems: 'center',
    flexDirection: 'row',
  },
  textLeftDays: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 12,
    color: '#ffffff',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  textCreditAmount: {
    fontFamily: 'AvenirLTStd-Light',
    textAlign: 'center',
    color: '#ffffff',
    paddingTop: 0,
    marginLeft: 6,
    fontSize: 14,
    lineHeight: 18,
    textTransform: 'uppercase',
  },
});

export default AmountAvailableCreditNewsFeed;
