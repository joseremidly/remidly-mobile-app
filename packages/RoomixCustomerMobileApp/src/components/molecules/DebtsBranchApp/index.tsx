// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
    TouchableOpacity
} from 'react-native';

export interface Props {
  status: string;
  src: string;
  loans: any
}

const DebtsPaymentBranchApp: React.FC<Props> = (props) => {

  return (
    <View style={styles(props).container}>
      <View style={styles(props).containerImage}>
        <Image style={styles(props)[props.type]} resizeMode='contain' source={{uri : props.src}}></Image>
      </View>
      <View style={styles(props).containerDebtInformation}>
        <Text style={styles(props).textTotalPay}>TOTAL A PAGAR:</Text>
        <Text style={styles(props).textNumberToPay}>${(props.loans.amount + (props.loans.amount * props.loans.rate))}</Text>
        <Text style={styles(props).textTotalComision}>+ COMISIÓN</Text>
        <Text style={styles(props).textTotalInstructions}>(dependerá de tu banco)</Text>
      </View>
      <View style={styles(props).containerDebtReferences}>
        <Text style={styles(props).textReference}>
          NOMBRE DEL BANCO
        </Text>
        <View style={styles(props).buttonReference}>
          <Text style={styles(props).textNumberReference}>
            BBVA México
          </Text>
        </View>
      </View>
      <View style={styles(props).containerDebtReferences}>
        <Text style={styles(props).textReference}>
          CLABE INTERBANCARIA 
        </Text>
        <View style={styles(props).buttonReference}>
          <Text style={styles(props).textNumberReference}>
            012180001157594127
          </Text>
        </View>
      </View>
      <View style={styles(props).containerDebtReferences}>
        <Text style={styles(props).textReference}>
          TITULAR DE CUENTA
        </Text>
        <View style={styles(props).buttonReference}>
          <Text style={styles(props).textNumberAccount}>
            Crecy Technologies S.A.P.I de C.V
          </Text>
        </View>
      </View>
      <View style={styles(props).containerDebtReferencesLast}>
        <Text style={styles(props).textReference}>
          # DE REFERENCIA 
        </Text>
        <View style={styles(props).buttonReference}>
          <Text style={styles(props).textNumberReference}>
            {props.loans.reference}
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles= (props: any) => StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: 12,
  },
  containerDebtInformation: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 3
  },
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 25,
  },
  containerDebtReferences: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20
  },
  containerDebtReferencesLast: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 40
  },
  buttonReference: {
    width: '100%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: props.status === 'rejected' || props.status === 'expired' ? '#ed1e79' : '#7b3eff',
    borderRadius: 6,
    marginTop: 5,
    paddingHorizontal: 20,
  }, 
  app: {
    width: 90,
    height: 90,
  }, 
  branch: {
    width: 60,
    height: 60,
  },
  iconCodeBar: {
    width: 150,
    height: 40,
  },
  textTotalPay: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#5a6175',
    lineHeight: 15,
    marginTop: 15,
    marginBottom: 5,
  },
  textTotalComision: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 12,
    color: '#5a6175',
    lineHeight: 15,
    marginTop: 5,
  },
  textTotalInstructions: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    color: '#5a6175',
    lineHeight: 15,
    marginBottom: 25,
  },
  textReference: {
    fontFamily: 'Avenir-Black',
    fontSize: 12,
    color: '#999999',
    lineHeight: 15,
  },
  textReferenceCodeBar: {
    fontFamily: 'Avenir-Black',
    fontSize: 16,
    color: '#5a6175',
    lineHeight: 15,
    marginTop: 10,
    marginBottom: 20
  },
  textNumberReference: {
    fontFamily: 'Avenir-Black',
    fontSize: 16,
    color: 'white',
  },
  textNumberAccount: {
    fontFamily: 'Avenir-Black',
    fontSize: 13,
    color: 'white',
  },
  textNumberToPay: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 31,
    color: '#5a6175',
  },
  
});

export default DebtsPaymentBranchApp;
