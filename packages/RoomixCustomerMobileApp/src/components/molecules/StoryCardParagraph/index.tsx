import React from 'react';
import { StyleSheet, Text, Image, View } from 'react-native';

export interface Props {
  mainText: string;
  mainTextColor: string;
  iconURI: string | null;
  titleStyle: string | null;
  titleText: string | null;
  titleType: string | null;
  titleColor: string | null;
  titleBackgroundColor: string | null;
}

const StoryCardParagraph: React.FC<Props> = (props: any) => {  
  let titleStyle = null;
  let titleContainerStyle = null;

  if (props.titleText) {
    titleStyle = props.titleType == 'center' ? 'titleCenter' : 'titleLeft';
    titleContainerStyle = props.titleType == 'center' ? 'titleContainerCenter' : 'titleContainerLeft';
  }

  return (
    <>
      <View style={styles(props).container}>
        {props.iconURI && (
          <View style={styles(props).imageContainer}>
            <Image style={styles(props).image} source={{ uri: props.iconURI }} />
          </View>
        )}
        {props.titleText && (
          <View style={styles(props).titleContainer}>
            <View style={styles(props).[titleContainerStyle]}>
              <Text style={styles(props).[titleStyle]}>{props.titleText}</Text>
            </View>
          </View>
        )}
        <Text style={styles(props).mainText}>{props.mainText}</Text>
      </View>
    </>
  );
}

const styles = props => StyleSheet.create({
  container: {
    marginBottom: 18,
  },
  imageContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
  },
  image: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
  },
  titleContainer: {
    marginTop: 10,
  },
  titleContainerLeft: {
    flexDirection: 'row',
  },
  titleContainerCenter: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  titleCenter: {
    color: props.titleColor,
    textAlign: 'center',
    fontFamily: 'AvenirLTStd-Medium',
    fontWeight: 'bold',
    fontSize: 16,
    textTransform: 'uppercase',
  },
  titleLeft: {
    backgroundColor: props.titleBackgroundColor,
    color: props.titleColor,
    borderRadius: 6,
    paddingVertical: 4,
    paddingHorizontal: 10,
    fontFamily: 'AvenirLTStd-Medium',
    fontWeight: 'bold',
    textAlign: 'left',
    fontSize: 14,
    textTransform: 'uppercase',
  },
  mainText: {
    marginTop: 8,
    color: props.mainTextColor,
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 16,
  }
});

export default StoryCardParagraph;
