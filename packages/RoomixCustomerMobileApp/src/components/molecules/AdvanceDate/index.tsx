import React from 'react';

import { StyleSheet, View, Text, Image } from 'react-native';

export interface Props {
  isDeposit: string;
  dateText: string;
}

const AdvanceDate: React.FC<Props> = (props: any) => {
  let infoText = '';
  let arrowIconURI =
    'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1605743667/app/Crecy%20icons%20new%20dimensions/Crecy_icons_paycheck_arrow_velvet_bhwqal.png';

  if (props.isDeposit) {
    infoText = 'Se depositará a tu cuenta';
  } else {
    infoText = 'Se cobrará de tu cuenta';
  }

  return (
    <>
      <View style={styles(props).container}>
        <Text style={styles(props).infoText}>{infoText}</Text>
        <View style={styles(props).dateRow}>
          <Image
            style={styles(props).arrowImage}
            source={{ uri: arrowIconURI }}
          />
          <Text style={styles(props).dateText}>{props.dateText}</Text>
        </View>
      </View>
    </>
  );
};

const styles = (props: any) =>
  StyleSheet.create({
    container: {
      flex: 0.5,
      marginHorizontal: 20,
    },
    dateRow: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    infoText: {
      textTransform: 'uppercase',
      fontSize: 14,
      color: '#5a6175',
    },
    arrowImage: {
      width: 16,
      height: 16,
      resizeMode: 'contain',
      marginRight: 6,
      transform: props.isDeposit ? [] : [{ rotate: '180deg' }],
    },
    dateText: {
      fontFamily: 'Baloo2-Bold',
      fontSize: 20,
      color: '#5a6175',
    },
  });

export default AdvanceDate;
