// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    View,
    Text,
} from 'react-native';

const CreditLineStartInvitationCode: React.FC<Props> = (props: any) => {
  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>
        Ingresa aquí tu
        <Text style={styles.colorText}> código de invitación</Text>
      </Text>
      <View style={styles.horizontalSeparator} />
      <Text style={styles.informationHeaderText}>No tengo código</Text>
      <Text style={styles.informationBenefitsText}>Estamos trabajando duro para poder brindarte la mejor herramienta para tus finanzas lo antes posible, envía un mensaje de WhatsApp al 33 2813 7849 para registrarte</Text>
      <View style={styles.horizontalSeparator} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 10,
  },
  headerText: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 24,
    color: '#5a6175',
    marginBottom: 10,
    marginHorizontal: 50,
    textAlign: 'center',
    lineHeight: 31
  },
  colorText: {
    color: '#6133E2',
  },
  informationHeaderText: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 18,
    color: '#5a6175',
    marginHorizontal: 30,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  informationBenefitsText: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 16,
    color: '#5a6175',
    marginHorizontal: 30,
    marginBottom: 15,
    textAlign: 'center',
    lineHeight: 20
  },
  horizontalSeparator: {
    marginVertical: 5,
    alignItems: 'center',
    width: '84%',
    marginHorizontal: '8%',
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#e6e6e6',
  },
});

export default CreditLineStartInvitationCode;
