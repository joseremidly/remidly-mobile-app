// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    View,
} from 'react-native';
import AmountAvailableCreditNewsFeed from 'src/components/molecules/AmountAvailableCreditNewsFeed';
import LeftDaysToPayNewsFeedGraph from 'src/components/molecules/LeftDaysToPayNewsFeedGraph';

export interface Props {
  onPress: Function;
  onConnectPlatform: Function;
  buttonName: string;
  src: string;
  type: string;
  creditAmount: any;
  availableAmount: any;
  maxDaysToPay: number;
  status: string;
  stateLoans: any;
  loansData: any;
}

const NewsFeedCard: React.FC<Props> = (props: any) => {
  return (
    <>
      {props.type === false ?
        <View style={styles.containerNewsFeedCard}>
          <View style={styles.containerImageNewsFeedCard1}>
            <Image style={styles.imageNewsFeedCard} source={{uri: props.src}}></Image>
          </View>
          <View style={styles.containerInfoTotalNewsFeedCard}>
            <View style={styles.containerHeader}>
              <Text style={styles.textHeader}>{'Línea de crédito básica'}</Text>
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginVertical: 15}}>
              <AmountAvailableCreditNewsFeed creditAmount={props.creditAmount} availableAmount={props.availableAmount} />
              <LeftDaysToPayNewsFeedGraph maxDaysToPay={props.maxDaysToPay} stateLoans={props.stateLoans} loansData={props.loansData} />
            </View>
            <TouchableOpacity style={styles.buttonStyle} onPress={props.onPress}>
              <Text style={styles.textButtonStyle}>{props.buttonName}</Text>
            </TouchableOpacity>
          </View>
        </View>
        : 
        <View style={styles.containerNewsFeedCard}>
          <View style={styles.containerImageNewsFeedCard1}>
            <Image style={styles.imageNewsFeedCard} source={{uri: props.src}}></Image>
          </View>
          <View style={styles.containerInfoTotalNewsFeedCard}>
            <View style={styles.containerHeader}>
              <Text style={styles.textHeader}>{'Línea de crédito básica'}</Text>
            </View>
            <View style={styles.containerInformationCreditLine}>
              <View style={styles.containerCreditNumber}>
                <Text style={styles.CreditNumber}>
                  ${props.creditAmount}
                </Text>
                <Text style={styles.CreditDecimal}>
                  {'.00'}
                </Text>
              </View>
              {props.status === 'under_review'
                && (
                  <TouchableOpacity style={styles.pendingButton}>
                    <Text style={styles.textButton}>
                      En revisión
                    </Text>
                  </TouchableOpacity>
                )}
              {props.status === 'approved'
                && (
                  <TouchableOpacity style={styles.buttonConected}>
                    <Text style={styles.textButton}>
                      Límite aprobado
                    </Text>
                  </TouchableOpacity>
                )}
              {props.status === 'rejected'
                && (
                  <TouchableOpacity style={styles.rejectedButton}>
                    <Text style={styles.textButton}>
                      No aprobada
                    </Text>
                  </TouchableOpacity>
                )}
              {props.status === 'pending'
                && (
                  <View style={styles.containerExtraInfoNeeded}>
                    <TouchableOpacity style={styles.pendingInfoButton}>
                      <Text style={styles.textButton}>
                        Información pendiente
                      </Text>
                    </TouchableOpacity>
                    <Text style={styles.textExtraInfoNeeded}>
                      Espera un periodo de dos semanas para volver a aplicar por una línea de crédito
                    </Text>
                  </View>
                )}
              {props.status === 'extra_info_needed'
                && (
                  <View style={styles.containerExtraInfoNeeded}>
                    <TouchableOpacity style={styles.extraInfoButton}>
                      <Text style={styles.textButton}>
                        Información extra necesaria
                      </Text>
                    </TouchableOpacity>
                    <Text style={styles.textExtraInfoNeeded}>
                      Si ya conectaste más cuentas en máximo 24 hrs verás tu estatus actualizado
                    </Text>
                  </View>
                )}
            </View>
            {props.status === 'approved'
              && (
                <TouchableOpacity style={styles.buttonStyle} onPress={props.onPress}>
                  <Text style={styles.textButtonStyle}>{props.buttonName}</Text>
                </TouchableOpacity>
              )}
            {props.status === 'extra_info_needed' || props.status === 'pending'
              && (
                <TouchableOpacity style={styles.extraInfoButtonStyle} onPress={props.onConnectPlatform}>
                  <Text style={styles.textButtonStyle}>Conectar más plataformas</Text>
                </TouchableOpacity>
              )}
          </View>
        </View>
      }
    </>
  );
};

const styles = StyleSheet.create({
  containerNewsFeedCard: {
    width: '100%',
    position: 'relative',
    justifyContent: 'space-between',
    backgroundColor: '#7b3eff',
    borderRadius: 18,
    marginVertical: 7,
    paddingTop: 8,
    overflow: 'hidden',
  },
  containerImageNewsFeedCard1: {
    width: 90,
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  imageNewsFeedCard: {
    width: '80%',
    height: 70,
  },
  containerInfoTotalNewsFeedCard: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },
  containerInformation: {
    marginHorizontal: 50,
  },
  containerHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textHeader: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 20,
    color: 'white',
  },
  textInformation: {
    fontFamily: 'AvenirLTStd-Light',
    textAlign: 'center',
    color: '#ffffff',
    paddingTop: 0,
    marginLeft: 6,
    fontSize: 14,
    lineHeight: 18,
    textTransform: 'uppercase',
  },
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 12,
    height: 30,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#7b3eff',
    borderRadius: 19,
    margin: 10,
  },
  extraInfoButtonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 12,
    height: 30,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#7b3eff',
    borderRadius: 19,
    margin: 10,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Medium',
    textAlign: 'center',
    fontSize: 14,
    textTransform: 'uppercase',
    color: '#7b3eff',
  },
  pendingButton: {
    borderRadius: 3,
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 3,
    backgroundColor: '#FBB03B',
    alignItems: 'center',
    flexDirection: 'row',
  },
  rejectedButton: {
    borderRadius: 3,
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 3,
    backgroundColor: '#ed1e79',
    alignItems: 'center',
    flexDirection: 'row',
  },
  containerExtraInfoNeeded: {
    alignItems: 'center'
  },
  textExtraInfoNeeded: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 12,
    color: '#ffffff',
    textAlign: 'center',
    marginTop: 5,
    marginHorizontal: 25
  },
  extraInfoButton: {
    borderRadius: 3,
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 3,
    backgroundColor: '#00B9DD',
    alignItems: 'center',
    flexDirection: 'row',
  },
  pendingInfoButton: {
    borderRadius: 3,
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 3,
    backgroundColor: '#FC5490',
    alignItems: 'center',
    flexDirection: 'row',
  },
  buttonConected: {
    borderRadius: 3,
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 3,
    backgroundColor: '#49c0aa',
    alignItems: 'center',
    flexDirection: 'row',
  },
  textButton: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 12,
    color: '#ffffff',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  containerInformationCreditLine: {
    marginVertical: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerInformationCredit: {
    marginRight: 30,
  },
  containerCreditNumber: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    marginTop: -4
  },
  CreditNumber: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    color: '#ffffff',
    fontSize: 30,
  },
  CreditDecimal: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    color: '#ffffff',
    fontSize: 18,
    marginBottom: 5
  },
  containerImgAbsolute: {
    position: 'absolute',
    zIndex: 2,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    width: 50,
    height: 40,
  },
  textButtonStylePay: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    fontSize: 9,
    lineHeight: 12,
    textTransform: 'uppercase',
    color: '#ffffff',
  },
});

export default NewsFeedCard;