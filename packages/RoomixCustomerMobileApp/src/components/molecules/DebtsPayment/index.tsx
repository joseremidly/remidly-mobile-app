// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
    TouchableOpacity
} from 'react-native';

export interface Props {
  loans: any;
  goToLatePayment: Function;
}

const DebtsPayment: React.FC<Props> = (props) => {
  return (
    <>
      <View style={styles(props).container}>
        <View style={styles(props).containerReference}>
          <Text style={styles(props).textReference}># DE REFERENCIA: {props.loans.reference}</Text>
        </View>
        <View style={styles(props).containerDebt}>
          <View style={styles(props).containerDebtInformation}>
            <Text style={styles(props).textDebtInformation}>
              RETIRO DE {`${props.loans.amount !== undefined ? (props.loans.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[0] : ''}`} MXN DE LA LINEA DE CRÉDITO
            </Text>
            <View style={styles(props).containerStatusPayment}>
              <Text style={styles(props).textStatus}>
                PAGO PENDIENTE
              </Text>
            </View>
          </View>
          <View style={styles(props).containerAmountPayment}>
            <Text style={styles(props).textToPay}>TOTAL A PAGAR:</Text>
            <View style={styles(props).containerCredit}>
              <Text style={styles(props).CreditNumber}>
              {`$${(props.loans.amount + (props.loans.amount * props.loans.rate)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[0]}.`}
              </Text>
              <Text style={styles(props).CreditDecimal}>
              {(props.loans.amount + (props.loans.amount * props.loans.rate)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".").length > 1 ? (props.loans.amount + (props.loans.amount * props.loans.rate)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[1].substring(0,2) : '00'}
              </Text>
            </View>
          </View>
        </View>
        <View style={{marginBottom: 25, marginTop: 10}}>
          <View style={styles(props).containerTime}>
            <Image style={styles(props).iconTime} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1610351843/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_icon_timer_grey_loiro6.png'}}></Image>
            <Text style={styles(props).textDebtInformationTime}>TIENES {props.loans.deadline_days} DÍAS PARA PAGAR ESTA DEUDA</Text>
          </View>
          <TouchableOpacity style={styles(props).containerButtonTransparent} onPress={props.goToLatePayment}>
            <Text style={styles(props).TextButtonTime}>
              ¿Qué pasa si no pago a tiempo? {'>'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 5,
    backgroundColor: '#ffffff',
    borderRadius: 12,
    paddingVertical: 5,
    paddingHorizontal: 10
    
  },
  containerReference: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  textReference: {
    fontFamily: 'Avenir-Heavy',
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#7b3eff',
  },
  textReferenceId: {
    fontFamily: 'Avenir-Heavy',
    textTransform: 'uppercase',
    fontSize: 12,
    color: '#7b3eff',
  },
  containerDebt: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingHorizontal: 5,
    marginVertical: 5,
    borderBottomColor: '#f1f1f1',
    borderBottomWidth: 1,
    marginTop: 30,
    paddingBottom: 25
  },
  containerDebtInformation: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: '50%',
  },
  containerStatusPayment: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FBB03B',
    marginTop: 5,
    borderRadius: 3
  },
  textDebtInformation: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 12,
    color: '#5a6175',
    lineHeight: 14
  },
  textDebtInformationTime: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 12,
    color: '#5a6175',
    lineHeight: 15
  },
  textStatus: {
    fontFamily: 'AvenirLTStd-Medium',
    textTransform: 'uppercase',
    fontSize: 11,
    color: '#ffffff',
    marginHorizontal: 4,
    marginVertical: 3
  },
  containerAmountPayment: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    width: '25%',
    marginLeft: 20,
  },
  textToPay: {
    fontFamily: 'AvenirLTStd-Light',
    textTransform: 'uppercase',
    fontSize: 9,
    color: '#5a6175',
  },
  containerCredit: {
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    flexDirection: 'row'
  },
  CreditNumber: {
    fontFamily: 'AvenirLTStd-Medium',
    textTransform: 'uppercase',
    fontSize: 22,
    paddingTop: 0,
    color: '#5a6175',
  },
  CreditDecimal: {
    fontFamily: 'AvenirLTStd-Book',
    textTransform: 'uppercase',
    fontSize: 15,
    paddingTop: 10,
    paddingBottom: 3,
    color: '#5a6175',
  },
  iconTime: {
    width: 20,
    height: 20,
    marginRight: 5
  },
  containerTime: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    margin: 10,
  },
  containerButtonTransparent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  TextButtonTime: {
    fontFamily: 'AvenirLTStd-Roman',
    textTransform: 'uppercase',
    fontSize: 10,
    color: '#7b3eff',
  },
  buttonPay: {
    backgroundColor: '#7b3eff',
    borderRadius: 9,
    minWidth: 100,
    minHeight: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 25
  },
  buttonPayText: {
    fontFamily: 'Avenir-Heavy',
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#ffffff',
  },
});

export default DebtsPayment;
