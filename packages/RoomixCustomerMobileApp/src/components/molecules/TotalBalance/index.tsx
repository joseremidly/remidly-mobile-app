import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    View,
} from 'react-native';

export interface Props {
  update: Function;
  accountsLinked: number;
  balance: number;
  updatedDate: string;
}

const TotalBalance: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containerTotalBalance}>
        <View style={styles.containerImageCrecy}>
          <Image style={styles.imageCrecy} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599533279/app/crecy-login_hlr5pj.png'}}></Image>
        </View>
        <View style={styles.containerInfoTotalBalance}>
          <View style={styles.containerLinkedAccount}>
            <Text style={styles.textLinkedAccount}>CUENTAS CONECTADAS</Text>
            <View style={styles.iconLinkedAccount}>
              <Text style={styles.numberLinkedAccount}>{props.accountsLinked}</Text>
            </View>
          </View>
          <Text style={styles.textTotalBalance}>Balance total</Text>
          <Text style={styles.numberTotalBalance}>{`$${props.balance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[0]}.`}
            <Text style={styles.decimalsTotalBalance}>{props.balance && props.balance !== 0 ? (props.balance.toString().includes(".") ? props.balance.toString().split(".")[1].substring(0,2) : '00') : '00'}</Text>
          </Text>
        </View>
        <View style={styles.containerUpdatedDate}>
          {/* <TouchableOpacity style={styles.buttonUpdatedDate} onPress={props.updateData}>
            <Text style={styles.textUpdatedDate}>{props.updatedDate}</Text>
            <Image source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1600060530/app/crecy_icons_refresh_button_card_balance_jy67ne.png' }} style={styles.iconUpdatedDate} />
          </TouchableOpacity> */}
          <Text style={styles.textUpdatedDate}>{props.updatedDate}</Text>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerTotalBalance: {
    justifyContent: 'space-between',
    backgroundColor: 'white',
    borderRadius: 9,
    height: 200,
    margin: 10,
  },
  containerImageCrecy: {
    width: 70,
    position: 'absolute',
    top: 80,
    left: 0,
  },
  imageCrecy: {
    width: '80%',
    height: 70,
  },
  containerInfoTotalBalance: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  containerLinkedAccount: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textLinkedAccount: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 12,
    lineHeight: 16,
    color: '#999999',
  },
  iconLinkedAccount: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5a6175',
    width: 20,
    height: 20,
    borderRadius: 50,
    paddingTop: 2,
    marginLeft: 5,
  },
  numberLinkedAccount: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 14,
    lineHeight: 20,
    color: '#ffffff',
  },
  textTotalBalance: {
    fontFamily: 'Baloo2-Bold',
    color: '#5a6175',
    fontSize: 24,
  },
  numberTotalBalance: {
    color: '#5a6175',
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 24,
  },
  decimalsTotalBalance: {
    color: '#5a6175',
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 20,
  },
  containerUpdatedDate: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    padding: 5,
  },
  buttonUpdatedDate: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: 40,
  },
  textUpdatedDate: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 10,
    textTransform: 'uppercase',
    color: '#5a6175',
    marginBottom: 3,
    marginRight: 4
  },
  iconUpdatedDate: {
    width: 19,
    height: 23,
    marginLeft: 5,
    marginRight: 10,
  }
});

export default TotalBalance;
