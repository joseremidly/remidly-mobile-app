import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';

export interface Props {
  placeholder: string;
  onChangeOption: Function;
}

const SelectorReusable: React.FC<Props> = (props: any) => {
  const [selected, setSelected] = React.useState('');
  const selectOption = (data: any) => {
    setSelected(data);
    props.onChangeOption(data);
  }
  return (
    <>
      <View style={styles.containerSelector}>
        <TouchableOpacity style={styles.containerOptionSelectorReusable} onPress={() => selectOption('Masculino')}>
          <Image style={styles.iconOptionSelectorReusable} source={{ uri: selected === 'Masculino' ? 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604014175/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_advances_green_icon_ready_istaq2.png' : 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1605955019/app/Crecy%20icons%20new%20dimensions/Crecy_icons_done_grey_2_hy9grk.png'}}/>
          <Text style={styles.textOptionSelectorReusable}>{`Masculino`}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.containerOptionSelectorReusable} onPress={() => selectOption('Femenino')}>
          <Image style={styles.iconOptionSelectorReusable} source={{ uri: selected === 'Femenino' ? 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604014175/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_advances_green_icon_ready_istaq2.png' : 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1605955019/app/Crecy%20icons%20new%20dimensions/Crecy_icons_done_grey_2_hy9grk.png'}}/>
          <Text style={styles.textOptionSelectorReusable}>{`Femenino`}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.containerOptionSelectorReusable} onPress={() => selectOption('Otro')}>
          <Image style={styles.iconOptionSelectorReusable} source={{ uri: selected === 'Otro' ? 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604014175/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_advances_green_icon_ready_istaq2.png' : 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1605955019/app/Crecy%20icons%20new%20dimensions/Crecy_icons_done_grey_2_hy9grk.png'}}/>
          <Text style={styles.textOptionSelectorReusable}>{`Otro`}</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerSelector: {
    alignItems: 'center',
    width: '90%',
    marginVertical: 30,
  },
  containerOptionSelectorReusable: {
    width: '80%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginVertical: 15,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#e6e6e6',
  },
  iconOptionSelectorReusable: {
    width: 28,
    height: 28,
    marginLeft: 50,
  },
  textOptionSelectorReusable: {
    width: '80%',
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'left',
    letterSpacing: .5,
    fontSize: 22,
    color: '#5a6175',
    marginLeft: 20,
  }
});

export default SelectorReusable;
