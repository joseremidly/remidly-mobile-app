import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    View,
} from 'react-native';

export interface Props {
  onPress: Function;
}

const PaycheckAdvance: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containerPaycheckAdvance}>
        <View style={styles.containerImageBriefcase}>
          <Image style={styles.imageBriefcase} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1600413118/app/crecy_icons_bag_with_money_i9zibo.png'}}></Image>
        </View>
        <View style={styles.containerDetailsAdvance}>
        </View>
        <View style={styles.containerInfoTotalPaycheck}>
          <View style={styles.containerHeader}>
            <Text style={styles.textHeader}>Paycheck Advance</Text>
          </View>
          <View style={styles.containerInformation}>
            <Text style={styles.textInformation}>Need an extra cash? Get
            <Text style={styles.spanBoldText}> Up to $100</Text>
                <Text style={styles.textInformation}> Intereste free</Text>
            </Text>
          </View>
          <TouchableOpacity style={styles.buttonStyle} onPress={props.onPress}>
            <Text style={styles.textButtonStyle}>GET IT!</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
    containerPaycheckAdvance: {
    justifyContent: 'space-between',
    backgroundColor: '#7b3eff',
    borderRadius: 9,
    marginVertical: 30,
  },
  containerImageBriefcase: {
    width: 120,
    position: 'absolute',
    top: 83,
    left: 0,
  },
  imageBriefcase: {
    width: '80%',
    height: 70,
  },
  containerDetailsAdvance: {

  },
  containerInfoTotalPaycheck: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 15
  },
  containerInformation: {
    marginHorizontal: 90,
  },
  containerHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textHeader: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 23,
    color: 'white',
    textTransform: 'uppercase',
  },
  textInformation: {
    fontFamily: 'Baloo2-Regular',
    textAlign: 'center',
    color: '#ffffff',
    marginTop: 3,
    fontSize: 14,
    lineHeight: 19,
    textTransform: 'uppercase',
  },
  spanBoldText: {
    color: '#ffffff',
    fontFamily: 'Baloo2-Bold',
    fontSize: 14,
    textTransform: 'uppercase',
  },
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 25,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#7b3eff',
    borderRadius: 19,
    margin: 10,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Regular',
    textAlign: 'center',
    fontSize: 14,
    color: '#7b3eff',
    textTransform: 'uppercase',
  },
});

export default PaycheckAdvance;