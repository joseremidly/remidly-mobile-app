// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Image,
    Text,
		View,
		Dimensions,
		ActivityIndicator,
} from 'react-native';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  onPress: Function;
  stateService: string;
	status: any;
}

const PaycheckKyc: React.FC<Props> = (props) => {
	const {width, height} = Dimensions.get('window');
  return (
    <>
      <View style={styles(height).principalContainer}>
				<View style={{justifyContent: 'center', alignItems: 'center',}}>
					<View>
            <Image source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604090159/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_ID_verification_rjdepe.png' }} style={styles(height).iconUpdatedDate} />
					</View>
					<View style={styles(width).ContainerSecondParagraph} >
						<Text style={styles(props).textStyle}>verifica tu identidad</Text>
					</View>
				</View>
				<View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: props.status === false ? '#FBB03B': '#49c0aa', marginVertical: 10, borderRadius: 4, paddingHorizontal: 8}}>
					<Text style={styles(props).textStatus}>
						{props.status === false ? 'pendiente' : 'listo'}
					 </Text>
				</View>
				<View style={{justifyContent: 'flex-start', alignItems: 'flex-start', marginVertical: 30, width: '60%'}}>
					<Text style={styles(props).textStepsKyc}>- Número de celular #</Text>
					<Text style={styles(props).textStepsKyc}>- Foto de identificación oficial</Text>
					<Text style={styles(props).textStepsKyc}>- Foto de rostro completo</Text>
				</View>
				<View style={styles(height).containerButton}>
          {props.stateService === 'loading'
            && (
              <ActivityIndicator size='large' color='#7b3eff' animating={true} />
            )}
          {props.stateService !== 'loading'
            && (
              <ButtonMid label={props.status === false ? 'Iniciar' : 'Continuar'} secondary={!props.status} onPress={props.status ? props.onPress : () => true}></ButtonMid>
            )}
				</View>
				<View style={styles(props).containerRequeriments}>
					<Image style={styles(props).iconRequeriments} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604090158/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_see_requirements_purple_wl2vfs.png'}} resizeMode='contain'></Image>
					<Text style={styles(props).textRequeriments}>Ver requerimientos</Text>
					<Image style={styles(props).iconRequerimentsArrow} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604090158/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_see_requirements_arrow_purple_dtexf8.png'}} resizeMode='contain'></Image>
				</View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  principalContainer: {
		width: '100%',
    justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#ffffff',
		borderRadius: 19
  },
  ContainerSecondParagraph: {
    justifyContent: 'center',
    alignItems: 'center',
		flexDirection: 'row',
		marginHorizontal: props <= 380 ? 60 : 100,
  },
  textStyle: {
    fontFamily: 'Baloo2-Bold',
    textTransform: 'uppercase',
    fontSize: 24,
		color: '#5a6175',
		textAlign: 'center',
		lineHeight: 27
	},
  textStatus: {
    fontFamily: 'AvenirLTStd-Roman',
    textTransform: 'uppercase',
    fontSize: 13,
		color: 'white',
		textAlign: 'center',
    marginTop: 7,
    marginBottom: 4,
  },
  textStepsKyc: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 16,
		color: '#5a6175',
		textAlign: 'left'
  },
	iconUpdatedDate: {
    width: 160,
		height: 120,
		marginVertical: props <= 680 ? 20 : 30,
	},
	containerButton: {
		justifyContent: 'center',
		alignItems: 'center',
    marginTop: 20,
    marginBottom: 50,
	},
	buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 260,
    height: 44,
    backgroundColor: '#7b3eff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#7b3eff',
    borderRadius: 20,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 22,
    color: '#ffffff',
	},
	containerRequeriments: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
		flexDirection: 'row',
		marginBottom: 30,
  },
  textRequeriments: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 15,
    color: '#5a6175',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  iconRequeriments: {
    width: 20,
    height: 20,
    marginRight: 10
	},
  iconRequerimentsArrow: {
    width: 15,
    height: 15,
    marginRight: 10
  },
});

export default PaycheckKyc;