// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
    TouchableOpacity
} from 'react-native';

export interface Props {
  paymentDate: any;
  amount: number;
  rate: number;
  daysToPay: number;
  loan: any;
  id: any;
  onPressPay: Function;
  onPressDetails: Function;
  goToLatePayment: Function;
}

const ActiveDebts: React.FC<Props> = (props) => {
  let initialDate = new Date()
  let finishDate = new Date(props.loan.payment_due_date)
  const getDays = (initial: any, finish: any) => {
    let Resta = finish - initial
    return((Resta/(1000*60*60*24)).toFixed(0))
  }
  return (
    <>
      <View style={styles(props).container}>
        <TouchableOpacity onPress={() => props.onPressDetails(props.loan)}>
          <View style={styles(props).containerReference}>
            <Text style={styles(props).textReference}># DE REFERENCIA: {props.id}</Text>
          </View>
          <View style={styles(props).containerDebt}>
            <View style={styles(props).containerDebtInformation}>
              <Text style={styles(props).textDebtInformation}>
                RETIRO DE {`${(props.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[0]}`} MXN DE LA LINEA DE CRÉDITO
              </Text>
              <View style={styles(props).containerStatusPayment}>
                <Text style={styles(props).textStatus}>
                  PAGO PENDIENTE
                </Text>
              </View>
            </View>
            <View style={styles(props).containerAmountPayment}>
              <Text style={styles(props).textToPay}>TOTAL A PAGAR:</Text>
              <View style={styles(props).containerCredit}>
                <Text style={styles(props).CreditNumber}>
                  {`$${(props.amount + (props.amount * props.rate)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[0]}.`}
                </Text>
                <Text style={styles(props).CreditDecimal}>
                  {(props.amount + (props.amount * props.rate)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".").length > 1 ? (props.amount + (props.amount * props.rate)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[1].substring(0,2) : '00'}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
        <View style={{paddingBottom: 25, borderBottomColor: '#f1f1f1', borderBottomWidth: 1, paddingTop: 10}}>
          <View style={styles(props).containerTime}>
            <Image style={styles(props).iconTime} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1610351843/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_icon_timer_grey_loiro6.png'}}></Image>
            <Text style={styles(props).textDebtInformation}>TIENES {getDays(initialDate, finishDate)} DÍAS PARA PAGAR ESTA DEUDA</Text>
          </View>
          <TouchableOpacity style={styles(props).containerButtonTransparent} onPress={props.goToLatePayment}>
            <Text style={styles(props).TextButtonTime}>
              ¿Qué pasa si no pago a tiempo? {'>'}
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={styles(props).buttonPay} onPress={() => props.onPressPay(props.loan)}>
          <Text style={styles(props).buttonPayText}>
            PAGAR
          </Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 5,
    backgroundColor: '#ffffff',
    borderRadius: 12,
    paddingVertical: 5,
    paddingHorizontal: 30,
    marginVertical: 10,
  },
  containerReference: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  textReference: {
    fontFamily: 'Avenir-Heavy',
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#7b3eff',
  },
  containerDebt: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingHorizontal: 5,
    paddingTop: 0,
    borderBottomColor: '#f1f1f1',
    borderBottomWidth: 1,
    marginTop: 30,
    paddingBottom: 12
  },
  containerDebtInformation: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: '55%',
  },
  containerStatusPayment: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FBB03B',
    marginTop: 5,
    borderRadius: 3
  },
  textDebtInformation: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 12,
    color: '#5a6175',
    lineHeight: 14
  },
  textStatus: {
    fontFamily: 'AvenirLTStd-Medium',
    textTransform: 'uppercase',
    fontSize: 11,
    color: '#ffffff',
    marginHorizontal: 4,
    marginVertical: 3
  },
  containerAmountPayment: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: '35%',
    paddingBottom: 0,
    marginLeft: 40,
    marginBottom: 25
  },
  textToPay: {
    fontFamily: 'AvenirLTStd-Light',
    textTransform: 'uppercase',
    fontSize: 9,
    color: '#5a6175',
  },
  containerCredit: {
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    flexDirection: 'row'
  },
  CreditNumber: {
    fontFamily: 'AvenirLTStd-Medium',
    textTransform: 'uppercase',
    fontSize: 25,
    paddingTop: 0,
    color: '#5a6175',
  },
  CreditDecimal: {
    fontFamily: 'AvenirLTStd-Book',
    textTransform: 'uppercase',
    fontSize: 15,
    paddingTop: 10,
    paddingBottom: 3,
    color: '#5a6175',
  },
  iconTime: {
    width: 20,
    height: 20,
    marginRight: 5
  },
  containerTime: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    margin: 10,
  },
  containerButtonTransparent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  TextButtonTime: {
    fontFamily: 'AvenirLTStd-Roman',
    textTransform: 'uppercase',
    fontSize: 10,
    color: '#7b3eff',
  },
  buttonPay: {
    backgroundColor: '#7b3eff',
    borderRadius: 9,
    minWidth: 120,
    minHeight: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 28,
    marginBottom: 23
  },
  buttonPayText: {
    fontFamily: 'Avenir-Heavy',
    textTransform: 'uppercase',
    fontSize: 16,
    color: '#ffffff',
  },
});

export default ActiveDebts;
