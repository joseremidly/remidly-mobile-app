// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    View,
} from 'react-native';
import Convert from '../../../utils/convert';
import { ProgressCircle } from 'react-native-svg-charts';

export interface Props {
  onPress: Function;
  header: string;
}

const CardAnalyticsEmpty: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles(props).containerAnalyticsEmpty} >
        <View style={styles(props).containerInfoTotalCardAnalyticsEmpty}>
          <View style={styles(props).containerHeader}>
            <Text style={styles(props).textHeader}>{props.header}</Text>
            <Image style={styles(props).iconHeader} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599251160/app/back_1_bl4as0.png'}}></Image>
          </View>
          <View style={styles(props).containerTotalBalance}>
            <Image style={styles(props).imageTotalBalnce} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1611043169/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_connect_a_bank_velvet_background_uy2gs0.png'}}></Image>
            <Text style={styles(props).textTotalBalance}>POR FAVOR, CONECTA POR LO MENOS UNA CUENTA BANCARIA (O TARJETA DE CRÉDITO) PARA PODER UTILIZAR ESTA HERRAMIENTA</Text>
          </View>
          <TouchableOpacity style={styles(props).buttonConected} onPress={props.onPress}>
            <Text style={styles(props).textButton}>
              Conectar
            </Text>
            <Image style={styles(props).iconButton} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1605814313/app/Crecy%20icons%20new%20dimensions/crecy_icons_little_arrow_white_header_r6sjsz.png'}} resizeMode='contain'>
            </Image>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  containerAnalyticsEmpty: {
    width: '100%',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    borderRadius: 18,
    marginVertical: 0,
    paddingTop: 8,
  },
  containerInfoTotalCardAnalyticsEmpty: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 15,
  },
  containerHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textHeader: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 22,
    color: '#5a6175',
  },
  iconHeader: {
    width: 12,
    height: 10,
    marginLeft: 5,
  },
  containerTotalBalance: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '90%',
    marginVertical: 20
  },
  imageTotalBalnce: {
    width: 90,
    height: 90,
  },
  textTotalBalance: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    color: '#999999',
    fontSize: 11,
    textTransform: 'uppercase',
    lineHeight: 15,
    width: '75%',
    marginLeft: 20
  },
  buttonConected: {
    borderRadius: 4,
    backgroundColor: '#49c0aa',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginBottom: 15
  },
  textButton: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 18,
    color: '#ffffff',
    textAlign: 'center',
    textTransform: 'uppercase'
  },
  iconButton: {
    width: 10,
    height: 10,
    marginLeft: 5
  },
});

export default CardAnalyticsEmpty;