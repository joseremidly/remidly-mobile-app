import React from 'react';

import { StyleSheet, View, TouchableOpacity, Text, Image } from 'react-native';

export interface Props {
  availableAccounts: Object | null;
  selectedAccount: Object | null;
  selectAccount: Function;
  disabled: boolean;
}

const AdvanceCardSelection: React.FC<Props> = (props: any) => {
  const selectAccount = () => {
    if (props.disabled) {
      return;
    }

    props.selectAccount(props.availableAccounts[0]);
  };

  let image = '';
  let text = '';

  if (props.selectedAccount) {
    image = props.selectedAccount.accountLogo;
    text = props.selectedAccount.lastDigits;
  } else {
    image =
      'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1601939661/app/add_2_ycwugv.png';
    text = 'seleccionar';
  }

  return (
    <>
      <TouchableOpacity
        onPress={selectAccount}
        style={styles(props).containter}>
        <Image style={styles(props).accountLogo} source={{ uri: image }} />
        <Text style={styles(props).text}>{text}</Text>
      </TouchableOpacity>
    </>
  );
};

const styles = (props: any) =>
  StyleSheet.create({
    containter: {
      backgroundColor: props.selectedAccount
        ? props.selectedAccount.accountColour
        : '#cccccc',
      paddingHorizontal: 8,
      paddingVertical: 4,
      borderRadius: 4,
      height: 30,
      width: props.selectedAccount ? 80 : 130,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    accountLogo: {
      height: 15,
      width: 30,
      resizeMode: 'contain',
    },
    text: {
      fontFamily: 'AvenirLTStd-Medium',
      fontSize: 11,
      color: props.selectedAccount ? 'white' : '#555',
      textTransform: 'uppercase',
    },
  });

export default AdvanceCardSelection;
