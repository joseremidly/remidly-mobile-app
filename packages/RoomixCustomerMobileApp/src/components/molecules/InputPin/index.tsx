// @ts-nocheck
import React from 'react';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import {
    StyleSheet,
    View,
} from 'react-native';

export interface Props {
  onChangeInput: Function;
}

const InputPin: React.FC<Props> = (props: any) => {
  const [value, setValue] = React.useState('');
  const onChangePin = (pin) => {
    setValue(pin)
    if (pin.length == 6) {
      props.onChangeInput(pin)
    }
  }
  return (
    <>
      <View style={styles.containerInputPin}>
        <SmoothPinCodeInput
          mask={<View style={{
            width: 10,
            height: 10,
            borderRadius: 25,
            backgroundColor: '#5a6175',
          }}></View>}
          maskDelay={100}
          ref={this.pinInput}
          codeLength={6}
          value={value}
          password={true}
          onTextChange={pin => onChangePin(pin)}
          onFulfill={this._checkCode}
          onBackspace={this._focusePrevInput}
          cellStyle={{
            width: 44,
            height: 60,
            backgroundColor: '#f1edfe',
            borderRadius: 6,
          }}
          cellStyleFocused={{
            borderStyle: 'solid',
            borderWidth: 2,
            borderColor: '#7b3eff'
          }}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerInputPin: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'stretch',
    marginHorizontal: 25,
  }
});

export default InputPin;
