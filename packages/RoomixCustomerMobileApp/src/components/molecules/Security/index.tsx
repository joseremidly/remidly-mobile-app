import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
		View,
		Switch
} from 'react-native';
import Rewards from '../../atoms/Rewards';

export interface Props {
  onPressPin: Function;
  isEnable: any;
  toggleSwitch: any;
}

const SecurityCard: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles(props).containerCard}>
        <View style={{width: '100%', backgroundColor: 'white'}}>
          <TouchableOpacity style={styles(props).containerPinCard} onPress={props.onPressPin}>
						<View style={{flexDirection: 'row'}}>
							<Image style={styles(props).imageStylePin} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604689249/app/Crecy%20icons%20new%20dimensions/Crecy_app_icons_PIN_code_ffzoim.png'}} width={20} height={20}></Image>
							<Text style={styles(props).textPin}>
								Change PIN code
							</Text>
						</View>
						<View>
							<Image style={styles(props).imageStyle} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599251160/app/back_1_bl4as0.png'}} width={15} height={15}></Image>
						</View>
          </TouchableOpacity>
					<TouchableOpacity style={styles(props).containerPinCard} onPress={props.onPressPin}>
						<View style={{flexDirection: 'row'}}>
							<Image style={styles(props).imageStylePin} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604689249/app/Crecy%20icons%20new%20dimensions/Crecy_app_icons_touch_id_uoliva.png'}} width={20} height={20}></Image>
							<Text style={styles(props).textPin}>
								Touch ID 
							</Text>
							<Text style={styles(props).textSwitch}>
                {props.isEnable ? 'Enable' : 'Disable'}
							</Text>
						</View>
						<View >
              <Switch
                trackColor={{ false: "#767577", true: "#7b3eff" }}
                thumbColor={props.isEnable === true ? "white" : "#f4f3f4"}
                onValueChange={() => props.toggleSwitch(props.isEnable)}
                value={props.isEnable}
              />
						</View>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
    containerCard: {
    width: '100%',
  },
  containerPinCard: {
		flexDirection: "row",
		justifyContent: 'space-between',
		alignItems: 'center',
		marginVertical: 10,
		marginHorizontal: 20
  },
  imageStylePin: {
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: 5
  },
  textPin: {
    fontFamily: 'Baloo2-Regular',
    textAlign: 'center',
    color: '#5a6175',
		fontSize: 20,
		marginLeft: 10
  },
  textSwitch: {
    fontFamily: 'Baloo2-Regular',
    textAlign: 'center',
    color: props.isEnable ? 'green' : 'red',
		fontSize: 16,
    marginLeft: 10,
    marginTop: 3
  },
  containerInformationCard: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 15
  },
  containerInformation: {
    marginHorizontal: 30,
  },
  containerHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textHeader: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 24,
    color: '#5a6175',
  },
  textInformation: {
    fontFamily: 'Baloo2-Regular',
    textAlign: 'center',
    color: '#5a6175',
    marginTop: 3,
    fontSize: 14,
    lineHeight: 16,
    textTransform: 'uppercase',
  },
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '30%',
    backgroundColor: '#5a6175',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: props.colorCard,
    borderRadius: 19,
    margin: 10,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Bold',
    textAlign: 'left',
    fontSize: 10,
    textTransform: 'uppercase',
    color: 'white',
  },
});

export default SecurityCard;