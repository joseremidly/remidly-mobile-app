import React from 'react';

import { StyleSheet, View, Text, Image } from 'react-native';

export interface Props {
  type: string;
  fee: number;
}

const AdvanceType: React.FC<Props> = (props: any) => {
  let typeImageUri = '';
  let typeRedable = '';
  let amount = '';

  if (props.type === 'flash') {
    typeRedable = 'Flash';
    amount = '$' + props.fee + ' FEE';
    typeImageUri =
      'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1605743667/app/Crecy%20icons%20new%20dimensions/Crecy_icons_paycheck_flash_white_m3xoyn.png';
  } else {
    typeRedable = 'Estándar';
    amount = 'GRATIS';
    typeImageUri =
      'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1605743667/app/Crecy%20icons%20new%20dimensions/Crecy_icons_paycheck_star_white_dltbxm.png';
  }

  return (
    <>
      <View style={styles.container}>
        <View style={styles.typeImageContainer}>
          <Image style={styles.typeImage} source={{ uri: typeImageUri }} />
        </View>
        <Text style={styles.typeText}>{typeRedable}</Text>
        <View style={styles.amountContainer}>
          <Text style={styles.amountText}>{amount}</Text>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  typeImageContainer: {
    marginHorizontal: 4,
  },
  typeImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginVertical: 8,
  },
  typeText: {
    marginHorizontal: 4,
    fontFamily: 'Baloo2-Bold',
    fontSize: 22,
    color: '#ffffff',
  },
  amountContainer: {
    marginHorizontal: 4,
    backgroundColor: '#ffffff',
    paddingHorizontal: 4,
    paddingVertical: 2,
    borderRadius: 4,
  },
  amountText: {
    color: '#7b3eff',
    fontSize: 12,
  },
});

export default AdvanceType;
