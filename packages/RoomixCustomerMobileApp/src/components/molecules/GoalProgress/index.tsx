import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';

export interface Props {
  goalsCompleted: number;
  goalsTotal: number;
}

const GoalProgress: React.FC<Props> = (props: any) => {
  const navigation = useNavigation();
  const goalImage =
    'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1606238025/app/Crecy%20icons%20new%20dimensions/Crecy_icons_financial_edu_achievment_ylz4ut.png';

  const openGoalDetail = () => {
    navigation.navigate('GoalDetail', {
      goalsCompleted: props.goalsCompleted,
      goalsTotal: props.goalsTotal,
    });
  };

  return (
    <>
      <Text style={styles.goalText}>Meta diaria</Text>
      <TouchableOpacity onPress={openGoalDetail}>
        <View style={styles.goalProgressContainter}>
            <Image style={styles.image} source={{ uri: goalImage }} />
            <Text style={styles.goalProgressText}>
              {props.goalsCompleted} de {props.goalsTotal}
            </Text>
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  goalText: {
    fontSize: 12,
    fontFamily: 'AvenirLTStd-Medium',
    color: '#ffffff',
    textTransform: 'uppercase',
  },
  goalProgressContainter: {
    flexDirection: 'row',
    overflow: 'visible',
    height: 40,
    backgroundColor: '#6432e3',
    borderRadius: 20,
    marginTop: 6,
  },
  image: {
    alignSelf: 'flex-start',
    overflow: 'visible',
    resizeMode: 'contain',
    height: 50,
    width: 36,
    marginTop: 2,
    marginLeft: 2,
  },
  goalProgressText: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 12,
    marginRight: 10,
    marginLeft: 6,
    fontWeight: 'bold',
    fontFamily: 'AvenirLTStd-Medium',
    textTransform: 'uppercase',
  },
});

export default GoalProgress;
