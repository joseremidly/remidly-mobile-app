// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  ActivityIndicator,
} from 'react-native';
import LoadingView from 'src/components/organisms/LoadingView';
import { RNCamera } from 'react-native-camera';
import { useCamera } from 'react-native-camera-hooks';

export interface Props {
  nextStep: Function;
}

const CameraSelfie: React.FC<Props> = (props: any) => {
  const [buttonActive, setButtonActive] = React.useState(false);
  const [photoReady, setPhotoReady] = React.useState(false);
  const [isLoadingPhoto, setIsLoadingPhoto] = React.useState(false);
  const [photo, setPhoto] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const [{ cameraRef }, { takePicture }] = useCamera();
  const pickPhoto = async () => {
    setIsLoadingPhoto(true)
    const options = {
      quality: 0.5,
      width: 2000,
      mirrorImage: true,
      orientation: 'portraitUpsideDown'
    }
    let data = await takePicture(options)
    if (data.uri) {
      setButtonActive(true)
      setPhoto(data.uri)
      setPhotoReady(true)
    }
  }
  const restartCamera = () => {
    setPhotoReady(false)
    setButtonActive(false)
    setIsLoadingPhoto(false)
    setPhoto('')
  }
  const goToTheNext = () => {
    setIsLoading(true)
    props.nextStep(photo)
  }
  return (
    <View style={styles.containerID}>
      <RNCamera
        ref={cameraRef}
        style={{ flex: 1 }}
        captureAudio={false}
        type={RNCamera.Constants.Type.front}
      >
        {({ status }) => {
            if (status !== 'READY') 
              return (
                <View style={{ width: '100%', backgroundColor: '#F4F4F7' }}>
                  <LoadingView />
                </View>
              )
            return (
              <View style={styles.containerIDForm}>
                {photoReady
                  && (
                    <Image style={styles.containerImageID} source={{ uri:photo }} />
                  )}
                <View style={styles.containerShapeID}>
                  <View style={styles.shapeTopIDForm}></View>
                  <View style={styles.containerShapeIDForm}>
                    <View style={styles.shapeIDLeft} />
                    <View style={styles.shapeIDCenter}>
                      <Image source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1617092910/app/Crecy%20icons%20new%20dimensions/corner_nne7kp.png' }} style={styles.cornerTopLeft} />
                      <Image source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1617092910/app/Crecy%20icons%20new%20dimensions/corner_nne7kp.png' }} style={styles.cornerTopRight} />
                      <Image source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1617092910/app/Crecy%20icons%20new%20dimensions/corner_nne7kp.png' }} style={styles.cornerBottomLeft} />
                      <Image source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1617092910/app/Crecy%20icons%20new%20dimensions/corner_nne7kp.png' }} style={styles.cornerBottomRight} />
                      <Image source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1617098907/app/Crecy%20icons%20new%20dimensions/face.png' }} style={styles.faceIcon} />
                    </View>
                    <View style={styles.shapeIDRight} />
                  </View>
                  <View style={styles.shapeBottomIDForm}></View>
                </View>
                <View style={styles.containerSectionButton}>
                  {!buttonActive
                    ? (
                      isLoadingPhoto
                        ? (
                          <View style={styles.containerLoader}>
                            <ActivityIndicator size='large' color='#ffffff' animating={true} />
                          </View>
                        ) : (
                          <TouchableOpacity onPress={() => pickPhoto()} style={styles.captureButton}>
                            <Image style={styles.iconCamera} resizeMode='contain' source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1617086219/app/rec_1_oqobib.png' }}/>
                          </TouchableOpacity>
                        )
                    ) : (
                      isLoading
                        ? (
                          <View style={styles.containerLoader}>
                            <ActivityIndicator size='large' color='#ffffff' animating={true} />
                          </View>
                        ) : (
                          <View style={styles.containerSectionButtons}>
                            <TouchableOpacity onPress={() => restartCamera()} style={styles.functionButtonNext}>
                              <Image style={styles.iconCameraNext} resizeMode='contain' source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1617096261/app/Crecy%20icons%20new%20dimensions/reload-arrow_rsmdzz.png' }}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => goToTheNext()} style={styles.functionButtonNext}>
                              <Image style={styles.iconCameraNext} resizeMode='contain' source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1617096348/app/Crecy%20icons%20new%20dimensions/check_1_ji8cmo.png' }}/>
                            </TouchableOpacity>
                          </View>
                        )
                    )}
                </View>
              </View>
            )
          }}
      </RNCamera>
    </View>
  );
};

const styles = StyleSheet.create({
  containerID: {
    width: '100%',
    height: '100%',
    backgroundColor: '#F4F4F7'
  },
  containerIDForm: {
    position: 'relative',
    width: '100%',
    height: '100%',
  },
  containerImageID: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  containerShapeID: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  shapeTopIDForm: {
    width: '100%',
    height: 88,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  containerShapeIDForm: {
    flexDirection: 'row',
  },
  shapeIDLeft: {
    width: '100%',
    height: 474,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  shapeIDCenter: {
    width: 297,
    height: 474,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  cornerTopLeft: {
    position: 'absolute',
    width: 45,
    height: 45,
    left: -5,
    top: -5,
  },
  cornerTopRight: {
    position: 'absolute',
    width: 45,
    height: 45,
    right: -5,
    top: -5,
    transform: [{ rotate: '90deg'}],
    zIndex: 2,
  },
  cornerBottomLeft: {
    position: 'absolute',
    width: 45,
    height: 45,
    left: -5,
    bottom: -5,
    transform: [{ rotate: '270deg'}],
    zIndex: 2,
  },
  cornerBottomRight: {
    position: 'absolute',
    width: 45,
    height: 45,
    right: -5,
    bottom: -5,
    transform: [{ rotate: '180deg'}],
    zIndex: 2,
  },
  faceIcon: {
    width: 45,
    height: 45,
    bottom: 10,
    zIndex: 2,
  },
  shapeIDRight: {
    width: '100%',
    height: 474,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  shapeBottomIDForm: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  containerSectionButton: {
    width: '100%',
    height: '100%',
    justifyContent: 'flex-end',
  },
  captureButton: {
    position: 'relative',
    alignSelf: 'center',
    margin: 20,
    bottom: 100,
  },
  iconCamera: {
    width: 80,
    height: 80,
  },
  containerLoader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
    bottom: 120,
  },
  containerSectionButtons: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
    bottom: 100,
  },
  functionButtonNext: {
    marginHorizontal: 50,
  },
  iconCameraNext: {
    width: 70,
    height: 70,
  }
});

export default CameraSelfie;