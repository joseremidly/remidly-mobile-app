// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
} from 'react-native';

const CreditLineStartInfo: React.FC<Props> = (props: any) => {
  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>
        ¿Qué es una
        <Text style={styles.colorText}> línea de crédito Crecy</Text>?
      </Text>
      <View styles={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
        <Image style={styles.iconCredit} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833466/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_magic_card_l3cddn.png' }} resizeMode='contain'></Image>
      </View>
      <Text style={styles.headerText}>
        ¡Es pura magia!
      </Text>
      <Text style={styles.informationText}>
        Gracias a nuestra tecnología
        <Text style={styles.colorText}> podemos convertir tu tarjeta de débito </Text>
        (en donde recibes tu salario o ingresos de Uber, Rappi y otras 10+ plataformas)
        <Text style={styles.colorText}> en una línea de crédito revolvente</Text>
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  headerText: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 24,
    color: '#5a6175',
    marginBottom: 10,
    marginHorizontal: 50,
    textAlign: 'center',
    lineHeight: 31
  },
  colorText: {
    color: '#6133E2',
  },
  iconCredit: {
    width: 150,
    height: 160,
    marginBottom: 20
  },
  informationText: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 21,
    color: '#5a6175',
    marginBottom: 10,
    marginHorizontal: 30,
    textAlign: 'center',
    lineHeight: 26
  }
});

export default CreditLineStartInfo;
