import React from 'react';
import { StyleSheet, ImageBackground, View } from 'react-native';

export interface Props {
  children: Object;
  backgroundImage: string | null;
  backgroundColor: string | null;
}

const StoryCardBackground: React.FC<Props> = (props: any) => {
  return (
    <>
      {props.backgroundImage ? (
        <ImageBackground 
          source={{ uri: props.backgroundImage }}
          style={styles(props).backgroundImage}
        >
          <View style={styles(props).overlay}>
            {props.children}
          </View>
        </ImageBackground>
      ) : (
        <View style={styles(props).backgroundColor}>{props.children}</View>
      )}
    </>
  );
};

const styles = (props) =>
  StyleSheet.create({
    backgroundImage: {
      height: '100%',
      width: '100%',
      resizeMode: 'cover',
    },
    overlay: {
      paddingTop: 70,
      backgroundColor: 'rgba(0,0,0,0.75)',
      height: '100%',
      width: '100%',
    },
    backgroundColor: {
      paddingTop: 70,
      height: '100%',
      width: '100%',
      backgroundColor: props.backgroundColor,
    },
  });

export default StoryCardBackground;
