// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    Image,
} from 'react-native';

export interface Props {
  src: string;
}

const FaceIcon: React.FC<Props> = (props) => {
  return (
    <>
      <TouchableOpacity style={styles.faceIconEditStyle}>
        <Image style={styles.iconStyle} source={{uri: props.src}}></Image>
        <View style={styles.iconEdit}>
          <Image 
            style={styles.iconStyleEdit}
            source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604997219/app/Crecy%20icons%20new%20dimensions/Crecy_app_icons_edit_pencil_settings_t6wvd1.png'}}
            resizeMode="contain"
          ></Image>
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  faceIconEditStyle: {
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    height: '100%',
    borderRadius: 100,
  },
  iconStyle: {
    width: '100%',
    borderRadius: 100,
    alignSelf: 'stretch',
  },
  iconEdit: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5a6175',
    borderRadius: 50,
  },
  iconStyleEdit: {
    margin: 10,
    width: 17,
    height: 17,
  },
});

export default FaceIcon;
