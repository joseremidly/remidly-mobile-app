// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    View,
} from 'react-native';

export interface Props {
  onPress: Function;
  header: string;
  information: string;
  buttonName: string;
  src: string;
  colorCard: string;
  type: string;
}

const NewsFeedCard: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles(props).containerNewsFeedCard}>
        <View style={styles(props)['containerImageNewsFeedCard' + props.type]}>
          <Image style={styles(props).imageNewsFeedCard} source={{uri: props.src}}></Image>
        </View>
        <View style={styles(props).containerInfoTotalNewsFeedCard}>
          <View style={styles(props).containerHeader}>
            <Text style={styles(props).textHeader}>{props.header}</Text>
          </View>
          <View style={styles(props).containerInformation}>
            <Text style={styles(props).textInformation}>
              {props.information}
            </Text>
          </View>
          <TouchableOpacity style={styles(props).buttonStyle} onPress={props.onPress}>
            <Text style={styles(props).textButtonStyle}>{props.buttonName}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  containerNewsFeedCard: {
    width: '100%',
    position: 'relative',
    justifyContent: 'space-between',
    backgroundColor: props.colorCard,
    borderRadius: 18,
    marginVertical: 7,
    paddingTop: 8,
    overflow: 'hidden',
  },
  containerImageNewsFeedCard1: {
    width: 120,
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  containerImageNewsFeedCard2: {
    width: 120,
    position: 'absolute',
    bottom: 0,
    right: 5,
  },
  containerImageNewsFeedCard3: {
    width: 120,
    position: 'absolute',
    bottom: 0,
    left: 30,
  },
  imageNewsFeedCard: {
    width: '80%',
    height: 70,
  },
  containerInfoTotalNewsFeedCard: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },
  containerInformation: {
    marginHorizontal: 40,
  },
  containerHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textHeader: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 20,
    color: 'white',
  },
  textInformation: {
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'center',
    color: '#ffffff',
    marginTop: 5,
    fontSize: 13,
    lineHeight: 16,
    textTransform: 'uppercase',
  },
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 12,
    height: 30,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: props.colorCard,
    borderRadius: 19,
    margin: 10,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Medium',
    textAlign: 'center',
    fontSize: 14,
    textTransform: 'uppercase',
    color: props.colorCard,
  },
});

export default NewsFeedCard;