import React from 'react';
import { StyleSheet, View, TouchableOpacity, Text, Image } from 'react-native';

export interface Props {
  selected: boolean;
  steps: number;
  title: string;
  onSelect: Function;
}

const GoalSelector: React.FC<Props> = (props: any) => {
  const lessonsText = props.steps === 1 ? 'lección' : 'lecciones';
  const checkImage = props.selected
    ? 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1606238025/app/Crecy%20icons%20new%20dimensions/Crecy_icons_financial_edu_done_udoxp3.png'
    : 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1605955019/app/Crecy%20icons%20new%20dimensions/Crecy_icons_done_grey_2_hy9grk.png';

  return (
    <>
      <View style={styles.container}>
        <TouchableOpacity onPress={props.onSelect} style={styles.button}>
          <Image source={{ uri: checkImage }} style={styles.checkImage} />
          <View style={styles.infoContainer}>
            <Text style={styles.textTitle}>{props.title}</Text>
            <Text style={styles.textDetail}>
              {props.steps} {lessonsText} al día
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  button: {
    width: '80%',
    paddingVertical: 18,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 12,
    alignSelf: 'center',
  },
  infoContainer: {
    justifyContent: 'flex-start',
  },
  checkImage: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    marginHorizontal: 18,
  },
  textTitle: {
    marginTop: -4,
    fontSize: 20,
    fontFamily: 'Baloo2-Bold',
    color: '#5a6175',
  },
  textDetail: {
    marginTop: -4,
    fontSize: 14,
    fontFamily: 'AvenirLTStd-Medium',
    color: '#5a6175',
    textTransform: 'uppercase',
  },
});

export default GoalSelector;
