// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import DescriptionTransaction from 'src/components/atoms/DescriptionTransaction';
import CardIconTransaction from 'src/components/atoms/CardIconTransaction';
import AmountTransaction from 'src/components/atoms/AmountTransaction';
import categories from 'src/utils/categories';

export interface Props {
  id: string;
  data: any;
}
const TransactionsList: React.FC<Props> = (props: any) => {
  return (
    <>
      <TouchableOpacity style={styles.containerTransaction}>
        <DescriptionTransaction icon={categories[props.data.category].src  === undefined ? categories['default'].src : categories[props.data.category].src} description={props.data.description} category={categories[props.data.category].name === undefined ? categories['default'].name : categories[props.data.category].name} width='50%' />
        <View style={styles.containerCardIcon}>
          <CardIconTransaction card={'amex'} name={props.data.account.name} cardNumber={props.data.account.public_identification_value.replace(/\s/g, '')} color={'#AA92EA'}/>
        </View>
        <View style={styles.containerAmount}>
          <AmountTransaction amount={props.data.amount} expense={props.data.type === 'INFLOW' ? false : true}/>
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  containerTransaction: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    color: 'blue',
    width: '100%',
    marginVertical: 15,
  },
  containerCardIcon: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '25%',
  },
  containerAmount: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    width: '25%',
  }
});

export default TransactionsList;
