// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    View,
} from 'react-native';
import Convert from '../../../utils/convert';
import { ProgressCircle } from 'react-native-svg-charts';

export interface Props {
  onPress: Function;
  header: string;
  src: string;
  type: string;
  totalBalance: any;
  income: any;
  expenses: any;
  nextDate: any;
}

const CardAnalytics: React.FC<Props> = (props: any) => {
  return (
    <>
      <TouchableOpacity style={styles(props).containerCardAnalytics} onPress={props.onPress}>
        {props.type !== '1' ?
            <View style={styles(props)['containerImageNewCard' + props.type]}>
              <Image style={styles(props).imageCardAnalytics} source={{uri: props.src}}></Image>
            </View>
          :
            <>
            </>
        }
        <View style={styles(props).containerInfoTotalNewsFeedCard}>
          <View style={styles(props).containerHeader}>
            <Text style={styles(props).textHeader}>{props.header}</Text>
            <Image style={styles(props).iconHeader} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599251160/app/back_1_bl4as0.png'}}></Image>
          </View>
          {props.type === '1' ? 
            <>
              <View style={styles(props).containerInformationTotalBalance}>
                <View style={{}}>
                  <Text style={styles(props).textInformation}>
                  {`$${props.totalBalance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[0]}.`}
                  </Text>
                </View>
                <View>
                  <Text style={styles(props).textDecimal}>{props.totalBalance && props.totalBalance !== 0 ? (props.totalBalance.toString().includes(".") ? props.totalBalance.toString().split(".")[1].substring(0,2) : '00') : '00'}</Text>
                </View>
              </View>
              <View style={styles(props).containerTotalBalance}>
                <Image style={styles(props).imageTotalBalnce} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1603746838/app/Crecy%20icons%20new%20dimensions/Crecy_app_icon_analytics-01_faumqc.png'}}></Image>
                <Text style={styles(props).textTotalBalance}>Balance Total</Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 10, marginBottom: 15}}>
                <View style={{backgroundColor: '#AA92EA', borderRadius: 9, paddingHorizontal: 15}}>
                  <Text style={styles(props).textCardColor}>Ingresos</Text>
                  <Text style={styles(props).textCardColorNumber}>${Convert(props.income)}</Text>
                </View>
                <View style={{backgroundColor: '#FC5490', borderRadius: 9, marginLeft: 15, paddingHorizontal: 15}}>
                  <Text style={styles(props).textCardColor}>Gastos</Text>
                  <Text style={styles(props).textCardColorNumber}>${Convert(props.expenses)}</Text>
                </View>
              </View>
            </>
          : (props.type === '2' ? 
            <>
              <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginVertical: 15, marginBottom: 30}}>
                <View style={styles(props).containerExpensesInformation}>
                  <View style={styles(props).containerExpensesNumber}>
                    <Text style={styles(props).creditNumber}>
                      ${0}
                    </Text>
                    <Text style={styles(props).creditDecimalNumber}>
                      {'.00'}
                    </Text>
                  </View>
                  <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginTop: -5}}>
                    <View style={styles(props).buttonConected}>
                      <Text style={styles(props).textButton}>
                        Quedan
                      </Text>
                    </View>
                    <Text style={styles(props).textInformationCard}>
                      de ${0}
                    </Text>
                  </View>
                </View>
                <View style={{position: 'relative', marginRight: 5, justifyContent: 'center', alignItems: 'center'}}>
                  <View style={{ zIndex: 0}}>
                    <ProgressCircle style={{width: 80, height: 80}} strokeWidth={6} progress={1} progressColor={'#49c0aa'} />
                  </View>
                  <View style={styles(props).expensesImgAbsolut}>  
                    <Text style={styles(props).textButtonStylePay}>Faltan {0} dias</Text>
                  </View>
                </View>
              </View>
            </>
          :
            <>
              <View style={{flexDirection: 'row', marginTop: 20, marginBottom: 20}}>
                <View style={{backgroundColor: '#FF8700', borderRadius: 14}}>
                  <Text style={styles(props).textCardColorType3}>
                    upcoming bills
                  </Text>
                  <Text style={styles(props).textCardColorNumber}>
                    ${Convert(props.income)}
                  </Text>
                </View>
                <View style={{backgroundColor: '#49c0aa', borderRadius: 14, marginLeft: 15}}>
                  <Text style={styles(props).textCardColorType3}>
                    paid bills
                  </Text>
                  <Text style={styles(props).textCardColorNumber}>
                    ${Convert(props.expenses)}
                  </Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', marginBottom: 30}}>
                <Image style={styles(props).imageWarning} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1601068293/app/crecy_icons_alert_mdully.png'}}></Image>
                <View style={{marginRight: 4}}>
                    <Text style={styles(props).textCreditInfo}>Next bill due on: </Text>
                </View>
                <View>
                    <Text style={styles(props).textCreditInfo}>{props.nextDate}</Text>
                </View>
              </View>
            </>
          )}
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  containerCardAnalytics: {
    width: '100%',
    position: 'relative',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    borderRadius: 18,
    marginVertical: 0,
    paddingTop: 8,
    overflow: 'hidden',
  },
  containerImageNewCard2: {
    width: 100,
    height: 62,
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  containerImageNewCard3: {
    width: 120,
    position: 'absolute',
    bottom: 0,
    right: 10,
  },
  imageCardAnalytics: {
    width: '75%',
    height: 65,
  },
  imageTotalBalnce: {
    width: 40,
    height: 20,
  },
  imageWarning: {
    width: 15,
    height: 15,
    marginTop: 2,
    marginRight: 4
  },
  containerInfoTotalNewsFeedCard: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 15,
  },
  containerInformation: {
    marginHorizontal: 50,
  },
  containerInformationTotalBalance: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  containerInformationBudgetData: {
    flexDirection: 'row',
  },
  containerInformationBudget: {
  },
  containerTotalBalance: {
    flexDirection: 'row',
  },
  containerHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textHeader: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 20,
    color: '#5a6175',
  },
  iconHeader: {
    width: 12,
    height: 10,
    marginLeft: 5,
  },
  textInformation: {
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'center',
    color: '#5a6175',
    marginTop: 20,
    fontSize: 34,
  },
  textDataBudget: {
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'center',
    color: '#5a6175',
    fontSize: 26,
    textTransform: 'uppercase',
  },
  textTotalBalance: {
    fontFamily: 'Baloo2-Medium',
    textAlign: 'center',
    color: '#5a6175',
    fontSize: 16,
    textTransform: 'uppercase',
    marginLeft: 5
  },
  textCardColor: {
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'center',
    color: 'white',
    fontSize: 11,
    textTransform: 'uppercase',
    marginTop: 10,
    marginBottom: 3,
  },
  textCardColorType3: {
    fontFamily: 'Baloo2-Medium',
    textAlign: 'center',
    color: 'white',
    fontSize: 12,
    textTransform: 'uppercase',
    marginTop: 8,
    marginHorizontal: 8
  },
  textCardColorNumber: {
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'center',
    color: 'white',
    fontSize: 18,
    textTransform: 'uppercase',
    marginBottom: 5,
    marginHorizontal: 10
  },
  textDecimal: {
    fontFamily: 'Baloo2-Medium',
    color: '#5a6175',
    fontSize: 20,
    marginTop: 15,
  },
  textCreditInfo: {
    fontFamily: 'Baloo2-Medium',
    color: '#5a6175',
    fontSize: 14,
    textTransform: 'uppercase'
  },
  containerExpensesInformation: {
    marginRight: 30,
  },
  containerExpensesNumber: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    marginTop: -4
  },
  creditNumber: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    color: '#5a6175',
    fontSize: 30,
  },
  creditDecimalNumber: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    color: '#5a6175',
    fontSize: 18,
    marginBottom: 5
  },
  buttonConected: {
    borderRadius: 3,
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 3,
    backgroundColor: '#49c0aa',
    alignItems: 'center',
    flexDirection: 'row',
  },
  textButton: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 12,
    color: '#ffffff',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  textInformationCard: {
    fontFamily: 'AvenirLTStd-Light',
    textAlign: 'center',
    color: '#5a6175',
    paddingTop: 0,
    marginLeft: 6,
    fontSize: 14,
    lineHeight: 18,
    textTransform: 'uppercase',
  },
  expensesImgAbsolut: {
    position: 'absolute',
    zIndex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    width: 50,
    height: 40,
  },
  textButtonStylePay: {
    fontFamily: 'Avenir-Black',
    textAlign: 'center',
    fontSize: 9,
    lineHeight: 12,
    textTransform: 'uppercase',
    color: '#5a6175',
  },
});

export default CardAnalytics;