import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View,
  Alert,
} from 'react-native';

export interface Props {
  accountNumber: string;
  color: String;
  background: String;
  icon: String;
  nameBank: String;
  onRemoveAccount: Function;
}

const AccountCardFunctions: React.FC<Props> = (props: any) => {
  const removeAccount = (accountId: any) => {
    console.log(accountId)
    Alert.alert('Eliminar cuenta', '¿Seguro que quieres desconectar esta cuenta?', [
      {
        text: 'Cancelar',
        onPress: () => {},
        style: "cancel"
      },
      { text: 'Eliminar', onPress: () => props.onRemoveAccount(accountId) }
    ]);
  }
  return (
    <>
      <View style={styles(props).containerAccountCardElement}>
        <View style={styles(props).containerAccountCardElementIcon}>
          <Image source={{ uri: props.icon }} style={styles(props).iconInstitution} resizeMode='contain' />
        </View>
        <View style={styles(props).containerAccountCardElementName}>
          <Text style={styles(props).textAccountCardElementName}>{props.nameBank}</Text>
        </View>
        <TouchableOpacity style={styles(props).containerIconArrow} onPress={() => removeAccount(props.accountNumber)}>
          <Image source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1620355488/app/trash_mcqmvs.png'}} style={styles(props).iconArrow} resizeMode='contain' />
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  containerAccountCardElement: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: props.background,
    borderRadius: 9,
    height: 70,
    marginVertical: 5,
  },
  containerAccountCardElementIcon: {
    width: '30%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  iconInstitution: {
    width: 80,
    height: 20,
    marginLeft: 10,
  },
  containerAccountCardElementName: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: '50%',
  },
  textAccountCardElementName: {
    fontFamily: 'Avenir-Black',
    fontSize: 14,
    color: props.color,
  },
  containerLinkedAccount: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerIconArrow: {
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
    backgroundColor: '#ED1C24',
    borderRadius: 10,
  },
  iconArrow: {
    width: 30,
    height: 30,
    marginHorizontal: 10,
    marginVertical: 10,
  }
});

export default AccountCardFunctions;
