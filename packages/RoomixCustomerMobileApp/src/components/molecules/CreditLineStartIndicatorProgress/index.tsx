// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    View,
} from 'react-native';
import ProgressPoint from 'src/components/atoms/ProgressPoint';

export interface Props {
  step: String;
  setStep: Function; 
  goToStart: Function;
}

const CreditLineStartIndicatorProgress: React.FC<Props> = (props: any) => {
  const [logicPoints, setLogicPoints] = React.useState({ one: true, two: false, three: false });
  const steps = {
    first: { one: true, two: false, three: false },
    second: { one: true, two: true, three: false },
    third: { one: true, two: true, three: true }
  }
  React.useEffect(() => {
    setLogicPoints(steps[props.step])
  }, [props.step])
  return (
    <View style={styles.containerCenterCircles}>
      <ProgressPoint state={logicPoints.one} onPress={() => props.setStep('first')}/>
      <ProgressPoint state={logicPoints.two} onPress={() => props.setStep('second')}/>
      <ProgressPoint state={logicPoints.three} onPress={() => props.setStep('third')}/>
    </View>
  );
};

const styles = StyleSheet.create({
  containerCenterCircles: {
    justifyContent: 'center',
    alignItems: 'center', 
    flexDirection: 'row',
    marginVertical: 0,
  },
});

export default CreditLineStartIndicatorProgress;
