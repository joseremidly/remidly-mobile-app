// @ts-nocheck
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { CreditCardInput } from "react-native-credit-card-input";

export interface Props {
  onFocus: Function;
  onChange: Function;
}

const AddCard: React.FC<Props> = (props: any) => {

  return (
    <>
      <View style={styles.container}>
				<CreditCardInput
						autoFocus

						requiresName
						requiresCVC

						cardScale={1.1}
						labelStyle={styles.label}
						inputStyle={styles.input}
						validColor={"black"}
						invalidColor={"red"}
						placeholderColor={"darkgray"}
						allowScroll={true}
						onChange={props.onChange} />
			</View>
    </>
  );
};

const styles = StyleSheet.create({
	input: {
    fontSize: 16,
    color: "black",
  },
  label: {
    color: "black",
    fontSize: 12
  },
  container: {
    backgroundColor: "#F5F5F5",
    marginVertical: 20,
		padding: 30,
		borderRadius: 18
  },
});

export default AddCard;