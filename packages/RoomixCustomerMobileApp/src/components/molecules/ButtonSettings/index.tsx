// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Image,
    Text,
} from 'react-native';

export interface Props {
  src: String;
  text: String;
  size: Number, default: 38;
  color: String;
  route: String;
  onClick: Function;
}

const defaultProps: Props = {
  size: 38,
}

const FaceIcon: React.FC<Props> = (props) => {

  return (
    <>
      <TouchableOpacity style={styles(props).buttonSettingsStyle} onPress={() => props.onClick(props.route)}>
        <Image
          style={styles(props).iconButtonSettingsStyle}
          source={props.src}
          resizeMode="contain"
        ></Image>
        <Text style={styles(props).textButtonSettings}>{props.text}</Text>
        <Image
          style={styles(props).iconArrow}
          source={require('assets/images/settings-icons/settings_arrow.png')}
          resizeMode="contain"
        ></Image>
      </TouchableOpacity>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  buttonSettingsStyle: {
    position: 'relative',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    height: 50,
  },
  iconButtonSettingsStyle: {
    width: props.size,
    height: props.size,
  },
  textButtonSettings: {
    width: '75%',
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 20,
    color: '#5a6175',
    overflow: 'scroll',
    textAlign: 'left',
  },
  iconArrow: {
    width: 12,
    height: 15,
  },
});

FaceIcon.defaultProps = defaultProps;

export default FaceIcon;
