// @ts-nocheck
import React from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import CreditOnboardingInfo from 'src/components/atoms/CreditOnboardingInfo';
import LinkedAccountsElements from 'src/components/organisms/LinkedAccountsElements';

export interface Props {
  accounts: any;
  headerTextOne: string;
  headerTextTwo: string;
  headerTextHighlighted: string;
  infoText: string;
  goToConnect: Function;
  onSelectAccount: Function;
}

const CreditOnboardingSelectAccount: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containter}>
        <Text style={styles.headerText}>
          {props.headerTextOne}
          <Text style={styles.textHighlighted}>
            {' '}{props.headerTextHighlighted}{' '}
          </Text>
          {props.headerTextTwo}
        </Text>
        <CreditOnboardingInfo text={props.infoText} />
        <ScrollView style={styles.containerScrollLinkedAccountsElements}>
          <LinkedAccountsElements accounts={props.accounts} goToConnect={props.goToConnect} onSelectAccount={props.onSelectAccount} />
        </ScrollView>
        {/* <Text style={styles.whyText}>¿Por qué me piden esto?</Text> */}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containter: {
    alignItems: 'center',
  },
  headerText: {
    paddingHorizontal: 30,
    marginVertical: 10,
    textAlign: 'center',
    fontFamily: 'Avenir-Black',
    fontSize: 20,
    lineHeight: 25,
    color: '#5a6175',
  },
  textHighlighted: {
    color: '#7f47dd',
  },
  buttonContainer: {
    marginTop: 40,
  },
  whyText: {
    marginTop: 16,
    textAlign: 'center',
    fontFamily: 'AvenirLTStd-Roman',
    color: '#7f47dd',
  },
  containerScrollLinkedAccountsElements: {
    width: '100%',
  },

});

export default CreditOnboardingSelectAccount;
