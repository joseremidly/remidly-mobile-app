import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View,
} from 'react-native';

export interface Props {
  accountNumber: string;
  color: String;
  background: String;
  icon: String;
  nameBank: String;
  onSelectAccount: Function;
}

const AccountCardElement: React.FC<Props> = (props: any) => {
  return (
    <>
      <TouchableOpacity style={styles(props).containerAccountCardElement} onPress={() => props.onSelectAccount(props.accountNumber)}>
        <View style={styles(props).containerAccountCardElementIcon}>
          <Image source={{ uri: props.icon }} style={styles(props).iconInstitution} resizeMode='contain' />
        </View>
        <View style={styles(props).containerAccountCardElementName}>
          <Text style={styles(props).textAccountCardElementName}>{props.nameBank}</Text>
        </View>
        <View style={styles(props).containerIconArrow}>
          <Image source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1605814313/app/Crecy%20icons%20new%20dimensions/crecy_icons_little_arrow_white_header_r6sjsz.png'}} style={styles(props).iconArrow} />
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  containerAccountCardElement: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: props.background,
    borderRadius: 9,
    height: 70,
    marginVertical: 5,
  },
  containerAccountCardElementIcon: {
    width: '30%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  iconInstitution: {
    width: 80,
    height: 20,
    marginLeft: 10,
  },
  containerAccountCardElementName: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: '50%',
  },
  textAccountCardElementName: {
    fontFamily: 'Avenir-Black',
    fontSize: 14,
    color: props.color,
  },
  containerLinkedAccount: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerIconArrow: {
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 5,
  },
  iconArrow: {
    width: 9,
    height: 18,
    marginHorizontal: 10,
  }
});

export default AccountCardElement;
