import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    View,
} from 'react-native';
import Rewards from '../../atoms/Rewards';

export interface Props {
  onPress: Function;
  star: string,
  header: string;
  information: string;
  buttonName: string;
  src: string;
  colorCard: string;
  levelNumber: string;
  percentage: string;
}

const CardRewards: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles(props).containerCard}>
        <View style={styles(props).containerImageAbsolute}>
          <Image style={styles(props).imageStyle} source={{uri: props.src}}></Image>
        </View>
        <View style={styles(props).containerDetailsBill}>
          <View style={{flexDirection: "row"}}>
          <Image style={styles(props).imageStyle} source={{uri: props.star}} width={15} height={15}></Image>
            <Text style={styles(props).textLevel}>
              Level {props.levelNumber}
            </Text>
          </View>
          <View  style={{width: 55, height: 4, left: 5}}>
            <Rewards percentage={props.percentage} color='#5a6175' />
          </View>
        </View>
        <View style={styles(props).containerInformationCard}>
          <View style={styles(props).containerHeader}>
            <Text style={styles(props).textHeader}>{props.header}</Text>
          </View>
          <View style={styles(props).containerInformation}>
            <Text style={styles(props).textInformation}>
              {props.information}
            </Text>
          </View>
        </View>

        <View style={{justifyContent: 'center', alignItems: 'flex-end'}}>
          <TouchableOpacity style={styles(props).buttonStyle} onPress={props.onPress}>
            <Text style={styles(props).textButtonStyle}>{props.buttonName}</Text>
          </TouchableOpacity>
        </View>
        
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
    containerCard: {
    width: '80%',
    backgroundColor: props.colorCard,
    borderRadius: 9,
    marginVertical: 10,
  },
  containerImageAbsolute: {
    width: 120,
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  imageStyle: {
    width: '80%',
    height: 70,
  },
  containerDetailsBill: {
    top: 8,
    left: 5,

  },
  textLevel: {
    fontFamily: 'Baloo2-Regular',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    textAlign: 'center',
    color: '#5a6175',
    marginTop: 3,
    marginLeft: 3,
    fontSize: 12,
    lineHeight: 16,
    textTransform: 'uppercase',
  },
  containerInformationCard: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 15
  },
  containerInformation: {
    marginHorizontal: 30,
  },
  containerHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textHeader: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 24,
    color: '#5a6175',
  },
  textInformation: {
    fontFamily: 'Baloo2-Regular',
    textAlign: 'center',
    color: '#5a6175',
    marginTop: 3,
    fontSize: 14,
    lineHeight: 16,
    textTransform: 'uppercase',
  },
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '30%',
    backgroundColor: '#5a6175',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: props.colorCard,
    borderRadius: 19,
    margin: 10,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Bold',
    textAlign: 'left',
    fontSize: 10,
    textTransform: 'uppercase',
    color: 'white',
  },
});

export default CardRewards;