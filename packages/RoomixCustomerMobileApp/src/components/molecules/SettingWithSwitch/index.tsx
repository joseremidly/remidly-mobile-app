// @ts-nocheck
import React from 'react';
import { StyleSheet, View, Text, Switch } from 'react-native';

export interface Props {
  text: string;
  checked: boolean;
  toggleSwitch: Function;
  disabled: boolean | null | undefined;
}

const SettingWithSwitch: React.FC<Props> = (props: any) => {
  const disabled = props.disabled | false;

  const trackColorFalse = !disabled ? '#767577' : '#AAAAAA';
  const trackColorTrue = !disabled ? '#7B3EFF' : '#A67CFF';

  return (
    <>
      <View style={styles(props).wrapper}>
        <Text style={styles(props).text}>{props.text}</Text>
        <Switch
          trackColor={{ false: trackColorFalse, true: trackColorTrue }}
          thumbColor={props.checked ? 'white' : '#F4F3F4'}
          onValueChange={props.toggleSwitch}
          disabled={props.disabled}
          value={props.checked}
        />
      </View>
    </>
  );
};

const styles = (props: any) =>
  StyleSheet.create({
    wrapper: {
      width: '100%',
      justifyContent: 'space-between',
      flexDirection: 'row',
      paddingVertical: 8,
    },
    text: {
      fontFamily: 'AvenirLTStd-Medium',
      fontSize: 18,
      color: !props.disabled ? '#5a6175' : '#bbbbbb',
    },
  });

export default SettingWithSwitch;
