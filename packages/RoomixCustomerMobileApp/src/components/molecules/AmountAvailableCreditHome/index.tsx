// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

export interface Props {
  creditAmount: any;
  availableAmount: any;
}

const AmountAvailableCreditHome: React.FC<Props> = (props: any) => {
  return (
    <View style={styles.containerCreditInformation}>
      <View style={styles.containerCreditNumber}>
        <Text style={styles.creditNumber}>${props.availableAmount}</Text>
        <Text style={styles.creditDecimal}>{'.00'}</Text>
      </View>
      <View style={{flexDirection: 'row', justifyContent: 'center', alignItems:'center', marginTop: -8}}>
        <View style={styles.containerLeftDays}>
          <Text style={styles.textLeftDays}>Quedan</Text>
        </View>
        <Text style={styles.textCreditAmount}>de ${props.creditAmount}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerCreditInformation: {
    marginRight: 30,
  },
  containerCreditNumber: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  creditNumber: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    color: '#ffffff',
    fontSize: 34,
    textTransform: 'uppercase',
  },
  creditDecimal: {
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
    color: '#ffffff',
    fontSize: 16,
    textTransform: 'uppercase',
    paddingTop: 10
  },
  containerLeftDays: {
    borderRadius: 3,
    backgroundColor: '#49c0aa',
    alignItems: 'center',
    flexDirection: 'row',
  },
  textLeftDays: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 14,
    color: '#ffffff',
    textAlign: 'center',
    textTransform: 'uppercase',
    paddingHorizontal: 4
  },
  textCreditAmount: {
    fontFamily: 'AvenirLTStd-Roman',
    textAlign: 'center',
    color: '#ffffff',
    marginLeft: 6,
    fontSize: 13,
    lineHeight: 18,
    textTransform: 'uppercase',
  },
});

export default AmountAvailableCreditHome;
