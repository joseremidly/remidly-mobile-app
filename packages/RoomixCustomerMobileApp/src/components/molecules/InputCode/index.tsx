// @ts-nocheck
import React from 'react';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import {
    StyleSheet,
    View,
} from 'react-native';

export interface Props {
  onChangeInput: Function;
}

const InputCode: React.FC<Props> = (props: any) => {
  const [value, setValue] = React.useState('');
  const onChangeCode = (code) => {
    setValue(code)
    if (code.length == 4) {
      props.onChangeInput(code)
    }
  }
  return (
    <>
      <View style={styles.containerInputCode}>
        <SmoothPinCodeInput
          mask={<View style={{
            width: 10,
            height: 10,
            borderRadius: 25,
            backgroundColor: '#5a6175',
          }}></View>}
          maskDelay={100}
          ref={this.pinInput}
          codeLength={4}
          value={value}
          password={true}
          onTextChange={code => onChangeCode(code)}
          onFulfill={this._checkCode}
          onBackspace={this._focusePrevInput}
          cellStyle={{
            width: 44,
            height: 60,
            backgroundColor: '#f1edfe',
            borderRadius: 6,
          }}
          cellStyleFocused={{
            borderStyle: 'solid',
            borderWidth: 2,
            borderColor: '#7b3eff'
          }}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerInputCode: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'stretch',
    margin: 25,
  }
});

export default InputCode;
