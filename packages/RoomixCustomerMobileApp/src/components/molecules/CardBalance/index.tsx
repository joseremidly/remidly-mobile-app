import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View,
} from 'react-native';

export interface Props {
  update: Function;
  background: string;
  color: string;
  icon: string;
  accountsLinked: number;
  category: string;
  balance: number;
  name: string;
  updatedDate: string;
  positionLogo: any;
  institution: any;
}

const CardBalance: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles(props).containerCardBalance}>
        <View style={styles(props).containerInfoCardBalanceIcon}>
          <Image source={{ uri: props.icon }} style={styles(props).iconInstitution} resizeMode='contain'/>
        </View>
        <View style={styles(props).containerInfoCardBalance}>
          {/* <View style={styles(props).containerLinkedAccount}>
            <Text style={styles(props).textLinkedAccount}>TARJETAS CONECTADAS</Text>
            <View style={styles(props).iconLinkedAccount}>
              <Text style={styles(props).numberLinkedAccount}>{props.accountsLinked}</Text>
            </View>
          </View> */}
          <View style={styles(props).containerLinkedAccount}>
            <Text style={styles(props).textLinkedAccount}>{props.name}</Text>
          </View>
          <Text style={styles(props).textCardBalance}>{props.institution.type === 'gig' ? 'Total ingresos' : 'Balance total'}</Text>
          <Text style={styles(props).numberCardBalance}>{`$${props.category === 'CREDIT_CARD' ? props.balance.toString() === '0' ? '' : '-' : ''}${props.balance.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").split(".")[0]}.`}
            <Text style={styles(props).decimalsCardBalance}>{props.balance && props.balance !== 0 && props.balance.toString().split(".").length > 1 ? props.balance.toString().split(".")[1].substring(0,2) : '00'}</Text>
          </Text>
        </View>
        <View style={styles(props).containerUpdatedDate}>
          {/* <TouchableOpacity style={styles(props).buttonUpdatedDate} onPress={props.updateData}>
            <Text style={styles(props).textUpdatedDate}>{props.updatedDate}</Text>
            <Image source={{ uri: props.color === '#FFFFFF' ? 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1601946594/app/crecy_icons_refresh_white_for_button_albqtn.png' : 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1600060530/app/crecy_icons_refresh_button_card_balance_jy67ne.png' }} style={styles(props).iconUpdatedDate} />
          </TouchableOpacity> */}
          <Text style={styles(props).textUpdatedDate}>{props.updatedDate}</Text>
        </View>
      </View>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  containerCardBalance: {
    justifyContent: 'space-between',
    backgroundColor: props.background,
    borderRadius: 9,
    height: 200,
    margin: 10,
  },
  containerInfoCardBalanceIcon: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  iconInstitution: {
    width: props.positionLogo !== '' ? props.positionLogo.width : 140,
    height: props.positionLogo !== '' ? props.positionLogo.height : 20,
    marginTop: 10,
    marginLeft: 10,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  containerInfoCardBalance: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerLinkedAccount: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textLinkedAccount: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 14,
    color: props.color,
  },
  iconLinkedAccount: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: props.color,
    width: 20,
    height: 20,
    borderRadius: 50,
    paddingTop: 2,
    marginLeft: 5,
  },
  numberLinkedAccount: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 14,
    lineHeight: 20,
    color: props.background,
  },
  textCardBalance: {
    fontFamily: 'Baloo2-Bold',
    color: props.color,
    fontSize: 22,
  },
  numberCardBalance: {
    color: props.color,
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 24,
  },
  decimalsCardBalance: {
    color: props.color,
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 20,
  },
  containerUpdatedDate: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    padding: 5,
  },
  buttonUpdatedDate: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: 40,
  },
  textUpdatedDate: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 10,
    color: props.color,
    textTransform: 'uppercase',
    marginBottom: 3,
    marginRight: 4
  },
  iconUpdatedDate: {
    width: 19,
    height: 23,
    marginLeft: 5,
    marginRight: 10,
  }
});

export default CardBalance;
