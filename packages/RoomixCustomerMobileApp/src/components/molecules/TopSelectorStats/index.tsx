// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    View,
} from 'react-native';

export interface Props {
  overall: Function;
  category: any;
}

const TopSelectorStats: React.FC<Props> = (props: any) => {
  return (
    <View style={styles(props).containerHeader}>
      <TouchableOpacity style={styles(props).containerTextHeader} onPress={() => props.overall(true)}>
          <Text style={styles(props).textHeader}>En general</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles(props).containerTextHeaderCategory} onPress={() => props.overall(false)}>
          <Text style={styles(props).textHeader}>Por categoria</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles= (props: any) => StyleSheet.create({
  containerHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 20
  },
  containerTextHeader: {
    borderRadius: 6,
    backgroundColor: props.category === true ? '#E7E6FF' : 'white',
    margin: 5,
    minHeight: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerTextHeaderCategory: {
    borderRadius: 6,
    backgroundColor: props.category === true ? 'white' : '#E7E6FF',
    margin: 5,
    minHeight: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textHeader: {
    fontFamily: 'Avenir-Black',
    fontSize: 18,
    color: '#5a6175',
    marginHorizontal: 10,
  }
});

export default TopSelectorStats;