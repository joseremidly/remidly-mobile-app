import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import { ProgressCircle } from 'react-native-svg-charts';
import { useNavigation } from '@react-navigation/native';

export interface Props {
  progress: number;
  navigation: Object;
}

const CoursesHomeHeader: React.FC<Props> = (props: any) => {
  const navigation = useNavigation();

  const hatImage =
    'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1606238026/app/Crecy%20icons%20new%20dimensions/Crecy_icons_financial_edu_hat_lvobst.png';

  const goBackImage =
    'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599596734/app/crecy_icons_arrow_header_white-01_dg8lby.png';

  return (
    <>
      <View style={styles.container}>
        <View style={styles.leftPadding}>
          <TouchableOpacity onPress={navigation.goBack}>
            <Image style={styles.goBack} source={{ uri: goBackImage }} />
          </TouchableOpacity>
        </View>
        <View style={styles.centralContainter}>
          <Image style={styles.image} source={{ uri: hatImage }} />
          <Text style={styles.headerText}>Educación</Text>
        </View>
        <View style={styles.progressContainer}>
          <ProgressCircle
            style={styles.progressCircle}
            strokeWidth={4}
            progress={props.progress / 100}
            progressColor="#fcfbff"
            backgroundColor="#a275ff">
            <View style={styles.progressTextContainter}>
              <Text style={styles.progressText}>{props.progress}%</Text>
            </View>
          </ProgressCircle>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    paddingHorizontal: 12,
    marginBottom: 4,
  },
  goBack: {
    width: 25,
    height: 25,
    marginLeft: 20,
  },
  leftPadding: {
    flex: 0.2,
    height: '100%',
  },
  centralContainter: {
    flex: 0.6,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  progressContainer: {
    flex: 0.2,
    alignItems: 'flex-end',
  },
  image: {
    height: 36,
    width: 36,
    resizeMode: 'contain',
    marginRight: 10,
  },
  progressCircle: {
    width: 50,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
  },
  progressTextContainter: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  progressText: {
    fontSize: 12,
    fontFamily: 'Baloo2-Medium',
    color: '#ffffff',
  },
  headerText: {
    fontSize: 28,
    fontFamily: 'Baloo2-Medium',
    color: '#ffffff',
  },
});

export default CoursesHomeHeader;
