import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import CreditOnboardingInfo from 'src/components/atoms/CreditOnboardingInfo';

import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  showError: boolean;
  headerTextOne: string;
  headerTextTwo: string;
  headerTextHighlighted: string;
  infoText: string;
  imageURI: string;
  buttonText: string;
  nextStep: string;
  setStep: Function;
}

const CreditOnboardingCard: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containter}>
        <Text style={styles.headerText}>
          {props.headerTextOne}
          <Text style={styles.textHighlighted}>
            {' '}
            {props.headerTextHighlighted}{' '}
          </Text>
          {props.headerTextTwo}
        </Text>

        <CreditOnboardingInfo text={props.infoText} />

        <Image style={styles.mainImage} source={{ uri: props.imageURI }} />

        <View style={styles.buttonContainer}>
          <ButtonMid
            label={props.buttonText}
            secondary={false}
            onPress={() => props.setStep(props.nextStep)}
          />
        </View>
        {/* <Text style={styles.whyText}>¿Por qué me piden esto?</Text> */}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containter: {
    alignItems: 'center',
  },
  headerText: {
    paddingHorizontal: 40,
    marginBottom: 30,
    textAlign: 'center',
    fontFamily: 'AvenirLTStd-Roman',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#5a6175',
  },
  textHighlighted: {
    color: '#7f47dd',
  },
  mainImage: {
    marginTop: 20,
    width: 150,
    height: 150,
    resizeMode: 'contain',
  },
  buttonContainer: {
    marginTop: 40,
  },
  whyText: {
    marginTop: 16,
    textAlign: 'center',
    fontFamily: 'AvenirLTStd-Roman',
    color: '#7f47dd',
  },
});

export default CreditOnboardingCard;
