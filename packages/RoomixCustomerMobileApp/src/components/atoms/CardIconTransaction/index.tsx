// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  Image,
  View,
  Text,
} from 'react-native';
export interface Props {
  card: string;
  cardNumber: any;
  color: string;
}

const CardIconTransaction: React.FC<Props> = (props: any) => {
  const cardsIcons = {
    visa: require('assets/images/crecy_icons_visa_white.png'),
    mastercard: require('assets/images/crecy_icons_master_card.png'),
    amex: require('assets/images/crecy_icons_amex_white.png'),
  }
  return (
    <>
      <View style={[styles.containerCardIconTransaction, { backgroundColor: props.color}]}>
        {/* <Image style={styles[props.card]} source={cardsIcons[props.card]} resizeMode='contain'/> */}
        <Text style={styles.cardNumber}>{props.cardNumber === '' ? props.name.substring(props.name.length - 4) : props.cardNumber.substring(props.cardNumber.length - 4)}</Text>
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  containerCardIconTransaction: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: 24,
    width: 40,
    borderRadius: 6,
  },
  visa: {
    width: 20,
    height: 6,
    marginHorizontal: 5,
  },
  mastercard: {
    width: 20,
    height: 12,
    marginHorizontal: 5,
  },
  amex: {
    width: 20,
    height: 18,
    marginHorizontal: 5,
  },
  cardNumber: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 11,
    color: '#ffffff',
    marginRight: 5,
    overflow: 'scroll',
  }
})

export default CardIconTransaction;