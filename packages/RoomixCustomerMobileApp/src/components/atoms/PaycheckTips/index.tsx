import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
} from 'react-native';

export interface Props {
  labelImportan: string;
  label: string;
}

const PaycheckTips: React.FC<Props> = (props) => {
  return (
    <>
      <View style={styles.container}>
        <Image style={styles.iconContainer} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604014175/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_advances_green_icon_ready_istaq2.png'}} resizeMode='contain'></Image>
        <Text style={styles.textGreenStyle}>{props.labelImportan}<Text style={styles.textStyle}> {props.label}</Text></Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 2,
  },
  textStyle: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 20,
    color: '#5a6175',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  textGreenStyle: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 20,
    color: '#49c0aa',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  iconContainer: {
    width: 25,
    height: 25,
    marginLeft: 30,
    marginRight: 10,
  },
});

export default PaycheckTips;
