import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
} from 'react-native';

export interface Props {
  label: string;
  secondary: boolean;
  onPress: Function;
  src: string;
}

const ButtonSendCode: React.FC<Props> = (props: any) => {
  return (
    <>
      <TouchableOpacity style={styles(props).buttonStyle} onPress={props.onPress}>
        <Text style={styles(props).textButtonStyle}>{props.label}</Text>
        <Image style={styles(props).iconButtonStyle} source={{uri: props.src}}></Image>
      </TouchableOpacity>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    height: 40,
    backgroundColor: props.secondary ? '#e6e6e6' : '#49c0aa',
    borderRadius: 7,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 16,
    lineHeight: 25,
    marginHorizontal: 15,
    color: props.secondary ? '#999999' : '#ffffff',
  },
  iconButtonStyle: {
    width: 15,
    height: 18,
    marginRight: 10,
  },
});

export default ButtonSendCode;
