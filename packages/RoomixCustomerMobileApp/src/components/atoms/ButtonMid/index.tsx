import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
} from 'react-native';

export interface Props {
  label: String;
  secondary: Boolean;
  onPress: Function;
  backgroundColor: String;
}

const ButtonMid: React.FC<Props> = (props: any) => {
  return (
    <>
      <TouchableOpacity style={styles(props).buttonStyle} onPress={props.onPress}>
        <Text style={styles(props).textButtonStyle}>{props.label}</Text>
      </TouchableOpacity>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 250,
    height: 46,
    backgroundColor: props.backgroundColor ? props.secondary ? '#e6e6e6' : props.backgroundColor : props.secondary ? '#e6e6e6' : '#7b3eff',
    borderRadius: 17,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 20,
    color: props.secondary ? '#999999' : '#ffffff',
  },
});

export default ButtonMid;
