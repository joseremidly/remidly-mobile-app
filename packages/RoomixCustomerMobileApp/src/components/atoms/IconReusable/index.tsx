import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Image,
} from 'react-native';

export interface Props {
  src: string;
  color: string;
  size: number;
  padding: number;
  margin: number;
  onPress: Function;
}

const Icons: React.FC<Props> = (props: any) => {

  return (
    <>
      <TouchableOpacity style={styles(props).iconStyle} onPress={props.onPress}>
        <Image style={styles(props).iconSelectStyle} source={{uri: props.src}} resizeMode="contain" ></Image>
      </TouchableOpacity>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  iconStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: props.color,
    borderRadius: 50,
    margin: props.margin,
  },
  iconSelectStyle: {
    margin: props.padding,
    width: props.size,
    height: props.size,
  },
});

export default Icons;
