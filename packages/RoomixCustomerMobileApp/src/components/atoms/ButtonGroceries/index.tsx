import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
} from 'react-native';

export interface Props {
  label: string;
  src: string;
  onPress: Function;
}

const ButtonGroceries: React.FC<Props> = (props) => {
  return (
    <>
      <TouchableOpacity style={styles.buttonStyle} onPress={props.onPress} >
        <Image style={styles.iconButtonStyle} source={{uri: props.src}} resizeMode='contain'></Image>
        <Text style={styles.textButtonStyle}>{props.label}</Text>
        <Image style={styles.arrowIconStyle} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599250202/app/back_rzgh1o.png'}}></Image>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    alignSelf: 'stretch',
    paddingHorizontal: 10,
    height: 45,
    width: 150,
    backgroundColor: '#ed1e79',
    borderRadius: 10,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 16,
    color: '#ffffff',
  },
  iconButtonStyle: {
    width: 15,
    height: 15,
    marginLeft: 2,
    marginBottom: 4,
  },
  arrowIconStyle: {
    width: 12,
    height: 12,
    marginLeft: 2,
    marginBottom: 1,
  },
});

export default ButtonGroceries;
