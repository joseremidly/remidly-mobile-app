import React from 'react';
import {
    StyleSheet,
    Image,
    View,
} from 'react-native';
import Moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';

export interface Props {
  placeholder: string;
  onChangeInput: Function;
  date: any;
}

const DateInput: React.FC<Props> = (props: any) => {
  // Moment.locale('es');
  const [dataPickerActive, setDataPickerActive] = React.useState(true)
  const [currentDate, setCurrentDate] = React.useState(new Date(Moment(props.date).format('l')))

  const updateDate = (date: any) => {
    setCurrentDate(date)
    props.onChangeInput(date)
  }
  return (
    <View style={styles.inputStyle}>
      <Image style={styles.iconStyleDate} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1606030276/app/Crecy%20icons%20new%20dimensions/Crecy_icons_calendar_grey_ua7glf.png'}} resizeMode="contain" ></Image>
      <View style={{width: '35%', marginHorizontal: dataPickerActive ?  10 : 0}}>
        <DateTimePicker
          testID="dateTimePicker"
          value={currentDate}
          mode="date"
          display="default"
          onChange={(event: any, date: any) => updateDate(date)}
          style={{ justifyContent: 'center', alignItems: 'center', }}
        />
    </View>
    <Image style={styles.iconStyleEdit} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1606030276/app/Crecy%20icons%20new%20dimensions/Crecy_icons_edit_lite_grey_lw29xs.png'}} resizeMode="contain" ></Image>
  </View>
  );
};

const styles = StyleSheet.create({
  inputStyle: {
    width: '100%',
    flexDirection: 'row',
  },
  iconStyleDate: {
    width: 25,
    height: 25, 
    marginTop: 2
  },
  iconStyleEdit: {
    width: 25,
    height: 25, 
    marginTop: 2
  },
  textInformationBirthday: {
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'center',
    letterSpacing: .5,
    fontSize: 18,
    lineHeight: 18,
    color: '#999999',
  }
});

export default DateInput;
