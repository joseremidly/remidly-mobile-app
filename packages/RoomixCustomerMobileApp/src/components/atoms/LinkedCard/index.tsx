import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
		View,
		Switch,
} from 'react-native';
import Convert from '../../../utils/convert';

export interface Props {
	background: string;
	isEnableAccount: any;
	toggleSwitchAccount: any;
	isEnableBalance: any;
  toggleSwitchBalance: any;
  typeCard: any;
  numberCard: any;
  totalAccount: any;
  src: any;
}

const CardBanckAccounts: React.FC<Props> = (props: any) => {
  let numberCard = props.numberCard.substring(props.numberCard.length - 4, props.numberCard.length)
  return (
    <>
			<View style={styles(props).containerCard}>
				<View style={styles(props).accountBank}>
					<View style={{justifyContent: 'center',alignItems:'center', borderRadius: 3, flexDirection: 'row'}}>
						<Image style={styles(props).iconEdit} source={{uri: props.src}} width={50} resizeMode='contain'></Image>
            <Text style={styles(props).textPointCard}>....</Text>
						<Text style={styles(props).textCardNumber}>{numberCard}</Text>
					</View>
					<View>
            <Text style={styles(props).textCardAccounts}>$ {Convert(parseInt(props.totalAccount))}</Text>
						<View style={{justifyContent: 'center', alignItems: 'flex-end', marginBottom: 5}}>
              <View style={{backgroundColor: 'white', borderRadius: 15}}>
							  <Text style={styles(props).textTypeCard}>{props.typeCard}</Text>
              </View>
						</View>
					</View>
				</View>
			</View>
			<View style={styles(props).containerSwitch}>
				<View style={{width: '50%', justifyContent: 'center', alignItems: 'center', borderRightColor: 'black'}}>
					<Text style={styles(props).textSwitch}>
						Show the account
					</Text>
					<Switch
						trackColor={{ false: "#767577", true: "#7b3eff" }}
						thumbColor={props.isEnableAccount === true ? "white" : "#f4f3f4"}
						onValueChange={() => props.toggleSwitchAccount(props.isEnableAccount, props.numberCard)}
						value={props.isEnableAccount}
					/>
				</View>
				<View style={{width: '50%', justifyContent: 'center', alignItems: 'center'}}>
					<Text style={styles(props).textSwitch}>
						Show the balance
					</Text>
					<Switch
						trackColor={{ false: "#767577", true: "#7b3eff" }}
						thumbColor={props.isEnableBalance === true ? "white" : "#f4f3f4"}
						onValueChange={() => props.toggleSwitchBalance(props.isEnableBalance, props.numberCard)}
						value={props.isEnableBalance}
					/>
				</View>
			</View>
		</>
  );
};

const styles = (props: any) => StyleSheet.create({
  containerCard: {
    width: '90%',
    backgroundColor: props.background,
    borderRadius: 9,
    marginHorizontal: 20
  },
  accountBank: {
		marginVertical: 10,
		justifyContent: 'space-between',
    flexDirection: 'row',
    marginHorizontal: 20
	},
	containerSwitch: {
		flexDirection: 'row',
		marginVertical: 10,
  },
  textPointCard: {
    fontFamily: 'Baloo2-Regular',
    color: 'white',
		fontSize: 34,
    marginLeft: 10,
    marginBottom: 5
	},
  textCardNumber: {
    fontFamily: 'Baloo2-Regular',
    color: 'white',
		fontSize: 20,
    marginHorizontal: 5,
    marginTop: 10
	},
  textTypeCard: {
    fontFamily: 'Baloo2-Regular',
    color: props.background,
		fontSize: 12,
    marginHorizontal: 10,
	},
  textSwitch: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 16,
		color: '#5a6175',
		marginVertical: 5,
  },
  textCardAccounts: {
    fontFamily: 'Baloo2-Bold',
    color: 'white',
		fontSize: 24,
  },
  iconEdit: {
		height: 30,
		marginTop: 5
  },
  containerInfoCardBalance: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerLinkedAccount: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textLinkedAccount: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 14,
    color: props.color,
  },
  iconLinkedAccount: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: props.color,
    width: 20,
    height: 20,
    borderRadius: 50,
    paddingTop: 2,
    marginLeft: 5,
  },
  numberLinkedAccount: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 14,
    lineHeight: 20,
    color: props.background,
  },
  numberCardBalance: {
    color: props.color,
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 24,
  },
  decimalsCardBalance: {
    color: props.color,
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 20,
  },
  containerUpdatedDate: {
    justifyContent: 'flex-end',
  },
  buttonUpdatedDate: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: 40,
  },
  textUpdatedDate: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 12,
    color: props.color,
  },
  iconUpdatedDate: {
    width: 19,
    height: 23,
    marginLeft: 5,
    marginRight: 10,
  }
});

export default CardBanckAccounts;
