import React from 'react';
import { Text } from 'react-native';

export interface Props {
  color: string;
}

const Dash: React.FC<Props> = (props: any) => {
  return (
    <Text
      style={{color: props.color, marginTop: -2}}
      ellipsizeMode="clip"
      numberOfLines={1}>
      – – – – – – – – – – – – – – – – – – – – – – – – – – – – – – – – – – – – –
      – – – – – – – – – – – – – – – – –{' '}
    </Text>
  );
};

export default Dash;
