import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    View,
} from 'react-native';
import Convert from '../../../utils/convert';

export interface Props {
  label: string;
  number: number;
  src: string;
  more: Function;
  less: Function;
}

const BudgetCategory: React.FC<Props> = (props) => {
  let sizeNumber = props.number.toString()
  return (
    <>
      <View style={styles(props, sizeNumber).buttonStyle} >
        <View style={styles(props, sizeNumber).iconsCategory}>
          <Image style={styles(props, sizeNumber).iconButtonStyleCategory} source={{uri: props.src}} resizeMode='contain'></Image>
          <Text style={styles(props, sizeNumber).textButtonStyle}>{props.label}</Text>
        </View>
        <View style={styles(props, sizeNumber).iconsView}>
          <TouchableOpacity style={styles(props, sizeNumber).containerMoreLess} onPress={() => props.less(props.number, props.label)}>
            <Text style={styles(props, sizeNumber).textNumber}>-</Text>
          </TouchableOpacity>
          <View >
            <Text style={styles(props, sizeNumber).textPrice}>$ {Convert(props.number)}</Text>
          </View>
          <TouchableOpacity style={styles(props, sizeNumber).containerMoreLess} onPress={() => props.more(props.number, props.label)}>
            <Text style={styles(props, sizeNumber).textNumber}>+</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles= (props: any, sizeNumber: any) => StyleSheet.create({
  buttonStyle: {
    width: '100%',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    alignSelf: 'stretch',
    paddingHorizontal: 5,
    paddingVertical: 15,
  },
  iconsCategory: {
    width: '50%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    alignSelf: 'stretch',
    marginLeft: 5
  },
  iconsView: {
    width: '40%',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  containerMoreLess: {
    backgroundColor: '#7b3eff',
    borderRadius: 50,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Bold',
    textTransform: 'uppercase',
    fontSize: 15,
    color: '#5a6175',
    paddingLeft: 15,
  },
  textNumber: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 20,
    color: '#ffffff',
    marginHorizontal: 10
  },
  textPrice: {
    fontFamily: 'Baloo2-Medium',
    fontSize: sizeNumber.length > 6 ? 13 : 16,
    color: '#5a6175',
    paddingHorizontal: 9,
  },
  iconButtonStyleCategory: {
    width: 15,
    height: 15,
    marginVertical: 3
  },
});

export default BudgetCategory;
