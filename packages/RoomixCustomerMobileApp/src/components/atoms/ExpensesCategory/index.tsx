//@ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
} from 'react-native';

export interface Props {
  category: string;
  transactions: string;
  expenses: any;
  percentage: any;
  background: any;
  src: string;
}

const ExpensesCategory: React.FC<Props> = (props) => {
  let result = (props.percentage.toString().includes('.') ? props.percentage.toFixed(2) : props.percentage)

  console.log(result)
  const examples = {
    background: props.background,
    size: result,
  }
  return (
    <>
      <View style={styles(props, examples).Container}>
        <View style={{backgroundColor: `${examples.background}1A`, position: 'absolute', width: `${examples.size}%`, marginLeft: 0, height: 60, borderRadius: 16}}>
          <Text style={{textAlign: 'left'}}/>
        </View>
        <View style={styles(props, examples).ContainerColor}>
          <View style={styles(props, examples).containerBackground}> 
            <View style={styles(props, examples).containerImgText}>
              <View style={styles(props, examples).containerImgAbsolute}>
                <Image style={styles(props, examples).iconButtonStyle} source={{uri: props.src}} resizeMode='contain'></Image>
              </View>
              <View style={{ width: '100%' , paddingLeft: 10, marginBottom: 5}}>
                <Text style={styles(props, examples).textCategory}>{props.category}</Text>
                <Text style={styles(props, examples).textTransaction}>{props.transactions} {props.transactions === 1 ? 'transacción': 'transacciones'}</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={{paddingLeft: 20}}>
          <Text style={styles(props, examples).textCategory}>${props.expenses !== undefined ? (props.expenses.toString().includes('.') ? props.expenses.toFixed(2) : props.expenses) : '' }</Text>
          <Text style={styles(props, examples).textPercentage}>{result}%</Text>
        </View>
      </View>
    </>
  );
};

const styles= (props: any, examples: any) => StyleSheet.create({
  ContainerColor : {
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderRadius: 18,
    width: '70%'
  },
  Container: {
    zIndex: 3,
    width: '90%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    marginHorizontal: 18,
  },
  containerBackground: {
    padding: 15 ,
    borderRadius: 16,
    zIndex: 2
  },
  containerImgAbsolute: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: `${examples.background}33`,
    padding: 10,
    borderRadius: 50
  },
  containerImgText: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
    marginHorizontal: 5
  },
  textCategory: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    color: '#5a6175',
    marginLeft: 20,
  },
  textTransaction: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 12,
    color: '#808080',
    marginLeft: 20,
    width: '100%',
    lineHeight: 13,
  },
  textPercentage: {
    fontFamily: 'AvenirLTStd-Roman',
    textAlign: 'right',
    fontSize: 12,
    color: '#808080',
    marginLeft: 0,
    lineHeight: 14,
  },
  iconButtonStyle: {
    width: 20,
    height: 20,
  },
});

export default ExpensesCategory;
