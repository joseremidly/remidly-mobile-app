import React from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    TouchableOpacity,
    Text,
    Image,
    View,
} from 'react-native';

export interface Props {
  onPress: Function;
  star: string,
  header: string;
  information: string;
  buttonName: string;
  type: any;
  typeFunc: Function;
  selected: any;
}

const DeliveryMethod: React.FC<Props> = (props: any) => {
  return (
    <TouchableOpacity onPress={() => props.typeFunc(props.type, props.selected)}>
      <View style={props.selected === true ? styles(props).containerCardSelected : styles(props).containerCard} >
        <View style={styles(props).containerDetailsBill}>
          <Image style={styles(props).imageStyle} source={{uri: props.star}}></Image>
        </View>
        <View style={styles(props).containerInformationCard}>
          <View style={styles(props).containerHeader}>
            <Text style={styles(props).textHeader}>{props.header}</Text>
          </View>
          <View style={styles(props).containerInformation}>
            <Text style={styles(props).textInformation}>
              {props.information}
            </Text>
            {props.type === 'flash' ?
              <Text style={styles(props).textFlash}>
                No disponible ahora
              </Text>
              :
              <>
              </>
            }
          </View>
        </View>
        <View style={{justifyContent: 'center', alignItems: 'center', marginBottom: 15}}>
          <View style={styles(props).buttonStyle}>
            <Text style={styles(props).textButtonStyle}>{props.buttonName}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles= (props: any) => StyleSheet.create({
  containerCard: {
    backgroundColor: 'white',
    borderRadius: 9,
    opacity: .3,
    width: '100%',
  },
  containerCardSelected: {
    backgroundColor: 'white',
    borderRadius: 9,
    opacity: 1,
    width: '100%',
  },
  imageStyle: {
    width: props.type === 'flash' ? 20 : 40,
    height: props.type === 'flash' ? 20 : 40,
    marginVertical: props.type === 'flash' ? 8 : 0,
  },
  containerDetailsBill: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
  },
  textLevel: {
    fontFamily: 'Baloo2-Regular',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    textAlign: 'center',
    color: '#5a6175',
    marginTop: 3,
    marginLeft: 3,
    fontSize: 12,
    lineHeight: 16,
    textTransform: 'uppercase',
  },
  containerInformationCard: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerInformation: {
    width: '60%',
    marginBottom: props.type === 'flash' ? 2 : 15
  },
  containerHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  textHeader: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 20,
    color: '#5a6175',
  },
  textInformation: {
    fontFamily: 'Baloo2-Medium',
    textAlign: 'center',
    color: '#5a6175',
    fontSize: 14,
    lineHeight: 16,
    textTransform: 'uppercase',
  },
  textFlash: {
    fontFamily: 'Baloo2-Medium',
    textAlign: 'center',
    color: '#7b3eff',
    fontSize: 9,
    textDecorationLine: 'underline',
  },
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '40%',
    backgroundColor: '#7b3eff',
    borderRadius: 6,
    margin: 10,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Bold',
    textAlign: 'left',
    fontSize: 10,
    textTransform: 'uppercase',
    color: 'white',
    margin: 4
  },
});

export default DeliveryMethod;