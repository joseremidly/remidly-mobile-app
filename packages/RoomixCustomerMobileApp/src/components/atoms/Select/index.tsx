import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
} from 'react-native';

export interface Props {
  defaultValue: string;
  src: string;
  onPress: Function;
}

const Select: React.FC<Props> = (props: any) => {
  const [value, setValue] = React.useState(props.defaultValue);

  return (
    <>
      <TouchableOpacity style={styles.selectStyle} onPress={props.onPress}>
        <Text style={styles.textSelectStyle}>{`+${value}`}</Text>
        <Image style={styles.iconSelectStyle} source={{uri: props.src}} resizeMode='contain'></Image>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  selectStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#7b3eff',
    borderRadius: 6,
    paddingTop: 3,
    paddingHorizontal: 8,
    height: 43,
    marginRight: 6,
  },
  textSelectStyle: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 16,
    color: '#ffffff',
  },
  iconSelectStyle: {
    width: 10,
    height: 8,
    marginLeft: 4,
  },
});

export default Select;
