import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
} from 'react-native';

export interface Props {
  label: string;
  secondary: boolean;
  onPress: Function;
}

const Button: React.FC<Props> = (props: any) => {
  return (
    <>
      <TouchableOpacity style={styles(props).buttonStyle} onPress={props.onPress}>
        <Text style={styles(props).textButtonStyle}>{props.label}</Text>
      </TouchableOpacity>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 360,
    height: 54,
    backgroundColor: props.secondary ? '#ffffff' : '#7b3eff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#7b3eff',
    borderRadius: 6,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 22,
    color: props.secondary ? '#7b3eff' : '#ffffff',
  },
});

export default Button;
