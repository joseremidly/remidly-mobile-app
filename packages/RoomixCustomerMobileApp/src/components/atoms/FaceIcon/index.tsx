import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Image,
} from 'react-native';

export interface Props {
  src: string;
}

const FaceIcon: React.FC<Props> = (props) => {
  return (
    <>
      <TouchableOpacity style={styles.faceIconStyle}>
        <Image style={styles.iconStyle} source={{uri: props.src}} resizeMode='contain'></Image>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  faceIconStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    height: '100%',
    borderRadius: 100,
  },
  iconStyle: {
    width: '100%',
    borderRadius: 100,
    alignSelf: 'stretch',
  },
});

export default FaceIcon;
