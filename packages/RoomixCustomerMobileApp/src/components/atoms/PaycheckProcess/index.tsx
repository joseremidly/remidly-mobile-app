import React from 'react';
import {
    StyleSheet,
    Text,
		View,
		Dimensions
} from 'react-native';

export interface Props {
  type: string;
	status: any;
	width: number;
}

const PaycheckTips: React.FC<Props> = (props) => {
	const {width, height} = Dimensions.get('window');
  return (
    <>
			<View style={{width: '100%'}}>
				<View style={styles(props).containerTop}>
					<View style={styles(props).containerKyc}>
					</View>
					<View>
						<Text style={styles(props).textLineKyc}>
							--------------------
						</Text>
					</View><View style={styles(props).containerBank}>
					</View>
					<View>
						<Text style={styles(props).textLineBank}>
							--------------------
						</Text>
					</View>
					<View style={styles(props).containerFinish}>
					</View>
				</View>
				<View style={styles(props).containerBottom}>
					<View style={{width: '32%', alignItems: 'center'}}>
						<Text style={styles(props).textKyc}>
							KYC
						</Text>
					</View>
					<View style={{width: '36%', alignItems: 'center'}}>
						<Text style={styles(props).textBank}>
							CUENTA BANCARIA
						</Text>
					</View>
					<View style={{width: '32%', alignItems: 'center'}}>
						<Text style={styles(props).textFinish}>
							FINALIZAR
						</Text>
					</View>
				</View>
			</View>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
	containerTop: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	containerBottom: {
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center'
	},
	containerKyc: {
    width: 15,
    height: 15,
    justifyContent: 'center',
    alignItems: 'center',
		backgroundColor: props.type === 'kyc' ? props.status === false ? '#FBB03B' : '#49c0aa' : '#49c0aa',
		borderRadius: 50
	},
	containerBank: {
    width: 15,
    height: 15,
    justifyContent: 'center',
    alignItems: 'center',
		backgroundColor: props.type === 'bank' ? props.status === false ? '#FBB03B' : '#49c0aa' : props.type === 'kyc' ? '#999999' : '#49c0aa' ,
		borderRadius: 50
	},
	containerFinish: {
    width: 15,
    height: 15,
    justifyContent: 'center',
    alignItems: 'center',
		backgroundColor: props.type === 'finish' ? '#FBB03B' : '#999999',
		borderRadius: 50
  },
  textLineKyc: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 16,
    color: props.type === 'kyc' ? '#999999' : '#49c0aa',
		textTransform: 'uppercase',
	},
  textLineBank: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 16,
    color: props.type === 'finish' ? '#49c0aa' : '#999999' ,
		textTransform: 'uppercase',
	},
  textKyc: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 14,
    color: props.type === 'kyc' ? props.status === false ? '#FBB03B' : '#49c0aa' : '#49c0aa',
		textTransform: 'uppercase',
    textAlign: 'center',
    marginTop: 5,
	},
	textBank: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 14,
    color: props.type === 'bank' ? props.status === false ? '#FBB03B' : '#49c0aa' : props.type === 'kyc' ? '#999999' : '#49c0aa',
		textTransform: 'uppercase',
    textAlign: 'center',
    marginTop: 5,
	},
	textFinish: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 14,
    color: props.type === 'finish' ? '#FBB03B' : '#999999',
		textTransform: 'uppercase',
    textAlign: 'center',
    marginTop: 5,
  },
});

export default PaycheckTips;
