import React from 'react';
import {
    StyleSheet,
    TextInput,
    Platform,
} from 'react-native';

export interface Props {
  placeholder: string;
  content: string;
  onChangeInput: Function;
  autoCapitalize: string;
  rfc: boolean;
}

const EditInputReusable: React.FC<Props> = (props: any) => {
  const [value, setValue] = React.useState(props.content);
  const onChangeData = (data: any) => {
    props.onChangeInput(data);
    setValue(data);
  }
  React.useEffect(() => {
  },[])
  return (
    <>
      <TextInput
        style={styles(props).inputStyle}
        placeholder={props.placeholder}
        onChangeText={text => onChangeData(text)}
        placeholderTextColor={props.content === '' || props.content === undefined ? 'red' : '#5a6175'}
        autoCapitalize={props.autoCapitalize === 'deactivate' ? 'none' : 'characters'}
        value={value}
      />
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  inputStyle: {
    fontFamily: 'AvenirLTStd-Roman',
    width: '95%',
    height: Platform.select({
      ios: 44,
      android: 44
    }),
    lineHeight: Platform.select({
      ios: 30,
    }),
    paddingHorizontal: Platform.select({
      ios: 10,
      android: 2
    }),
    fontSize: 16,
    paddingBottom: Platform.select({
      android:0
    }),
    borderBottomColor: props.content === '' || props.content === undefined ? 'red' : '#e6e6e6',
    borderBottomWidth: 1,
    textTransform: props.autoCapitalize === 'deactivate' ? 'lowercase' : 'uppercase',
    color: props.content === '' || props.content === undefined ? 'red' : '#5a6175',
  }
});

export default EditInputReusable;
