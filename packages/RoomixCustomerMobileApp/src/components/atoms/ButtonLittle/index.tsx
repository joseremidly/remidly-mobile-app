import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
} from 'react-native';

export interface Props {
  label: string;
  src: string;
}

const ButtonLittle: React.FC<Props> = (props) => {
  return (
    <>
      <TouchableOpacity style={styles.buttonStyle}>
        <Text style={styles.textButtonStyle}>{props.label}</Text>
        <Image style={styles.iconButtonStyle} source={{uri: props.src}}></Image>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    alignSelf: 'stretch',
    paddingHorizontal: 10,
    height: 32,
    backgroundColor: '#7b3eff',
    borderRadius: 20,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 14,
    textTransform: 'uppercase',
    color: '#ffffff',
  },
  iconButtonStyle: {
    width: 10,
    height: 10,
    marginLeft: 2,
  },
});

export default ButtonLittle;
