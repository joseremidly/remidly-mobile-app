import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
} from 'react-native';

export interface Props {
  label: string;
  src: string;
}

const ButtonIcon: React.FC<Props> = (props) => {
  return (
    <>
      <TouchableOpacity style={styles.buttonStyle}>
        <Text style={styles.textButtonStyle}>{props.label}</Text>
        <Image style={styles.iconButtonStyle} source={{uri: props.src}}></Image>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width: 200,
    height: 44,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#7b3eff',
    borderRadius: 19,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 20,
    color: '#7b3eff',
  },
  iconButtonStyle: {
    width: 12,
    height: 12,
    marginLeft: 10,
  },
});

export default ButtonIcon;
