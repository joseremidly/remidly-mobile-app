import React from 'react';
import {
    StyleSheet,
    TextInput,
    Platform,
} from 'react-native';

export interface Props {
  placeholder: string;
  onChangeInput: Function;
  transform: any;
}

const InputReusable: React.FC<Props> = (props: any) => {
  const [value, setValue] = React.useState('');
  const onChangeData = (data: any) => {
    props.onChangeInput(data);
    setValue(data);
  }
  return (
    <>
      <TextInput
        style={styles(props).inputStyle}
        placeholder={props.placeholder}
        onChangeText={text => onChangeData(text)}
        placeholderTextColor='#b3b3b3'
        autoCapitalize='none'
        value={value}
      />
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  inputStyle: {
    fontFamily: 'Baloo2-Regular',
    width: Platform.select({
      ios: 230,
      android: 230
    }),
    height: Platform.select({
      ios: 44,
      android: 44
    }),
    lineHeight: Platform.select({
      ios: 30,
    }),
    paddingHorizontal: Platform.select({
      ios: 25,
      android: 12
    }),
    fontSize: Platform.select({
      ios: 18,
      android: 18
    }),
    paddingBottom: Platform.select({
      android: 5
    }),
    backgroundColor: '#f1edfe',
    borderRadius: 6,
    color: '#5a6175',
    textTransform: props.transform ? props.transform : 'capitalize',
  }
});

export default InputReusable;
