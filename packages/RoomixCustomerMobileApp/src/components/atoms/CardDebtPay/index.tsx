// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  Image,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
export interface Props {
  icon: string;
  title: any;
  type: string;
  onPress: Function;
}

const CardDebtPay: React.FC<Props> = (props: any) => {
  
  return (
    <>
      <TouchableOpacity style={styles.containerCardIconTransaction} onPress={props.onPress}>
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
          <Image style={styles[props.type]} source={{ uri: props.icon }} resizeMode='contain'/>
          <Text style={styles.cardTitle}>{props.title}</Text>
        </View>
        <Image style={styles.arrow} source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833887/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_right_arrow_grey_i5kazc.png' }} resizeMode='contain'/>
      </TouchableOpacity>
    </>
  )
}

const styles = StyleSheet.create({
  containerCardIconTransaction: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    backgroundColor: 'white',
    marginVertical: 5,
    paddingVertical: 10,
    minHeight: 65,
    borderRadius: 6,

  },
  arrow: {
    width: 20,
    height: 12,
    marginHorizontal: 5,
  },
  oxxo: {
    width: 40,
    height: 18,
    marginHorizontal: 10,
  },
  app: {
    width: 40,
    height: 25,
    marginHorizontal: 10,
  },
  bank: {
    width: 40,
    height: 30,
    marginHorizontal: 10,
  },
  cardTitle: {
    fontFamily: 'Avenir-Heavy',
    textTransform: 'uppercase',
    fontSize: 12,
    color: '#5a6175',
  },
  
})

export default CardDebtPay;