import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    View,
} from 'react-native';
import Convert from '../../../utils/convert';

export interface Props {
  number: number;
  more: Function;
  less: Function;
}

const BudgetGoal: React.FC<Props> = (props) => {
  let sizeNumber = props.number.toString()
  return (
    <>
      <View style={styles(sizeNumber).buttonStyle} >
        <View style={styles(sizeNumber).iconsView}>
          <TouchableOpacity style={styles(sizeNumber).containerMoreLess} onPress={() => props.less(props.number)}>
            <Text style={styles(sizeNumber).textNumber}>-</Text>
          </TouchableOpacity>
          <View >
            <Text style={styles(sizeNumber).textPrice}>$ {Convert(props.number)}</Text>
          </View>
          <TouchableOpacity style={styles(sizeNumber).containerMoreLess} onPress={() => props.more(props.number)}>
            <Text style={styles(sizeNumber).textNumber}>+</Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  buttonStyle: {
    width: '100%',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    alignSelf: 'stretch',
    backgroundColor: 'white',
    borderRadius: 12,
    paddingVertical: 25
  },
  iconsView: {
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  containerMoreLess: {
    backgroundColor: '#7b3eff',
    borderRadius: 50,
  },
  textNumber: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 24,
    color: '#ffffff',
    marginVertical: 8,
    marginHorizontal: 20
  },
  textPrice: {
    fontFamily: 'Baloo2-Medium',
    fontSize: props.length > 6 ? 20 : 34,
    color: '#5a6175',
    marginHorizontal: 25
  },
});

export default BudgetGoal;
