// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
export interface Props {
  date: string;
}

const TransactionDate: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containerTransactionDate}>
        <Text style={styles.textTransactionDate}>{props.date}</Text>
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  containerTransactionDate: {
    paddingHorizontal: 5,
    borderRadius: 4,
    marginLeft: 5,
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    backgroundColor: '#7b3eff',
    marginTop: 20,
    marginBottom: 10,
  },
  textTransactionDate: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 13,
    color: '#ffffff',
    textTransform: 'uppercase',
  }
})

export default TransactionDate;