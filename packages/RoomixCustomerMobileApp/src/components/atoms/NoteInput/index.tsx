import React from 'react';
import {
    StyleSheet,
    TextInput,
    View,
} from 'react-native';

export interface Props {
  value: string;
  onChange: Function;
}

const Inputs: React.FC<Props> = (props: any) => {

  return (
    <View style={styles.shadowStyle}>
      <TextInput 
        style={styles.inputTextStyle}
        placeholder={props.value === '' ? 'ADD NOTE' : props.value} 
        onChangeText={(text) => props.onChange(text)}
        value={props.value}
        maxLength={120}
      >
      </TextInput>
    </View>
  );
};

const styles = StyleSheet.create({
  inputTextStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    backgroundColor: '#ffff',
    width: 250,
    height: 60,
    fontFamily: 'Baloo2-ExtraBold',
    alignSelf: 'stretch',
    color: '#5a6175',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: .3 },
    shadowOpacity:  0.1,
    shadowRadius: 3,
    elevation: 5,
  },
  shadowStyle: {
    overflow: 'hidden',
    paddingBottom: 5,
  },
});

export default Inputs;
