import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
} from 'react-native';

export interface Props {
  label: string;
  secondary: boolean;
  onPress: Function;
  src: string;
}

const ButtonBiometric: React.FC<Props> = (props: any) => {
  return (
    <>
      <TouchableOpacity style={styles(props).buttonBiometricStyle} onPress={props.onPress}>
        <Text style={styles(props).textBiometricStyle}>{props.label}</Text>
        <Image style={styles(props).iconBiometricStyle} source={{uri: props.src}}></Image>
      </TouchableOpacity>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  buttonBiometricStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 250,
    height: 46,
    backgroundColor: props.secondary ? '#e6e6e6' : '#7b3eff',
    borderRadius: 17,
  },
  textBiometricStyle: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 20,
    color: props.secondary ? '#999999' : '#ffffff',
  },
  iconBiometricStyle: {
    width: 22,
    height: 24,
    marginLeft: 10,
  }
});

export default ButtonBiometric;
