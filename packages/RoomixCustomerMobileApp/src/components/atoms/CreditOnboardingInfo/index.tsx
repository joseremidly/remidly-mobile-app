import React from 'react';
import { StyleSheet, Image, Text, View } from 'react-native';

export interface Props {
  text: string;
  text2: string;
}

const CreditOnboardingInfo: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.container}>
        <Image 
        style={styles.exclamationImage}
        source={{uri: "https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1610351895/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_icon_exclamation_new_rzx2i6.png"}}/>
        <Text style={styles.text}>{props.text}</Text>
      </View>
      {props.text2
        && (
          <View style={styles.container}>
            <Image 
            style={styles.exclamationImage}
            source={{uri: "https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1610351895/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_icon_exclamation_new_rzx2i6.png"}}/>
            <Text style={styles.text}>{props.text2}</Text>
          </View>
        )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f6fcfc',
    paddingHorizontal: 30,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  exclamationImage: {
    height: 25,
    width: 25,
    // paddingRight: 20,
    resizeMode: 'contain',
  },
  text: {
    paddingLeft: 20,
    color: '#5a6175'
  },
});

export default CreditOnboardingInfo;
