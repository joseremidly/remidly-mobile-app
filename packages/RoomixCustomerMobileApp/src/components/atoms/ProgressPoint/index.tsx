// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
} from 'react-native';

export interface Props {
  state: Boolean;
  onPress: Function;
}

const ProgressPoint: React.FC<Props> = (props: any) => {
  return (
    <View style={styles.containerCircle}>
      <View style={styles.outerCircle}>
        {props.state ? (
            <TouchableOpacity onPress={() => props.onPress(!props.state)}>
              <View style={styles.innerCircle} />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity onPress={() => props.onPress(!props.state)}>
              <View style={styles.innerCircle2} />
            </TouchableOpacity>
          )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerCircle: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  outerCircle: {
    borderRadius: 40,
    width: 20,
    height: 20,
    backgroundColor: "white",
  },
  innerCircle: {
    borderRadius: 35,
    width: 12,
    height: 12,
    backgroundColor: "#7b3eff"
  },
  innerCircle2: {
    borderRadius: 35,
    width: 12,
    height: 12,
    backgroundColor: "#e6e6e6"
  },
});

export default ProgressPoint;
