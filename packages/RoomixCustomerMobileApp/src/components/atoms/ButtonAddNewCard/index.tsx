import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Image,
} from 'react-native';

export interface Props {
  onPress: Function;
  src: string;
}

const ButtonAddNewCard: React.FC<Props> = (props: any) => {
  return (
    <>
      <TouchableOpacity style={styles.buttonAddNewCardStyle} onPress={props.onPress}>
        <Image style={styles.iconAddNewCard} source={{uri: props.src}}></Image>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  buttonAddNewCardStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 35,
    height: 80,
    marginLeft: 10,
    marginTop: 10,
    backgroundColor: '#e6e6e6',
    borderRadius: 6,
  },
  iconAddNewCard: {
    width: 10,
    height: 10,
  }
});

export default ButtonAddNewCard;
