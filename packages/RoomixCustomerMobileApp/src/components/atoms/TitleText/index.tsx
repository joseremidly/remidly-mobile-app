import React from 'react';
import {
    StyleSheet,
    Text,
} from 'react-native';

export interface Props {
  textPrimary: string;
  textSecondary: string;
  fontSize: number;
  lineHeight: number;
}

const TitleText: React.FC<Props> = (props) => {
  return (
    <>
      <Text style={styles(props).textPrimaryStyle}>{props.textPrimary}</Text>
      <Text style={styles(props).textSecondaryStyle}>{props.textSecondary}</Text>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  textPrimaryStyle: {
    flexDirection: 'row',
    fontFamily: 'Baloo2-ExtraBold',
    textAlign: 'center',
    alignSelf: 'stretch',
    color: '#5a6175',
    fontSize: props.fontSize,
    lineHeight: props.lineHeight,
  },
  textSecondaryStyle: {
    flexDirection: 'row',
    fontFamily: 'Baloo2-ExtraBold',
    textAlign: 'center',
    alignSelf: 'stretch',
    color: '#7b3eff',
    fontSize: props.fontSize,
    marginTop: 0,
    lineHeight: props.lineHeight,
  }
});

export default TitleText;
