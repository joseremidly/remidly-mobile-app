// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  Text,
} from 'react-native';
export interface Props {
  amount: number;
  expense: boolean;
}

const AmountTransaction: React.FC<Props> = (props: any) => {
  return (
    <>
      <Text style={styles(props).amountTransaction}>{props.amount ? `$${props.expense ? '-' : ''}${props.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}` : '$0.00'}</Text>
    </>
  )
}

const styles = (props: any) => StyleSheet.create({
  amountTransaction: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 14,
    color: props.expense ? '#5a6175' : '#34a853',
    marginLeft: 5,
    overflow: 'scroll',
  }
})

export default AmountTransaction;