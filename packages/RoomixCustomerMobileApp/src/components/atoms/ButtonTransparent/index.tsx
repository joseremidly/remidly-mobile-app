import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
} from 'react-native';

export interface Props {
  onPress: Function;
  label: string;
  fontSize: number;
  src: string;
}

const ButtonLittle: React.FC<Props> = (props: any) => {
  return (
    <>
      <TouchableOpacity style={styles(props).buttonStyle} onPress={props.onPress}>
        <Text style={styles(props).textButtonStyle}>{props.label}</Text>
        <Image style={styles(props).iconButtonStyle} source={{uri: props.src}}></Image>
      </TouchableOpacity>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Regular',
    fontSize: props.fontSize,
    textTransform: 'uppercase',
    color: '#999999',
  },
  iconButtonStyle: {
    width: 10,
    height: 10,
    marginLeft: 2,
  },
});

export default ButtonLittle;
