import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    View,
} from 'react-native';
import { ProgressCircle } from 'react-native-svg-charts';

export interface Props {
  type: boolean;
  label: string;
  number: number;
  src: string;
  onPress: Function;
}

const ButtonCheckup: React.FC<Props> = (props) => {
  return (
    <>
      <TouchableOpacity style={styles(props).buttonStyle} onPress={props.onPress} >
        {props.type === true ? (
          <View style={styles(props).iconsView}>
            <Image style={styles(props).iconButtonStyleAlert} source={{uri: props.src}} resizeMode='contain'></Image>
            <Text style={styles(props).textButtonStyle}>{props.label}</Text>
          </View>
        ) : (
          <View style={styles(props).iconsView}>
            <View style={{position: 'relative', marginRight: 5}}>
              <View style={{ zIndex: 0}}>
                <ProgressCircle style={{width: 40, height: 40}} strokeWidth={3} progress={(props.number/100)} progressColor={'rgb(134, 65, 244)'} />
              </View>
              <View style={styles(props).containerImgAbsolute}>
                <Image style={styles(props).iconButtonStyle} source={{uri: props.src}} resizeMode='contain'></Image>
              </View>
            </View>
            <Text style={styles(props).textButtonStyle}>{props.label}</Text>
        </View>
        )}
        <View style={styles(props).iconsView}>
          <View style={styles(props).containerTextNumber}>
            <Text style={styles(props).textNumber}>{props.type === true ? props.number : `${props.number}/100`}</Text>
          </View>
          <Image style={styles(props).arrowIconStyle} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599251160/app/back_1_bl4as0.png'}}></Image>
        </View>
      </TouchableOpacity>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  buttonStyle: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    alignSelf: 'stretch',
    paddingHorizontal: 5,
    backgroundColor: '#ffffff',
    borderRadius: 12,
    paddingVertical: 10,
  },
  containerImgAbsolute: {
    position: 'absolute',
    zIndex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  iconsView: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    alignSelf: 'stretch',
    paddingHorizontal: 10,
  },
  containerTextNumber: {
    backgroundColor: props.type === true ? '#FF8700' : '#7b3eff',
    borderRadius: props.type === true ? 50 : 6,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    textTransform: 'uppercase',
    fontSize: 16,
    color: '#5a6175',
    paddingLeft: 5,
  },
  textNumber: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 16,
    color: '#ffffff',
    paddingHorizontal: 9,
  },
  iconButtonStyle: {
    width: 30,
    height: 30,
  },
  iconButtonStyleAlert: {
    width: 30,
    height: 30,
    marginRight: 15,
    marginVertical: 3
  },
  arrowIconStyle: {
    width: 15,
    height: 15,
    marginLeft: 13,
    marginBottom: 1,
  },
});

export default ButtonCheckup;
