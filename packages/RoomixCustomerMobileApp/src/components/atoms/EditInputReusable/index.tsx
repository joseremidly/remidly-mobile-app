import React from 'react';
import {
    StyleSheet,
    TextInput,
    Platform,
} from 'react-native';

export interface Props {
  placeholder: string;
  content: string;
  onChangeInput: Function;
}

const EditInputReusable: React.FC<Props> = (props: any) => {
  const [value, setValue] = React.useState(props.content);
  const onChangeData = (data: any) => {
    props.onChangeInput(data);
    setValue(data);
  }
  React.useEffect(() => {
    console.log(props.content)
  },[])
  return (
    <>
      <TextInput
        style={styles.inputStyle}
        placeholder={props.placeholder}
        onChangeText={text => onChangeData(text)}
        placeholderTextColor='#999999'
        value={value}
      />
    </>
  );
};

const styles = StyleSheet.create({
  inputStyle: {
    fontFamily: 'Baloo2-Regular',
    width: '85%',
    height: Platform.select({
      ios: 44,
      android: 44
    }),
    lineHeight: Platform.select({
      ios: 30,
    }),
    paddingHorizontal: Platform.select({
      ios: 10,
      android: 12
    }),
    fontSize: Platform.select({
      ios: 18,
      android: 18
    }),
    paddingBottom: Platform.select({
      android: 5
    }),
    borderBottomColor: '#e6e6e6',
    borderBottomWidth: 1,
    color: '#5a6175',
  }
});

export default EditInputReusable;
