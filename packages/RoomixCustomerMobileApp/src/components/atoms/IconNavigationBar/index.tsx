import React from 'react';
import {
    StyleSheet,
    View,
    Image,
} from 'react-native';

export interface Props {
  src: string;
  onPress: Function;
  size: number;
  selected: boolean;
}

const IconNavigationBar: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={props.selected ? styles(props).iconNavigationStyleSelected : styles(props).iconNavigationStyle}>
        <Image style={styles(props).iconStyle} source={props.src} resizeMode='contain'></Image>
      </View>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  iconNavigationStyleSelected: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 70,
    height: 70,
    backgroundColor: '#7b3eff',
    borderColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 4,
    borderRadius: 30,
    marginBottom: 25,
  },
  iconNavigationStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 45,
    height: 45,
    backgroundColor: 'transparent',
    borderRadius: 28,
  },
  iconStyle: {
    width: props.size,
    height: props.size,
  },
});

export default IconNavigationBar;
