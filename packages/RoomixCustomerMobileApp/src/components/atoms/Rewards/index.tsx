import React from 'react';
import {
    StyleSheet, View, Text
} from 'react-native';

export interface Props {
  percentage: any;
  color: string;
}

const Rewards: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles(props).container}>
        <View style={styles(props).containerLevel}>
          <Text style={{textAlign: 'left'}}/>
        </View>
      </View>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  container : {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#e6e6e6',
    borderRadius: 18,
    width: '100%'
  },
  containerLevel: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: `${props.color}`,
    borderRadius: 18,
    width: `${props.percentage}%`
  }
});

export default Rewards;
