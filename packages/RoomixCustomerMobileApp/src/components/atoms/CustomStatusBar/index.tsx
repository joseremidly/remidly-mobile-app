// @ts-nocheck
import React from 'react';
import {
  View,
  StatusBar,
  Platform,
} from 'react-native';

export interface Props {
  backgroundColor: String;
  barStyle: String;
}

const CustomStatusBar: React.FC<Props> = (props: any) => {
  return (
    <View style={{ backgroundColor: props.backgroundColor, height: Platform.OS === 'ios' ? '6%' : 0 }}>
      <StatusBar backgroundColor={props.backgroundColor} barStyle={props.barStyle} />
    </View>
  );
};

export default CustomStatusBar;
