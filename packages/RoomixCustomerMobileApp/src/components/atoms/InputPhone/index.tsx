import React from 'react';
import {
    StyleSheet,
    TextInput,
    Platform,
    Keyboard,
} from 'react-native';

export interface Props {
  placeholder: string;
  onChangeInput: Function;
}

const InputPhone: React.FC<Props> = (props: any) => {
  const [value, setValue] = React.useState('');
  const cleanNumber = (number: any) => {
    return ('' + number).replace(/\D/g, '').replace('(', '').replace(')', '').replace('-', '')
  }
  const onChangeData = (data: any) => {
      let cleaned = ('' + data).replace(/\D/g, '');
      const match = cleaned.match(/^(1|)?(\d{2})(\d{4})(\d{4})$/);
      if (match) {
        let number = ['(', match[2], ') ', match[3], '-', match[4]].join('');
        setValue(number);
        Keyboard.dismiss();
        props.onChangeInput(cleanNumber(data));
        return;
      }
      props.onChangeInput(false);
      setValue(data);
  }
  return (
    <>
      <TextInput
        style={styles.inputStyle}
        placeholder={props.placeholder}
        onChangeText={text => onChangeData(text)}
        placeholderTextColor='#b3b3b3'
        keyboardType='number-pad'
        textContentType='telephoneNumber'
        maxLength={14}
        value={value}
        caretHidden={true}
      />
    </>
  );
};

const styles = StyleSheet.create({
  inputStyle: {
    fontFamily: 'Baloo2-Regular',
    width: Platform.select({
      ios: 230,
      android: 230
    }),
    height: Platform.select({
      ios: 44,
      android: 44
    }),
    lineHeight: Platform.select({
      ios: 30,
    }),
    paddingHorizontal: Platform.select({
      ios: 25,
      android: 12
    }),
    fontSize: Platform.select({
      ios: 18,
      android: 18
    }),
    paddingBottom: Platform.select({
      android: 5
    }),
    backgroundColor: '#EDF1F8',
    borderRadius: 6,
    color: '#5a6175',
    textTransform: 'capitalize',
  }
});

export default InputPhone;
