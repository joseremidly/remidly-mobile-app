// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  Image,
  View,
  Text,
} from 'react-native';

export interface Props {
  icon: string;
  description: string;
  category: string;
  width: string;
}

const DescriptionTransaction: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles(props).containerDescriptionTransaction}>
        <Text style={styles(props).textDescriptionTransaction}>{props.description.substring(0,18)}</Text>
        <View style={styles(props).containerTextDescriptionTransaction}>
          <Image style={styles(props).iconTransaction} source={{ uri: props.icon ? props.icon : '' }} resizeMode='contain'/>
          <Text style={styles(props).categoryDescriptionTransaction}>{props.category ? props.category : 'CATEGORÍA DESCONOCIDA'}</Text>
        </View>
      </View>
    </>
  )
}

const styles = (props: any) => StyleSheet.create({
  containerDescriptionTransaction: {
    alignItems: 'flex-start',
    width: props.width,
  },
  iconTransaction: {
    width: 15,
    height: 15,
  },
  containerTextDescriptionTransaction: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    width: '90%',
  },
  textDescriptionTransaction: {
    fontFamily: 'AvenirLTStd-Medium',
    fontWeight: 'bold',
    textTransform: 'capitalize',
    letterSpacing: .5,
    fontSize: 13,
    color: '#5a6175',
    lineHeight: 13,
  },
  categoryDescriptionTransaction: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 10,
    textTransform: 'uppercase',
    color: '#5a6175',
    marginLeft: 5,
  }
})

export default DescriptionTransaction;