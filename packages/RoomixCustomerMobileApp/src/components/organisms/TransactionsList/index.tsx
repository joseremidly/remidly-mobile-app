// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
} from 'react-native';
import TransactionItem from 'src/components/molecules/TransactionItem';
import TransactionsDate from 'src/components/atoms/TransactionsDate';

export interface Props {
  transactions: any;
}
const TransactionsList: React.FC<Props> = (props: any) => {
  const TransactionsLists = props.transactions.map((transaction, index) => {
    return (
      <View key={index}>
        <TransactionItem id={index} data={transaction}/>
      </View>
    )
  })
  return (
    <>
      <ScrollView style={styles.transactions}>
        <View style={styles.containerScrollTransactionsList}>
          {TransactionsLists}
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  transactions: {
    paddingHorizontal: 20,
    paddingTop: 0,
  },
  containerScrollTransactionsList: {
    paddingBottom: 450,
  }
});

export default TransactionsList;
