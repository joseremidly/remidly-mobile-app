// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
    TouchableOpacity
} from 'react-native';

export interface Props {
  values: any;
  status: string;
  loan: any;
  accountId: any;
  onPressDetails: Function;
}

const CreditLinePaymentDetailsExpired: React.FC<Props> = (props) => {
  const writeDate = (date: any) => {
    if (date !== undefined) {
      let example = date.split('T')
      example = example[0].split('-')
      return(`${example[2]}/${example[1]}/${example[0]}`)
    }
  }
  return (
    <>
      <View style={styles(props).container}>
        <View style={styles(props).containerReference}>
          <Text style={styles(props).textReference}># DE REFERENCIA: {props.loan.reference}</Text>
        </View>
        <View style={styles(props).containerDebtInformation}>
          <Text style={styles(props).textToPay}>CONCEPTO DE RETIRO:</Text>
          <Text style={styles(props).textDebtInformation}>RETIRO DE {props.loan.amount} MXN DE LA LINEA DE CRÉDITO</Text>
          <View style={styles(props).containerStatusPayment}>
            <Text style={styles(props).textStatus}>
              PAGO VENCIDO
            </Text>
          </View>
        </View>
        <View style={{width: '100%', flexDirection: 'row', alignItems:'center', justifyContent: 'space-between', marginVertical: 10}}>
          <View style={{width: '50%'}}>
            <Text style={styles(props).textInformationCuenta}>FECHA DEL RETIRO DE DINERO:</Text>
            <View style={{flexDirection: 'row', alignItems:'center', justifyContent: 'flex-start', marginVertical:3}}>
              <Image style={styles(props).imgPay} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609789908/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_arrow_velvet_f6hy2z.png'}} resizeMode='contain'></Image>
              <Text style={styles(props).textDepositoDescription}> {writeDate(props.loan.created_at)}</Text>
            </View>
          </View>
          <View style={{width: '50%', marginLeft: 15}}>
            <Text style={styles(props).textInformationDeposito}>FECHA LÍMITE DE PAGO:</Text>
            <View style={{flexDirection: 'row', alignItems:'center', justifyContent: 'flex-start', marginVertical:3}}>
              <Image style={styles(props).imgPay} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609789908/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_arrow_red_wtty6p.png'}} resizeMode='contain'></Image>
              <Text style={styles(props).textDepositoDescription}> {writeDate(props.loan.payment_due_date)}</Text>
            </View>
          </View>
        </View>
        <View style={styles(props).containerBaknsDeposito}>
          <View>
            <Text  style={styles(props).CrecyDeposito}>DEPOSITADO A:</Text>
            <Text  style={styles(props).CrecyDepositoCuenta}>Cuenta terminación</Text>
          </View>
          <View style={styles(props).containerBakns}>
            {/* <Image style={styles(props).imageBanks}  source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604538258/app/Crecy_app_logo_citi_banamex_white_high_resolution_fngo6f.png'}} resizeMode='contain'></Image> */}
            <Text style={styles(props).textButton}>{props.accountId === '' ? '......' : props.accountId.substring(props.accountId.length - 4)}</Text>
          </View>
        </View>
        <View style={{flexDirection:'row',width: '100%', marginBottom: 20}}>
          <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: -8}}>
            <View style={styles(props).containerCircle}>
              <View style={styles(props).outerCircle}>
                <View style={styles(props).innerCircle} />
              </View>
            </View>
            <Text style={styles(props).textLine}>- - - - - -</Text>
            <View style={styles(props).containerCircle}>
              <View style={styles(props).outerCircle}>
                <View style={styles(props).innerCircle} />
              </View>
            </View>
            <Text style={styles(props).textLine}>- - - - - -</Text>
            <View style={styles(props).containerCircle}>
              <View style={styles(props).outerCircle}>
                <View style={styles(props).innerCircle} />
              </View>
            </View>
            <Text style={styles(props).textLine}>- - - - - -</Text>
            <View style={styles(props).containerCircle}>
              <View style={styles(props).outerCircle}>
                <View style={styles(props).innerCircle} />
              </View>
            </View>
          </View>
          <View>
            <View style={{ marginTop: 0, paddingTop: 0}}>
              <Text style={styles(props).textMoneyHeader}>MONTO DEL RETIRO:</Text>
              <Text style={styles(props).textMoney}>${props.loan.amount}</Text>
            </View>

            <View style={{ marginVertical: 0, paddingTop: 15}}>
              <Text style={styles(props).textMoneyHeader}>MONTO DE INTERESES:</Text>
              <Text style={styles(props).textMoney}>${(props.loan.amount * props.loan.rate)}</Text>
            </View>

            <View style={{ marginVertical: 0, paddingTop: 15}}>
              <Text style={styles(props).textMoneyHeader}>MONTO DE INTERES MORATORIOS:</Text>
              <Text style={styles(props).textMoney}>${(props.loan.amount * props.loan.rate)}</Text>
            </View>

            <View style={{ paddingVertical: 0, paddingTop: 15}}>
              <Text style={styles(props).textMoneyHeader}>TOTAL A PAGAR:</Text>
              <Text style={styles(props).textMoney}>${props.loan.amount + (props.loan.amount * props.loan.rate) + (props.loan.amount * props.loan.rate)}</Text>
            </View>
            
          </View>
        </View>
        {props.status === 'rejected' ?
          <View style={{marginVertical: 15, paddingBottom: 15}}>
            <View style={styles(props).containerTime}>
              <Image style={styles(props).iconTime} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1616540287/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/Crecy_credit_line_expired_payment_hlhxnc.png'}}></Image>
              <Text style={styles(props).textDebtInformationTime}>TU TARJETA NO PASÓ</Text>
            </View>
            <View style={styles(props).containerButtonTransparent}>
              <Text style={styles(props).TextButtonTime}>
                Por favor, paga tu deuda manuelmente. Recuerda que con cada semana
                retraso a tu deuda se agregan los intereses moratorios 
              </Text>
            </View>
          </View>
          : 
          <View style={{marginVertical: 5, paddingBottom: 15}}>
            <View style={styles(props).containerTime}>
              <Image style={styles(props).iconTime} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1616540287/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/Crecy_credit_line_expired_payment_hlhxnc.png'}}></Image>
              <Text style={styles(props).textDebtInformationTime}>ES URGENTE PAGAR ESTA DEUDA</Text>
            </View>
            <View style={styles(props).containerButtonTransparent}>
              <Text style={styles(props).TextButtonTime}>
                En caso de no pagar tu deuda hoy antes de las 12 de la noche tendrás que pagar un interés moratorio de 1.42% por cada día de retraso. Todos los días de retraso se reportarán a buró de crédito
              </Text>
            </View>
          </View>
        }
        <TouchableOpacity style={styles(props).buttonPay} onPress={() => props.onPressDetails(props.loan)}>
          <Text style={styles(props).buttonPayText}>
            PAGAR
          </Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  container: {
    paddingHorizontal: 5,
    backgroundColor: '#ffffff',
    borderRadius: 12,
    paddingVertical: 5,
    paddingHorizontal: 20,
    
  },
  containerReference: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  textReference: {
    fontFamily: 'Avenir-Heavy',
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#ed1e79',
  },
  textReferenceId: {
    fontFamily: 'Avenir-Heavy',
    textTransform: 'uppercase',
    fontSize: 12,
    color: '#ed1e79',
  },
  containerDebtInformation: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginVertical: 30,
    width: '70%'
  },
  containerStatusPayment: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ed1e79',
    marginTop: 5,
    borderRadius: 3
  },
  textDebtInformation: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 16,
    color: '#5a6175',
  },
  textDebtInformationTime: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 11,
    color: '#ed1e79',
    lineHeight: 15,
    marginBottom: 10
  },
  textStatus: {
    fontFamily: 'AvenirLTStd-Medium',
    textTransform: 'uppercase',
    fontSize: 11,
    color: '#ffffff',
    marginHorizontal: 4,
    marginVertical: 3
  },
  containerAmountPayment: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: '25%',
    paddingBottom: 0,
    marginLeft: 40,
    marginVertical: 25
  },
  textToPay: {
    fontFamily: 'AvenirLTStd-Light',
    textTransform: 'uppercase',
    fontSize: 9,
    color: '#5a6175',
  },
  containerCredit: {
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    flexDirection: 'row'
  },
  CreditNumber: {
    fontFamily: 'AvenirLTStd-Medium',
    textTransform: 'uppercase',
    fontSize: 25,
    paddingTop: 10,
    color: '#5a6175',
  },
  CreditDecimal: {
    fontFamily: 'AvenirLTStd-Book',
    textTransform: 'uppercase',
    fontSize: 15,
    paddingTop: 10,
    paddingBottom: 3,
    color: '#5a6175',
  },
  iconTime: {
    width: 20,
    height: 20,
    marginRight: 5,
    marginBottom: 10
  },
  containerTime: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    margin: 10,
  },
  containerButtonTransparent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  TextButtonTime: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 12,
    color: '#5a6175',
    textAlign: 'center',
    paddingHorizontal: 25,
  },
  buttonPay: {
    height: 30,
    backgroundColor: '#ed1e79',
    borderRadius: 9,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 50,
    marginHorizontal: '30%'
  },
  buttonPayText: {
    fontFamily: 'Avenir-Heavy',
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#ffffff',
  },
  textInformationCuenta: {
    flexDirection: 'row',
    fontFamily: 'Avenir-Black',
    textAlign: 'left',
    color: '#5a6175',
    fontSize: 12,
  },
  imgPay: {
    width: 15,
    height: 15,
    marginVertical: 3
  },
  imgPayButton: {
    width: 25,
    height: 15,
    marginVertical: 3,
    transform: [{ rotate: "180deg" }]

  },
  textInformationDeposito: {
    width:'70%',
    flexDirection: 'row',
    fontFamily: 'Avenir-Black',
    textAlign: 'left',
    color: '#5a6175',
    fontSize: 12,
  },
  textDepositoDescription: {
    flexDirection: 'row',
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'left',
    color: '#5a6175',
    fontSize: 16,
    marginHorizontal: 0
  },
  containerCircle: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'white',
    marginVertical: 20,
    
  },
  outerCircle: {
    borderRadius: 40,
    width: 12,
    height: 12,
    backgroundColor: "#ed1e79",
  },
  innerCircle: {
    borderRadius: 35,
    width: 12,
    height: 12,
    backgroundColor: "#ed1e79"
  },
  textLine: {
    fontFamily: 'AvenirLTStd-Medium',
    color: '#ed1e79',
    fontSize: 14,
    transform: [{ rotate: "90deg" }]
  },
  textMoneyHeader: {
    flexDirection: 'row',
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'left',
    color: '#ed1e79',
    fontSize: 12,
    marginTop: 13
  },
  textMoney: {
    flexDirection: 'row',
    fontFamily: 'Avenir-black',
    textAlign: 'left',
    color: '#ed1e79',
    fontSize: 20,
    marginHorizontal: 0
  },
  CrecyDeposito: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 16,
    color: '#5a6175',
    textAlign: 'left',
  },
  CrecyDepositoCuenta: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 16,
    color: '#5a6175',
    textAlign: 'left',
    lineHeight: 20
  },
  containerBakns: {
    backgroundColor: '#49c0aa',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 8,
  },
  containerBaknsDeposito: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 8,
    width: '99%',
    marginVertical: 20,
  },
  textButton: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    color: '#ffffff',
    textAlign: 'center',
    textTransform: 'uppercase',
    marginVertical: 5,
    marginHorizontal: 15,
  },
  imageBanks: {
    width: 60,
    height: 40,
  },
});

export default CreditLinePaymentDetailsExpired;