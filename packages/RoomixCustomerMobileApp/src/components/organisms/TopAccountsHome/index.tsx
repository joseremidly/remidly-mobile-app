// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    View,
} from 'react-native';
import CardsScrollView from 'src/components/organisms/CardsScrollView';

export interface Props {
  navigation: Object;
  addAccount: Function; 
  accounts: Array;
  totalBalance: number;
}

const TopAccountsHome: React.FC<Props> = (props: any) => {
  return (
    <View style={styles(props).container}>
      <View style={styles(props).backgroundCover}></View>
      {props.accounts.length == 0
        ? (
          <>
            <View style={styles(props).customInfoHeader}>
              <View style={styles(props).containerBanks}>
                <View style={styles(props).circleOpacity}>
                  <Text></Text>
                </View>
                < Image style={styles(props).iconBankAccounts} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/f_auto,q_auto/v1611043169/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_connect_a_bank_velvet_background_uy2gs0.png'}}></Image>
              </View>
              <TouchableOpacity style={styles(props).containerInformation} onPress={props.addAccount}>
                <Image style={styles(props).iconPercentage} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/f_auto,q_auto/v1609783107/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_connect_a_bank_account_diagram_icon_white_ehb7ke.png'}} resizeMode='contain'>
                </Image>
                <Text style={styles(props).textInfoHeader}>
                  crea presupuestos y controla los gastos
                </Text>
                <Text style={styles(props).textInformation}>
                  de todas tus cuentas en un solo lugar
                </Text>
                <View style={styles(props).buttonConected}>
                  <Text style={styles(props).textButton}>
                    Conectar
                  </Text>
                  <Image style={styles(props).iconButton} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1605814313/app/Crecy%20icons%20new%20dimensions/crecy_icons_little_arrow_white_header_r6sjsz.png'}} resizeMode='contain'>
                  </Image>
                </View>
              </TouchableOpacity>
            </View>
          </>
        ) : (
          <CardsScrollView totalBalance={props.totalBalance} accounts={props.accounts} addAccount={() => props.navigation.navigate('BelvoLinkAccount')}/>
        )}
    </View>
  );
};

const styles = (props: any) => StyleSheet.create({
  container: {
    backgroundColor: props.accounts.length == 0 ? '#7b3eff' : '#F4F4F7',
    width: '100%',
  },
  backgroundCover: {
    position: 'absolute',
    top: -25,
    backgroundColor: props.accounts.length == 0 ? '#7b3eff' : '#F4F4F7',
    width: '100%',
    height: 40,
  },
  customInfoHeader: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    alignSelf: 'stretch',
    width: '100%',
    paddingVertical: 30
  },
  iconBankAccounts: {
    width: 110,
    height: 120,
  },
  iconPercentage: {
    width: 60,
    height: 25,
  },
  textInfoHeader: {
    width: '80%',
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 14,
    color: '#ffffff',
    textAlign: 'center',
    lineHeight: 16,
    paddingTop: 10,
    textTransform: 'uppercase'
  },
  textInformation: {
    width: '80%',
    fontFamily: 'Baloo2-Regular',
    fontSize: 14,
    color: '#ffffff',
    textAlign: 'center',
    lineHeight: 16,
    textTransform: 'uppercase',
  },
  containerBanks: {
    width: '35%',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  circleOpacity: {
    borderRadius: 70,
    backgroundColor: '#AA92EA59',
    position: 'absolute',
    width: 110,
    height: 110
  },
  containerInformation: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonConected: {
    borderRadius: 4,
    backgroundColor: '#49c0aa',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginVertical: 5,
    marginTop: 10
  },
  textButton: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 15,
    color: '#ffffff',
    textAlign: 'center',
    textTransform: 'uppercase'
  },
  iconButton: {
    width: 10,
    height: 10,
    marginLeft: 5
  },
});

export default TopAccountsHome;
