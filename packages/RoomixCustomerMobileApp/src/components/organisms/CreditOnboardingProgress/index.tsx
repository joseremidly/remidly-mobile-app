import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Dash from 'src/components/atoms/Dash';

export interface Props {
  step: string;
  jobType: string | null;
}

const CreditOnboardingProgress: React.FC<Props> = (props: any) => {
  let elements;
  let stepNames = [];

  if (props.step == 'jobType') {
    elements = 4;
    stepNames = ['Ocupación', 'Cuentas', 'Identidad', 'Final'];
  } else if (props.jobType == 'gigWorker') {
    elements = 5;
    stepNames = [
      'Ocupación',
      'Cuenta de plataforma',
      'Cuenta bancaria',
      'Identidad',
      'Final',
    ];
  } else if (props.jobType == 'formalWorker') {
    elements = 4;
    stepNames = ['Ocupación', 'Cuenta bancaria', 'Identidad', 'Final'];
  }

  // if (props.jobType == 'gigWorker') {
  //   elements = 5;
  //   stepNames = [
  //     'Ocupación',
  //     'Cuenta de plataforma',
  //     'Cuenta bancaria',
  //     'Identidad',
  //     'Final'
  //   ]
  // } else if (props.jobType == 'formalWorker') {
  //   elements = 4;
  //   stepNames = [
  //     'Ocupación',
  //     'Cuenta bancaria',
  //     'Identidad',
  //     'Final'
  //   ]
  // } else {
  //   elements = 4;
  //   stepNames = [
  //     'Ocupación',
  //     'Cuentas',
  //     'Identidad',
  //     'Final'
  //   ]
  // }

  let currentStep;

  // define progress depending on step
  switch (props.step) {
    case 'jobType':
      currentStep = 1;
      break;

    case 'platformAccount':
      currentStep = 2;
      break;

    case 'bankAccount':
      currentStep = props.jobType == 'gigWorker' ? 3 : 2;
      break;

    case 'id':
      currentStep = props.jobType == 'gigWorker' ? 4 : 3;
      break;

    case 'finish':
      currentStep = props.jobType == 'gigWorker' ? 5 : 4;
      break;
  }

  const progress = ((currentStep - 1) / (elements - 1)) * 100;

  const styleParams = { elements, progress };

  return (
    <>
      <View style={styles(styleParams).container}>
        {/* render progress' dashes */}
        <View style={styles(styleParams).progressContainter}>
          <View style={styles(styleParams).progressDone}>
            <Dash color="white" />
          </View>
          <View style={styles(styleParams).progressLeft}>
            <Dash color="#cab2ff" />
          </View>
        </View>

        {/* render steps' circles */}
        <View style={styles(styleParams).stepsContainer}>
          {[...Array(elements)].map((x, i) => (
            <View key={i} style={styles(styleParams).step}>
              <View style={styles(styleParams, i < currentStep).circle} />
            </View>
          ))}
        </View>

        {/* render steps' descriptions */}
        <View style={styles(styleParams).stepCaptionsContainer}>
          {stepNames.map((name, i) => (
            <View key={name} style={styles(styleParams).step}>
              <Text style={styles(styleParams, i < currentStep).text}>
                {name}
              </Text>
            </View>
          ))}
        </View>
      </View>
    </>
  );
};

const styles = (styleParams, active) =>
  StyleSheet.create({
    container: {
      backgroundColor: '#7b3eff',
      height: 90,
      alignItems: 'center',
      justifyContent: 'center',
      paddingHorizontal: 5,
    },
    progressContainter: {
      justifyContent: 'center',
      position: 'absolute',
      width: `${100 - 100 / styleParams.elements}%`,
    },
    progressDone: {
      position: 'absolute',
      justifyContent: 'center',
      width: `${styleParams.progress}%`,
    },
    progressLeft: {
      position: 'absolute',
      right: 0,
      width: `${100 - styleParams.progress}%`,
    },
    stepsContainer: {
      position: 'absolute',
      width: '100%',
      flexDirection: 'row',
    },
    step: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 10,
    },
    circle: {
      height: 12,
      width: 12,
      borderRadius: 6,
      backgroundColor: active ? 'white' : '#cab2ff',
    },
    stepCaptionsContainer: {
      position: 'absolute',
      paddingTop: 50,
      width: '100%',
      flexDirection: 'row',
    },
    text: {
      fontFamily: 'AvenirLTStd-Medium',
      textAlign: 'center',
      fontSize: 8,
      lineHeight: 10,
      color: active ? 'white' : '#cab2ff',
      textTransform: 'uppercase',
    },
  });

export default CreditOnboardingProgress;
