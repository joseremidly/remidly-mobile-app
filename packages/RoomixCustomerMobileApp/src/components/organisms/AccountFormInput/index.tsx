// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  ActivityIndicator,
} from 'react-native';
import TitleText from 'src/components/atoms/TitleText';
import InputReusable from 'src/components/atoms/InputReusable';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  onChangeAccount: Function;
  stateService: String;
}

const AccountFormInput: React.FC<Props> = (props: any) => {
  const [active, setActive] = React.useState(false);
  const [account, setAccount] = React.useState('');

  const onChangeInput = (account: any) => {
    setActive(account.length > 7 ? true : false )
    setAccount(account)
  }
  const changedAccount = () => {
    if (active) {
      props.onChangeAccount(account)
    }
  }
  return (
    <>
      <View style={styles.containerAccountFormInput}>
        <TitleText textPrimary='Por favor, ingresa' textSecondary='tu cuenta CLABE para depositarte' fontSize={22} lineHeight={29}/>
        <View style={styles.containerAccountForm}>
          <InputReusable placeholder='CLABE' onChangeInput={onChangeInput} transform='uppercase' />
        </View>
        {props.stateService === 'loading'
          && (
            <ActivityIndicator size='large' color='#7b3eff' animating={true} />
          )}
        {props.stateService !== 'loading'
          && (
            <ButtonMid label='Continuar' secondary={!active} onPress={changedAccount}></ButtonMid>
          )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerAccountFormInput: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 250,
    flex: 1,
    paddingHorizontal: 50,
  },
  containerAccountForm: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 60,
  },
});

export default AccountFormInput;
