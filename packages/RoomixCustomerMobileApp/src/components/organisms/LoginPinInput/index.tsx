// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Keyboard,
} from 'react-native';

import FaceIcon from 'src/components/atoms/FaceIcon';
import ButtonTransparent from 'src/components/atoms/ButtonTransparent';
import TitleText from 'src/components/atoms/TitleText';
import InputPin from 'src/components/molecules/InputPin';

export interface Props {
  onPinLogin: Function;
  onResetPin: Function;
}

const LoginPinInput: React.FC<Props> = (props: any) => {
  const getPin = (pin: number) => {
    props.onPinLogin(pin)
    Keyboard.dismiss()
  }
  return (
    <>
      <View style={styles.containerLoginFormInput}>
        <View style={styles.containerFaceIcon} >
          <FaceIcon src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599599220/app/crecy_panda_circle_profile_logo_ew0djz.png' ></FaceIcon>
        </View>
        <TitleText textPrimary='Hola, ¡Bienvenido!' textSecondary='Por favor, ingresa tu PIN' fontSize={30} lineHeight={39}/>
        <View style={styles.containerPinForm} >
          <InputPin onChangeInput={(pin) => getPin(pin)}/>
          <View style={styles.containerForgotPin} >
            <ButtonTransparent label='¿Olvidaste tu PIN?' fontSize={18} onPress={props.onResetPin}></ButtonTransparent>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerLoginFormInput: {
    alignItems: 'center',
  },
  containerFaceIcon: {
    width: 110,
    height: 110,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 40,
  },
  containerPinForm: {
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 100,
  },
  containerForgotPin: {
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 100,
  },
  iconInputForm: {
    width: 20,
    height: 20,
    margin: 10,
  }
});

export default LoginPinInput;
