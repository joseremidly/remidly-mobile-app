// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
		View,
		Dimensions,
} from 'react-native';
import Process from 'src/components/atoms/PaycheckProcess';
import Bank from 'src/components/molecules/PaycheckBank';

export interface Props {
  stateService: String;
	onPress: Function;
	type: string;
	status: any;
	accounts: any;
}

const PaycheckBank: React.FC<Props> = (props) => {
	const { width, height } = Dimensions.get('window');
  return (
    <>
      <View style={styles(height).principalContainer}>
				<View style={{justifyContent: 'center', alignItems: 'center',}}>
					<View style={styles(width).ContainerText} >
						<Text style={styles(props).textStyle}>(proceso de aplicación)</Text>
					</View>
					<View style={{marginVertical: 20}}>
						<Process
              type={props.type}
              status={props.status}
              width={width}
            />
          </View>
					<View style={styles(width).ContainerSecondParagraph} >
            <Bank 
              stateService={props.stateService}
              onPress={props.onPress}
              status={props.status}
							accounts={props.accounts}
						/>
					</View>
				</View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  principalContainer: {
		width: '100%',
    justifyContent: 'center',
		alignItems: 'center',
  },
  ContainerText: {
    justifyContent: 'center',
    alignItems: 'center',
		flexDirection: 'row',
		marginHorizontal: props <= 380 ? 30 : 50,
  },
  ContainerSecondParagraph: {
    justifyContent: 'center',
    alignItems: 'center',
		flexDirection: 'row',
		marginVertical: 20,
		marginHorizontal: 5
  },
  textGreenStyle: {
    fontFamily: 'Baloo2-Bold',
    textTransform: 'uppercase',
    fontSize: 22,
    color: '#49c0aa',
  },
  textMediumStyle: {
    fontFamily: 'Baloo2-Regular',
    textTransform: 'uppercase',
    fontSize: 22,
    color: '#5a6175',
  },
  textStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 16,
		color: '#5a6175',
		textAlign: 'center',
  },
  textMoreButton: {
    fontFamily: 'Baloo2-Bold',
    textTransform: 'uppercase',
    fontSize: 16,
    color: '#b3b3b3',
    marginVertical: 10,
    marginHorizontal: 16
  },
  containerButtonPress: {
    backgroundColor: '#E7E6FF',
    borderRadius: 50,
	},
	iconUpdatedDate: {
    width: 120,
		height: 120,
		marginVertical: props <= 680 ? 20 : 30,
	},
	containerButton: {
		justifyContent: 'center',
		alignItems: 'center',
		marginVertical: props <= 680 ? 30 : 50,
	},
	containerRequeriments: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  textRequeriments: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 15,
    color: '#5a6175',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  iconRequeriments: {
    width: 20,
    height: 20,
    marginRight: 10
  },
});

export default PaycheckBank;