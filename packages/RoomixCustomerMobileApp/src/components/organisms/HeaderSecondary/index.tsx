import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Platform,
  View,
  BackHandler,
} from 'react-native';
import {
  CommonActions,
} from '@react-navigation/native';
import { useNavigation } from '@react-navigation/native';

export interface Props {
  title: string;
  onHome: boolean;
  especificRoute: string;
  backgroundColor: string;
  uppercaseHeader: boolean;
  arrowColor: string;
  textColor: string;
}

const defaultProps: Props = {
  title: '',
  onHome: false,
  especificRoute: '',
  backgroundColor: '#f4f4f7',
  uppercaseHeader: false,
  arrowColor: 'grey',
  textColor: '#5a6175',
}

const HeaderSecondary: React.FC<Props> = (props: any) => {
  const navigation = useNavigation();
  let arrowImg = 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604027969/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_advances_arrow_grey_iqr36s.png';

  if (props.arrowColor === 'gray') {
    arrowImg = 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604027969/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_advances_arrow_grey_iqr36s.png';
  } else if (props.arrowColor === 'white') {
    arrowImg = 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599596734/app/crecy_icons_arrow_header_white-01_dg8lby.png';
  }
  const leftAction = () => {
    {{props.onHome ? navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          { name: 'Home' },
        ],
      })
    ) : (props.especificRoute !== ''
      ? (navigation.navigate(props.especificRoute)) : (navigation.goBack())
    )}}
  }
  const handleBackButtonClick = () => {
    leftAction()
    return true
  }
  React.useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);
  return (
    <>
      <View style={styles(props).customHeaderSecondary}>
        <TouchableOpacity onPress={leftAction} style={styles(props).containerIconButtonStyleLeft}>
          <Image style={styles(props).iconButtonStyleLeft} source={{uri: arrowImg}}></Image>
        </TouchableOpacity>
        <Text style={styles(props).textButtonStyle} adjustsFontSizeToFit>{props.title}</Text>
      </View>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  customHeaderSecondary: {
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: props.backgroundColor,
    width: '100%',
    height: Platform.select({
      ios: 50,
      android: 60
    }),
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 24,
    color: props.textColor,
    textTransform: props.uppercaseHeader ? 'uppercase' : 'none',
  },
  containerIconButtonStyleLeft: {
    position: 'absolute',
    left: 0,
    paddingLeft: 20,
    paddingTop: 20,
    paddingBottom: 20,
    paddingRight: 40
  },
  iconButtonStyleLeft: {
    width: props.arrowColor === 'gray' ? 23 : 30,
    height: props.arrowColor === 'gray' ? 23 : 30,
    resizeMode: 'contain'
  },
});

HeaderSecondary.defaultProps = defaultProps;

export default HeaderSecondary;
