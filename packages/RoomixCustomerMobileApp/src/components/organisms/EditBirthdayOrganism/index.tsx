// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
} from 'react-native';
import DateInput from 'src/components/atoms/DateInput';
import DateInputAndroid from 'src/components/atoms/DateInputAndroid';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  stateService: String;
  onChangeBirthday: Function;
}

const EditBirthdayOrganism: React.FC<Props> = (props: any) => {
  const [active, setActive] = React.useState(true);
  const [birthday, setBirthday] = React.useState('');
  
  const onChangeOption = (selectedDate: any) => {
    setBirthday(selectedDate);
  }
  const changedBirthday = () => {
    if (birthday) { 
      // setActive(!value)
      props.onChangeBirthday(birthday)
    }
  }
  return (
    <>
      <View style={styles.containerEditBirthdayOrganism}>
        <Text style={styles.textPrimaryEditBirthdayOrganism}>
          Te preguntaras por qué hacemos preguntas como
          <Text style={styles.textSecondaryEditBirthdayOrganism}> ¿Qué edad tienes? </Text>
        </Text>
        <View style={styles.containerEditBirthdayInput} >
          <Text style={styles.textInformationBirthday}>Usaremos esta información para personalizar tu experiencia y proveerte con datos más precisos y recomendaciones</Text>
          <View style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
            {Platform.OS  === 'android'
              ? (
                <DateInputAndroid onChangeInput={onChangeOption} placeholder='1239304' date={props.date}/>
              ) : (
                <DateInput onChangeInput={onChangeOption} placeholder='1239304' date={props.date}/>
              )}
          </View>
        </View>
        {props.stateService === 'loading'
          && (
            <ActivityIndicator size='large' color='#7b3eff' animating={true} />
          )}
        {props.stateService !== 'loading'
          && (
            <ButtonMid label='Guardar cambios' secondary={!active} onPress={changedBirthday}></ButtonMid>
          )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerEditBirthdayOrganism: {
    alignItems: 'center',
    paddingTop: 40,
  },
  textPrimaryEditBirthdayOrganism: {
    flexDirection: 'row',
    fontFamily: 'Baloo2-ExtraBold',
    textAlign: 'center',
    alignSelf: 'stretch',
    color: '#5a6175',
    fontSize: 28,
    lineHeight: 35,
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  textSecondaryEditBirthdayOrganism: {
    color: '#7b3eff',
  },
  containerEditBirthdayInput: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 80,
  },
  textInformationBirthday: {
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'center',
    letterSpacing: .5,
    fontSize: 22,
    color: '#999999',
    paddingHorizontal: 35,
    marginBottom: 20,
  }
});

export default EditBirthdayOrganism;

