// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ActivityIndicator,
} from 'react-native';

import ButtonMid from 'src/components/atoms/ButtonMid';
import FaceIcon from 'src/components/atoms/FaceIcon';
import InputPhone from 'src/components/atoms/InputPhone';
import Dropdown from 'src/components/molecules/Dropdown';

export interface Props {
  stateService: String;
  onSentCode: Function;
}

const PhoneFormInput: React.FC<Props> = (props: any) => {
  const [active, setActive] = React.useState(false);
  const [phone, setPhone] = React.useState('');

  const onChangeInput = (number: any) => {
    setActive(number && number !== '' ? true : false )
    setPhone(number)
  }
  const getCode = () => {
    if (active) {
      props.onSentCode(phone)
    }
  }
  return (
    <>
      <View style={styles.containerFormPhoneButtons}>
        <View style={styles.containerFaceIcon} >
          <FaceIcon src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599599220/app/crecy_panda_circle_profile_logo_ew0djz.png' ></FaceIcon>
        </View>
        <Text style={styles.textFormPhone}>
          Crear
          <Text style={styles.textSecondaryFormPhone}> nueva cuenta</Text>
        </Text>
        <Text style={styles.instructionsFormPhone}>Por favor, ingresa tu número de celular para poder verificar tu identidad.</Text>
        <View style={styles.containerNumberForm} >
          <Image style={styles.iconInputForm} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599597522/app/crecy_icons_phone_icon_black_z225ms.png'}}></Image>
          <Dropdown />
          <InputPhone placeholder='NÚMERO DE CELULAR' onChangeInput={onChangeInput} ></InputPhone>
        </View>
        {props.stateService === 'loading'
          && (
            <ActivityIndicator size='large' color='#7b3eff' animating={true} />
          )}
        {props.stateService !== 'loading'
          && (
            <ButtonMid label='Continuar' secondary={!active} onPress={getCode}></ButtonMid>
          )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerFormPhoneButtons: {
    alignItems: 'center',
  },
  containerFaceIcon: {
    width: 110,
    height: 110,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  textFormPhone: {
    flexDirection: 'row',
    fontFamily: 'Baloo2-ExtraBold',
    textAlign: 'center',
    alignSelf: 'stretch',
    color: '#5a6175',
    fontSize: 28,
  },
  textSecondaryFormPhone: {
    flexDirection: 'row',
    color: '#7b3eff',
    fontSize: 28,
  },
  instructionsFormPhone: {
    fontFamily: 'Baloo2-Regular',
    width: 300,
    fontSize: 16,
    lineHeight: 20,
    textAlign: 'center',
    color: '#5a6175',
    marginVertical: 30,
  },
  containerNumberForm: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 50,
  },
  iconInputForm: {
    width: 20,
    height: 20,
    margin: 10,
  }
});

export default PhoneFormInput;
