// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
} from 'react-native';

export interface Props {
  goToStart: Function;
}

const CreditLineLatePayment: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containerSecond}>
        <Text style={styles.headerText}>
          ¿Qué pasa
          <Text style={styles.colorText}> si no pago a tiempo</Text> o
          <Text style={styles.colorText}> no pago por completo mi deuda</Text>?
        </Text>
        <Text style={styles.informationHeaderText}>Intereses moratorios</Text>
        <Text style={styles.informationBenefitsText}>Deberás pagar una pena por mora por cada día de retraso. El monto de la mora/día lo puedes consultar en el documento llamado "Autorización expresa" que puedes solicitar en cualquier momento contactando a soporte cuando retires.</Text>
        <View style={styles.horizontalSeparator} />
        <Text style={styles.informationHeaderText}>Reporte a buró de crédito</Text>
        <Text style={styles.informationBenefitsText}>Vamos a reportar tu comportamiento negativo a Buró de Crédito. Eso va a afectar tu historial crediticio y como resultado para tí va a ser mucho más difícil conseguir un crédito o préstamo en un futuro con cualquier insitución que otorgue crédito.</Text>
        <View style={styles.horizontalSeparator} />
        <Text style={styles.informationHeaderText}>Suspensión de la cuenta</Text>
        <Text style={styles.informationBenefitsText}>Crecy puede suspender tu cuenta y el servicio de la línea de crédito o en el mejor de los casos cambiar las condiciones de la misma (va a aumentar la comisión por retiro y el plazo para pago de tus préstamos será menor).</Text>
      </View>
      <View style={styles.containerBottonText}>
        <Text style={styles.informationBottomText}>PARA CONSULTAR LA INFORMACIÓN COMPLETA, POR FAVOR REVISA EL CONTRATO DE CRÉDITO</Text>
      </View>
      {/* <TouchableOpacity style={styles.containerRequerimentos} onPress={props.onChangeInput}>
        <Image style={styles.iconRequerimientos} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833888/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_see_requirements_ipfr3h.png'}} resizeMode='contain'></Image>
        <Text style={styles.requerimientosText}>VER REQUERIMIENTOS</Text>
        <Image style={styles.iconRequerimientos} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833887/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_right_arrow_umqkdz.png'}} resizeMode='contain'></Image>
      </TouchableOpacity> */}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  containerSecond: {
    width: '100%',
    marginVertical: 10,
  },
  headerText: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 24,
    color: '#5a6175',
    marginBottom: 10,
    marginHorizontal: 30,
    textAlign: 'center',
    lineHeight: 31
  },
  colorText: {
    color: '#6133E2',
  },
  iconRequerimientos: {
    width: 15,
    height: 15,
  },
  containerRequerimentos: {
    justifyContent: 'center',
    alignItems: 'center', 
    flexDirection: 'row',
    marginVertical: 30,
  },
  requerimientosText: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 16,
    color: '#6133E2',
    marginHorizontal: 5,
    textAlign: 'center',
  },
  informationHeaderText: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 18,
    color: '#5a6175',
    marginHorizontal: 30,
    textAlign: 'left',
    marginTop: 10
  },
  containerBottonText: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  informationBottomText: {
    fontFamily: 'Avenir-Black',
    fontSize: 11,
    color: '#5a6175',
    textAlign: 'center',
    marginTop: 30
  },
  informationBenefitsText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    color: '#5a6175',
    marginHorizontal: 30,
    textAlign: 'left',
    lineHeight: 18
  },
  horizontalSeparator: {
    marginVertical: 5,
    alignItems: 'center',
    width: '84%',
    marginHorizontal: '8%',
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#e6e6e6',
  },
});

export default CreditLineLatePayment;
