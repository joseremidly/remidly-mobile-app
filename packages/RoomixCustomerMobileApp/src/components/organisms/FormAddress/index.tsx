// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import InputReusable from 'src/components/atoms/EditInputKycInformation';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  stateService: string;
  neighborhood: string;
  street: string;
  zipCode: string;
  province: string;
  city: string;
  OnChangeNeighborhood: Function;
  OnChangeStreet: Function;
  OnChangeZipCode: Function;
  OnChangeProvince: Function;
  OnChangeCity: Function;
  SendInformation: Function;
}

const FormAddressOrganisms: React.FC<Props> = (props: any) => {

  return (
    <View>
      <View style={{ justifyContent: 'center', alignItems: 'center', marginVertical: 0 }}>
        <Text style={styles.textRecomendation}>POR FAVOR, REVISA QUE LA DIRECCIÓN QUE EXTRAJIMOS DE TU DOCUMENTO OFICIAL ES CORRECTA:</Text>
      </View>
      <View style={{width: '79%',display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 35, marginHorizontal: 0}}>
        <View style={styles.containerNumber}>
          <Text style={styles.textVerificationNumberInformation}>1</Text>
        </View>
        <View>
          <Text style={styles.textVerificationInformationHeavy}>
            Si los datos de algún campo son incorrectos o incompletos, por favor, modifícalos
          </Text>
        </View>
      </View>
      <View style={{width: '89%',display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 15, marginHorizontal: 0}}>
        <View style={styles.containerNumber}>
          <Text style={styles.textVerificationNumberInformation}>2</Text>
        </View>
        <View>
          <Text style={styles.textVerificationInformationHeavy}>
            Recuerda que proporcionar los datos incompletos o incorrectos alarga el proceso de verificación
          </Text>
        </View>
      </View>
      <View style={styles.containerForm}>
        <Text style={styles.textFormInitial}>Calle, número exterior y número interior:</Text>
        <InputReusable
          placeholder=''
          content={props.street}
          onChangeInput={props.OnChangeStreet}
        />
        <Text style={styles.textForm}>Colonia:</Text>
        <InputReusable
          placeholder=''
          content={props.neighborhood}
          onChangeInput={props.OnChangeNeighborhood}
        />
        <Text style={styles.textForm}>Delegación o Municipio:</Text>
        <InputReusable
          placeholder=''
          content={props.province}
          onChangeInput={props.OnChangeProvince}
        />
        <Text style={styles.textForm}>Estado:</Text>
        <InputReusable
          placeholder=''
          content={props.city}
          onChangeInput={props.OnChangeCity}
        />
        <Text style={styles.textForm}>Código postal:</Text>
        <InputReusable
          placeholder=''
          content={props.zipCode}
          onChangeInput={props.OnChangeZipCode}
        />
      </View>
      {props.stateService === 'loading'
        && (
          <View style={styles.buttonContainer}>
            <ActivityIndicator size='large' color='#7b3eff' animating={true} />
          </View>
        )}
      {props.stateService !== 'loading'
        && (
          <View style={styles.buttonContainer}>
            <ButtonMid
              label='Continuar'
              secondary={!(props.street !== '' && props.neighborhood !== '' && props.province !== '' && props.city !== '' && props.zipCode !== '')}
              onPress={(props.street !== '' && props.neighborhood !== '' && props.province !== '' && props.city !== '' && props.zipCode !== '') ? props.SendInformation : () => {}}
            />
          </View>
        )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 40,
  },
  containerForm: {
    alignItems: 'flex-start',
    backgroundColor: 'white',
    marginVertical: 30,
    marginHorizontal: 10,
    width: '100%'
  },
  formInput: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    borderBottomColor: '#f1f1f1',
    borderBottomWidth: 2,
    height: 40,
    minWidth: '80%',
    fontFamily: 'AvenirLTStd-Light',
    color: '#b3b3b3',
  },
  textVerificationInformationHeavy: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    color: '#5a6175',
    textAlign: 'left',
    lineHeight: 15,
    paddingTop: 5
  },
  textVerificationNumberInformation: {
    fontFamily: 'Avenir-Black',
    fontSize: 12,
    color: 'white',
    lineHeight: 15,
    borderRadius: 50,
    marginHorizontal: 6,
    marginVertical: 2,
  },
  containerNumber: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#7b3eff',
    borderRadius: 12,
    marginRight: 15
  },
  textRecomendation: {
    fontFamily: 'Avenir-Black',
    fontSize: 16,
    color: '#5a6175',
    textAlign: 'center',
    width: '90%'
  },
  textForm: {
    fontFamily: 'AvenirLTStd-Medium',
    color: '#b3b3b3',
    textAlign: 'left',
    marginBottom: -15,
    marginTop: 20,
  },
  textFormInitial: {
    fontFamily: 'AvenirLTStd-Medium',
    color: '#b3b3b3',
    textAlign: 'left',
    marginBottom: -15,
  },
  buttonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 50,
  },
});

export default FormAddressOrganisms;
