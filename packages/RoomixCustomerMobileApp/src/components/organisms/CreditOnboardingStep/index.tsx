// @ts-nocheck
import React from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import CreditOnBoardingJobtype from 'src/components/organisms/CreditOnboardingJobtype';
import CreditOnboardingAccount from 'src/components/organisms/CreditOnboardingAccount';
import CreditOnboardingGig from 'src/components/organisms/CreditOnboardingGig';
import CreditOnboardingID from 'src/components/organisms/CreditOnboardingID';
import CreditOnboardingFinish from 'src/components/organisms/CreditOnboardingFinish';
import CreditOnboardingError from 'src/components/molecules/CreditOnboardingError';


export interface Props {
  showError: boolean;
  step: string;
  jobType: string;
  error: string;
  setStep: Function;
  setJobType: Function;
}

const CreditOnboardingStep: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.container}>
        <ScrollView style={styles.scrollContainer}>
          {/* show error component where there was an error */}
          {props.error && <CreditOnboardingError />}
          {props.step == 'jobType' && <CreditOnBoardingJobtype {...props}/>}
          {props.step == 'bankAccount' && <CreditOnboardingAccount {...props}/>}
          {props.step == 'platformAccount' && <CreditOnboardingGig {...props}/>}
          {props.step == 'id' && <CreditOnboardingID {...props}/>}
          {props.step == 'finish' && <CreditOnboardingFinish {...props}/>}
        </ScrollView>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#7b3eff",
    justifyContent: 'center',
    flex: 1,
  },
  scrollContainer: {
    paddingTop: 20,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    backgroundColor: 'white',
  }
});

export default CreditOnboardingStep;
