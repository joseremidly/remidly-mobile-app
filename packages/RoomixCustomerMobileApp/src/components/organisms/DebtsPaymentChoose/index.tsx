// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';
import DebtsPayment from 'src/components/molecules/DebtsPayment'
import DebtsCard from 'src/components/atoms/CardDebtPay'

export interface Props {
  loans: any;
  onPressApp: Function;
  onPressBranch: Function;
  onPressOxxo: Function;
  goToLatePayment: Function;
}

const DebtsPaymentChoose: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles(props).container}>
        <DebtsPayment
          loans={props.loans}
          goToLatePayment={props.goToLatePayment}
        />
      </View>
      <Text style={styles(props).textDebtInformation}>Elige como prefieres pagar tu deuda:</Text>
      <DebtsCard
        title='EN UNA TIENDA OXXO'
        icon='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1616149088/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/Crecy_credit_line_method_of_payment_oxxo_iybgp6.png'
        type='oxxo'
        onPress={props.onPressOxxo}
      />
      <DebtsCard
        title='EN UNA SUCURSAL BANCARIA'
        icon='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609783344/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_manual_repayment_bank_icon_wbj5kc.png'
        type='bank'
        onPress={props.onPressBranch}
      />
      <DebtsCard
        title='DESDE LA APP DE TU BANCO'
        icon='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1616149088/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/Crecy_credit_line_method_of_payment_mobile_banking_vb1own.png'
        type='app'
        onPress={props.onPressApp}
      />
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 12,
    
  },

  textDebtInformation: {
    fontFamily: 'Avenir-Black',
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#5a6175',
    lineHeight: 15,
    marginTop: 30,
    marginBottom: 10,
  },
  
});

export default DebtsPaymentChoose;
