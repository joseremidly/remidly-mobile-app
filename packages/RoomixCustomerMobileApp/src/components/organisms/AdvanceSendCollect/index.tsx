import React from 'react';

import { StyleSheet, View, Text } from 'react-native';

import AdvanceCardSelection from 'src/components/molecules/AdvanceCardSelection';

export interface Props {
  cardType: string;
  availableAccounts: Object | null;
  selectedAccount: Object | null;
  selectAccount: Function;
}

const AdvanceSendCollect: React.FC<Props> = (props: any) => {
  const cardTypeText = props.cardType === 'deposit' ? 'mandar a' : 'cobrar de';

  return (
    <>
      <View style={styles.containter}>
        <Text style={styles.cardTypeText}>{cardTypeText}</Text>
        <AdvanceCardSelection
          disabled={props.cardType === 'deposit'}
          availableAccounts={props.availableAccounts}
          selectedAccount={props.selectedAccount}
          selectAccount={(newAccount) => props.selectAccount(newAccount)}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containter: {
    marginTop: 20,
    marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  cardTypeText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    color: '#5a6175',
    textTransform: 'uppercase',
  },
});

export default AdvanceSendCollect;
