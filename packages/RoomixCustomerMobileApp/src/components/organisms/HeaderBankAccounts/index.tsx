import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    Platform,
    View,
} from 'react-native';

export interface Props {
	navigation: Object;
	edit: Function;
	headerNameBank: String;
	colorBankCard: any;
}

const Header: React.FC<Props> = (props: any) => {
  const leftAction = () => {
    {{props.navigation.goBack()}}
  }
  const rightAction = () => {
    {props.navigation.goSettigns()};
  }
  return (
    <>
      <View style={styles(props).customHeader}>
        <TouchableOpacity onPress={leftAction}>
          <Image style={styles(props).iconButtonStyleLeft} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604027969/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_advances_arrow_grey_iqr36s.png' }}></Image>
        </TouchableOpacity>
        <Text style={styles(props).textButtonStyle}>{props.headerNameBank}</Text>
        <TouchableOpacity onPress={rightAction}>
          <Image style={styles(props).iconButtonStyleRight} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604997219/app/Crecy%20icons%20new%20dimensions/Crecy_app_icons_edit_adjustment_header_settings_rlkjml.png'}}></Image>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  customHeader: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    alignSelf: 'stretch',
    backgroundColor: '#e6e6e6',
    width: '100%',
    borderBottomLeftRadius: 26,
    borderBottomRightRadius: 26,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: '#5a6175',
  },
  textButtonHeader: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 20,
    color: '#5a6175',
  },
  iconButtonStyleLeft: {
    width: 25,
    height: 20,
    marginLeft: 20,
  },
  iconButtonStyleRight: {
    width: 25,
    height: 25,
    marginRight: 20,
  },
});

export default Header;
