import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    Platform,
		View,
		Dimensions
} from 'react-native';

export interface Props {
  navigation: Object;
	home: boolean;
}

const HeaderPaycheckDetails: React.FC<Props> = (props: any) => {
	const {height} = Dimensions.get('window');
  const leftAction = () => {
    {{props.home ? props.navigation.navigate('Home') : props.navigation.goBack()}}
	}
	let dimensionHeight = height/4;
  return (
    <>
      <View style={styles(dimensionHeight).customHeader}>
        <TouchableOpacity onPress={leftAction}>
          <Image style={styles(dimensionHeight).iconButtonStyleLeft} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604027969/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_advances_arrow_grey_iqr36s.png' }}></Image>
        </TouchableOpacity>
        <Text style={styles(dimensionHeight).textButtonStyle}>Paycheck Advance</Text>
      </View>
    </>
  );
};

const styles = (dimensionHeight: any) => StyleSheet.create({
  customHeader: {
		position: 'absolute',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
		alignSelf: 'stretch',
    backgroundColor: '#e6e6e6',
    width: '100%',
		marginVertical: 5
  },
  textButtonStyle: {
    width: '70%',
    textAlign: 'center',
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: '#5a6175',
    marginLeft: 15,
  },
  iconButtonStyleLeft: {
    width: 30,
    height: 20,
		marginLeft: 10,
		marginTop: 10,
  },
});

export default HeaderPaycheckDetails;