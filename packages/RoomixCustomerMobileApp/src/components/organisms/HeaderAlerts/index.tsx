import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    Platform,
    View,
} from 'react-native';


const HeaderAlerts: React.FC = () => {
 
  return (
    <>
      <View style={styles.customHeaderSecondary}>
        <Image style={styles.iconButtonStyleLeft} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1617655793/app/Crecy%20icons%20new%20dimensions/Crecy_credit_app_alerts_icon_m6tqm9.png'}}></Image>
        <Text style={styles.textButtonStyle}>Alertas</Text>
      </View>
    </>
  );
};

const styles  = StyleSheet.create({
  customHeaderSecondary: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#7b3eff',
    width: '100%',
    height: Platform.select({
      ios: 90,
      android: 110
    }),
    borderRadius: 24,
    marginTop: -20,
    paddingTop: 20
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: 'white',
  },
  containerIconButtonStyleLeft: {
    position: 'absolute',
    left: 20,
  },
  iconButtonStyleLeft: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    marginRight: 5
  },
});

export default HeaderAlerts;
