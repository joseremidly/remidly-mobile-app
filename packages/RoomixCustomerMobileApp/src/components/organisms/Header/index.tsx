// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    View,
    BackHandler,
    Alert,
    Linking,
} from 'react-native';

import {
  CommonActions,
} from '@react-navigation/native';
import { useNavigation, useRoute } from '@react-navigation/native';

export interface Props {
  onAuth: boolean;
}

const Header: React.FC<Props> = (props: any) => {
  const navigation = useNavigation();
  const route = useRoute();

  const leftAction = () => {
    {{route.name === 'LoginPinForm' ? navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          { name: 'Login' },
        ],
      })
    ) : (
      (props.onAuth)
        ? (
          handleBackButtonClick()
        ) : (
          navigation.goBack()
        )
    )}}
  }
  const handleBackButtonClick = () => {
    Alert.alert('Cerrar sesión', '¿Desea salir de Crecy App?', [
      {
        text: 'Cancelar',
        onPress: () => null,
        style: "cancel"
      },
      { text: 'Aceptar', onPress: () => BackHandler.exitApp() }
    ]);
    return true
  }
  const rightAction = () => {
    let url = "";
    Linking.openURL(url)
      .then(data => {
        console.log("WhatsApp Opened successfully " + data);  //<---Success
      })
      .catch(() => {
        Alert.alert("Debes tener WhatsApp instalado en tu dispositivo");  //<---Error
      });
  }
  React.useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);
  return (
    <View style={styles(props).customHeader}>
      <TouchableOpacity onPress={leftAction} style={styles(props).containerButtomLeft}>
        <Image style={styles(props).iconButtonStyleLeft} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599596734/app/crecy_icons_arrow_header_white-01_dg8lby.png'}}></Image>
      </TouchableOpacity>
      <Text style={styles(props).textButtonStyle}>Crecy.</Text>
      <TouchableOpacity onPress={rightAction} style={styles(props).containerButtomRight}>
        <Image style={styles(props).iconButtonStyleRight} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599596733/app/crecy_icons_chat_button_white_header-01_dtswcl.png'}}></Image>
      </TouchableOpacity>
    </View>
  );
};

const styles = (props: any) => StyleSheet.create({
  customHeader: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    alignSelf: 'stretch',
    backgroundColor: '#7b3eff',
    width: '100%',
    borderBottomLeftRadius: 26,
    borderBottomRightRadius: 26,
    zIndex: 2,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: '#ffffff',
  },
  containerButtomLeft: {
    position: 'absolute',
    left: 0,
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
  },
  containerButtomRight: {
    position: 'absolute',
    right: 0,
    paddingRight: 15,
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 20,
  },
  iconButtonStyleLeft: {
    width: props.home ? 35 : 25,
    height: props.home ? 35 : 25,
  },
  iconButtonStyleRight: {
    width: 27,
    height: 27,
  },
});

export default Header;
