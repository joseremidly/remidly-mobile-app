// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  ActivityIndicator,
} from 'react-native';
import EditInputReusable from 'src/components/atoms/EditInputReusable';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  onChangeField: Function;
  stateService: String;
  placeholder: String;
  content: String;
}

const EditFieldOrganism: React.FC<Props> = (props: any) => {
  const [active, setActive] = React.useState(false);
  const [field, setField] = React.useState('');

  const onChangeInput = (field: any) => {
    setActive(field && field !== '' ? true : false )
    if (field.length >= 2) {
      setField(field)
    }
  }
  const changedField = () => {
    if (active) {
      props.onChangeField(field)
    }
  }
  React.useEffect(() => {
    if (props.field !== '') {
      setField(props.content)
    }
  },[])
  return (
    <>
      <View style={styles.containerEditField}>
        <View style={styles.containerEditFieldInput} >
          <EditInputReusable placeholder={props.placeholder} onChangeInput={onChangeInput} content={props.content} />
        </View>
        {props.stateService === 'loading'
          && (
            <ActivityIndicator size='large' color='#7b3eff' animating={true} />
          )}
        {props.stateService !== 'loading'
          && (
            <ButtonMid label='Guardar cambios ' secondary={!active} onPress={changedField}></ButtonMid>
          )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerEditField: {
    alignItems: 'center',
    paddingTop: 10,
  },
  containerEditFieldInput: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 50,
  },
});

export default EditFieldOrganism;
