import React from 'react';
import { StyleSheet, View, TouchableOpacity, Text, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import StoriesNavigator from 'src/components/molecules/StoriesNavigator';

export interface Props {
  children: object;
  totalStories: number;
  currentStory: number;
}

const StoriesViewer: React.FC<Props> = (props: any) => {
  const navigation = useNavigation();
  const closeIcon = "https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599871411/white%20icons%20mobile/crecy_icons_close_header_white-01_iawfmz.png";

  return (
    <>
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity style={styles.imageContainer} onPress={navigation.goBack}>
            <Image source={{uri: closeIcon}} style={styles.closeImage}/>
          </TouchableOpacity>
          <Text style={styles.title}>¿Qué es un crédito?</Text>
        </View>

        <StoriesNavigator 
          totalStories={props.totalStories}
          currentStory={props.currentStory}
        />
      </View>
      {props.children}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    justifyContent: 'flex-start',
    top: 0,
    zIndex: 1,
    width: '100%',
    height: 75,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 14,
  },
  imageContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    left: 10,
    position: 'absolute',
  },
  closeImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  title: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 16,
    color: 'white',
    textAlign: 'center',
  }
});

export default StoriesViewer;
