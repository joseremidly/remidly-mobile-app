// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
} from 'react-native';

const EnableTouchIDMessage = () => {
  return (
    <>
      <View style={styles.containerEnableTouchIDMessage}>
        <Image style={styles.containerImageEnableTouchIDMessage} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1600885394/app/crecy_icons_touch_id_icon_velvet_1_hltrtm.png'}} />
        <Text style={styles.textPrimaryEnableTouchIDMessage}>
          Activa tu
          <Text style={styles.textSecondaryEnableTouchIDMessage}> TouchID </Text>
          para un acceso más rápido y seguro en Crecy
        </Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerEnableTouchIDMessage: {
    alignItems: 'center',
  },
  containerImageEnableTouchIDMessage: {
    width: 95,
    height: 103,
    marginBottom: 80,
  },
  textPrimaryEnableTouchIDMessage: {
    flexDirection: 'row',
    fontFamily: 'Baloo2-ExtraBold',
    textAlign: 'center',
    alignSelf: 'stretch',
    color: '#5a6175',
    fontSize: 22,
    paddingHorizontal: 40,
  },
  textSecondaryEnableTouchIDMessage: {
    color: '#7b3eff',
  },
});

export default EnableTouchIDMessage;
