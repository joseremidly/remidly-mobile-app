// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Image,
  Text,
} from 'react-native';
import IconReusable from 'src/components/atoms/IconReusable';

export interface Props {
  onSearch: Function;
}
const TopTransactions: React.FC<Props> = (props: any) => {
  const [input, setInput] = React.useState('');

  const changeInput = (text: string) => {
    setInput(text)
    props.onSearch(input)
  }
  const sendSearch = () => {
    props.onSearch(input)
  }
  return (
    <>
      <View style={styles.containerTopTransactions}>
          <View style={styles.containerTitleTransactions}>
            <Image style={styles.iconTitleTransactions} source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599773128/app/crecy_icons_bottom_bar_transaction_rqx80b.png' }} />
            <Text style={styles.titleTransactions}>Transacciones</Text>
          </View>
          <View style={styles.containerSearchTransactions}>
            <IconReusable src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1601319471/app/crecy_icons_find_transaction_bwqwky.png' color='#7b3eff' size={20} padding={8} onPress={sendSearch}/>
            <TextInput
                style={styles.inputSearchTransactions}
                placeholder='¿BUSCAS ALGO EN ESPECÍFICO?'
                onChangeText={changeInput}
                underlineColorAndroid="transparent"
            />
          </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerTopTransactions: {
    alignItems: 'center',
    backgroundColor: '#7b3eff',
    width: '100%',
  },
  containerTitleTransactions: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  iconTitleTransactions: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  titleTransactions: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: '#ffffff',
  },
  containerSearchTransactions: {
    margin: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: 46,
    paddingLeft: 5,
    paddingRight: 15,
    borderRadius: 50,
    marginBottom: 60,
  },
  iconSearchTransactions: {
    backgroundColor: 'blue',
    width: 30,
    height: 30,
    borderRadius: 50,
    marginRight: 10,
  },
  inputSearchTransactions: {
    backgroundColor: 'white',
    fontFamily: 'AvenirLTStd-Roman',
    marginLeft: 10,
    fontSize: 12,
    lineHeight: 16,
    width: 230,
    borderRadius: 50,
  }
});

export default TopTransactions;
