// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

import FaceIcon from 'src/components/atoms/FaceIcon';
import TitleText from 'src/components/atoms/TitleText';
import InputPin from 'src/components/molecules/InputPin';

export interface Props {
  onPinSecurity: Function;
}

const OPTPinForm: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containerOPTPinFormInput}>
        <View style={styles.containerFaceIcon} >
          <FaceIcon src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599599220/app/crecy_panda_circle_profile_logo_ew0djz.png' ></FaceIcon>
        </View>
        <TitleText textPrimary='Ingresa el código de seguridad' textSecondary='(enviado por sms)' fontSize={25} lineHeight={32}/>
        <View style={styles.containerOPTPinForm} >
          <InputPin onChangeInput={(pin) => props.onPinSecurity(pin)}/>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerOPTPinFormInput: {
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  containerFaceIcon: {
    width: 110,
    height: 110,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    marginBottom: 40,
  },
  containerOPTPinForm: {
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 100,
  },
});

export default OPTPinForm;
