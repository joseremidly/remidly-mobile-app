// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  Text,
} from 'react-native';

export interface Props {
  Categories: any;
}
const CategoriesList: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containerCategoriesList}>
        <View style={styles.containerTitleCategoriesList}>
          <Image style={styles.iconTitleCategoriesList} source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1600116574/app/crecy_icons_Categories_screen_Categories_grey_1_ryxhbg.png' }} />
          <Text style={styles.textTitleCategoriesList}>0 TRANSACCIONES</Text>
          {/* <Text style={styles.textTitleCategoriesList}>{props.Categories}</Text> */}
          <View style={styles.Categories}>
            {props.Categories.map((prop, key) => {
              return (
                <View style={styles.transaction} key={key}>
                  <Text>{prop.id}</Text>
                  <Text>{prop.description}</Text>
                  <Text>{prop.amount}</Text>
                </View>
              );
            })}
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerCategoriesList: {
    position: 'absolute',
    top: 270,
    alignItems: 'center',
    backgroundColor: 'white',
    width: '100%',
    zIndex: 2,
    borderTopLeftRadius: 26,
    borderTopRightRadius: 26,
    height: 500,
  },
  containerTitleCategoriesList: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  iconTitleCategoriesList: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  textTitleCategoriesList: {
    fontFamily: 'Baloo2-Regular',
    color: '#5a6175',
    fontSize: 22,
  },
  Categories: {
    alignItems: 'center',
  },
  transaction: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    color: 'blue',
  }
});

export default CategoriesList;
