import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    Platform,
    View,
} from 'react-native';
import {
  CommonActions,
} from '@react-navigation/native';
import { useNavigation } from '@react-navigation/native';

export interface Props {
  title: string;
  onHome: boolean;
  backgroundColor: string;
  uppercaseHeader: boolean;
  arrowColor: string;
  textColor: string;
}

const defaultProps: Props = {
  title: '',
  onHome: false,
  backgroundColor: '#f4f4f7',
  uppercaseHeader: false,
  arrowColor: 'grey',
  textColor: '#5a6175',
}

const HeaderCreditOnboarding: React.FC<Props> = (props: any) => {
  const navigation = useNavigation();

  let arrowImg = 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604027969/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_advances_arrow_grey_iqr36s.png';

  if (props.arrowColor === 'grey') {
    arrowImg = 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604027969/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_advances_arrow_grey_iqr36s.png';
  } else if (props.arrowColor === 'white') {
    arrowImg = 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599596734/app/crecy_icons_arrow_header_white-01_dg8lby.png';
  }
  const leftAction = () => {
    {{props.onHome ? navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          { name: 'Home' },
        ],
      })
    ) : navigation.goBack()}}
  }
  return (
    <>
      <View style={styles(props).customHeaderSecondary}>
        <TouchableOpacity onPress={leftAction} style={styles(props).containerIconButtonStyleLeft}>
          <Image style={styles(props).iconButtonStyleLeft} source={{uri: arrowImg}}></Image>
        </TouchableOpacity>
        <Text style={styles(props).textButtonStyle}>{props.title}</Text>
      </View>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  customHeaderSecondary: {
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: props.backgroundColor,
    width: '100%',
    height: Platform.select({
      ios: 50,
      android: 60
    }),
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: props.textColor,
    textTransform: props.uppercaseHeader ? 'uppercase' : 'none',
  },
  containerIconButtonStyleLeft: {
    position: 'absolute',
    left: 20,
  },
  iconButtonStyleLeft: {
    width: 30,
    height: 30,
    resizeMode: 'contain'
  },
});

HeaderCreditOnboarding.defaultProps = defaultProps;

export default HeaderCreditOnboarding;
