// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
import ButtonCheckup from 'src/components/atoms/ButtonCheckup';

export interface Props {
  goRoute: Function;
  alerts: number;
  crecyScore: number;
}

const NewsFeedCrecyCheckup: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containerNewsFeedCrecyCheckup}>
        <Text style={styles.textNewsFeedCrecyCheckup}>ESTATUS</Text>
        {props.alerts !== 0
          && (
            <View style={styles.containerButtonCheckup} >
              <ButtonCheckup
                  label='alerts'
                  type={true}
                  number={props.alerts}
                  onPress={props.goRoute}
                  src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1601068293/app/crecy_icons_alert_mdully.png'
                ></ButtonCheckup>
            </View>
          )}
        <View style={styles.containerButtonCheckup} >
          <ButtonCheckup
              label='Crecy score'
              type={false}
              number={props.crecyScore}
              onPress={props.goRoute}
              src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599599220/app/crecy_panda_circle_profile_logo_ew0djz.png'
            ></ButtonCheckup>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerNewsFeedCrecyCheckup: {
    alignItems: 'center',
    marginHorizontal: 20,
  },
  textNewsFeedCrecyCheckup: {
    width: '100%',
    flexDirection: 'row',
    fontFamily: 'Baloo2-Regular',
    textAlign: 'left',
    alignSelf: 'stretch',
    color: '#5a6175',
    fontSize: 14,
    lineHeight: 27,
  },
  containerButtonCheckup:{
    width: '100%',
    marginTop: 5,
    marginBottom: 10,
  }
});

export default NewsFeedCrecyCheckup;
