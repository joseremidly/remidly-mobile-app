// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Keyboard,
} from 'react-native';

import TitleText from 'src/components/atoms/TitleText';
import InputPin from 'src/components/molecules/InputPin';

export interface Props {
  onReadPin: Function;
  correct: boolean;
}

const ConfirmPinFormInput: React.FC<Props> = (props: any) => {
  const getPin = (pin) => {
    props.onReadPin(pin)
    Keyboard.dismiss()
  }
  return (
    <>
      <View style={styles.containerConfirmPin}>
        <TitleText textPrimary='Vuelve a ingresar el ' textSecondary='pin de 6 digitos' fontSize={30} lineHeight={38}/>
        <View style={styles.containerConfirmPinFormInput} >
          <InputPin onChangeInput={(pin) => getPin(pin)}/>
        </View>
        <Text style={styles.textConfirmPinFormInput} >Por favor, vuelve a ingresar tu PIN para confirmar</Text>
        {!props.correct
          && (
            <Text style={styles.textErrorConfirmPinFormInput} >Los códigos no coinciden.</Text>
          )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerConfirmPin: {
    alignItems: 'center',
    marginTop: 50,
    marginHorizontal: 10
  },
  containerConfirmPinFormInput: {
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  textConfirmPinFormInput: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 16,
    marginBottom: 65,
    color: '#5a6175',
  },
  textErrorConfirmPinFormInput: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 16,
    marginBottom: 65,
    color: '#FF2169',
  },
});

export default ConfirmPinFormInput;
