// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
} from 'react-native';
import Moment from 'moment';
import TotalBalance from 'src/components/molecules/TotalBalance';
import CardBalance from 'src/components/molecules/CardBalance';
import ButtonAddNewCard from 'src/components/atoms/ButtonAddNewCard';
import institutions from 'src/utils/institutions';

export interface Props {
  accounts: Object;
  addAccount: Function;
  totalBalance: Number;
}

const CardsScrollView: React.FC<Props> = (props: any) => {
  // Moment.locale('es');
  const [dateUpdated, setDateUpdated] = React.useState(Moment(new Date()).format('l'))

  const Accounts = props.accounts.map((account, index) =>
    <View style={styles.containerCard} key={account.id + index.toString()}>
      <CardBalance
        accountsLinked={0}
        background={institutions[account.institution.name] === undefined ? institutions['default'].primary_color : institutions[account.institution.name].primary_color}
        icon={institutions[account.institution.name] === undefined ? institutions['default'].logo : institutions[account.institution.name].logo}
        positionLogo={institutions[account.institution.name] === undefined ? '' : (institutions[account.institution.name].positionLogo ? institutions[account.institution.name].positionLogo: '')}
        color={institutions[account.institution.name] === undefined ? '#FFFFFF' : (institutions[account.institution.name].font_color ? institutions[account.institution.name].font_color: '#FFFFFF')}
        balance={account.balance.current}
        name={account.name}
        category={account.category}
        updatedDate={`Actualizado: ${dateUpdated.toString()}`}
        institution={account.institution}
        update={() => updateBalance()}
      />
    </View>
  );
  return (
    <>
      <ScrollView  style={styles.containerCards} horizontal={true} showsHorizontalScrollIndicator={false}>
        <ButtonAddNewCard src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1601939661/app/add_2_ycwugv.png' onPress={props.addAccount} />
        <View style={styles.containerCard}>
          <TotalBalance accountsLinked={props.accounts.length} balance={props.totalBalance} updatedDate={`Actualizado: ${dateUpdated.toString()}`} update={() => updateBalance()}/>
        </View>
        {Accounts}
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  containerCards: {
    marginTop: 20,
  },
  containerCard: {
    width: 320,
  }
});

export default CardsScrollView;
