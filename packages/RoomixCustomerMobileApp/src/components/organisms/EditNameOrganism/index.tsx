// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
} from 'react-native';
import EditInputReusable from 'src/components/atoms/EditInputReusable';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  onChangeField: Function;
  stateService: String;
  name: String;
  lastName: String;
}

const EditNameOrganism: React.FC<Props> = (props: any) => {
  const [active, setActive] = React.useState(false);
  const [name, setName] = React.useState('');
  const [lastName, setLastName] = React.useState('');

  const onChangeName = (name: any) => {
    if (name.length >= 2) {
      setName(name)
    }
  }
  const onChangeLastName = (lastName: any) => {
    setActive(((name && name !== '') && (lastName && lastName !== '')) ? true : false )
    if (lastName.length >= 2) {
      setLastName(lastName)
    }
  }
  const changedField = () => {
    if (active) {
      props.onChangeField({name: name, last_name: lastName})
    }
  }
  React.useEffect(() => {
    if (props.name !== '') {
      setName(props.name)
    }
    if (props.lastName !== '') {
      setLastName(props.lastName)
    }
  },[])
  return (
    <>
      <View style={styles.containerEditName}>
        <View style={styles.containerEditNameInput} >
          <Text style={styles.labelTextInput}>Nombre</Text>
          <EditInputReusable placeholder={props.name} onChangeInput={onChangeName} content={props.name} />
          <View style={styles.separatorName}></View>
          <Text style={styles.labelTextInput}>Apellido(s)</Text>
          <EditInputReusable placeholder={props.lastName} onChangeInput={onChangeLastName} content={props.lastName} />
        </View>
        {props.stateService === 'loading'
          && (
            <ActivityIndicator size='large' color='#7b3eff' animating={true} />
          )}
        {props.stateService !== 'loading'
          && (
            <ButtonMid label='Guardar cambios ' secondary={!active} onPress={changedField}></ButtonMid>
          )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerEditName: {
    alignItems: 'center',
    paddingTop: 10,
  },
  containerEditNameInput: {
    width: '100%',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 50,
  },
  labelTextInput: {
    width: '80%',
    flexDirection: 'row',
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'left',
    color: '#5a6175',
    fontSize: 14,
    marginHorizontal: 0
  },
  separatorName: {
    height: 20,
  }
});

export default EditNameOrganism;
