// @ts-nocheck
import React from 'react';
import { StyleSheet, ScrollView, Text } from 'react-native';
import AlertSwitch from 'src/components/organisms/AlertSwitch';
import { useServices } from 'src/data/providers/ServicesProvider';

export interface Props {
  alertsSettings: any;
  stateService: string;
  setAlertSettingsValue: Function;
}

const AlertsSettings: React.FC<Props> = (props: any) => {
  const updateValue = (code, newValue) => {
    props.setAlertSettingsValue(code, newValue);
  };

  return (
    <>
      <ScrollView style={styles.container}>
        <Text style={styles.text}>Cuentas conectadas</Text>
        <AlertSwitch
          text="Balance bajo"
          checked={props.alertsSettings.low_balance}
          stateService={props.stateService}
          updateValue={(newValue) => updateValue('low_balance', newValue)}
        />
        <AlertSwitch
          text="Cargo doble"
          checked={props.alertsSettings.double_charge}
          stateService={props.stateService}
          updateValue={(newValue) => updateValue('double_charge', newValue)}
        />
        <AlertSwitch
          text="Comisiones bancarias"
          checked={props.alertsSettings.bank_fees}
          stateService={props.stateService}
          updateValue={(newValue) => updateValue('bank_fees', newValue)}
        />
        <AlertSwitch
          text="Pago de nómina"
          checked={props.alertsSettings.paychecks}
          stateService={props.stateService}
          updateValue={(newValue) => updateValue('paychecks', newValue)}
        />
        <AlertSwitch
          text="Transferencias"
          checked={props.alertsSettings.money_transfers}
          stateService={props.stateService}
          updateValue={(newValue) => updateValue('money_transfers', newValue)}
        />
        <AlertSwitch
          text="Transacciones"
          checked={props.alertsSettings.transactions}
          stateService={props.stateService}
          updateValue={(newValue) => updateValue('transactions', newValue)}
        />
        <AlertSwitch
          text="Fecha límite de pago"
          checked={props.alertsSettings.bills_due_date}
          stateService={props.stateService}
          updateValue={(newValue) => updateValue('bills_due_date', newValue)}
        />

        <Text style={styles.text}>Reporte de gastos e ingresos</Text>
        <AlertSwitch
          text="Semanal"
          checked={props.alertsSettings.income_spending_report_weekly}
          stateService={props.stateService}
          updateValue={(newValue) =>
            updateValue('income_spending_report_weekly', newValue)
          }
        />
        <AlertSwitch
          text="Mensual"
          checked={props.alertsSettings.income_spending_report_monthly}
          stateService={props.stateService}
          updateValue={(newValue) =>
            updateValue('income_spending_report_monthly', newValue)
          }
        />
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: '#F4F4F7',
    paddingHorizontal: 24,
    paddingTop: 8,
  },
  text: {
    fontFamily: 'AvenirLTStd-Medium',
    textTransform: 'uppercase',
    fontSize: 18,
    color: '#5a6175',
    marginTop: 20,
    marginBottom: 10,
  },
});

export default AlertsSettings;
