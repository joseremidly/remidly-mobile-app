import React from 'react';
import { StyleSheet, View } from 'react-native';
import CoursesHomeHeader from 'src/components/molecules/CoursesHomeHeader';
import LevelProgress from 'src/components/molecules/LevelProgress';
import GoalProgress from 'src/components/molecules/GoalProgress';

export interface Props {
  progressTotal: number;
  progressLevel: number;
  levelCurrent: number;
  goalsTotal: number;
  goalsCompleted: number;
}

const CoursesHomeSummary: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.container}>
        <CoursesHomeHeader progress={props.progressTotal} />
        <LevelProgress
          level={props.levelCurrent}
          progress={props.progressLevel}
        />
        <GoalProgress
          goalsCompleted={props.goalsCompleted}
          goalsTotal={props.goalsTotal}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    paddingBottom: 36,
    backgroundColor: '#7b3eff',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomLeftRadius: 26,
    borderBottomRightRadius: 26,
    marginBottom: 40,
  },
});

export default CoursesHomeSummary;
