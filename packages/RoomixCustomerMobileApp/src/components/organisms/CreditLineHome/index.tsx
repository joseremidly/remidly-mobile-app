// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import ActiveDebts from 'src/components/molecules/ActiveDebts';
import ActiveDebtsExpired from 'src/components/molecules/ActiveDebtsExpired'
import DebtsHistory from 'src/components/molecules/DebtsHistory';

export interface Props {
  loansData: any;
  loansDataExpired: any;
  loansHistoryData: any;
  onPressPay: Function;
  onPressDetails: Function;
  goToLatePayment: Function;
  onRefresh: Function;
}

const CreditLineHome: React.FC<Props> = (props) => {
  const [refreshing, setRefreshing] = React.useState(false);
  const [record, setRecord] = React.useState(true);
  const selectRecord = (value: any) => {
    setRecord(value)
  }
  const Debts = props.loansData.map((loan, index) => {
    return (
      <ActiveDebts
        key={index}
        paymentDate={loan.payment_due_date}
        amount={loan.amount}
        rate={loan.rate}
        daysToPay={15}
        loan={loan}
        id={loan.reference}
        onPressPay={props.onPressPay}
        onPressDetails={props.onPressDetails}
        goToLatePayment={props.goToLatePayment}
      />
    )
  });
  const DebtsExpired = props.loansDataExpired.map((loan, index) => {
    return (
      <ActiveDebtsExpired
        key={index}
        paymentDate={loan.payment_due_date}
        amount={loan.amount}
        rate={loan.rate}
        daysToPay={15}
        loan={loan}
        id={loan.reference}
        onPressPay={props.onPressPay}
        onPressDetails={props.onPressDetails}
      />
    )
  });
  const HistoryDebts = props.loansHistoryData.map((loan, index) => {
    return (
      <DebtsHistory
        key={index}
        paymentDate={loan.payment_due_date}
        amount={loan.amount}
        rate={loan.rate}
        daysToPay={15}
        loan={loan}
        id={loan.reference}
        onPressPay={props.onPressPay}
        onPressDetails={props.onPressDetails}
        goToLatePayment={props.goToLatePayment}
      />
    )
  });
  return (
    <View style={styles(props, record).container}>
      <View style={styles(props, record).containerButtonSelected}>
        <TouchableOpacity style={styles(props, record).buttonSelected} onPress={() => selectRecord(true)}>
          <Text style={styles(props, record).textButtonSelected}>Deudas activas</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles(props, record).buttonSelectedHistory} onPress={() => selectRecord(false)}>
          <Text style={styles(props, record).textButtonSelectedRecord}>Historial</Text>
        </TouchableOpacity>
      </View>
      {record
        ? (
          <>
            {props.loansData.length > 0 || props.loansDataExpired.length > 0
              ? (
                <ScrollView 
                  style={styles(props, record).containerDebt}
                  showsVerticalScrollIndicator={false}
                  refreshControl={
                    <RefreshControl
                      refreshing={refreshing}
                      onRefresh={props.onRefresh}
                    />
                  }
                >
                  {DebtsExpired}
                  {Debts}
                </ScrollView>
              ) : (
                <View style={styles(props, record).emptyHistory}>
                  <Text style={styles(props, record).emptyTextHistory}>No tienes deudas activas</Text>
                </View>
              )}
          </>
        ) : (
          <>
            {props.loansHistoryData.length > 0
              ? (
                <ScrollView 
                  style={styles(props, record).containerDebt}
                  showsVerticalScrollIndicator={false}
                  refreshControl={
                    <RefreshControl
                      refreshing={refreshing}
                      onRefresh={props.onRefresh}
                    />
                  }
                >
                  {HistoryDebts}
                </ScrollView>
              ) : (
                <View style={styles(props, record).emptyHistory}>
                  <Text style={styles(props, record).emptyTextHistory}>No tienes historial de deudas pagadas</Text>
                </View>
              )}
          </>
        )
      }
    </View>
  );
};

const styles = (props: any, select: any) => StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: '#e6e6e6',
    borderRadius: 19,
  },
  containerButtonSelected: {
    flexDirection: 'row',
    marginHorizontal: 30,
    marginTop: 10
  },
  buttonSelected: {
    minHeight: 40,
    minWidth: 70,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'red',
  },
  buttonSelectedHistory: {
    minHeight: 40,
    minWidth: 70,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'red',
    marginLeft: 20
  },
  textButtonSelected: {
    fontFamily: 'Avenir-Black',
    fontSize: 14,
    color: '#5a6175',
    textDecorationLine: select ? 'underline' : 'none',
  },
  textButtonSelectedRecord: {
    fontFamily: 'Avenir-Black',
    fontSize: 14,
    color: '#5a6175',
    textDecorationLine: select ? 'none' : 'underline',
  },
  containerDebt: {
    width: '100%',
    paddingHorizontal: 20,
    marginBottom: 400,
  },
  emptyHistory: {
    height: '100%'
  },
  emptyTextHistory: {
    fontFamily: 'Baloo2-Bold',
    color: '#b3b3b3',
    fontSize: 25,
    lineHeight: 32,
    textAlign: 'center',
    marginTop: 100,
    marginHorizontal: 30,
  }
});

export default CreditLineHome;
