// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Modal,
} from 'react-native';
import { WebView } from 'react-native-webview';
import CheckBox from '@react-native-community/checkbox';
import JobTypeSelector from 'src/components/molecules/JobTypeSelector';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  step: string;
  jobType: string;
  setStep: Function;
  setJobType: Function;
}
const links = {
  plataform: 'https://drive.google.com/file/d/1aXuA8bk1ZQRphFbwf41579tzibMPVDrd/view?usp=sharing',
  formal: 'https://drive.google.com/file/d/1ShZ3XI72RhdJ85arM9QiLEHHHvy0H5ND/view?usp=sharing',
  terms: 'https://drive.google.com/file/d/18Z93LkeNBWbt_29BfZqAToBZ9THQcD1O/view?usp=sharing'
}
const CreditOnBoardingJobtype: React.FC<Props> = (props: any) => {
  const [jobSelected, setJobSelected] = React.useState(false);
  const [active, setActive] = React.useState(true);
  const [policy, setPolicy] = React.useState(false);
  const [terms, setTerms] = React.useState(false);
  const [modalActive, setModalActive] = React.useState(false);
  const [documentActive, setDocumentActive] = React.useState('');

  const onJobTypeSelect = (type: any) => {
    setJobSelected(type)
    props.setJobType(type);
  };
  const showModal = (document: any) => {
    if (document === 'policy' && jobSelected) {
      if (jobSelected === 'formalWorker') setDocumentActive(links.formal)
      if (jobSelected === 'gigWorker') setDocumentActive(links.plataform)
      setModalActive(true)
    }
    if (document === 'terms') {
      setDocumentActive(links.terms)
      setModalActive(true)
    }
  }
  const nextStep = () => {
    if (props.jobType == null || !policy || !terms) {
      return
    }
    if (props.jobType == 'formalWorker') {
      props.setStep('CreditOnboardingAccount', { jobType: 'formalWorker' })
    }
    if (props.jobType == 'gigWorker') {
      props.setStep('CreditOnboardingGig', { jobType: 'gigWorker' })
    }
  }
  React.useEffect(() => {
    {policy && terms && jobSelected ? setActive(false) : setActive(true)}
  }, [jobSelected, policy, terms])
  return (
    <>
      <View style={styles.container}>
        <Modal  visible={modalActive}>
          <View style={styles.modal}>
            <View style={styles.modalContainer}>
              <WebView
                style={{ flex : 1 }}
                source={{uri: documentActive}}
              />
              <View style={styles.containerCloseButton}>
                <ButtonMid
                  label="Cerrar"
                  onPress={() => setModalActive(false)}
                  secondary={false}
                />
              </View>      
            </View>
          </View>
        </Modal>
        <Text style={styles.headerText}>
          Por favor, elige
          <Text style={styles.textHighlighted}> tu ocupación</Text>
        </Text>
        <JobTypeSelector
          jobType="formalWorker"
          selectedJobType={props.jobType}
          // onPress={onJobTypeSelect}
          onPress={() => {}}
        />
        <View style={styles.horizontalSeparator} />
        <JobTypeSelector
          jobType="gigWorker"
          selectedJobType={props.jobType}
          onPress={onJobTypeSelect}
        />
        <View style={styles.checkboxContainer}>
          <View style={styles.checkboxOptionContainer}>
            <CheckBox
              value={policy}
              onValueChange={(value: boolean) => setPolicy(value)}
              style={styles.checkbox}
              tintColors={{ true: '#7b3eff' }}
              onCheckColor='#7b3eff'
              boxType='circle'
              animationDuration={0.2}
            />
            <TouchableOpacity onPress={() => showModal('policy')}>
              <Text style={styles.labelCheckbox}>Política de privacidad</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.checkboxOptionContainer}>
            <CheckBox
              disabled={false}
              value={terms}
              onValueChange={(value: boolean) => setTerms(value)}
              style={styles.checkbox}
              tintColors={{ true: '#7b3eff' }}
              onCheckColor='#7b3eff'
              boxType='circle'
              animationDuration={0.2}
            />
            <TouchableOpacity onPress={() => showModal('terms')}>
              <Text style={styles.labelCheckbox}>Términos y condiciones</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <ButtonMid
            label="Continuar"
            onPress={nextStep}
            secondary={active}
          />
          <Text style={styles.textToAccept}>Al continuar aceptas la política de privacidad y términos y condiciones</Text>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  modal : {
    flex : 1,
    justifyContent : 'center',
    alignItems : 'center',
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  modalContainer : {
    backgroundColor : 'white',
    width : '90%',
    height : '90%',
  },
  containerCloseButton: {
    position: 'absolute',
    alignItems: 'center',
    bottom: 20,
    width: '100%'
  },
  headerText: {
    marginBottom: 15,
    textAlign: 'center',
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    color: '#5a6175',
  },
  textHighlighted: {
    color: '#7f47dd',
  },
  textToAccept: {
    marginTop: 5,
    textAlign: 'center',
    fontFamily: 'AvenirLTStd-Book',
    fontSize: 12,
    color: '#5a6175',
  },
  horizontalSeparator: {
    marginVertical: 20,
    width: '30%',
    height: StyleSheet.hairlineWidth,
    backgroundColor: 'grey',
  },
  checkbox: {
    marginVertical: 3,
    marginHorizontal: 5,
  },
  checkboxContainer: {
    marginTop: 20,
    alignItems: 'flex-start'
  },
  checkboxOptionContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  labelCheckbox: {
    textAlign: 'center',
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    color: '#6133E2',
    textDecorationLine: 'underline'
  },
  buttonContainer: {
    width: '100%',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 70,
  },
});

export default CreditOnBoardingJobtype;
