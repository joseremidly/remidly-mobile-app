import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    Platform,
		View,
		Dimensions
} from 'react-native';

export interface Props {
  navigation: Object;
	home: boolean;
}

const HeaderCreditLine: React.FC<Props> = (props: any) => {
	const {height} = Dimensions.get('window');
  const leftAction = () => {
    {{props.home ? props.navigation.navigate('Home') : props.navigation.goBack()}}
	}
	let dimensionHeight = height/4;
  return (
    <>
      <View style={styles(dimensionHeight).customHeader}>
        <TouchableOpacity onPress={leftAction} style={{justifyContent: "center", alignItems: 'center'}}>
          <Image style={styles(dimensionHeight).iconButtonStyleLeft} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604027969/app/Crecy%20icons%20new%20dimensions/crecy_icons_paycheck_advances_arrow_grey_iqr36s.png' }} resizeMode='contain'></Image>
        </TouchableOpacity>
        <Text style={styles(dimensionHeight).textButtonStyle}>Línea de crédito Crecy</Text>
      </View>
    </>
  );
};

const styles = (dimensionHeight: any) => StyleSheet.create({
  customHeader: {
		position: 'absolute',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
		alignSelf: 'stretch',
    backgroundColor: 'white',
    width: '100%',
		zIndex: 4,
  },
  textButtonStyle: {
    width: '70%',
    textAlign: 'center',
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: '#5a6175',
    marginLeft: 15,
  },
  iconButtonStyleLeft: {
    width: 20,
    height: 20,
    marginLeft: 20,
    marginVertical: 10
  },
  iconButtonStyleRight: {
    width: 25,
    height: 25,
    marginRight: 20,
  },
});

export default HeaderCreditLine;