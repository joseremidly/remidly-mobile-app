// @ts-nocheck
import React from 'react';
import { StyleSheet } from 'react-native';
import CreditOnboardingCard from 'src/components/molecules/CreditOnboardingCard';

export interface Props {
  goToKyc: Function;
}

const CreditOnboardingID: React.FC<Props> = (props: any) => {
  return (
    <>
      <CreditOnboardingCard
        headerTextOne="Por favor,"
        headerTextTwo=""
        headerTextHighlighted="verifica tu identidad"
        infoText="Nececitamos que pases por el proceso de verificación de identidad para comprobar que eres tú la persona que solicita la línea de crédito y no alguien más"
        imageURI="https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1610351789/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_icon_KYC_new_degljm.png"
        buttonText="Empezar"
        nextStep="finish"
        setStep={props.goToKyc}
      />
    </>
  );
};

const styles = StyleSheet.create({});

export default CreditOnboardingID;
