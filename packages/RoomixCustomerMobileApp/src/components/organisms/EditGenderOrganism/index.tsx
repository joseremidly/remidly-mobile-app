// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
} from 'react-native';
import SelectorReusable from 'src/components/molecules/SelectorReusable';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  stateService: String;
  onChangeGender: Function;
}

const EditGenderOrganism: React.FC<Props> = (props: any) => {
  const [active, setActive] = React.useState(false);
  const [gender, setGender] = React.useState('');

  const onChangeOption = (gender: any) => {
    setActive(gender && gender !== '' ? true : false )
    if (gender.length >= 2) {
      setGender(gender)
    }
  }
  const changedGender = () => {
    if (active) {
      props.onChangeGender(gender)
    }
  }
  return (
    <>
      <View style={styles.containerEditGenderOrganism}>
        <Text style={styles.textPrimaryEditGenderOrganism}>
          Elige
          <Text style={styles.textSecondaryEditGenderOrganism}> tu género </Text>
        </Text>
        <View style={styles.containerEditGenderInput} >
          <Text style={styles.textInformationGender}>Usaremos esta información para personalizar tu experiencia y proveerte con datos más precisos y recomendaciones</Text>
          <SelectorReusable onChangeOption={onChangeOption} />
        </View>
        {props.stateService === 'loading'
          && (
            <ActivityIndicator size='large' color='#7b3eff' animating={true} />
          )}
        {props.stateService !== 'loading'
          && (
            <ButtonMid label='Guardar cambios' secondary={!active} onPress={changedGender}></ButtonMid>
          )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerEditGenderOrganism: {
    alignItems: 'center',
    paddingTop: 40,
  },
  textPrimaryEditGenderOrganism: {
    flexDirection: 'row',
    fontFamily: 'Baloo2-ExtraBold',
    textAlign: 'center',
    alignSelf: 'stretch',
    color: '#5a6175',
    fontSize: 32,
  },
  textSecondaryEditGenderOrganism: {
    color: '#7b3eff',
  },
  containerEditGenderInput: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 80,
  },
  textInformationGender: {
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'center',
    letterSpacing: .5,
    fontSize: 22,
    color: '#999999',
    paddingHorizontal: 35,
    marginBottom: 20,
  }
});

export default EditGenderOrganism;
