// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

import ButtonMid from 'src/components/atoms/ButtonMid';
import FaceIcon from 'src/components/atoms/FaceIcon';
import InputPhone from 'src/components/atoms/InputPhone';
import TitleText from 'src/components/atoms/TitleText';
import Dropdown from 'src/components/molecules/Dropdown';

export interface Props {
  onLogin: Function;
}

const LoginFormInput: React.FC<Props> = (props: any) => {
  const [active, setActive] = React.useState(false);
  const [phone, setPhone] = React.useState('');

  const onChangeInput = (number: any) => {
    setActive(number && number !== '' ? true : false )
    setPhone(number)
    if (number && number !== '') {
      props.onLogin(number)
    }
  }
  const login = () => {
    if (active) {
      props.onLogin(phone)
    }
  }
  return (
    <>
      <View style={styles.containerLoginFormInput}>
        <View style={styles.containerFaceIcon} >
          <FaceIcon src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599599220/app/crecy_panda_circle_profile_logo_ew0djz.png' ></FaceIcon>
        </View>
        <TitleText textPrimary='Por favor, ingresa tu' textSecondary='teléfono celular' fontSize={30} lineHeight={40}/>
        <View style={styles.containerNumberForm} >
          <Dropdown />
          <InputPhone placeholder='NÚMERO DE CELULAR' onChangeInput={onChangeInput} ></InputPhone>
        </View>
        <ButtonMid label='Ingresar' secondary={!active} onPress={login}></ButtonMid>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerLoginFormInput: {
    alignItems: 'center',
  },
  containerFaceIcon: {
    width: 110,
    height: 110,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 40,
  },
  containerNumberForm: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 100,
  },
  iconInputForm: {
    width: 20,
    height: 20,
    margin: 10,
  }
});

export default LoginFormInput;
