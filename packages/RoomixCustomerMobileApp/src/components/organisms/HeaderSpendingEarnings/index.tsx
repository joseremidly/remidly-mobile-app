import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
		View,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';

export interface Props {
  navigation: Object;
  home: boolean;
  onPress: Function;
  select: any;
}

const HeaderSpendingsEarnings: React.FC<Props> = (props: any) => {
  const navigation = useNavigation();
  const leftAction = () => {
    {{props.home ? navigation.navigate('Home') : navigation.goBack()}}
	}
  return (
    <>
			<View style={styles(props).container}>
				<View style={styles(props).customHeader}>
					<TouchableOpacity onPress={leftAction}>
						<Image style={styles(props).iconButtonStyleLeft} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599596734/app/crecy_icons_arrow_header_white-01_dg8lby.png' }}></Image>
					</TouchableOpacity>
          <Text style={styles(props).textButtonStyle}>Análisis</Text>
				</View>
				<View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: '100%', marginVertical: 30,}}>
					<TouchableOpacity style={styles(props).buttonExpenses} onPress={() => props.onPress(true)}>
						<Text style={styles(props).textExpenses}>
							{'  '}Gastos{'  '}
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles(props).buttonIncome} onPress={() => props.onPress(false)}>
						<Text style={styles(props).textIncome}>
							{'  '}Ingresos{'  '}
						</Text>
					</TouchableOpacity>
				</View>
			</View>
    </>
  );
};

const styles = (props: any) => StyleSheet.create({
  container: {
		position: 'absolute',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
		alignSelf: 'stretch',
    backgroundColor: '#7b3eff',
    width: '100%',
    height: '30%',
    zIndex: 0,
	},
	customHeader: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
		alignSelf: 'stretch',
    width: '100%',
    marginTop: 10,
  },
  textButtonStyle: {
    width: '70%',
    textAlign: 'center',
    fontFamily: 'Avenir-Black',
    fontSize: 26,
    color: '#ffffff',
    marginLeft: 15,
  },
  textExpenses: {
    textAlign: 'center',
    fontFamily: 'Avenir-Black',
    fontSize: 22,
    color: '#ffffff',
    paddingBottom: 5
  },
  textIncome: {
    textAlign: 'center',
    fontFamily: 'Avenir-Black',
    fontSize: 22,
    color: '#ffffff',
    paddingBottom: 5
  },
  iconButtonStyleLeft: {
    width: 30,
    height: 30,
    marginLeft: 20,
  },
  buttonExpenses: {
    borderBottomColor: props.select === true ? '#FFFFFF' : '#A378FF',
    borderBottomWidth: 1,
  },
  buttonIncome: {
    borderBottomColor: props.select === true ? '#A378FF' : '#FFFFFF',
    borderBottomWidth: 1,
  }
});

export default HeaderSpendingsEarnings;
