// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ActivityIndicator,
} from 'react-native';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  navigation: Object;
  stateService: String;
  continue: Function;
}

const Start: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.sectionContainer}>
        {/* <Text style={styles.headerText}>Pasos para verificar</Text> */}
        <Image style={styles.imgHeader} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1611600041/app/Crecy%20icons%20new%20dimensions/Crecy%20web%20page/Crecy_web_page_crecy_all_benefits_wo_buro_duejq7.png'}}/>
        <Text style={styles.informationText}>1.- Toma una foto de tu identificación oficial por ambos lados. <Text> (INE, IFE, Pasaporte)</Text></Text>
        <Text style={styles.informationText}>2.- Completa tus datos personales</Text>
        <Text style={styles.informationText}>3.- Tomate una selfie</Text>
        <View style={styles.containerButtonStartKyc}>
          {props.stateService === 'loading'
            && (
              <ActivityIndicator size='large' color='#7b3eff' animating={true} />
            )}
          {props.stateService !== 'loading'
            && (
              <ButtonMid
                label='Empezar el trámite'
                secondary={false}
                onPress={() => props.continue()}
              />
            )}
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    backgroundColor: '#F4F4F7',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20
  },
  imgHeader: {
    width: 150,
    height: 150,
    marginTop: 20,
    marginBottom: 30,
  },
  headerText: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 25,
    color: '#5a6175',
    marginBottom: 0,
    marginHorizontal: 50,
    textAlign: 'center',
    lineHeight: 30
  },
  informationText: {
    width: '90%',
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    color: '#5a6175',
    marginBottom: 25,
    marginLeft: 25,
    textAlign: 'left',
    lineHeight: 23
  },
  containerButtonStartKyc: {
    marginTop: 30,
    marginBottom: 20,
  },
});

export default Start;