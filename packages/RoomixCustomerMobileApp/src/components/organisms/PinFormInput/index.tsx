// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Keyboard
} from 'react-native';

import TitleText from 'src/components/atoms/TitleText';
import InputPin from 'src/components/molecules/InputPin';

export interface Props {
  onReadPin: Function;
}

const PinFormInput: React.FC<Props> = (props: any) => {
  const getPin = (pin) => {
    props.onReadPin(pin)
    Keyboard.dismiss()
  }
  return (
    <>
      <View style={styles.containerPinFormInput}>
        <TitleText textPrimary='Crea un' textSecondary='pin de 6 digitos' fontSize={30} lineHeight={38}/>
        <View style={styles.containerPinForm} >
          <InputPin onChangeInput={(pin) => getPin(pin)}/>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerPinFormInput: {
    alignItems: 'center',
    marginTop: 50,
  },
  containerPinForm: {
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 100,
  },
});

export default PinFormInput;
