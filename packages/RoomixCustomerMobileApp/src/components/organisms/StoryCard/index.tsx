import React from 'react';
import { StyleSheet, View, TouchableOpacity} from 'react-native';
import StoryCardBackground from 'src/components/molecules/StoryCardBackground';
import StoryCardTitle from 'src/components/molecules/StoryCardTitle';
import StoryCardParagraphs from 'src/components/molecules/StoryCardParagraphs';

export interface Props {
  data: Object;
  showNextStory: Function;
  showPreviousStory: Function;
}

const StoryCard: React.FC<Props> = (props: any) => {
  const backgroundData = props.data.background.content;
  const paragraphsData = props.data.paragraphs;
  const titleData = props.data.title.content;

  return (
    <>
      <StoryCardBackground
        backgroundImage={backgroundData.image?.uri}
        backgroundColor={backgroundData.solidColor?.color}>
        <View style={styles.control}>
          <TouchableOpacity 
            style={styles.touchLeft}
            onPress={props.showPreviousStory} />
          <TouchableOpacity 
            style={styles.touchRight}
            onPress={props.showNextStory} />
        </View>

        <View style={styles.bodyContainer}>
          <StoryCardTitle
            text={titleData.text.text}
            textColor={titleData.text.color}
            iconURI={titleData.icon?.uri}
          />
          <StoryCardParagraphs paragraphsData={paragraphsData}/>
        </View>
      </StoryCardBackground>
    </>
  );
};

const styles = StyleSheet.create({
  control: {
    flexDirection: 'row',
    position: 'absolute',
    height: '100%',
    width: '100%',
    zIndex: 2,
  },
  touchLeft: {
    width: '50%',
    height: '100%',
  },
  touchRight: {
    width: '50%',
    height: '100%',
  },
  bodyContainer: {
    paddingHorizontal: 20,
  }, 
});

export default StoryCard;
