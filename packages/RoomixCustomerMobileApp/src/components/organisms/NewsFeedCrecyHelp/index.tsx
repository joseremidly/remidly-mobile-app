// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
import NewsFeedCard from 'src/components/molecules/NewsFeedCard';
import NewFeedCardCredit from 'src/components/molecules/NewFeedCardCredit';
import LoadingView from 'src/components/organisms/LoadingView';
import CardAddGigAccount from 'src/components/molecules/CardAddGigAccount';

export interface Props {
  goRoute: Function;
  creditLineStatus: any;
  loansData: any;
}

const NewsFeedCrecyHelp: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containerNewsFeedCrecyHelp}>
        <Text style={styles.textNewsFeedCrecyHelp}>CRECY CRÉDITO</Text>
        {!props.creditLineStatus.type
          && (
            <LoadingView />
          )}
        {props.creditLineStatus.type === 'credit_line_not_found'
          && (
            <NewsFeedCard onPress={() => props.goRoute('CreditOnboardingStart')} header='Línea de crédito básica' information='Conecta tu cuenta bancaria y habilita una línea de crédito de hasta $1,000' buttonName='Solicitar' src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1600413118/app/crecy_icons_bag_with_money_i9zibo.png' colorCard='#7b3eff' type='1' />
          )}
        {props.creditLineStatus.type === 'general_credit' && props.creditLineStatus.amount === props.creditLineStatus.available_amount
          && (
            <NewFeedCardCredit
              onPress={() => props.goRoute('CreditLine')}
              onConnectPlatform={() => props.goRoute('PalencaLinkAccount', { nextStep: 'home' })}
              buttonName='Retirar'
              src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1600413118/app/crecy_icons_bag_with_money_i9zibo.png'
              type={true}
              creditAmount={props.creditLineStatus.status === 'approved' ? props.creditLineStatus.amount : '0'}
              availableAmount='0'
              maxDaysToPay={15}
              status={props.creditLineStatus.status}
            />
          )}
        {props.creditLineStatus.type === 'general_credit' && props.creditLineStatus.status === 'approved' && props.creditLineStatus.amount !== props.creditLineStatus.available_amount && props.loansData !== ''
          && (
            <NewFeedCardCredit
              onPress={() => props.goRoute('CreditLine')}
              buttonName='Retirar'
              src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1600413118/app/crecy_icons_bag_with_money_i9zibo.png'
              type={false}
              creditAmount={props.creditLineStatus.amount}
              availableAmount={props.creditLineStatus.available_amount}
              maxDaysToPay={15}
              stateLoans={props.stateLoans}
              loansData={props.loansData}
            />
          )}
        <Text style={styles.textNewsFeedCrecyHelpProx}>NOTICIAS</Text>
        <CardAddGigAccount 
          onPress={() => props.goRoute('PalencaLinkAccount', { nextStep: 'home' })} 
          header='Conecta más plataformas'
          information='Aumenta la probabilidad de ser aprobado o mejora tu línea de crédito Crecy'
          buttonName='Conectar' 
          src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1625465929/app/Crecy%20icons%20new%20dimensions/Crecy_invitation_web_page/Crecy_app_add_more_gig_accounts_card_1_iwogi8.png'
          colorCard='#2FA9F5'
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerNewsFeedCrecyHelp: {
    alignItems: 'center',
    marginHorizontal: 20,
    marginBottom: 30,
  },
  textNewsFeedCrecyHelp: {
    width: '100%',
    flexDirection: 'row',
    fontFamily: 'Baloo2-SemiBold',
    textAlign: 'left',
    alignSelf: 'stretch',
    color: '#9AA6A7',
    fontSize: 14,
    lineHeight: 27,
    marginBottom: -4,
    marginLeft: 9
  },
  textNewsFeedCrecyHelpProx: {
    width: '100%',
    flexDirection: 'row',
    fontFamily: 'Baloo2-SemiBold',
    textAlign: 'left',
    alignSelf: 'stretch',
    color: '#9AA6A7',
    fontSize: 14,
    lineHeight: 27,
    marginTop: 15,
    marginBottom: -3,
    marginLeft: 9
  },
});

export default NewsFeedCrecyHelp;
