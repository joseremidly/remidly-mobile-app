// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
	View,
} from 'react-native';
import AdvanceChoose from 'src/components/molecules/AdvanceChoose';

export interface Props {
	type: boolean;
  onRequestLoan: Function;
  creditLineStatus: any;
  interest: any;
}

const LoanSelectAmount: React.FC<Props> = (props: any) => {
  const [valueSlider, setValueSlider] = React.useState(10);
  const changeValueSlider = (newValue: any) => {
    setValueSlider(parseInt(newValue))
  }
  const requestLoan = () => {
    props.onRequestLoan(valueSlider * 10)
  }
  return (
    <View style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
      <AdvanceChoose
        type={false}
        onPress={() => requestLoan()}
        values={valueSlider}
        interest={props.interest}
        maxValue={props.creditLineStatus.available_amount / 10}
        changeValue={(value: any) => changeValueSlider(value)}
        src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599599220/app/crecy_panda_circle_profile_logo_ew0djz.png'
        srcChat='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1603150339/app/Crecy%20icons%20new%20dimensions/Crecy_icons_bubble-03_mbe3dw.png'
      ></AdvanceChoose>
    </View>
  );
};

const styles = StyleSheet.create({
  sectionPaycheckContainer: {
    backgroundColor: '#F4F4F7',
    width: '100%',
  },
  sectionContainer: {
    backgroundColor: '#F4F4F7',
    width: '100%',
	},
	containerText: {
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'flex-start',
		width: '90%',
		marginTop: 40
	},
  textChooseDeliveryMethod: {
    flexDirection: 'row',
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'left',
    alignSelf: 'stretch',
    color: '#5a6175',
    fontSize: 14,
    lineHeight: 24,
  },
  textInfo: {
    flexDirection: 'row',
    fontFamily: 'AvenirLTStd-Medium',
    textAlign: 'center',
    alignSelf: 'stretch',
    color: 'white',
		fontSize: 14,
		marginVertical: 5,
	},
	texUnderlineDecoration: {
    fontFamily: 'AvenirLTStd-Light',
    textAlign: 'center',
    color: '#7b3eff',
    fontSize: 13,
		textDecorationLine: 'underline'
	},
	containerTextDecoration: {
		justifyContent: 'flex-start',
		alignItems: 'flex-start',
		width: '90%',
	},
  spaceTestDelivery: {
    flexDirection: 'row',
		marginTop: 20,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerDelivery: {
    width: '40%',
    marginHorizontal: 10,
  },
  spaceTest: {
    marginTop: 50,
    marginBottom: 20,
		justifyContent: 'center',
		alignItems: 'center'
  },
});

export default LoanSelectAmount;