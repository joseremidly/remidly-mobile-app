import React from 'react';
import {
    StyleSheet,
    Image,
    Text,
		View,
		Dimensions,
} from 'react-native';
import Process from '../../atoms/PaycheckProcess'

export interface Props {
	onPress: Function;
	type: string;
	status: any;
}

const PaycheckFinish: React.FC<Props> = (props) => {
	const {width, height} = Dimensions.get('window');
  return (
    <>
      <View style={styles(height).principalContainer}>
				<View style={{justifyContent: 'center', alignItems: 'center',}}>
					<View style={styles(width).containerText} >
						<Text style={styles(props).textHeader}>(application process)</Text>
					</View>
					<View style={{marginVertical: 20}}>
						<Process
              type={props.type}
              status={props.status}
              width={width}
            />
          </View>
				</View>
				<View style={{width: '100%', marginTop: 30}}>
					<Image source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599533279/app/crecy-login_hlr5pj.png' }} style={styles(height).iconUpdatedDate} />
				</View>
				<View style={{justifyContent: 'center', alignItems: 'center',}}>
					<View style={styles(width).containerTitleParagraph} >
						<Text style={styles(props).textStyle}>¡Casi estamos listos!</Text>
					</View>
					<View style={styles(width).containerSecondParagraph} >
						<Text style={styles(props).textParagraph}>
							Por favor, danos algo de tiempo para procesar tu información
							y verificar si eres elegible para usar nuestro servicio de micro ayudas.
						</Text>
					</View>
					<View style={{marginVertical: width <= 380 ? 20 : 60,}}>
						<View style={styles(width).containerSecondParagraph} >
							<Text style={styles(props).textMediumStyle}>
								Puedes
								<Text style={styles(props).textStrong}>{' '}ver el progreso</Text>
								{' '}de tu aplicación
								<Text style={styles(props).textStrong}>{' '}en esta sección</Text>
							</Text>
						</View>
						<View style={styles(width).containerSecondParagraph} >
							<Text style={styles(props).textStrongIcon}>&</Text>
						</View>
						<View style={styles(width).containerSecondParagraph} >
							<Text style={styles(props).textMediumStyle}>
								Además nosotros
								<Text style={styles(props).textStrong}>
									{' '}the notificaremos acerca del progreso de tu aplicación vía SMS
								</Text>
							</Text>
						</View>
					</View>
				</View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  principalContainer: {
		width: '100%',
    justifyContent: 'center',
		alignItems: 'center',
  },
  containerText: {
    justifyContent: 'center',
    alignItems: 'center',
		flexDirection: 'row',
		marginHorizontal: props <= 380 ? 30 : 50,
		marginTop: props <= 380 ? 50 : 0,
  },
  containerTitleParagraph: {
    justifyContent: 'center',
    alignItems: 'center',
		flexDirection: 'row',
    marginHorizontal: 5,
    marginBottom: 10,
  },
  containerSecondParagraph: {
    justifyContent: 'center',
    alignItems: 'center',
		flexDirection: 'row',
		marginHorizontal: props <= 380 ? 50 : 60
	},
  textHeader: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 16,
		color: '#5a6175',
		textAlign: 'center',
  },
  textParagraph: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 20,
    lineHeight: 25,
		color: '#5a6175',
		textAlign: 'center',
  },
  textMediumStyle: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 16,
		color: '#5a6175',
		textAlign: 'center'
	},
	textStrong: {
		fontFamily: 'Baloo2-Bold',
    fontSize: 16,
		color: '#5a6175',
		textAlign: 'center',
		lineHeight: 18,
	},
	textStrongIcon: {
		fontFamily: 'Baloo2-Bold',
    fontSize: 18,
		color: '#5a6175',
		textAlign: 'center',
	},
  textStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
		color: '#7b3eff',
		textAlign: 'center',
  },
  textMoreButton: {
    fontFamily: 'Baloo2-Bold',
    textTransform: 'uppercase',
    fontSize: 16,
    color: '#b3b3b3',
    marginVertical: 10,
    marginHorizontal: 16
  },
  containerButtonPress: {
    backgroundColor: '#E7E6FF',
    borderRadius: 50,
	},
	iconUpdatedDate: {
    width: 120,
		height: 140,
		left: 0,
	},
	containerButton: {
		justifyContent: 'center',
		alignItems: 'center',
		marginVertical: props <= 680 ? 30 : 50,
	},
	buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 260,
    height: 44,
    backgroundColor: '#49c0aa',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#49c0aa',
    borderRadius: 20,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 22,
    color: '#ffffff',
	},
	containerRequeriments: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  textRequeriments: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 15,
    color: '#5a6175',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  iconRequeriments: {
    width: 20,
    height: 20,
    marginRight: 10
  },
});

export default PaycheckFinish;