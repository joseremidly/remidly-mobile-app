// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
} from 'react-native';
import institutions from 'src/utils/institutions';
import AccountCardElement from 'src/components/molecules/AccountCardElement';

export interface Props {
  accounts: any;
  goToConnect: Function;
  onSelectAccount: Function;
}

const LinkedAccountsElements: React.FC<Props> = (props: any) => {
  const Accounts = props.accounts.map((account, index) =>
    <View style={styles.containerLinkedAccountsElements} key={index}>
      <AccountCardElement
        accountNumber={account.public_identification_value.replace(/\s/g, '')}
        color={`#ffffff`}
        background={institutions[account.institution.name] === undefined ? institutions['default'].primary_color : institutions[account.institution.name].primary_color}
        icon={institutions[account.institution.name] === undefined ? institutions['default'].logo : institutions[account.institution.name].logo}
        nameBank={account.name}
        onSelectAccount={props.onSelectAccount}
      />
    </View>
  );
  return (
    <>
      <View style={styles.containerLinkedAccountsOrganisms} >
        {Accounts}
        <TouchableOpacity style={styles.buttonAddLinkedAccounts} onPress={props.goToConnect}>
          <Image style={styles.iconAddLinkedAccounts} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1601939661/app/add_2_ycwugv.png'}}></Image>
          <Text style={styles.textAddLinkedAccounts}>AGREGAR OTRA CUENTA</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerLinkedAccountsOrganisms: {
    width: '100%',
    alignItems: 'center',
  },
  containerLinkedAccountsElements: {
    width: '90%',
  },
  buttonAddLinkedAccounts: {
    width: '90%',
    flexDirection: 'row',
    height: 60,
    backgroundColor: '#e6e6e6',
    borderRadius: 9,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
  },
  iconAddLinkedAccounts: {
    height: 12,
    width: 12,
    marginHorizontal: 10,
    marginBottom: 3,
  },
  textAddLinkedAccounts: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    color: '#5a6175',
    textAlign: 'left',
  },
});

export default LinkedAccountsElements;
