// @ts-nocheck
import React from 'react';
import { StyleSheet } from 'react-native';
import CreditOnboardingCardConnect from 'src/components/molecules/CreditOnboardingCardConnect';

export interface Props {
  goToConnect: Function;
}

const CreditOnboardingGig: React.FC<Props> = (props: any) => {
  return (
    <>
      <CreditOnboardingCardConnect
        headerTextOne="Por favor,"
        headerTextTwo="de Rappi, Uber u otra plataforma"
        headerTextHighlighted="conecta tu cuenta"
        infoText="Esto nos permitirá revisar tus ingresos y tu comportamiento dentro de la plataforma. De acuerdo a estas variables tomaremos la decisión de aprobación de tu línea de crédito"
        imageURI="https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833466/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_uber_rappi_credit_request_process_gdhtbd.png"
        goToConnect={props.goToConnect}
      />
    </>
  );
};

const styles = StyleSheet.create({});

export default CreditOnboardingGig;
