// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
} from 'react-native';
import DebtsOXXO from 'src/components/molecules/DebtsOXXO'

export interface Props {
  status: string;
  reference: any;
  loan: any;
}

const DebtsPaymentOxxo: React.FC<Props> = (props) => {
  React.useEffect(() => {
    console.log(props.loan)
  }, [])
  return (
    <>
      <View style={styles(props).container}>
        <DebtsOXXO
          status={props.status}
          reference={props.reference}
          loan={props.loan}
        />
      </View>
      <View style={{width: '85%',display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginTop: 35,}}>
        <View style={styles(props).containerNumber}>
          <Text style={styles(props).textDebtNumberInformation}>1</Text>
        </View>
        <View>
          <Text style={styles(props).textDebtInformationHeavy}>
            <Text style={styles(props).textDebBlack}>Acude a la tienda </Text>
            OXXO más cercana
          </Text>
        </View>
      </View>
      <View style={{width: '85%',display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 25,}}>
        <View style={styles(props).containerNumber}>
          <Text style={styles(props).textDebtNumberInformation}>2</Text>
        </View>
        <View>
          <Text style={styles(props).textDebtInformationHeavy}>
            Indica en la caja que quieres 
            <Text style={styles(props).textDebBlack}> realizar un pago de OXXOPay </Text>
          </Text>
        </View>
      </View>
      <View style={{width: '85%',display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 25,}}>
        <View style={styles(props).containerNumber}>
          <Text style={styles(props).textDebtNumberInformation}>3</Text>
        </View>
        <View>
          <Text style={styles(props).textDebtInformationHeavy}> 
            <Text style={styles(props).textDebBlack}>Dicta al cajero el número de referencia </Text>
            que está arriba para que tecleé directamente en la pantalla de la venta o 
            <Text style={styles(props).textDebBlack}> muestralé el código de barras</Text>
          </Text>
        </View>
      </View>
      <View style={{width: '85%',display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 25,}}>
        <View style={styles(props).containerNumber}>
          <Text style={styles(props).textDebtNumberInformation}>4</Text>
        </View>
        <View>
          <Text style={styles(props).textDebtInformationHeavy}>
            <Text style={styles(props).textDebBlack}>Realiza el pago correspondiente </Text>
            con dinero en efectivo. Considera que tienes que pagar el monto de tu deuda más la comisión que te cobra el OXXO
          </Text>
        </View>
      </View>
      <View style={{width: '85%',display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 25,}}>
        <View style={styles(props).containerNumber}>
          <Text style={styles(props).textDebtNumberInformation}>5</Text>
        </View>
        <View>
          <Text style={styles(props).textDebtInformationHeavy}>
            <Text style={styles(props).textDebBlack}>
              Al confirmar tu pago, el cejaro te entregará un comprobante impreso.
            </Text>
            {' '}En el podrás verificar que se haya realizado correctamente. Crecy detectará tu pago automáticamente
            pero, por favor, conserva este comprobante de pago para futuras aclaraciones.
          </Text>
        </View>
      </View>
      <View style={{justifyContent: 'center', alignItems: 'center', marginVertical: 45 }}>
        <Text style={styles(props).textRecomendation}>CUANDO DETECTEMOS TU PAGO</Text>
        <Text style={styles(props).textRecomendationInformation}>LA DEUDA APARECERÁ EN LA SECCIÓN "HISTORIAL" Y CAMBIARA SU ESTATÚS</Text>
        <View style={{flexDirection:'row', marginTop: 10, justifyContent: 'center', alignItems: 'center'}}>
          <View style={{backgroundColor: `${props.loan.status === 'expired' || props.loan.status === 'rejected' ? '#ed1e79' : '#FBB03B'}`, borderRadius: 3, marginHorizontal: 5, justifyContent: 'center', alignItems: 'center', paddingVertical: 2}} >
            <Text style={styles(props).textExpired}>
              <Text style={styles(props).textAprove}>DE: </Text>
              {props.loan.status === 'rejected' || props.loan.status === 'expired' ? 'PAGO VENCIDO' : 'PAGO PENDIENTE'}
            </Text>
          </View>
          <Image style={styles(props).iconArrow} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/a_0180/v1609833887/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_grey_arrow_ij4vl7.png'}}></Image>
          <View style={{backgroundColor: '#49c0aa', borderRadius: 3 ,marginHorizontal: 5, justifyContent: 'center', alignItems: 'center', paddingVertical: 2}}>
            <Text style={styles(props).textExpired}>
              <Text style={styles(props).textAprove}>A: </Text>
              DEUDA PAGADA
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 12,
  },
  textRecomendation: {
    fontFamily: 'Avenir-Black',
    fontSize: 16,
    color: '#5a6175',
    textAlign: 'left',
  },
  textDebBlack: {
    fontFamily: 'Avenir-Black',
    fontSize: 13,
    color: '#5a6175',
    lineHeight: 15,
    borderRadius: 50,
    marginHorizontal: 5,
    marginVertical: 2,
  },
  textRecomendationInformation: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 15,
    color: '#5a6175',
    textAlign: 'center',
  },
  textDebtInformationHeavy: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    color: '#5a6175',
    textAlign: 'left',
  },
  textDebtNumberInformation: {
    fontFamily: 'Avenir-Black',
    fontSize: 12,
    color: 'white',
    lineHeight: 15,
    borderRadius: 50,
    marginHorizontal: 6,
    marginVertical: 2,
  },
  containerNumber: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#7b3eff',
    borderRadius: 12,
    marginRight: 15
  },
  iconArrow: {
    width: 15,
    height: 15,
  },
  textExpired: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    color: 'white',
    textAlign: 'left',
    marginHorizontal: 5
  },
  textAprove: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    color: 'white',
    textAlign: 'left',
  },
  
});

export default DebtsPaymentOxxo;