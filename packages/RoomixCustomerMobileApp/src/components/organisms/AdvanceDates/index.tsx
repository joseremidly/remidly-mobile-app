import React from 'react';

import { StyleSheet, View } from 'react-native';
import AdvanceDate from 'src/components/molecules/AdvanceDate';

export interface Props {
  dateDepositText: string;
  dateChargeText: string;
}

const AdvanceDates: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containter}>
        <AdvanceDate isDeposit={true} dateText={props.dateDepositText} />
        <View style={styles.verticalSeparator} />
        <AdvanceDate isDeposit={false} dateText={props.dateChargeText} />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containter: {
    marginTop: 26,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderStyle: 'solid',
    borderColor: '#999',
    paddingBottom: 20,
  },
  verticalSeparator: {
    borderStyle: 'solid',
    borderColor: '#999',
    height: '100%',
    borderLeftWidth: StyleSheet.hairlineWidth,
  },
});

export default AdvanceDates;
