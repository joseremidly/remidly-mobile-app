// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  Text,
} from 'react-native';

export interface Props {
  return: Function;
  goBudget: Function;
}
const TopCategories: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containerTopCategories}>
        <View style={styles.headerTopCategories}>
          <TouchableOpacity style={styles.buttonReturn} onPress={props.return}>
            <Image style={styles.iconButtonReturn} source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599596734/app/crecy_icons_arrow_header_white-01_dg8lby.png' }} />
          </TouchableOpacity>
          <View style={styles.containerTitleCategories}>
            <Image style={styles.iconTitleCategories} source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1600114213/app/crecy_icons_groceries_white_xnf4ff.png' }} />
            <Text style={styles.titleCategories}>Transacciones</Text>
          </View>
          <TouchableOpacity style={styles.buttonFilterCategories} onPress={props.addAccount}>
            <Text style={styles.textFilterCategories}>Mensual</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.dateCategories}>01 Agosto - 30 Agosto</Text>
        <View style={styles.containerMoneySpent}>
          <Text style={styles.textTitleMoneySpent}>Gastos totales</Text>
          <Text style={styles.numberTitleMoneySpent}>$2,520.
            <Text style={styles.decimalsTitleMoneySpent}> 39</Text>
          </Text>
          <TouchableOpacity style={styles.buttonGoBudget} onPress={props.goBudget}>
            <Text style={styles.textButtonGoBudget}>DE $3,000 MXN DE TU META</Text>
            <Image style={styles.iconButtonGoBudget} source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599250202/app/back_rzgh1o.png' }} />
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerTopCategories: {
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: '#7b3eff',
    width: '100%',
  },
  headerTopCategories: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
  },
  iconButtonReturn: {
    width: 20,
    height: 20,
    marginLeft: 10,
  },
  containerTitleCategories: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconTitleCategories: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  titleCategories: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: '#ffffff',
  },
  buttonFilterCategories: {
    width: 80,
    height: 25,
    backgroundColor: 'white',
    borderRadius: 4,
    marginRight: 10,
  },
  textFilterCategories: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 18,
    lineHeight: 28,
    textAlign: 'center',
    color: '#7b3eff',
  },
  dateCategories: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 24,
    lineHeight: 30,
    marginVertical: 30,
    textAlign: 'center',
    color: '#ffffff',
  },
  containerMoneySpent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  textTitleMoneySpent: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 24,
    color: 'white',
  },
  numberTitleMoneySpent: {
    color: '#ffffff',
    fontFamily: 'Baloo2-Regular',
    fontSize: 44,
    lineHeight: 60,
  },
  decimalsTitleMoneySpent: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 24,
  },
  buttonGoBudget: {
    height: 30,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#49c0aa',
    borderRadius: 2,
    marginRight: 10,
    marginBottom: 30,
    paddingHorizontal: 5,
  },
  textButtonGoBudget: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 18,
    color: 'white',
  },
  iconButtonGoBudget: {
    width: 15,
    height: 10,
    marginLeft: 5,
  }
});

export default TopCategories;
