// @ts-nocheck
import React, { useRef } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  PermissionsAndroid,
  Platform,
} from 'react-native';
// import * as ImagePicker from 'react-native-image-picker';
import { RNCamera } from 'react-native-camera';
import { useCamera } from 'react-native-camera-hooks';
import CameraID from 'src/components/molecules/CameraID';

const UploadIDOrganisms: React.FC<Props> = (props: any) => {
  const [openCamera, setOpenCamera] = React.useState(false);
  const [photoFront, setPhotoFront] = React.useState(false);
  const [photoBack, setPhotoBack] = React.useState(false);
  const [front, setFront] = React.useState();
  const [back, setBack] = React.useState();
  const [
    { cameraRef, type, ratio, autoFocus, autoFocusPoint, isRecording },
    {
      toggleFacing,
      touchToFocus,
      textRecognized,
      facesDetected,
      takePicture,
      recordVideo,
      setIsRecording,
    },
  ] = useCamera();
  const takePicturex = async () => {
    try {
      const checking = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA)
      console.log('Permission status: ' + checking)
      const options = { quality: 0.5, base64: true };
      const data = await cameraRef.current.takePictureAsync(options);
      console.log(data.uri);
    } catch (err) {
      console.warn(err)
    }
  }
  // const takePhoto = () => {
  //   ImagePicker.launchCamera(
  //     {
  //       mediaType: 'photo',
  //       includeBase64: false,
  //       maxHeight: 450,
  //       maxWidth: 150,
  //     },
  //     (response) => {
  //       console.log(response.uri)
  //       setPhotoFront(true)
  //       setFront(response.uri)
  //     },
  //   )
  // }
  // const photoTestBack = () => {
  //   ImagePicker.launchCamera(
  //     {
  //       mediaType: 'photo',
  //       includeBase64: false,
  //       maxHeight: 200,
  //       maxWidth: 100,
  //     },
  //     (response) => {
  //       console.log(response.uri);
  //       setPhotoBack(true)
  //       setBack(response.uri)
  //     },
  //   )
  // }
  // const checkPermissions = async () => {
  //   if (Platform.OS  === 'android') {
  //     const checking = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA)
  //     console.log('Permission status: ' + checking)
  //     if (checking) {
  //       takePhoto()
  //     } else {
  //       try {
  //         const granted = await PermissionsAndroid.request(
  //           PermissionsAndroid.PERMISSIONS.CAMERA,
  //           {
  //             title: "Camera Permission",
  //             message: "Crecy App needs access to your camera ",
  //             buttonNeutral: "Ask Me Later",
  //             buttonNegative: "Cancel",
  //             buttonPositive: "OK"
  //           }
  //         )
  //         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //           takePhoto()
  //         } else {
  //           console.log("Camera permission denied")
  //         }
  //       } catch (err) {
  //         console.warn(err)
  //       }
  //     }
  //   }
  // }
  const goCameraId = () => {
    console.log('Opening')
    setOpenCamera(true)
  }
  const PendingView = () => (
    <View
      style={{
        flex: 1,
        backgroundColor: 'lightgreen',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Text>Waiting</Text>
    </View>
  );
  return (
    <View style={styles.sectionContainer}>
      <Image style={styles.imgHeader} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1610351789/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_icon_KYC_new_degljm.png'}}/>
      <Text style={styles.headerText}>Ayúdanos por favor con un par de fotos</Text>
      {/* <RNCamera
        ref={cameraRef}
        style={{ flex: 1 }}
      >
        {({ camera, status, recordAudioPermissionStatus }) => {
            if (status !== 'READY') return <PendingView />;
            return (
              <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                <TouchableOpacity onPress={() => this.takePicture(camera)} style={styles.capture}>
                  <Text style={{ fontSize: 14 }}> SNAP </Text>
                </TouchableOpacity>
              </View>
            );
          }}
      </RNCamera> */}
      {openCamera
        && (
          <CameraID></CameraID>
        )}
      <TouchableOpacity style={styles.buttonInitial} onPress={goCameraId}>
        <Image></Image>
        <Text style={styles.textButtonInitial}>Parte frontal de tu indentificación</Text>
        <Text style={styles.textButtonIcon}>+</Text>
      </TouchableOpacity>
      {/* {photoFront ? 
        (
          <View style={styles.containerID}>
            <Image
              source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833888/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_done_th7phc.png'}}
              style={styles.imgAbsolute}
            />
            <Image
              source={{uri: front}}
              style={{width: 150, height: 250, borderRadius: 13, transform: [{ rotate: '90deg'}]}}
            />
            <TouchableOpacity style={styles.buttonStyle}  onPress={takePicture}>
              <Text style={styles.textButtonStyle}>Tomar otra</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <TouchableOpacity style={styles.buttonInitial} onPress={takePicture}>
            <Image></Image>
            <Text style={styles.textButtonInitial}>Parte frontal de tu indentificación</Text>
            <Text style={styles.textButtonIcon}>+</Text>
          </TouchableOpacity>
        )} */}
      {/* {photoBack ?
        (
          <View style={styles.containerID}>
            <Image
              source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833888/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_done_th7phc.png'}}
              style={styles.imgAbsolute}
            />
            <Image
              source={{uri: back}}
              style={{width: 150, height: 250, borderRadius: 13, transform: [{ rotate: '90deg'}]}}
            />
            <TouchableOpacity style={styles.buttonStyle} onPress={photoTestBack}>
              <Text style={styles.textButtonStyle}>Tomar otra</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <TouchableOpacity style={styles.buttonInitial} onPress={photoTestBack}>
            <Image></Image>
            <Text style={styles.textButtonInitial}>El reverso de tu indentificación</Text>
            <Text style={styles.textButtonIcon}>+</Text>
          </TouchableOpacity>
        )}
      {photoBack && photoFront ?
        <TouchableOpacity style={styles.buttonContinue} onPress={() => console.log('continue')}>
          <Text style={styles.textButtonStyle}>Continuar</Text>
        </TouchableOpacity>
        :
        <>
        </>
      } */}
    </View>
  );
};

const styles = StyleSheet.create({
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  sectionContainer: {
    backgroundColor: '#F4F4F7',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20
  },
  imgHeader: {
    width: 130,
    height: 70,
    marginBottom: 20
  },
  headerText: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 25,
    lineHeight: 32,
    color: '#5a6175',
    marginBottom: 20,
    marginHorizontal: 50,
    textAlign: 'center'
  },
  informationText: {
    width: '80%',
    fontFamily: 'Baloo2-Medium',
    fontSize: 18,
    color: '#5a6175',
    marginBottom: 25,
    marginHorizontal: 55,
    textAlign: 'left',
    lineHeight: 25
  },
  buttonStyle: {
    zIndex: 15,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 44,
    backgroundColor: '#7b3eff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#7b3eff',
    borderRadius: 20,
    marginHorizontal: 60,
    marginVertical: 10,
  },
  buttonContinue: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 44,
    backgroundColor: '#7b3eff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#7b3eff',
    borderRadius: 20,
    marginHorizontal: 60,
    marginVertical: 10,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 18,
    color: '#ffffff',
  },
  buttonInitial: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: 300,
    height: 64,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderWidth: 2,
    borderColor: '#7b3eff',
    borderRadius: 20,
    marginHorizontal: 60,
    marginVertical: 20,
  },
  textButtonInitial: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 15,
    color: '#7b3eff',
  },
  textButtonIcon: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 24,
    color: '#7b3eff',
  },
  imgAbsolute: {
    zIndex: 15,
    width: 40,
    height: 40,
    right: -15,
    top: -15,
    position: 'absolute'
  },
  containerID: {
    width: 260,
    height: 160,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#67CC5C',
    borderWidth: 3,
    borderRadius: 14,
    marginBottom: 20,
  },
});

export default UploadIDOrganisms;