// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
import Moment from 'moment';
import LoadingView from 'src/components/organisms/LoadingView';
import ButtonSettings from 'src/components/molecules/ButtonSettings';

export interface Props {
  navigation: Object;
  dataUser: Object;
  goRoute: Function;
  stateService: String;
}

const EditProfileButtons: React.FC<Props> = (props: any) => {
  const getBirthday = (date: Date) => {
    // Moment.locale('es');
    return Moment(date).format('l')
  }
  return (
    <>
      <View style={styles.containerEditProfileElements} >
        {props.stateService === 'loading'
            && (
              <LoadingView />
            )}
        {props.stateService !== 'loading'
          && (
            <View style={styles.containerEditProfileButtonsSettings}>
              <ButtonSettings src={require('assets/images/settings-icons/edit_icons_profile.png')} size={28} text={props.dataUser.name !== '' ? props.dataUser.name + ' ' + props.dataUser.last_name : 'Nombre'} route='EditName' onClick={(route: any) => props.goRoute(route)} />
              <ButtonSettings src={require('assets/images/settings-icons/edit_icons_profile_email.png')} size={28} text={props.dataUser.email !== '' ? props.dataUser.email : 'Correo electrónico'} route='EditEmail' onClick={(route: any) => props.goRoute(route)} />
              <ButtonSettings src={require('assets/images/settings-icons/edit_icons_profile_gender.png')} size={28} text={props.dataUser.gender !== '' ? props.dataUser.gender : 'Género'} route='EditGender' onClick={(route: any) => props.goRoute(route)} />
              <ButtonSettings src={require('assets/images/settings-icons/edit_icons_profile_calendar.png')} size={28} text={props.dataUser.birthday !== '' || props.dataUser.birthday !== undefined ? `${getBirthday(props.dataUser.birthday)}` : 'Fecha de nacimiento'} route='EditBirthday' onClick={(route: any) => props.goRoute(route)} />
              {/* <ButtonSettings src={require('assets/images/settings-icons/edit_icons_profile_location.png')} size={28} text={props.dataUser.location} route='Home' onClick={(route: any) => props.goRoute(route)} /> */}
              <ButtonSettings src={require('assets/images/settings-icons/edit_icons_profile_mobile_phone.png')} size={28} text={props.dataUser.phone_number} route='Home' onClick={(route: any) => props.goRoute(route)} />
            </View>
          )}
        <TouchableOpacity style={styles.buttonDeleteAccount} onPress={() => props.goRoute('deleteAccount')}>
          <Text style={styles.buttonDeleteAccountText}>Borrar mi cuenta</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerEditProfileElements: {
    backgroundColor: '#F4F4F7',
    width: '100%',
    alignItems: 'center',
  },
  containerEditProfileButtonsSettings: {
    width: '75%',
    marginTop: 20,
  },
  buttonDeleteAccount: {
    width: '70%',
    height: 40,
    borderColor: '#ed1e79',
    borderWidth: 1,
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 50,
  },
  buttonDeleteAccountText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 21,
    color: '#ed1e79',
    textAlign: 'left',
    marginVertical: 5,
  },
});

export default EditProfileButtons;
