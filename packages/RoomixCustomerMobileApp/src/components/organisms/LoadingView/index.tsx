import React from 'react';
import {
    StyleSheet,
    Platform,
    View,
    ActivityIndicator,
} from 'react-native';

const LoadingView: React.FC = () => {
  return (
    <>
      <View style={styles.loadingViewStyle}>
        <ActivityIndicator size='large' color='#7b3eff' animating={true} />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  loadingViewStyle: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    height: '100%',
    paddingTop: Platform.select({
      ios: 25,
      android: 0
    }),
  }
});

export default LoadingView;
