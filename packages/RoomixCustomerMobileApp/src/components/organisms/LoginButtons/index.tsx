import React from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
// @ts-ignore
import ButtonLink from 'src/components/atoms/ButtonLink';
export interface Props {
  goLogin: Function;
  goCreate: Function;
}
const LoginButtons: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containerLoginButtons}>
        <Text style={styles.titleRemidlyLogin}>Remidly.</Text>
        <View style={styles.spaceTest} >
          <ButtonLink label='Iniciar sesión' onPress={props.goLogin}></ButtonLink>
        </View>
        <View style={styles.spaceTest} >
          <ButtonLink label='Crear nueva cuenta' secondary={true} onPress={props.goCreate}></ButtonLink>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerLoginButtons: {
    alignItems: 'center',
    marginTop: 100,
  },
  titleRemidlyLogin: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 45,
    color: '#5a6175',
    marginBottom: 15,
  },
  spaceTest: {
    marginBottom: 20,
  },
});

export default LoginButtons;
