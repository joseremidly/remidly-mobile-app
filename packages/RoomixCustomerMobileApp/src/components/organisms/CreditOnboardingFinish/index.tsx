// @ts-nocheck
import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  showError: boolean;
  headerTextOne: string;
  headerTextTwo: string;
  headerTextHighlighted: string;
  infoText: string;
  imageURI: string;
  buttonText: string;
  goToHome: Function;
}

const CreditOnboardingFinish: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containter}>
        <View style={styles.imageContainer}>
          <Image style={styles.mainImage} source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1611600042/app/Crecy%20icons%20new%20dimensions/Crecy%20web%20page/Crecy_web_page_crecy_panda_kuxbsw.png' }} />
        </View>
        <Text style={styles.headerText}>
          ¡Listo!
        </Text>
        <Text style={styles.mainText}>
          En menos de 24 horas te avisaremos si eres eligible para la línea de crédito Crecy.
          ¡Mientras tanto disfruta de otras funcionalidades de la app!
        </Text>
        <View style={styles.buttonContainer}>
          <ButtonMid
            label='Terminar'
            secondary={false}
            onPress={props.goToHome}
          />
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containter: {
    alignItems: 'center',
    flex: 1
  },
  imageContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flex: 1,
    width: '100%'
  },
  mainImage: {
    marginTop: 20,
    width: 125,
    height: 175,
    resizeMode: 'contain',
  },
  headerText: {
    paddingHorizontal: 40,
    marginBottom: 20,
    textAlign: 'center',
    fontFamily: 'Baloo2-Bold',
    // fontWeight: 'bold',
    fontSize: 36,
    color: '#5a6175',
  },
  mainText: {
    paddingHorizontal: 30,
    // marginTop: 20,
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'AvenirLTStd-Roman',
    color: '#5a6175',
  },
  buttonContainer: {
    marginTop: 40,
  },
});

export default CreditOnboardingFinish;
