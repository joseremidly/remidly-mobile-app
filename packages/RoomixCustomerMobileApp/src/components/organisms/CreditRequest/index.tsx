// @ts-nocheck
import React from 'react';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
} from 'react-native';

export interface Props {
  Information: boolean;
  onChangeInput: Function;
}

const InformationCredit: React.FC<Props> = (props: any) => {

  return (
    <>
      <View style={styles.container}>
        <View styles={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
          <Image style={styles.iconCredit} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833466/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_magic_card_l3cddn.png' }} resizeMode='contain'></Image>
        </View>
      </View>
      <View style={{backgroundColor: '#FDD25C', alignItems: 'center', width: '20%', marginHorizontal: 40, marginBottom: 30}}>
        <Text style={styles.headerText}>
          Pendiente
        </Text>
      </View>
      <Text style={styles.informationText}>
        Para que pueds solicitar tu primer retiro, por favor,  
        <Text style={styles.colorText}>
          {' '}agrega una tarjeta de débito a cual recibes tu nómina o ingresos de las plataformas
        </Text>
      </Text>
      <View style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity style={styles.buttonStyle}>
          <Text style={styles.textButtonStyle}>+ AGREGAR</Text>
        </TouchableOpacity>
      </View>
      <View style={{width: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: '#00B9DD0D', flexDirection: 'row', marginVertical: 40}}>
        <Image  style={styles.iconRequerimientos} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833887/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_question_mark_eezsyx.png' }} resizeMode='contain'></Image>
        <Text style={styles.informationHeaderText}>¿Por que te pedimos esto?{'\n'}
          <Text style={styles.informationBenefitsText}>
            En caso de no realizar el pago de tu deuda a tiempo nosotros 
            vamos a hacer el cargo de la deuda directamente a tu tarjeta de débito.{' '}
            Recuerda que cada semana de retraso en pago genera 5% de intereses moratorios
            </Text></Text>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerSecond: {
    width: '100%',
    marginVertical: 70,
  },
  headerText: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 16,
    color: '#ffffff',
    textAlign: 'left',
    lineHeight: 30,
  },
  colorText: {
    color: '#5a6175',
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 16,
    width: '80%'
  },
  iconCredit: {
    width: 150,
    height: 160,
    marginBottom: 20
  },
  informationText: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 20,
    color: '#5a6175',
    marginBottom: 25,
    marginHorizontal: 40,
    textAlign: 'left',
    lineHeight: 22
  },
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 140,
    height: 44,
    backgroundColor: '#e6e6e6',
    borderRadius: 6,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 18,
    color: '#5a6175',
  },
  iconRequerimientos: {
    width: 25,
    height: 25,
  },
  containerRequerimentos: {
    justifyContent: 'center',
    alignItems: 'center', 
    flexDirection: 'row',
    marginVertical: 30,
  },
  requerimientosText: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 16,
    color: '#6133E2',
    marginHorizontal: 5,
    textAlign: 'center',
  },
  containerCenterCircles: {
    justifyContent: 'center',
    alignItems: 'center', 
    flexDirection: 'row',
    marginVertical: 20,
  },
  containerCircle: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  outerCircle: {
    borderRadius: 40,
    width: 20,
    height: 20,
    backgroundColor: "white",
  },
  innerCircle: {
    borderRadius: 35,
    width: 12,
    height: 12,
    backgroundColor: "#7b3eff"
  },
  innerCircle2: {
    borderRadius: 35,
    width: 12,
    height: 12,
    backgroundColor: "#7b3effad"
  },
  informationHeaderText: {
    fontFamily: 'Baloo2-ExtraBold',
    width: '70%',
    fontSize: 16,
    color: '#5a6175',
    marginHorizontal: 30,
    textAlign: 'left',
    marginVertical: 20
  },
  informationBenefitsText: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 14,
    color: '#5a6175',
    marginHorizontal: 30,
    textAlign: 'left',
    lineHeight: 16
  },
});

export default InformationCredit;
