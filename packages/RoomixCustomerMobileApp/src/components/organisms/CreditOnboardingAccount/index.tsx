// @ts-nocheck 
import React from 'react';
import { StyleSheet } from 'react-native';
import CreditOnboardingCardConnect from 'src/components/molecules/CreditOnboardingCardConnect';
import CreditOnboardingSelectAccount from 'src/components/molecules/CreditOnboardingSelectAccount';
import LoadingView from 'src/components/organisms/LoadingView';

export interface Props {
  stateService: string;
  accounts: any;
  skip: Boolean;
  onSelectAccount: Function;
  goToConnect: Function;
  onSkip: Function;
}

const CreditOnboardingAccount: React.FC<Props> = (props: any) => {
  return (
    <>
      {props.stateService === 'loading' && props.accounts === ''
        && (
          <LoadingView />
        )}
      {props.accounts !== null && props.accounts !== ''
        ? (
          <CreditOnboardingSelectAccount
            accounts={props.accounts}
            headerTextOne="Por favor,"
            headerTextTwo="a la cual deseas que te depositemos tu línea de crédito"
            headerTextHighlighted="elige la cuenta bancaria"
            infoText="Esto nos permitirá saber la CLABE de tu cuenta bancaria, así depositaremos el dinero a tu cuenta en cuestión de minutos."
            goToConnect={props.goToConnect}
            onSelectAccount={props.onSelectAccount}
          />
        ) : (
          <CreditOnboardingCardConnect
            headerTextOne="Por favor,"
            headerTextTwo="en la cual recibes tu nómina o ingresos"
            headerTextHighlighted="conecta la cuenta bancaria"
            infoText="Realizaremos un balance de tus gastos e ingresos para considerar tu candidatura a la línea de crédito."
            infoText2="En caso de ser aprobado, el dinero de tu línea de crédito será depositado en esta cuenta bancaria."
            imageURI="https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1611043169/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_connect_a_bank_white_background_bqnn4k.png"
            skip={props.skip}
            onSkip={props.onSkip}
            goToConnect={props.goToConnect}
          />
        )}
    </>
  );
};

const styles = StyleSheet.create({});

export default CreditOnboardingAccount;
