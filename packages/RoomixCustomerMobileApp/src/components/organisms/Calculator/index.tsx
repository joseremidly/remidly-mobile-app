// @ts-nocheck
import React, { useEffect } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { StyleSheet, View, Text, TextInput, Image, Input } from 'react-native';
import { useServices } from 'src/data/providers/ServicesProvider';
import SettingWithSwitch from 'src/components/molecules/SettingWithSwitch';
import ButtonLink from 'src/components/atoms/ButtonLink';

export interface Props {
  text: string;
  checked: boolean;
  stateService: string;
  updateValue: Function;
}

const Calculator: React.FC<Props> = (props: any) => {
  const [value, setValue] = React.useState('')
  const disabled = props.stateService === 'loading';
  console.log(props.receive+'jaja')
  

  const handleChange = (e) => {
    setValue(e)
    console.log(e)
  };

  return (
    <>
      <View style={styles.container}>
        <Text style={styles.amountTransaction}>How money do you want to send ?</Text>
      </View>
      <View style={styles.currencysContainer}>
        <View style={styles.currency}>
          <Image style={styles.iconImgRoomix} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/v1625748150/app/Crecy%20icons%20new%20dimensions/flags-35_1_dwpsgk.png'}} resizeMode='contain'></Image>
          <Text style={styles.currencyText}>
            <Text style={styles.amount}>{props.dolar}</Text> USD
          </Text>
        </View>
        <Text style={styles.equal}>=</Text>
        <View style={styles.currency}>
          <Image style={styles.iconImgRoomix} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/v1625748113/app/Crecy%20icons%20new%20dimensions/flags-34_1_hno3mw.png'}} resizeMode='contain'></Image>
          <Text style={styles.currencyText}>
            <Text style={styles.amount}>{props.mxn}</Text> MXN
          </Text>
        </View>
      </View>
      <View style={styles.currencysContainerInputs}>
        <Text style={styles.regularText}>Send: </Text>
        <View style={{width: '100%'}}>
          <TextInput 
            style={styles.input} 
            onChangeText={(e) => props.handleChange(e)}
            onBlur={(props.onBlurInput)}
            value={props.send.toString()}
             />
          <Text style={styles.absolutePlaceholder}>USD(Dolares)</Text>
        </View>
      </View>
      <View style={styles.barInfoContainer}>
          <View style={styles.barContainer}>
            <View style={styles.circle}><Text>-</Text></View>
            <View style={styles.line}></View>
            <View style={styles.circle}><Text>=</Text></View>
            {/* <View style={styles.line}></View>
            <View style={styles.circle}></View> */}
          </View>

          <View style={styles.infoContainer}>
            {/* <View style={styles.info}>
              <Text style={styles.infoNumber}>
                {2} USD
              </Text>
              <Text style={{ ...styles.infoText, marginRight: 6 }}>
                commision del 
              </Text>
            </View> */}
            <View style={[styles.info]}>
              <Text style={styles.infoNumber}>
                {3} %
              </Text>
              <Text style={styles.infoText}>
                commisiones
              </Text>
            </View>
            <View style={[styles.info, {marginTop: 3}]}>
              <Text style={[styles.bold, styles.margin]}>
                {97} %
              </Text>
              <Text style={styles.infoText}>
                Monto a convertir
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.currencysContainerInputs}>
          <Text style={styles.regularText}>Receive: </Text>
          <View style={{width: '100%'}}>
            <TextInput 
              style={styles.input} 
              onChangeText={handleChange}
              editable={false}
              value={props.receive.toFixed(2)}
            />
            <Text style={styles.absolutePlaceholder}>USD(Dolares)</Text>
          </View>
        </View>
        <View style={styles.containerButton}>
          <ButtonLink label='Continuar' onPress={props.onPress}/>
        </View>
    </>
  );
};

const styles = StyleSheet.create({
  containter: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    marginBottom: 20,
  },
  amountTransaction: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 20,
    color: '#5a6175',
    overflow: 'scroll',
    textAlign: 'center',
  },
  currencysContainer: {
    marginTop: 30,
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 10,
  },
  currencysContainerInputs: {
    marginTop: 30,
    marginBottom: 30,
    alignItems: "flex-start",
    marginHorizontal: 10,
  },
  currency: {
    height: 44,
    width: 170,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#E4EAF4",
    backgroundColor: '#E4EAF4',
    borderRadius: 5,
    flexDirection: "row",
    justifyContent: 'center',
    alignItems: "center",
  },
  equal: {
    fontFamily: "Baloo2-Bold",
    fontSize: 24,
    color: '#5a6175',
  },
  amount: {
    fontFamily: "Baloo2-Bold",
    fontSize: 16,
  },
  currencyText: {
    marginLeft: 8,
    fontSize: 16,
    fontFamily: "Baloo2-Regular",
    color: '#5a6175',
  },
  barInfoContainer: {
    flexDirection: "row",
    width: "100%",
    marginTop: 3,
    marginBottom: 7,
  },

  infoContainer: {
    width: "100%",
    paddingRight: 10,
    paddingLeft: 10,
    paddingTop: 5
  },

  info: {
    flexDirection: "row",
    marginBottom: 17,
    alignItems: "center",
  },

  infoText: {
    fontFamily: "Baloo2-Regular",
    fontSize: 14,
    color: '#5a6175',
    marginLeft: 14,
  },

  infoNumber: {
    fontFamily: "Baloo2-Medium",
    fontSize: 16,
    color: '#5a6175',
  },

  bold: {
    fontFamily: "Baloo2-Bold",
    fontSize: 16,
    color: '#5a6175',
  },

  barContainer: {
    alignItems: "center",
    marginRight: 0,
    paddingTop: 7,
    paddingLeft: 15
  },

  circle: {
    width: 18,
    height: 18,
    backgroundColor: "#DCE6F6",
    borderRadius: 50,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },

  line: {
    width: 3,
    height: 29,
    backgroundColor: "#DCE6F6",
  },

  lastUpdate: {
    fontSize: 10,
    marginTop: 8,
    marginBottom: 1,
    color: '#E4EAF4',
    fontFamily: "regular",
  },

  attentionContainer: {
    flexDirection: "row",
    paddingRight: 20,
    marginTop: 4,
    marginBottom: 8,
    alignItems: "center",
  },

  attentionText: {
    fontSize: 14,
    fontFamily: "regular",
    color: '#E4EAF4',
    marginLeft: 6,
  },

  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  back: {
    zIndex: -5,
  },
  input: {
    width: "100%",
    backgroundColor: '#EDF1F8',
    borderRadius: 5,
    borderStyle: "solid",
    borderWidth: 0.5,
    borderColor: '#EDF1F8',
    height: 50,
    paddingHorizontal: 16,
    justifyContent: "center",
    fontFamily: "Baloo2-SemiBold",
    fontSize: 20,
  },
  absolutePlaceholder: {
    position: "absolute",
    right: 16,
    bottom: 16,
    fontSize: 14,
    fontFamily: "Baloo2-Regular",
    color: "#6E7C95",
  },
  iconImgRoomix: {
    width: 40,
    height: 30,
    position: "absolute",
    left: 10,
  },
  containerButton: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  regularText: {
    fontSize: 18,
    fontFamily: "Baloo2-SemiBold",
    color: "#6E7C95",
  },
});

export default Calculator;