// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';

export interface Props {
  loansData: any;
}

const CreditLineLoanFirstTime: React.FC<Props> = (props) => {
  const [record, setRecord] = React.useState(true);
  const selectRecord = (value: any) => {
    setRecord(value)
  }
  return (
    <View style={styles(props, record).container}>
      <View style={styles(props, record).containerButtonSelected}>
        <TouchableOpacity style={styles(props, record).buttonSelected} onPress={() => selectRecord(true)}>
          <Text style={styles(props, record).textButtonSelected}>Deudas activas</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles(props, record).buttonSelectedHistory} onPress={() => selectRecord(false)}>
          <Text style={styles(props, record).textButtonSelectedRecord}>Historial</Text>
        </TouchableOpacity>
      </View>
      <View style={styles(props, record).containerImageEmpty}>
        <View style={styles(props, record).containerImage}>
          <Image style={styles(props, record).imageEmpty} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609783110/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_crecy_active_loans_j7jpoq.png'}} resizeMode='contain'></Image>
          <Text style={styles(props, record).textEmpty}>Por el momento no tienes ninguna deuda activa...</Text>
        </View>
      </View>
    </View>
  );
};

const styles = (props: any, select: any) => StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: '#e6e6e6',
    borderRadius: 3,
  },
  containerButtonSelected: {
    flexDirection: 'row',
    marginHorizontal: 30,
    marginTop: 10
  },
  buttonSelected: {
    minHeight: 40,
    minWidth: 70,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'red',
  },
  textButtonSelected: {
    fontFamily: 'Avenir-Black',
    fontSize: 14,
    color: '#5a6175',
    textDecorationLine: select ? 'underline' : 'none',
  },
  textButtonSelectedRecord: {
    fontFamily: 'Avenir-Black',
    fontSize: 14,
    color: '#5a6175',
    textDecorationLine: select ? 'none' : 'underline',
  },
  buttonSelectedHistory: {
    minHeight: 40,
    minWidth: 70,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 20
  },
  containerDebt: {
    width: '100%',
    paddingHorizontal: 20,
    marginBottom: 350,
  },
  containerImageEmpty: {
    width: '100%',
    height: '60%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerImage: {
    width: 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageEmpty: {
    width: 200,
    height: 80,
    margin: 20,
  },
  textEmpty: {
    marginTop: 20,
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    textAlign: 'center',
    color: '#5a6175',
  }
});

export default CreditLineLoanFirstTime;
