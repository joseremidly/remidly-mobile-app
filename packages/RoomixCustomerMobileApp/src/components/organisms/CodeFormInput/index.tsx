// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';

import InputCode from 'src/components/molecules/InputCode';
import ButtonSendCode from 'src/components/atoms/ButtonSendCode';
import ButtonTransparent from 'src/components/atoms/ButtonTransparent';

export interface Props {
  phone: string;
  onCheckCode: Function;
  changeNumber: Function;
  resendCode: Function;
  help: Function;
}

const CodeFormInput: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containerCodeFormButtons}>
        <Text style={styles.instructionsCodeFormInput}>Por favor, ingresa el código de 4 digitos que te enviamos a +52 {props.phone.substring(0,6)} {props.phone.substring(6)}</Text>
        <View style={styles.containerCodeForm} >
          <InputCode onChangeInput={(code) => props.onCheckCode(code)}/>
          <View style={styles.containerReSendCodeButton}>
            <ButtonSendCode label='RE-ENVIAR CÓDIGO' secondary={false} onPress={props.resendCode} src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599871411/white%20icons%20mobile/crecy_icons_refresh_white_for_button_y6hsw2.png'/>
          </View>
          <View style={styles.containerCodeFormLinks} >
            <ButtonTransparent label='CAMBIAR MI NÚMERO TELEFÓNICO' fontSize={14} src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599251160/app/back_1_bl4as0.png' onPress={props.changeNumber} ></ButtonTransparent>
            <ButtonTransparent label='AYUDA' fontSize={14} src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599251160/app/back_1_bl4as0.png' onPress={props.help} ></ButtonTransparent>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerCodeFormButtons: {
    alignItems: 'center',
  },
  instructionsCodeFormInput: {
    fontFamily: 'Baloo2-Regular',
    width: 300,
    fontSize: 18,
    lineHeight: 22,
    textAlign: 'center',
    color: '#5a6175',
    marginTop: 80,
  },
  containerCodeForm: {
    alignItems: 'center',
    marginTop: 10,
  },
  containerReSendCodeButton: {
    marginTop: 30,
  },
  containerCodeFormLinks: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    width: '100%',
    marginBottom: 100,
  },
  iconInputForm: {
    width: 20,
    height: 20,
    margin: 10,
  }
});

export default CodeFormInput;
