// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
} from 'react-native';
import * as ImagePicker from 'react-native-image-picker'


const UploadIDOrganisms: React.FC<Props> = (props: any) => {
  const [photoSelfie, setPhotoFront] = React.useState(false);
  const [front, setFront] = React.useState();
  const photoTest = () => {
    ImagePicker.launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: 200,
        maxWidth: 400,
      },
      (response) => {
        console.log(response.uri);
        setPhotoFront(true)
        setFront(response.uri)
      },
    )
  }
  
  return (
    <View style={styles.sectionContainer}>
    <Image style={styles.imgHeader} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1611600041/app/Crecy%20icons%20new%20dimensions/Crecy%20web%20page/Crecy_web_page_crecy_faq_section_crecy_panda_rl6bgn.png'}}/>
      <Text style={styles.headerText}>Ayudanos por favor con una selfie</Text>
        
      {photoSelfie ? 
      (
        <View style={styles.containerID}>
          <Image
            source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833888/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_done_th7phc.png'}}
            style={styles.imgAbsolute}
          />
          <Image
            source={{uri: front}}
            style={{width: 350, height: 250, borderRadius: 13,}}
            resizeMode='contain'
            
          />
          <TouchableOpacity style={styles.buttonStyle}  onPress={photoTest}>
            <Text style={styles.textButtonStyle}>Tomar otra foto</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <TouchableOpacity style={styles.buttonInitial} onPress={photoTest}>
          <Image></Image>
          <Text style={styles.textButtonInitial}>Tomar foto </Text>
          <Text style={styles.textButtonIcon}>+</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    backgroundColor: '#F4F4F7',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 0
  },
  imgHeader: {
    width: 70,
    height: 80,
    marginBottom: 10
  },
  headerText: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 25,
    color: '#5a6175',
    marginBottom: 20,
    marginHorizontal: 50,
    textAlign: 'center',
    lineHeight: 30
  },
  informationText: {
    width: '80%',
    fontFamily: 'Baloo2-Medium',
    fontSize: 18,
    color: '#5a6175',
    marginBottom: 25,
    marginHorizontal: 55,
    textAlign: 'left',
    lineHeight: 25
  },
  buttonStyle: {
    zIndex: 15,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    backgroundColor: '#7b3eff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#7b3eff',
    borderRadius: 20,
    paddingHorizontal: 20,
    marginVertical: 10,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 18,
    color: '#ffffff',
  },
  buttonInitial: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: 300,
    height: 64,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderWidth: 2,
    borderColor: '#7b3eff',
    borderRadius: 20,
    marginHorizontal: 60,
    marginVertical: 20,
  },
  textButtonInitial: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 15,
    color: '#7b3eff',
  },
  textButtonIcon: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 24,
    color: '#7b3eff',
  },
  imgAbsolute: {
    zIndex: 15,
    width: 40,
    height: 40,
    right: -15,
    top: -15,
    position: 'absolute'
  },
  containerID: {
    width: 200,
    height: 260,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#67CC5C',
    borderWidth: 3,
    borderRadius: 34,
    marginBottom: 20,
  },
});

export default UploadIDOrganisms;