// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
} from 'react-native';
import SelectorReusable from 'src/components/molecules/SelectorReusable';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  stateService: String;
  onSelectAccount: Function;
}

const PaycheckSelectAccount: React.FC<Props> = (props: any) => {
  const [active, setActive] = React.useState(false);
  const [account, setAccount] = React.useState('');

  const onChangeOption = (account: any) => {
    setActive(account && account !== '' ? true : false )
  }
  const selectAccount = () => {
    if (active) {
      props.onSelectAccount(account)
    }
  }
  return (
    <>
      <View style={styles.containerSelectAccountOrganism}>
        <Text style={styles.textPrimarySelectAccountOrganism}>
          Para verificar que actualmente cumples con los requisitos para la micro ayuda es necesario
          <Text style={styles.textSecondarySelectAccountOrganism}> que elijas la cuenta en la cual recibes tus ingresos del trabajo:</Text>
        </Text>
        <View style={styles.containerSelectorAccount} >
          <SelectorReusable onChangeOption={onChangeOption} />
        </View>
        {props.stateService === 'loading'
          && (
            <ActivityIndicator size='large' color='#7b3eff' animating={true} />
          )}
        {props.stateService !== 'loading'
          && (
            <ButtonMid label='Guardar cambios' secondary={!active} onPress={selectAccount}></ButtonMid>
          )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerSelectAccountOrganism: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 40,
  },
  textPrimarySelectAccountOrganism: {
    paddingHorizontal: 40,
    flexDirection: 'row',
    fontFamily: 'Baloo2-Medium',
    textAlign: 'center',
    alignSelf: 'stretch',
    color: '#5a6175',
    fontSize: 18,
    lineHeight: 23,
  },
  textSecondarySelectAccountOrganism: {
    color: '#5a6175',
    fontFamily: 'Baloo2-Bold',
  },
  containerSelectorAccount: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 80,
  },
});

export default PaycheckSelectAccount;
