import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    Platform,
		View,
		Dimensions
} from 'react-native';

export interface Props {
  navigation: Object;
	home: boolean;
}

const HeaderPaycheck: React.FC<Props> = (props: any) => {
	const {height} = Dimensions.get('window');
  const leftAction = () => {
    {{props.home ? props.navigation.navigate('Home') : props.navigation.goBack()}}
	}
	let dimensionHeight = height/4;
  return (
    <>
      <View style={styles(dimensionHeight).customHeader}>
        <TouchableOpacity onPress={leftAction}>
          <Image style={styles(dimensionHeight).iconButtonStyleLeft} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599596734/app/crecy_icons_arrow_header_white-01_dg8lby.png' }}></Image>
        </TouchableOpacity>
        <Text style={styles(dimensionHeight).textButtonStyle}>Paycheck Advance</Text>
      </View>
    </>
  );
};

const styles = (dimensionHeight: any) => StyleSheet.create({
  customHeader: {
		position: 'absolute',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
		alignSelf: 'stretch',
    height: Platform.select({
      ios: dimensionHeight,
      android: 240
    }),
    backgroundColor: '#7b3eff',
    width: '100%',
		zIndex: 0,
  },
  textButtonStyle: {
    width: '70%',
    textAlign: 'center',
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: '#ffffff',
    marginLeft: 15,
  },
  iconButtonStyleLeft: {
    width: 35,
    height: 35,
    marginLeft: 20,
  },
  iconButtonStyleRight: {
    width: 25,
    height: 25,
    marginRight: 20,
  },
});

export default HeaderPaycheck;