import React from 'react';

import { StyleSheet, View, Text } from 'react-native';

import AdvanceType from 'src/components/molecules/AdvanceType';

export interface Props {
  type: string;
  fee: number;
  amount: number;
}

const AdvanceSummary: React.FC<Props> = (props: any) => {
  const totalAmount = props.fee + props.amount;

  return (
    <>
      <View style={styles.container}>
        <AdvanceType type={props.type} fee={props.fee} />
        <Text style={styles.amount}>${props.amount}</Text>
        <Text style={styles.amountCaption}>Monto de adelanto</Text>
        {props.type === 'flash' && (
          <>
            <Text style={styles.finalAmountText}>Se cobrará en total (incl. fee): ${totalAmount}</Text>
            <Text style={styles.nothingDueText}>No pagas nada todavía</Text>
          </>
        )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 36,
    backgroundColor: '#7b3eff',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomLeftRadius: 26,
    borderBottomRightRadius: 26,
  },
  amount: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 40,
    color: '#ffffff',
    marginTop: 12,
    marginBottom: 14,
  },
  amountCaption: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    color: '#ffffff',
    textTransform: 'uppercase',
    marginBottom: 8,
  },
  nothingDueText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 12,
    color: '#ffffff',
    textDecorationLine: 'underline',
    marginTop: 4,
  },
  finalAmountText: {
    fontFamily: 'AvenirLTStd-Bold',
    fontSize: 14,
    color: '#ffffff',
    marginTop: 6,
    textTransform: 'uppercase',
  }
});

export default AdvanceSummary;
