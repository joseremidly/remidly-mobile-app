// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    View,
    ActivityIndicator,
    Image,
} from 'react-native';
import CreditLineStartInvitationCode from 'src/components/molecules/CreditLineStartInvitationCode';
import InputCode from 'src/components/molecules/InputCode';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  stateService: String;
  validCode: Boolean;
  checkCode: Function;
  goToStart: Function;
}

const CreditOnBoardingStartThird: React.FC<Props> = (props: any) => {
  const getCode = (code: any) => {
    props.checkCode(code)
  }
  return (
    <View style={styles.container}>
      <CreditLineStartInvitationCode />
      <View style={styles.containerInputCode}>
        {props.validCode
          ? (
            <Image style={styles.successIcon} source={{ uri:'https://res.cloudinary.com/crecy-io/image/upload/v1617096348/app/Crecy%20icons%20new%20dimensions/check_1_ji8cmo.png' }} />
          ) : (
            <InputCode onChangeInput={(code: any) => getCode(code)} />
          )}
      </View>
      <View style={styles.containerButton}>
        {props.validCode
          ? (
            <View style={styles.containerButtonContinue}>
              <ButtonMid
                label='Empezar el trámite'
                secondary={false}
                onPress={props.goToStart}
              />
            </View>
          ) : (
            <View style={styles.containerButtonContinue}>
              {props.stateService === 'loading'
                ? (
                  <ActivityIndicator size='large' color='#7b3eff' animating={true} />
                ) : (
                  <ButtonMid
                    label='Validar'
                    secondary={true}
                    onPress={() => {}}
                  />
                )}
            </View>
          )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerInputCode: {
    marginBottom: 20,
  },
  successIcon: {
    width: 60,
    height: 60,
  },
  containerButton: {
    alignItems: 'center',
    marginBottom: 50
  },
  containerButtonContinue: {
    marginTop: 20
  },
});

export default CreditOnBoardingStartThird;


