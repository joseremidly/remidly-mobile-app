import React from 'react';
import { StyleSheet, Text, ScrollView } from 'react-native';

import LessonCard from 'src/components/molecules/LessonCard';

export interface Props {
  lessons: Object;
}

const Lessons: React.FC<Props> = (props: any) => {
  const LessonsCards = props.lessons.map((lesson, index) => (
    <LessonCard
      isLessonDone={lesson.isDone}
      lessonNumber={lesson.number}
      lessonName={lesson.name}
      imageURI={lesson.imageURI}
      key={lesson.number}
      hasLeftMargin={index === 0}
    />
  ));

  return (
    <>
      <ScrollView
        style={styles.container}
        horizontal={true}
        showsHorizontalScrollIndicator={false}>
        {LessonsCards}
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: 30,
    paddingBottom: 10,
  },
});

export default Lessons;
