// @ts-nocheck
import React, { useEffect } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { StyleSheet, View, Text, TextInput, Image } from 'react-native';
import { useServices } from 'src/data/providers/ServicesProvider';
import SettingWithSwitch from 'src/components/molecules/SettingWithSwitch';
import ButtonLink from 'src/components/atoms/ButtonLink';
import InputPhone from 'src/components/atoms/InputPhone';

export interface Props {
  text: string;
  checked: boolean;
  stateService: string;
  updateValue: Function;
}

const Receptor: React.FC<Props> = (props: any) => {
  const [value, setValue] = React.useState('')
  const [active, setActive] = React.useState(true)
  const [phone, setPhone] = React.useState('')
  const disabled = props.stateService === 'loading';

  const handleChange = (e) => {
    setValue(e)
    console.log(e)
  };
  const onChangeInput = (number: any) => {
    setActive(number && number !== '' ? true : false )
    setPhone(number)
  }

  return (
    <>
      <View style={styles.container}>
        <Text style={styles.amountTransaction}>Who are you sending money to?</Text>
        <Text style={styles.regularText}>Added recipients: </Text>
      </View>
      <View style={styles.currencysContainerInputs}>
        <Text style={styles.regularText}>Name: </Text>
        <View style={{width: '100%'}}>
          <TextInput 
            style={styles.input} 
            onChangeText={(e) => props.handleChange(e,'name')} />
        </View>
      </View>
      <View style={styles.currencysContainerInputs}>
        <Text style={styles.regularText}>Last name: </Text>
        <View style={{width: '100%'}}>
          <TextInput 
            style={styles.input} 
            onChangeText={(e) => props.handleChange(e,'lastName')} />
        </View>
      </View>
      <View style={styles.currencysContainerInputs}>
        <Text style={styles.regularText}>Alias/KickName: </Text>
        <View style={{width: '100%'}}>
          <TextInput 
            style={styles.input} 
            onChangeText={(e) => props.handleChange(e,'alias')} />
        </View>
      </View>
      <View style={styles.currencysContainerInputs}>
        <Text style={styles.regularText}>Email: </Text>
        <View style={{width: '100%'}}>
          <TextInput 
            style={styles.input} 
            onChangeText={(e) => props.handleChange(e,'email')} />
        </View>
      </View>
      <View style={styles.currencysContainerInputs}>
        <Text style={styles.regularText}>Phone number: </Text>
        <View style={{width: '100%', display: 'flex', flexDirection: 'row', alignItems: 'center',justifyContent: 'space-between'}}>
          <Text style={styles.inputText}>+ 52</Text>
          <InputPhone placeholder='NÚMERO DE CELULAR' onChangeInput={onChangeInput} ></InputPhone>
        </View>
      </View>
      <View style={styles.currencysContainerInputs}>
        <Text style={styles.regularText}>Clabe (18 digits): </Text>
        <View style={{width: '100%'}}>
          <TextInput 
            style={styles.input} 
            onChangeText={(e) => props.handleChange(e,'clabe')}
            placeholder={'XXXX-XXXX-XXXX-XXXX-XX'} 
            />
        </View>
      </View>
      <View style={styles.currencysContainerInputs}>
        <Text style={styles.regularText}>Bank: </Text>
        <View style={{width: '100%'}}>
          <TextInput 
            style={styles.input} 
            onChangeText={(e) => props.handleChange(e,'bank')} 
            />
        </View>
      </View>
      <View style={styles.containerButton}>
        <ButtonLink label='Continuar' onPress={props.onPress}/>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containter: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    marginBottom: 20,
  },
  amountTransaction: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 20,
    color: '#5a6175',
    overflow: 'scroll',
    textAlign: 'center',
  },
  currencysContainer: {
    marginTop: 30,
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 10,
  },
  currencysContainerInputs: {
    width: '90%',
    marginTop: 10,
    marginBottom: 10,
    alignItems: "flex-start",
  },
  currency: {
    height: 44,
    width: 170,
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#E4EAF4",
    backgroundColor: '#E4EAF4',
    borderRadius: 5,
    flexDirection: "row",
    justifyContent: 'center',
    alignItems: "center",
  },
  equal: {
    fontFamily: "Baloo2-Bold",
    fontSize: 24,
    color: '#5a6175',
  },
  amount: {
    fontFamily: "Baloo2-Bold",
    fontSize: 16,
  },
  currencyText: {
    marginLeft: 8,
    fontSize: 16,
    fontFamily: "Baloo2-Regular",
    color: '#5a6175',
  },
  barInfoContainer: {
    flexDirection: "row",
    width: "100%",
    marginTop: 7,
    marginBottom: 14,
  },

  infoContainer: {
    width: "100%",
    paddingRight: 10,
    paddingLeft: 10,
    paddingTop: 5
  },

  info: {
    flexDirection: "row",
    marginBottom: 17,
    alignItems: "center",
  },

  infoText: {
    fontFamily: "Baloo2-Regular",
    fontSize: 14,
    color: '#5a6175',
    marginLeft: 14,
  },

  infoNumber: {
    fontFamily: "Baloo2-Medium",
    fontSize: 16,
    color: '#5a6175',
  },

  bold: {
    fontFamily: "Baloo2-Bold",
    fontSize: 16,
    color: '#5a6175',
  },

  barContainer: {
    alignItems: "center",
    marginRight: 0,
    paddingTop: 7,
    paddingLeft: 15
  },

  circle: {
    width: 18,
    height: 18,
    backgroundColor: "#DCE6F6",
    borderRadius: 50,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },

  line: {
    width: 3,
    height: 29,
    backgroundColor: "#DCE6F6",
  },

  lastUpdate: {
    fontSize: 10,
    marginTop: 8,
    marginBottom: 1,
    color: '#E4EAF4',
    fontFamily: "regular",
  },

  attentionContainer: {
    flexDirection: "row",
    paddingRight: 20,
    marginTop: 4,
    marginBottom: 8,
    alignItems: "center",
  },

  attentionText: {
    fontSize: 14,
    fontFamily: "regular",
    color: '#E4EAF4',
    marginLeft: 6,
  },

  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  back: {
    zIndex: -5,
  },
  input: {
    width: "100%",
    backgroundColor: '#EDF1F8',
    borderRadius: 5,
    borderStyle: "solid",
    borderWidth: 0.5,
    borderColor: '#EDF1F8',
    height: 50,
    paddingHorizontal: 16,
    justifyContent: "center",
    fontFamily: "regular",
    fontSize: 14,
  },
  inputText: {
    width: "20%",
    backgroundColor: '#EDF1F8',
    borderRadius: 5,
    borderStyle: "solid",
    borderWidth: 0.5,
    borderColor: '#EDF1F8',
    height: 50,
    fontSize: 22,
    fontFamily: "Baloo2-SemiBold",
    color: "#6E7C95",
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
    paddingLeft: 5,
  },
  absolutePlaceholder: {
    position: "absolute",
    right: 16,
    bottom: 16,
    fontSize: 14,
    fontFamily: "Baloo2-Regular",
    color: "#6E7C95",
  },
  iconImgRoomix: {
    width: 40,
    height: 30,
    position: "absolute",
    left: 10,
  },
  containerButton: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  regularText: {
    fontSize: 18,
    fontFamily: "Baloo2-SemiBold",
    color: "#6E7C95",
  },
});

export default Receptor;