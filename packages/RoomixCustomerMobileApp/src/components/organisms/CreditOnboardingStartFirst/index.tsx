// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import CreditLineStartInfo from 'src/components/molecules/CreditLineStartInfo';
import CreditLineStartIndicatorProgress from 'src/components/molecules/CreditLineStartIndicatorProgress';

export interface Props {
  nextStep: Function;
}

const CreditOnBoardingStartFirst: React.FC<Props> = (props: any) => {
  return (
    <View style={styles.container}>
      <CreditLineStartInfo />
      <CreditLineStartIndicatorProgress step={'first'} setStep={() => {}}/>
      <View style={styles.containerButton}>
        <TouchableOpacity style={styles.buttonStyle} onPress={() => props.nextStep()}>
          <Text style={styles.textButtonStyle}>Siguiente</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerButton: {
    alignItems: 'center',
    marginBottom: 50
  },
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 280,
    height: 44,
    backgroundColor: '#7b3eff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#7b3eff',
    borderRadius: 20,
    marginTop: 20,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 22,
    color: '#ffffff',
  },
});

export default CreditOnBoardingStartFirst;
