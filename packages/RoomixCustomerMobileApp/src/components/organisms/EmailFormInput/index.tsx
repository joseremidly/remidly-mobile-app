// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  ActivityIndicator,
} from 'react-native';
import TitleText from 'src/components/atoms/TitleText';
import InputReusable from 'src/components/atoms/InputReusable';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  onChangeEmail: Function;
  stateService: String;
}

const EmailFormInput: React.FC<Props> = (props: any) => {
  const [active, setActive] = React.useState(false);
  const [email, setEmail] = React.useState('');

  const onChangeInput = (email: any) => {
    setActive(email && validateEmail(email) ? true : false )
    if (validateEmail(email)) {
      setEmail(email)
    }
  }
  const validateEmail = (email) => {
    const re = new RegExp(/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i)
    return re.test(email)
  }
  const changedEmail = () => {
    if (active) {
      props.onChangeEmail(email)
    }
  }
  return (
    <>
      <View style={styles.containerEmailFormInput}>
        <TitleText textPrimary='Por favor, ingresa' textSecondary='tu email' fontSize={28} lineHeight={35}/>
        <View style={styles.containerEmailForm} >
          <InputReusable placeholder='hola@crecy.io' onChangeInput={onChangeInput} transform='lowercase' />
        </View>
        {props.stateService === 'loading'
          && (
            <ActivityIndicator size='large' color='#7b3eff' animating={true} />
          )}
        {props.stateService !== 'loading'
          && (
            <ButtonMid label='Continuar' secondary={!active} onPress={changedEmail}></ButtonMid>
          )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerEmailFormInput: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 250,
    flex: 1,
  },
  containerEmailForm: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 60,
  },
});

export default EmailFormInput;
