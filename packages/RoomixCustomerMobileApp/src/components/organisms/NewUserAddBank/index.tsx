// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
import ButtonLink from 'src/components/atoms/ButtonLink';
export interface Props {
}

const NewUserAddBank: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containerNewUserAddBank}>
        <Text style={styles.textLinkAccount}>
          Empieza a usar Crecy
          <Text style={styles.textSecondaryLinkAccount}> conectando tu primer cuenta bancaria.</Text>
        </Text>
        <Text style={styles.noteLinkAccount}>(puedes agregar más de una cuenta)</Text>
        <TouchableOpacity style={styles.buttonStyle} onPress={props.addAccount}>
          <Text style={styles.textButtonStyle}>AGREGAR CUENTA BANCARIA  +</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerNewUserAddBank: {
    alignItems: 'center',
    marginTop: 25,
  },
  textLinkAccount: {
    width: '100%',
    flexDirection: 'row',
    fontFamily: 'Baloo2-Bold',
    textAlign: 'center',
    color: '#7b3eff',
    fontSize: 20,
    lineHeight: 27,
  },
  textSecondaryLinkAccount: {
    flexDirection: 'row',
    color: '#5a6175',
    fontSize: 20,
  },
  noteLinkAccount: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 14,
    lineHeight: 20,
    textAlign: 'center',
    color: '#5a6175',
    marginTop: 0,
    marginBottom: 20,
  },
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    height: 36,
    backgroundColor: '#7b3eff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#7b3eff',
    borderRadius: 6,
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Medium',
    fontSize: 14,
    color: '#ffffff',
  },
});

export default NewUserAddBank;
