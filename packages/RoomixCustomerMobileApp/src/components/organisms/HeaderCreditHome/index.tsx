// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    Image,
    View,
} from 'react-native';
import AmountAvailableCreditHome from 'src/components/molecules/AmountAvailableCreditHome';
import LeftDaysToPayGraph from 'src/components/molecules/LeftDaysToPayGraph';

export interface Props {
  onPress: Function;
  creditAmount: any;
  availableAmount: any;
  disable: boolean;
  maxDaysToPay: any;
  stateLoans: any;
  loansData: any
}

const HeaderCreditHome: React.FC<Props> = (props: any) => {
  return (
    <View style={styles.containerHeaderCreditHome}>
      <View style={styles.containerInformationCreditHome}>
        <AmountAvailableCreditHome creditAmount={props.creditAmount} availableAmount={props.availableAmount} />
        <LeftDaysToPayGraph maxDaysToPay={props.maxDaysToPay} stateLoans={props.stateLoans} loansData={props.loansData} />
      </View>
      <TouchableOpacity style={styles.buttonStyle} onPress={props.onPress} disabled={props.disable}>
        <Text style={styles.textButtonStyle}>{'Retirar'}</Text>
        <Image resizeMode='contain' style={styles.iconButtonStyle} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/a_270/v1609789908/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_arrow_velvet_f6hy2z.png'}}></Image>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  containerHeaderCreditHome: {
    width: '100%',
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#7b3eff',
    overflow: 'hidden',
  },
  containerInformationCreditHome: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 25,
  },
  buttonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 17,
    height: 40,
    backgroundColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#7b3eff',
    borderRadius: 16,
    margin: 10,
    marginBottom: 60
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-Bold',
    textAlign: 'center',
    fontSize: 19,
    textTransform: 'uppercase',
    color: '#7b3eff',
    marginLeft: 10
  },
  textInformation: {
    fontFamily: 'AvenirLTStd-Roman',
    textAlign: 'center',
    color: '#ffffff',
    marginLeft: 6,
    fontSize: 13,
    lineHeight: 18,
    textTransform: 'uppercase',
  },
  iconButtonStyle: {
    width: 15,
    height: 15,
    marginLeft: 10,
    marginRight: 10
  },
});

export default HeaderCreditHome;