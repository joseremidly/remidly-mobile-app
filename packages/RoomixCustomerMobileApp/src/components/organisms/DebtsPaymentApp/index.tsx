// @ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View
} from 'react-native';
import DebtsBranchApp from 'src/components/molecules/DebtsBranchApp'

export interface Props {
  status: string;
  loans: any;
}

const DebtsPaymentApp: React.FC<Props> = (props) => {

  return (
    <>
      <View style={styles(props).container}>
        <DebtsBranchApp
          status={props.status}
          src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833310/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_logos_mobile_app_repayment_u4vrav.png'
          type='app'
          loans={props.loans}
        />
      </View>
      <View style={{width: '85%' ,display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginTop: 35,}}>
        <View style={styles(props).containerNumber}>
          <Text style={styles(props).textDebtNumberInformation}>1</Text>
        </View>
        <View>
          <Text style={styles(props).textDebtInformationHeavy}>
            <Text style={styles(props).textDebBlack}>Abre la app </Text>
            tu banco
          </Text>
        </View>
      </View>
      <View style={{width: '85%' ,display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 25,}}>
        <View style={styles(props).containerNumber}>
          <Text style={styles(props).textDebtNumberInformation}>2</Text>
        </View>
        <View>
          <Text style={styles(props).textDebtInformationHeavy}>
            <Text style={styles(props).textDebBlack}>Ve a la sección</Text>
            de transferencias
          </Text>
        </View>
      </View>
      <View style={{width: '85%' ,display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 25,}}>
        <View style={styles(props).containerNumber}>
          <Text style={styles(props).textDebtNumberInformation}>3</Text>
        </View>
        <View>
          <Text style={styles(props).textDebtInformationHeavy}>
            Al momento de hacer la tranferencia 
            <Text style={styles(props).textDebBlack}> ingresa todos los datos que te solicite la app </Text>
            usando los datos mencionados arriba
          </Text>
        </View>
      </View>
      <View style={{width: '85%' ,display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 25,}}>
        <View style={styles(props).containerNumber}>
          <Text style={styles(props).textDebtNumberInformation}>4</Text>
        </View>
        <View>
          <Text style={styles(props).textDebtInformationHeavy}>
            <Text style={styles(props).textDebBlack}>Ingresa el monto que te aprece arriba </Text>
            y haz la tranferencia
          </Text>
        </View>
      </View>
      <View style={{width: '85%' ,display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 25,}}>
        <View style={styles(props).containerNumber}>
          <Text style={styles(props).textDebtNumberInformation}>5</Text>
        </View>
        <View>
          <Text style={styles(props).textDebtInformationHeavy}>
            <Text style={styles(props).textDebBlack}>
              Después de realizar la transferencia la app te mostrará el recibo electrónico,
            </Text>
             {' '}guárdalo a tu celular. Crecy dectectará tu pago automáticamente pero, por favor, conserva este 
             recibo electrónico para futuras aclaraciones.
          </Text>
        </View>
      </View>
      <View style={{justifyContent: 'center', alignItems: 'center', marginVertical: 45 }}>
        <Text style={styles(props).textRecomendation}>CUANDO DETECTEMOS TU PAGO</Text>
        <Text style={styles(props).textRecomendationInformation}>LA DEUDA APARECERÁ EN LA SECCIÓN "HISTORIAL" Y CAMBIARA SU ESTATÚS</Text>
        <View style={{flexDirection:'row', marginTop: 10, justifyContent: 'center', alignItems: 'center'}}>
          <View style={{backgroundColor: `${props.loans.status === 'expired' || props.loans.status === 'rejected' ? '#ed1e79' : '#Fcae16'}`, borderRadius: 4, marginHorizontal: 5 , justifyContent: 'center', alignItems: 'center'}} >
            <Text style={styles(props).textExpired}>
              <Text style={styles(props).textAprove}>DE: </Text>
              {props.loans.status === 'rejected' || props.loans.status === 'expired' ? 'PAGO VENCIDO' : 'PAGO PENDIENTE'}
            </Text>
          </View>
          <Image style={styles(props).iconArrow} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/a_0180/v1609833887/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_grey_arrow_ij4vl7.png'}}></Image>
          <View style={{backgroundColor: '#49c0aa', borderRadius: 4 ,marginHorizontal: 5, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={styles(props).textExpired}>
              <Text style={styles(props).textAprove}>A: </Text>
              DEUDA PAGADA
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};

const styles= (props: any) => StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 12,
  },
  textDebBlack: {
    fontFamily: 'Avenir-Black',
    fontSize: 13,
    color: '#5a6175',
    lineHeight: 15,
    borderRadius: 50,
    marginHorizontal: 5,
    marginVertical: 2,
  },
  textRecomendation: {
    fontFamily: 'Avenir-Black',
    fontSize: 16,
    color: '#5a6175',
    textAlign: 'left',
  },
  textRecomendationInformation: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 15,
    color: '#5a6175',
    textAlign: 'center',
  },
  textDebtInformationHeavy: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    color: '#5a6175',
    textAlign: 'left',
  },
  textDebtNumberInformation: {
    fontFamily: 'Avenir-Black',
    fontSize: 12,
    color: 'white',
    lineHeight: 15,
    borderRadius: 50,
    marginHorizontal: 6,
    marginVertical: 2,
  },
  containerNumber: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#7b3eff',
    borderRadius: 12,
    marginRight: 15
  },
  iconArrow: {
    width: 15,
    height: 15,
    marginTop: 2
  },
  textExpired: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    color: 'white',
    textAlign: 'left',
    marginHorizontal: 8,
    marginVertical: 2
  },
  textAprove: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    color: 'white',
    textAlign: 'left',
  },
  
});

export default DebtsPaymentApp;