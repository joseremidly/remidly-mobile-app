import React from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
} from 'react-native';
import LinkCard from '../../atoms/LinkedCard';
import BankAccount from '../../molecules/CardBankAccounts';

export interface Props {
	dataAccount: any;
	dataCards: any;
	edit: Function;
	toggleSwitchAccount: Function;
	toggleSwitchBalance: Function;
	toggleSwitchAccountBank: Function;
	toggleSwitchBalanceBank: Function;
}

const Header: React.FC<Props> = (props: any) => {

	const DataIteration = (value: any) => (
    <>
      <LinkCard
				background={value.background}
				typeCard={value.typeCard}
				numberCard={value.cardNumber}
				totalAccount={value.totalAccount}
				src={value.src}
				isEnableAccount={value.account}
				toggleSwitchAccount={props.toggleSwitchAccount}
				isEnableBalance={value.balance}
				toggleSwitchBalance={props.toggleSwitchBalance}
			/>
    </>
  );
  const Data = (props.dataCards !== '' ? props.dataCards.map(DataIteration) : '');

  return (
		<>
			<View style={{width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
				<Image style={{width:17, height:17, marginRight: 15}} source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604997219/app/Crecy%20icons%20new%20dimensions/Crecy_app_icons_safe_settings_cchzy4.png'}}></Image>
				<Text style={styles(props).textButtonHeader}>BANK ACCOUNT(S)</Text>
			</View>
			<View style={{marginHorizontal: 15}}>
				<BankAccount
					dataAccount={props.dataAccount}
					edit={props.edit}
					toggleSwitchAccount={props.toggleSwitchAccountBank}
					toggleSwitchBalance={props.toggleSwitchBalanceBank}
				/>
			</View>
			<View style={styles(props).container}>
				<View style={{width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginVertical: 10,}}>
					<Image style={{width:17, height:17, marginRight: 15}} source={{ uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1604997219/app/Crecy%20icons%20new%20dimensions/Crecy_app_icons_safe_settings_cchzy4.png'}}></Image>
					<Text style={styles(props).textButtonHeader}>LINKED CARD(S)</Text>
				</View>
				<View style={{width: '100%'}}>
					{Data}
				</View>
			</View>
		</>
  );
};

const styles = (props: any) => StyleSheet.create({
  container: {
		width: '98%',
		backgroundColor: 'white',
		borderRadius: 18
  },
  textButtonStyle: {
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: '#5a6175',
  },
  textButtonHeader: {
    fontFamily: 'Baloo2-Regular',
    fontSize: 20,
    color: '#5a6175',
  },
  iconButtonStyleLeft: {
    width: 25,
    height: 20,
    marginLeft: 20,
  },
  iconButtonStyleRight: {
    width: 25,
    height: 25,
    marginRight: 20,
  },
});

export default Header;
