// @ts-nocheck
import React, { useEffect } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { StyleSheet, View, Text } from 'react-native';
import { useServices } from 'src/data/providers/ServicesProvider';
import SettingWithSwitch from 'src/components/molecules/SettingWithSwitch';

export interface Props {
  text: string;
  checked: boolean;
  stateService: string;
  updateValue: Function;
}

const AlertSwitch: React.FC<Props> = (props: any) => {
  const disabled = props.stateService === 'loading';

  const toggleSwitch = () => {
    props.updateValue(!props.checked);
  };

  return (
    <>
      <View>
        <SettingWithSwitch
          checked={props.checked}
          text={props.text}
          toggleSwitch={toggleSwitch}
          disabled={disabled}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({});

export default AlertSwitch;
