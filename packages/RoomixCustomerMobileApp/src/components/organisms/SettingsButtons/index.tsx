// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
} from 'react-native';

import FaceIconEdit from 'src/components/molecules/FaceIconEdit';
import ButtonSettings from 'src/components/molecules/ButtonSettings';

export interface Props {
  navigation: Object;
  goRoute: Function;
}

const Settings: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.containerSettingsElements} >
        <View style={styles.containerFaceIconEdit} >
          <FaceIconEdit src='https://res.cloudinary.com/crecy-io/image/upload/v1625749790/app/Crecy%20icons%20new%20dimensions/WhatsApp_Image_2021-07-08_at_7.03.12_AM_jjyhbf.jpg' ></FaceIconEdit>
        </View>
        <View style={styles.containerButtonsSettings}>
          <Text style={styles.titleButtonsSettings}>General</Text>
          <ButtonSettings src={require('assets/images/settings-icons/settings_icons_profile.png')} color='#7b3eff' text='Editar perfil' route='Home' onClick={(route: any) => props.goRoute(route)} />
          <ButtonSettings src={require('assets/images/settings-icons/settings_icons_linked_accounts.png')} color='#7b3eff' text='Cuentas conectadas' route='Home' onClick={(route: any) => props.goRoute(route)} />
          <ButtonSettings src={require('assets/images/settings-icons/settings_icons_security.png')} color='#7b3eff' text='Seguridad' route='Home' onClick={(route: any) => props.goRoute(route)} />
          <ButtonSettings src={require('assets/images/settings-icons/settings_icons_alert.png')} color='#FF2169' text='Alertas' route='Home' onClick={() => props.goRoute('Home')} />
        </View>
        <View style={styles.containerButtonsSettings}>
          <Text style={styles.titleButtonsSettings}>Legal</Text>
          <ButtonSettings src={require('assets/images/settings-icons/settings_icons_tyc.png')} color='#7b3eff' text='Términos y condiciones' route='Home' onClick={(route: any) => props.goRoute(route)} />
          <ButtonSettings src={require('assets/images/settings-icons/settings_icons_pp.png')} color='#7b3eff' text='Política de privacidad' route='Home' onClick={(route: any) => props.goRoute(route)} />
        </View>
        <View style={styles.containerButtonsSettings}>
          <Text style={styles.titleButtonsSettings}>Ayuda</Text>
          <ButtonSettings src={require('assets/images/settings-icons/settings_icons_customer_support.png')} color='#49c0aa' text='Soporte al cliente' route='Home' onClick={(route: any) => props.goRoute(route)} />
        </View>
        <TouchableOpacity style={styles.buttonCloseSession} onPress={() => props.goRoute('SignOut')}>
          <Text style={styles.buttonCloseSessionText}>Cerrar sesión</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerSettingsElements: {
    backgroundColor: '#F4F4F7',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerButtonsSettings: {
    width: '80%',
    marginTop: 15,
  },
  titleButtonsSettings: {
    width: '75%',
    fontFamily: 'Avenir-Heavy',
    fontSize: 21,
    color: '#5a6175',
    textAlign: 'left',
    marginVertical: 5,
  },
  containerFaceIconEdit: {
    width: 120,
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 10,
  },
  buttonCloseSession: {
    width: '70%',
    height: 40,
    borderColor: '#ed1e79',
    borderWidth: 1,
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 40,
  },
  buttonCloseSessionText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 21,
    color: '#ed1e79',
    textAlign: 'left',
    marginVertical: 5,
  }
});

export default Settings;
