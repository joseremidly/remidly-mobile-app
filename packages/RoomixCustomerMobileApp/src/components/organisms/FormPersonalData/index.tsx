// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform,
  ActivityIndicator,
} from 'react-native';
import InputReusable from 'src/components/atoms/EditInputKycInformation';
import ButtonMid from 'src/components/atoms/ButtonMid';

export interface Props {
  stateService: string;
  email: string;
  name: string;
  lastName: string;
  lastNameMother: string;
  curp: string;
  gender: string;
  rfc: string;
  identificationID: string;
  OnChangeRfc: Function;
  OnChangeEmail: Function;
  OnChangeName: Function;
  OnChangeLastName: Function;
  OnChangeLastNameMother: Function;
  OnChangeCurp: Function;
  OnChangeGender: Function;
  OnChangeIdentificationID: Function;
  SendInformation: Function;
}

const FormPersonalDataOrganisms: React.FC<Props> = (props: any) => {
  return (
    <View>
      <View style={{ justifyContent: 'center', alignItems: 'center', marginVertical: 0 }}>
        <Text style={styles.textRecomendation}>POR FAVOR, INGRESA TU RFC Y REVISA QUE LOS DATOS PERSONALES QUE EXTRAJIMOS DE TU DOCUMENTO OFICIAL SON CORRECTOS:</Text>
      </View>
      <View style={{width: '85%',display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 35, marginHorizontal: 0}}>
        <View style={styles.containerNumber}>
          <Text style={styles.textVerificationNumberInformation}>1</Text>
        </View>
        <Text style={styles.textVerificationInformationHeavy}>
          Si los datos de algún campo son incorrectos o incompletos, por favor, modifícalos
        </Text>
      </View>
      <View style={{width: '89%',display: 'flex',flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginTop: 15, marginHorizontal: 0}}>
        <View style={styles.containerNumber}>
          <Text style={styles.textVerificationNumberInformation}>2</Text>
        </View>
        <Text style={styles.textVerificationInformationHeavy}>
          Recuerda que proporcionar los datos incompletos o incorrectos alarga el proceso de verificación
        </Text>
      </View>
      <View style={styles.containerForm}>
        <Text style={{color: `${props.rfc === '' || props.rfc === undefined ? 'red' : '#b3b3b3'}`, fontFamily: 'Avenir-Heavy', textAlign: 'left', marginBottom: -15, marginTop: 20, textTransform: 'uppercase'}}>
          RFC:
        </Text>
        <InputReusable
          placeholder='Ingresa aquí tu RFC'
          content={props.rfc}
          onChangeInput={props.OnChangeRfc}
        />
        <Text style={styles.textForm}>Email:</Text>
        <InputReusable
          placeholder='Correo electrónico'
          placeholder={props.email}
          content={props.email}
          onChangeInput={props.OnChangeEmail}
          autoCapitalize='deactivate'
        />
        <Text style={styles.textForm}>Nombre(s):</Text>
        <InputReusable
          placeholder={props.name}
          content={props.name}
          onChangeInput={props.OnChangeName}
        />
        <Text style={styles.textForm}>Apellido Paterno:</Text>
        <InputReusable
          placeholder={props.lastName}
          content={props.lastName}
          onChangeInput={props.OnChangeLastName}
        />
        <Text style={styles.textForm}>Apellido Materno:</Text>
        <InputReusable
          placeholder={props.lastNameMother}
          content={props.lastNameMother}
          onChangeInput={props.OnChangeLastNameMother}
        />
        <Text style={styles.textForm}>CURP:</Text>
        <InputReusable
          placeholder={props.curp}
          content={props.curp}
          onChangeInput={props.OnChangeCurp}
        />
        <Text style={styles.textForm}>Sexo:</Text>
        <View style={{flexDirection: 'row',justifyContent: 'center', alignItems: 'center', marginTop: 10}}>
          <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
            <TouchableOpacity onPress={() => props.OnChangeGender('M')}>
              {props.gender !== 'M'
                &&
                <Image style={styles.checkbox} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833888/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_undone_qrrdro.png'}}></Image>
                }
              {props.gender === 'M'
                &&
                <Image style={styles.checkbox}  resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833888/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_done_th7phc.png'}}></Image>
              }
              </TouchableOpacity>
            <Text style={styles.textCheckBox}>Mujer</Text>
          </View>
          <View style={{flexDirection: 'row',justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
            <TouchableOpacity onPress={() => props.OnChangeGender('H')}>
              {props.gender === 'H'
              &&
              <Image style={styles.checkbox}  resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833888/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_done_th7phc.png'}}></Image>
              }
              {props.gender !== 'H'
              &&
              <Image style={styles.checkbox}  resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1609833888/app/Crecy%20icons%20new%20dimensions/Crecy%20credit%20line/crecy_credit_line_undone_qrrdro.png'}}></Image>
              }
            </TouchableOpacity>
            <Text style={styles.textCheckBox}>Hombre</Text>
          </View>
        </View>
        <Text style={styles.textForm}>Número de documento oficial:</Text>
        <InputReusable
          placeholder={props.identificationID}
          content={props.identificationID}
          onChangeInput={props.OnChangeIdentificationID}
        />
      </View>
      {props.stateService === 'loading'
        && (
          <View style={styles.buttonContainer}>
            <ActivityIndicator size='large' color='#7b3eff' animating={true} />
          </View>
        )}
      {props.stateService !== 'loading'
        && (
          <View style={styles.buttonContainer}>
            <ButtonMid
              label='Continuar'
              secondary={!(props.name !== '' && props.lastName !== '' && props.lastNameMother !== '' && props.curp !== '' && props.identificationID !== '' && props.rfc !== undefined && props.rfc !== '')}
              onPress={(props.name !== '' && props.lastName !== '' && props.lastNameMother !== '' && props.curp !== '' && props.identificationID !== '' && props.rfc !== undefined && props.rfc !== '') ? props.SendInformation : () => {}}
            />
          </View>
        )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 40,
  },
  containerForm: {
    alignItems: 'flex-start',
    backgroundColor: 'white',
    marginTop: 10,
    marginBottom: 30,
    marginHorizontal: 10,
    width: '100%'
  },
  formInput: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    borderBottomColor: '#f1f1f1',
    borderBottomWidth: 2,
    height: 40,
    minWidth: '80%',
    fontFamily: 'AvenirLTStd-Light',
    color: '#b3b3b3',
  },
  textVerificationInformationHeavy: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    color: '#5a6175',
    textAlign: 'left',
    lineHeight: 15,
    paddingTop: 5
  },
  textVerificationNumberInformation: {
    fontFamily: 'Avenir-Black',
    fontSize: 12,
    color: 'white',
    lineHeight: 15,
    borderRadius: 50,
    marginHorizontal: 6,
    marginVertical: 2,
  },
  containerNumber: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#7b3eff',
    borderRadius: 12,
    marginRight: 15
  },
  textRecomendation: {
    fontFamily: 'Avenir-Black',
    fontSize: 16,
    color: '#5a6175',
    textAlign: 'center',
    marginTop: 20,
    width: '95%'
  },
  textForm: {
    fontFamily: 'AvenirLTStd-Roman',
    color: '#b3b3b3',
    textAlign: 'left',
    marginBottom: -15,
    marginTop: 20,
  },
  textCheckBox: {
    fontFamily: 'AvenirLTStd-Medium',
    color: '#5a6175',
    textAlign: 'right',
    marginRight: 7,
    fontSize: Platform.select({
      ios: 16,
      android: 18
    }),
  },
  textFormInitial: {
    fontFamily: 'Avenir-Heavy',
    color: '#b3b3b3',
    textAlign: 'left',
    marginBottom: -15,
  },
  buttonPay: {
    backgroundColor: '#7b3eff',
    width: '70%',
    height: 40,
    borderRadius: 17,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 25,
    marginHorizontal: '15%'
  },
  buttonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 50,
  },
  checkbox: {
    width: 20,
    height: 20,
    marginRight: 7
  }
});

export default FormPersonalDataOrganisms;
