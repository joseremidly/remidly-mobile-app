// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
} from 'react-native';

const ScanTouchIDMessage = () => {
  return (
    <>
      <View style={styles.containerScanTouchIDMessage}>
        <Text style={styles.textPrimaryScanTouchIDMessage}> Usa tu huella digital para acceder a tu cuenta</Text>
        <Image style={styles.containerImageScanTouchIDMessage} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599762241/app/crecy_icons_touch_id_white_ortjix.png'}} />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerScanTouchIDMessage: {
    alignItems: 'center',
  },
  textPrimaryScanTouchIDMessage: {
    flexDirection: 'row',
    fontFamily: 'Baloo2-ExtraBold',
    textAlign: 'center',
    alignSelf: 'stretch',
    color: '#ffffff',
    fontSize: 26,
    lineHeight: 35,
    paddingHorizontal: 40,
    marginBottom: 80,
  },
  containerImageScanTouchIDMessage: {
    width: 95,
    height: 103,
    marginBottom: 40,
  },
});

export default ScanTouchIDMessage;
