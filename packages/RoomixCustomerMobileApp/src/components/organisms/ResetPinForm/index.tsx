// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ActivityIndicator,
} from 'react-native';

import ButtonMid from 'src/components/atoms/ButtonMid';
import InputPhone from 'src/components/atoms/InputPhone';
import Dropdown from 'src/components/molecules/Dropdown';

export interface Props {
  stateService: String;
  onSentCode: Function;
}

const ResetPinForm: React.FC<Props> = (props: any) => {
  const [active, setActive] = React.useState(false);
  const [phone, setPhone] = React.useState('');

  const onChangeInput = (number: any) => {
    setActive(number && number !== '' ? true : false )
    setPhone(number)
  }
  const getCode = () => {
    console.log('TEST')
    if (active) {
      props.onSentCode(phone)
    }
  }
  return (
    <>
      <View style={styles.containerResetPinButtons}>
        <Text style={styles.titleResetPin}>¿Olvidaste tu PIN?</Text>
        <Text style={styles.textResetPin}>
          No te preocupes, te enviaremos un código
          <Text style={styles.secondarytextResetPin}> de 4 digitos </Text>
          a tu teléfono para poder reiniciar tu PIN
        </Text>
        <View style={styles.containerResetPinNumberForm} >
          <Image style={styles.iconInputForm} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599597522/app/crecy_icons_phone_icon_black_z225ms.png'}}></Image>
          <Dropdown />
          <InputPhone placeholder='TELÉFONO MÓVIL' onChangeInput={onChangeInput} ></InputPhone>
        </View>
        {props.stateService === 'loading'
          && (
            <ActivityIndicator size='large' color='#7b3eff' animating={true} />
          )}
        {props.stateService !== 'loading'
          && (
            <ButtonMid label='Reiniciar PIN' secondary={!active} onPress={() => getCode()}></ButtonMid>
          )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  containerResetPinButtons: {
    alignItems: 'center',
    marginTop: 50,
  },
  titleResetPin: {
    flexDirection: 'row',
    fontFamily: 'Baloo2-ExtraBold',
    textAlign: 'center',
    alignSelf: 'stretch',
    color: '#5a6175',
    fontSize: 24,
  },
  textResetPin: {
    marginTop: 30,
    flexDirection: 'row',
    fontFamily: 'Baloo2-Bold',
    textAlign: 'center',
    alignSelf: 'stretch',
    color: '#5a6175',
    lineHeight: 26,
    fontSize: 20,
    paddingHorizontal: 20,
  },
  secondarytextResetPin: {
    flexDirection: 'row',
    color: '#7b3eff',
    fontSize: 20,
  },
  containerResetPinNumberForm: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 50,
  },
  iconInputForm: {
    width: 20,
    height: 20,
    margin: 10,
  }
});

export default ResetPinForm;
