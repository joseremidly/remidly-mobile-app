//@ts-nocheck
import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    Platform,
		View,
		Dimensions
} from 'react-native';
import { useNavigation } from '@react-navigation/native';

export interface Props {
  navigation: Object;
	home: boolean;
  title: string;
  type: string;
  status: string;
}

const HeaderCreditLinePayment: React.FC<Props> = (props: any) => {
	const {height} = Dimensions.get('window');
  const navigation = useNavigation();
  const leftAction = () => {
    {{props.onHome ? navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [
          { name: 'Home' },
        ],
      })
    ) : navigation.goBack()}}
  }
  return (
    <>
      <View style={styles(props.status)[props.type]}>
        <TouchableOpacity onPress={leftAction} style={{justifyContent: "center", alignItems: 'center', marginTop: 5}}>
          <Image style={styles(props.status).iconButtonStyleLeft} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599596734/app/crecy_icons_arrow_header_white-01_dg8lby.png' }} resizeMode='contain'></Image>
        </TouchableOpacity>
        <Text style={styles(props.status)[`text${props.type}`]}>{props.title}</Text>
      </View>
    </>
  );
};

const styles = (type : any) => StyleSheet.create({
  home: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
		alignSelf: 'stretch',
    backgroundColor: type === 'expired' || type === 'rejected' ? '#ed1e79' : '#7b3eff',
    width: '100%',
    height: Platform.select({
      ios: 120,
      android: type === 'expired'  || type === 'rejected' ? 160 : 120
    }),
  },
  oxxo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
		alignSelf: 'stretch',
    backgroundColor: type === 'expired' || type === 'rejected' ? '#ed1e79' : '#7b3eff',
    width: '100%',
    height: Platform.select({
      ios: 120,
      android: 160
    }),
    paddingTop: 3,
  },
  app: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
    backgroundColor: type === 'expired' || type === 'rejected' ? '#ed1e79' : '#7b3eff',
    width: '100%',
    height: Platform.select({
      ios: 120,
      android: 180
    }),
    paddingTop: 8,
  },
  branch: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
		alignSelf: 'stretch',
    backgroundColor: type === 'expired' || type === 'rejected' ? '#ed1e79' : '#7b3eff',
    width: '100%',
    height: Platform.select({
      ios: 120,
      android: 160
    }),
    paddingTop: 3,
  },
  textButtonStyle: {
    width: '70%',
    textAlign: 'center',
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: 'white',
    marginLeft: 15,
  },
  texthome: {
    width: '70%',
    textAlign: 'center',
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: 'white',
    marginLeft: 15,
  },
  textoxxo: {
    width: '70%',
    textAlign: 'center',
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: 'white',
    marginLeft: 15,
  },
  textbranch: {
    width: '70%',
    textAlign: 'center',
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 26,
    color: 'white',
    marginLeft: 15,
  },
  textapp: {
    width: '70%',
    textAlign: 'center',
    fontFamily: 'Baloo2-ExtraBold',
    fontSize: 24,
    lineHeight: 27,
    color: 'white',
    paddingHorizontal: 35,
    marginTop: 5
  },
  iconButtonStyleLeft: {
    width: 30,
    height: 30,
    marginLeft: 20,
  },
  iconButtonStyleRight: {
    width: 25,
    height: 25,
    marginRight: 20,
  },
});

export default HeaderCreditLinePayment;