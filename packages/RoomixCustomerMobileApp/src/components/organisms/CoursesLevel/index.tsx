import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import Lessons from 'src/components/organisms/Lessons';

export interface Props {
  levelNumber: number;
  levelName: string;
  lessons: Object;
}

const CoursesLevel: React.FC<Props> = (props: any) => {
  return (
    <>
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <View style={styles.levelNumberContainer}>
            <Text style={styles.levelNumberText}>
              Nivel {props.levelNumber}
            </Text>
          </View>
          <View style={styles.levelNameContainer}>
            <Text style={styles.levelNameText}>{props.levelName}</Text>
          </View>
        </View>
        <Lessons lessons={props.lessons} />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {},
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    marginLeft: 20,
  },
  levelNumberContainer: {
    backgroundColor: '#7e3dff',
    borderRadius: 20,
    paddingHorizontal: 16,
    paddingVertical: 4,
    marginRight: 20,
  },
  levelNumberText: {
    fontFamily: 'Baloo2-Bold',
    color: 'white',
    fontSize: 14,
  },
  levelNameContainer: {},
  levelNameText: {
    fontFamily: 'Baloo2-Bold',
    fontSize: 14,
    color: '#5a6175',
    textTransform: 'uppercase',
  },
});

export default CoursesLevel;
