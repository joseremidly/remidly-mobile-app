// @ts-nocheck
import React from 'react';
import ReactNativeBiometrics from 'react-native-biometrics';
import {
  StyleSheet,
  SafeAreaView,
  View,
  ActivityIndicator,
} from 'react-native';
import {
  CommonActions,
} from '@react-navigation/native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import Header from 'src/components/organisms/Header';
import LoginPinInput from 'src/components/organisms/LoginPinInput';
import ButtonBiometric from 'src/components/atoms/ButtonBiometric';
import ButtonTransparent from 'src/components/atoms/ButtonTransparent';

export interface Props {
  navigation: Object;
  onCorrectLogin: Function;
  stateService: string;
}

const LoginPinForm: React.FC<Props> = (props: any) => {
  const [biometricsSupported, setBiometricsSupported] = React.useState(false);

  const loginBiometric = () => {
    ReactNativeBiometrics.isSensorAvailable()
      .then((resultObject) => {
        const { available, biometryType } = resultObject
        if (available && biometryType === ReactNativeBiometrics.TouchID) {
          console.log('TouchID is supported')
          setBiometricsSupported(true)
        } else if (available && biometryType === ReactNativeBiometrics.FaceID) {
          console.log('FaceID is supported')
          setBiometricsSupported(true)
        } else if (available && biometryType === ReactNativeBiometrics.Biometrics) {
          console.log('Biometrics is supported')
          setBiometricsSupported(true)
        } else {
          console.log('Biometrics not supported')
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }
  return (
    <>
      <StatusBar backgroundColor='#7b3eff' barStyle='light-content' />
      <SafeAreaView style={styles.sectionLoginPinContainer}>
        <Header />
        <LoginPinInput onPinLogin={(pin: number) => props.onCorrectLogin(pin)} onResetPin={() => props.navigation.navigate('ResetPin')} />
        <View style={styles.containerBiometricsArea}>
          {props.stateService === 'loading'
            && (
              <ActivityIndicator size='large' color='#7b3eff' animating={true} />
            )}
          {props.stateService !== 'loading' && biometricsSupported
            && (
              <ButtonBiometric label='Usar huella digital' onPress={() => loginBiometric()} src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599762241/app/crecy_icons_touch_id_white_ortjix.png' />
            )}
          <ButtonTransparent
            label='Cambiar de cuenta'
            fontSize={16}
            src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599251160/app/back_1_bl4as0.png'
            onPress={
              () =>
                props.navigation.dispatch(
                  CommonActions.reset({
                    index: 1,
                    routes: [
                      { name: 'Login' },
                    ],
                  })
                )
              }
          >
          </ButtonTransparent>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  sectionLoginPinContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  containerBiometricsArea: {
    height: 85,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  }
});

export default LoginPinForm;
