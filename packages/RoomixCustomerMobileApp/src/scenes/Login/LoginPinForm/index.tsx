// @ts-nocheck
import LoginPinForm from './LoginPinForm';
import LoginPinFormHOC from './LoginPinFormHOC';

export default LoginPinFormHOC(LoginPinForm);
