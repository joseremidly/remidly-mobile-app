// @ts-nocheck
import React from 'react';
import {
  CommonActions,
  useFocusEffect,
} from '@react-navigation/native';
import { useServices } from 'src/data/providers/ServicesProvider';
import { useAuth } from 'src/data/providers/AuthProvider';

export default (LoginPinForm) => (props: any) => {
  const { navigation, route } = props;
  const { stateService, services: { loginService } } = useServices();
  const { status, phoneSaved, signIn, saveLogin } = useAuth();
  const [phone, setPhone] = React.useState(route.params?.phone);
  const checkPreviousLogin = (phoneSaved: string) => {
    if (phoneSaved !== null) {
      setPhone(parseInt(phoneSaved))
    }
  }
  const login = (pin: number) => {
    loginService(phone, pin).then((res) => {
      if (res) {
        saveLogin(phone)
        signIn(res.access_token)
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              { name: 'Home' },
            ],
          })
        )
      }
    })
  }
  useFocusEffect(
    React.useCallback(() => {
      checkPreviousLogin(phoneSaved)
    }, [])
  );
  return (
    <LoginPinForm
      navigation={navigation}
      stateService={stateService}
      onCorrectLogin={(pin: number) => login(pin)}
    />
  );
};
