// @ts-nocheck
import LoginForm from './LoginForm';
import LoginFormHOC from './LoginFormHOC';

export default LoginFormHOC(LoginForm);
