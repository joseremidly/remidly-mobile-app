// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import Header from 'src/components/organisms/Header';
import LoginFormInput from 'src/components/organisms/LoginFormInput';

export interface Props {
  onClickLogin: Function;
}

const LoginForm: React.FC<Props> = (props: any) => {
  return (
    <>
      <StatusBar backgroundColor='#7b3eff' barStyle='light-content' />
      <SafeAreaView style={styles.sectionLoginContainer}>
        <Header />
        <LoginFormInput onLogin={props.onClickLogin}/>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  sectionLoginContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});

export default LoginForm;
