// @ts-nocheck
import React from 'react';

export default (LoginForm) => (props: any) => {
  const { navigation } = props;
  const login = (phone: number) => {
    console.log('login: ' + phone)
    navigation.navigate('LoginPinForm', { phone })
  }
  return (
    <LoginForm
      onClickLogin={(phone) => login(phone)}
    />
  );
};
