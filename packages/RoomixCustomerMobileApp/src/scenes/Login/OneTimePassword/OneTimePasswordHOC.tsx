// @ts-nocheck
import React from 'react';
import { useServices } from 'src/data/providers/ServicesProvider';
import { useAuth } from 'src/data/providers/AuthProvider';
import {
  CommonActions,
} from '@react-navigation/native';

export default (OneTimePassword) => (props: any) => {
  const { navigation, route } = props;
  const { stateService, services: { loginOneTime } } = useServices();
  const { signIn, saveLogin } = useAuth();

  const verify = (code: number) => {
    loginOneTime(route.params.phone, code).then(res => {
      if (res) {
        saveLogin(route.params.phone)
        signIn(res.access_token)
        navigation.navigate('PinForm')
        // navigation.dispatch(
        //   CommonActions.reset({
        //     index: 0,
        //     routes: [
        //       { name: 'Home' },
        //     ],
        //   })
        // )
      }
    })
  }
  return (
    <OneTimePassword
      navigation={navigation}
      route={route}
      stateService={stateService}
      onClickVerifyCode={(code) => verify(code)}
    />
  );
};

