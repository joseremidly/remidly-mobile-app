// @ts-nocheck
import OneTimePassword from './OneTimePassword';
import OneTimePasswordHOC from './OneTimePasswordHOC';

export default OneTimePasswordHOC(OneTimePassword);