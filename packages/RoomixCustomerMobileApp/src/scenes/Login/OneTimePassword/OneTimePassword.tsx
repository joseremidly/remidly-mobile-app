// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  ActivityIndicator,
  Keyboard,
} from 'react-native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import Header from 'src/components/organisms/Header';
import ButtonMid from 'src/components/atoms/ButtonMid';
import CodeFormInput from 'src/components/organisms/CodeFormInput';

export interface Props {
  navigation: Object;
  route: Object;
  onClickVerifyCode: Function;
  stateService: string;
}

const OneTimePassword: React.FC<Props> = (props: any) => {
  const [active, setActive] = React.useState(false);
  const [codeMemo, setCodeMemo] = React.useState('');

  const sendVerifyCode = (code: number) => {
    setCodeMemo(code)
    setActive(true)
    Keyboard.dismiss();
    props.onClickVerifyCode(code)
  }
  return (
    <>
      <StatusBar backgroundColor='#7b3eff' barStyle='light-content' />
      <SafeAreaView style={styles.sectionOneTimePasswordContainer}>
        <Header />
        <CodeFormInput phone={props.route.params?.phone ? props.route.params?.phone : '0000000000'} onCheckCode={(code) => sendVerifyCode(code)} changeNumber={() => props.navigation.navigate('PhoneForm')} resendCode={() => props.navigation.navigate('PhoneForm')} help={() => props.navigation.navigate('Home')}/>
        <View style={styles.containerOneTimePasswordButton}>
          {props.stateService === 'loading'
            && (
              <ActivityIndicator size='large' color='#7b3eff' animating={true} />
            )}
          {props.stateService !== 'loading'
            && (
              <ButtonMid label='Continuar' secondary={!active} onPress={() => sendVerifyCode(codeMemo)}></ButtonMid>
            )}
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  sectionOneTimePasswordContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  containerOneTimePasswordButton: {
    marginBottom: 20,
  }
});

export default OneTimePassword;
