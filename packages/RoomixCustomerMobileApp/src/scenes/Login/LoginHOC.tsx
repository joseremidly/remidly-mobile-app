// @ts-nocheck
import React from 'react';
import {
  useFocusEffect,
} from '@react-navigation/native';
import { useAuth } from 'src/data/providers/AuthProvider';

export default (Login) => (props: any) => {
  const { navigation } = props;
  const { signOut } = useAuth();
  useFocusEffect(
    React.useCallback(() => {
      signOut()
    }, [])
  );
  return (
    <Login
      navigation={navigation}
    />
  );
};
