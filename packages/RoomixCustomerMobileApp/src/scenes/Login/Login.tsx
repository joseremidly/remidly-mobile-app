// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Image,
  NativeModules,
  Platform,
  Dimensions
} from 'react-native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import ButtonLittle from '../../components/atoms/ButtonLittle';
import ButtonTransparent from '../../components/atoms/ButtonTransparent';
import LoginButtons from '../../components/organisms/LoginButtons';

export interface Props {
  navigation: Object;
}

const Login: React.FC<Props> = (props: any) => {
  const {width, height} = Dimensions.get('window');

  React.useEffect(() => {
    if (Platform.OS  === 'android') {
      NativeModules.FullScreen.disable();
    }
  }, []);
  return (
    <>
      <StatusBar backgroundColor='#ffffff' barStyle='dark-content' />
      <SafeAreaView style={styles(height).sectionLoginContainer}>
        <View style={styles(height).containerImageCrecy}>
          <Image style={styles(height).imageCrecy} source={{uri: 'https://res.cloudinay.com/crecy-io/image/upload/f_auto,q_auto/v1599533279/app/crecy-login_hlr5pj.png'}}></Image>
        </View>
        <View style={styles(height).containerImageBambu}>
          <Image style={styles(height).imageBambu} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599533307/app/background-login_ud73fd.png'}} resizeMode='contain'></Image>
        </View>
        <View style={styles(height).containerDiscoverButton} >
        </View>
        <LoginButtons goLogin={() => props.navigation.navigate('Home')} goCreate={() => console.log('click create account')}/>
        <View style={styles(height).containerTerms} >
          <ButtonTransparent label='TÉRMINOS Y CONDICIONES' fontSize={14} src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599251160/app/back_1_bl4as0.png' onPress={() => props.navigation.navigate('Example')} ></ButtonTransparent>
          <ButtonTransparent label='POLÍTICA DE PRIVACIDAD' fontSize={14} src='https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1599251160/app/back_1_bl4as0.png' onPress={() => props.navigation.navigate('Example')} ></ButtonTransparent>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = (height: any )  => StyleSheet.create({
  sectionLoginContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  containerImageCrecy: {
    width: 190,
    position: 'absolute',
    top: '20%',
    left: 0,
  },
  imageCrecy: {
    width: '80%',
    height: 190,
  },
  containerImageBambu: {
    width: 300,
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  imageBambu: {
    width: '80%',
    height: 400,
  },
  containerDiscoverButton: {
    marginTop: 20,
  },
  containerTerms: {
    marginBottom: 20,
  },
});

export default Login;
