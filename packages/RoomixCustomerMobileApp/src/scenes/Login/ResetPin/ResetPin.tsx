// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import Header from 'src/components/organisms/Header';
import ResetPinForm from 'src/components/organisms/ResetPinForm';

export interface Props {
  navigation: Object;
  stateService: String;
  onClickSendCode: Function;
}
const ResetPin: React.FC<Props> = (props: any) => {
  return (
    <>
      <StatusBar backgroundColor='#7b3eff' barStyle='light-content' />
      <SafeAreaView style={styles.sectionResetPinContainer}>
        <Header />
        <ResetPinForm stateService={props.stateService} onSentCode={props.onClickSendCode}/>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  sectionResetPinContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});

export default ResetPin;
