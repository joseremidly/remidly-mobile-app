// @ts-nocheck
import ResetPin from './ResetPin';
import ResetPinHOC from './ResetPinHOC';

export default ResetPinHOC(ResetPin);
