// @ts-nocheck
import React from 'react';
import { useServices } from 'src/data/providers/ServicesProvider';

export default (ResetPin) => (props: any) => {
  const { navigation } = props;
  const { stateService, services: { resetPassword } } = useServices();

  const reset = (phone: number) => {
    resetPassword(phone).then((res) => {
      console.log('Response: ')
      console.log(res)
      navigation.navigate('OneTimePassword', { phone })
    })
  }
  return (
    <ResetPin
      navigation={navigation}
      stateService={stateService}
      onClickSendCode={(phone) => reset(phone)}
    />
  );
};
