// @ts-nocheck
import Calculator from './Calculator';
import CalculatorHOC from './CalculatorHOC';

export default CalculatorHOC(Calculator);
