// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  ScrollView,
  SafeAreaView,
  RefreshControl,
  View,
  ActivityIndicator
} from 'react-native';
import {
  CommonActions,
} from '@react-navigation/native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderHome from 'src/components/molecules/HeaderHome';
import TopAccountsHome from 'src/components/organisms/TopAccountsHome';
import LoadingView from 'src/components/organisms/LoadingView';
import Calculator from 'src/components/organisms/Calculator';
// import NewsFeedCrecyCheckout from 'src/components/organisms/NewsFeedCrecyCheckout';
import NewsFeedCrecyHelp from 'src/components/organisms/NewsFeedCrecyHelp';

export interface Props {
  navigation: Object;
  stateService: String;
  dolar: any;
  mxn: any;
  onRefresh: Function;
}

const NewsFeed: React.FC<Props> = (props: any) => {
  const [refreshing, setRefreshing] = React.useState(false);
  return (
    <>
      <StatusBar backgroundColor='#7b3eff' barStyle='light-content' />
      <SafeAreaView style={styles.sectionNewsFeedContainer}>  
        <HeaderHome home />
        {props.stateService === 'loading' && (
          <View style={styles.loadingView}>
            <LoadingView size="large" color="#7b3eff" animating={true} />
          </View>
        )}
        {props.stateService !== 'loading' && (
            <>
              <ScrollView 
                  style={styles.sectionContainer}
                  showsVerticalScrollIndicator ={false}
                  refreshControl={
                    <RefreshControl
                      refreshing={refreshing}
                      onRefresh={props.onRefresh}
                    />
                  }
                >
                  <Calculator
                    dolar={props.dolar}
                    mxn={props.mxn}
                    send={props.send}
                    receive={props.receive}
                    handleChange={props.handleChange}
                    onBlurInput={props.onBlurInput}
                    onPress={props.onPress}
                   />
                </ScrollView>
            </>
        )}
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  sectionNewsFeedContainer: {
    backgroundColor: 'white',
    flex: 1,
    width: '100%',
  },
  sectionContainer: {
    backgroundColor: '#F4F4F7',
    width: '100%',
    paddingTop: 20,
  },
  containerTotalBalance: {
    paddingHorizontal: '7%',
  }
});

export default NewsFeed;
