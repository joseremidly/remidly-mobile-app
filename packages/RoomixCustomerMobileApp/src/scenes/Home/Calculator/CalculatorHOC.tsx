// @ts-nocheck
import React from 'react';
import {
  useFocusEffect,
} from '@react-navigation/native';
import { useAuth } from 'src/data/providers/AuthProvider';
import { useServices } from 'src/data/providers/ServicesProvider';

export default (NewsFeed) => (props: any) => {
  const { navigation } = props;
  const [dolar, setDolar] = React.useState('');
  const [mxn, setMxn] = React.useState('');
  const [send, setSend] = React.useState(0);
  const [value, setValue] = React.useState(0);
  const { stateService, services: { getPrices, postSendMoney, updateCalculator } } = useServices();

  const loadDataUser = (data: any) => {
    getPrices()
      .then(data => {
        console.log(data);
        setMxn(data['mxn'])
        setDolar(data['usd'])
      })
  };
  const handleChange = (data:number) => {
    console.log(data)
    console.log(parseInt(data )* 0.5)
    if (data >= 0 && data !== ''){
      setSend(parseInt(data))
      setPercentage()
    } else {
      setSend(0)
    }
  }
  const setPercentage = () => {
    console.log(send * 0.7)
  }
  const onBlurInput = () => {
    console.log('onBlurInput')
    if (send != "" && send >= 1) {
      calculateAverage()
    }
  };
  const calculateAverage = () => {
    postSendMoney(send)
    .then(data => {
      setValue(data['total'])
    })
  }
  const onPress = () => {
    updateCalculator(value)
    props.navigation.navigate('Recipient')
  }
  const onRefresh = React.useCallback(() => {
    loadDataUser();
  }, []);
  useFocusEffect(
    React.useCallback(() => {
      console.log('Loading data user...')
      loadDataUser();
    }, [])
  );
  useFocusEffect(
    React.useCallback(() => {
      if (send !== '') {
        console.log('calcular monto')
      }
    }, [send])
  );
  return (
    <>
      <NewsFeed
        stateService={stateService}
        navigation={navigation}
        dolar={dolar}
        mxn={mxn}
        send={send}
        receive={value}
        handleChange={handleChange}
        onBlurInput={onBlurInput}
        onRefresh={onRefresh}
        onPress={onPress}
      />
    </>
  );
};
