// @ts-nocheck
import Recipient from './Recipient';
import RecipientHOC from './RecipientHOC';

export default RecipientHOC(Recipient);
