// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  ScrollView,
  View,
  Text
} from 'react-native';
import CustomStatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderHome from 'src/components/molecules/HeaderHome';
import CardAnalytics from 'src/components/molecules/CardAnalytics';
import CardAnalyticsEmpty from 'src/components/molecules/CardAnalyticsEmpty'
import LoadingView from 'src/components/organisms/LoadingView';
import Receptor from 'src/components/organisms/Receptor'

export interface Props {
  stateService: String;
  goRoute: Function;
  accounts: any;
  totalBalance: number;
  statsBalance: any;
}

const Analytics: React.FC<Props> = (props: any) => {
  const onPress = () => {
    console.log('Tets')
  }
  return (
    <>
      <CustomStatusBar backgroundColor='#7b3eff' barStyle='light-content' />
      <SafeAreaView style={styles.containerAnalytics}>  
        <HeaderHome home />
        <ScrollView style={styles.scrollElementsAnalytics}>
          <Receptor onPress={props.onPress} handleChange={props.handleChange}/>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  containerAnalytics: {
    backgroundColor: '#F4F4F7',
    height: '100%',
    width: '100%',
    alignItems: 'center',
  },
  containerElementsAnalytics: {
    position: 'absolute',
    marginTop: 40,
    alignItems: 'center',
    width: '90%',
    height: '100%',
  },
  scrollElementsAnalytics: {
    width: '90%',
    marginTop: 30,
    paddingBottom: 20,
    marginBottom: 50,
  },
  containerCardAnalyticsHeader: {
    marginTop: 30,
    width: '100%',
  },
  containerCardAnalytics: {
    marginTop: 20,
    width: '100%',
  },
  textAnalytics: {
    width: '100%',
    flexDirection: 'row',
    fontFamily: 'Baloo2-SemiBold',
    textAlign: 'left',
    alignSelf: 'stretch',
    color: '#9AA6A7',
    fontSize: 14,
    lineHeight: 27,
    marginTop: 20,
    marginBottom:-15,
    marginLeft: 9
  },
});

export default Analytics;
