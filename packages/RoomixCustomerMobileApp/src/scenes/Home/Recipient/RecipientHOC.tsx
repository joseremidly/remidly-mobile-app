// @ts-nocheck
import React from 'react';
import {
  useFocusEffect,
} from '@react-navigation/native';
import { useServices } from 'src/data/providers/ServicesProvider';

export default (Analytics) => (props: any) => {
  const [accounts, setAccounts] = React.useState([]);
  const [edit, setEdit] = React.useState({})
  const [totalBalance, setTotalBalance] = React.useState('');
  const [statsBalance, setStatsBalance] = React.useState({ income: 0, outflow: 0 });
  const { stateService, calculator, recipient, services: { updateRecipient, getBalanceStats } } = useServices();
  const goToRoute = (route: string) => {
    props.navigation.navigate(route)
  }
  const handleChange = (e, concept) => {
    console.log(e, concept)
      setEdit({
        bodyRecipient: {
          ...edit.bodyRecipient,
          [concept]: e
        }
      })
  }
  const onPress = () => {
    console.log(edit)
    updateRecipient(edit)
    props.navigation.navigate('Transactions')
  }

  useFocusEffect(
    React.useCallback(() => {
      console.log('Loading data user...', calculator)
    }, [])
  );
  return (
    <Analytics
      stateService={stateService}
      goRoute={goToRoute}
      accounts={accounts}
      totalBalance={totalBalance}
      statsBalance={statsBalance}
      onPress={onPress}
      handleChange={handleChange}
    />
  );
};
