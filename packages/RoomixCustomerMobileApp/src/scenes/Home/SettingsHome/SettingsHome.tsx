// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  ScrollView,
  SafeAreaView,
  NativeModules,
  Platform,
} from 'react-native';
import {
  CommonActions,
} from '@react-navigation/native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderSecondary from 'src/components/organisms/HeaderSecondary';
import SettingsButtons from 'src/components/organisms/SettingsButtons';

export interface Props {
  navigation: Object;
}

const SettingsHome: React.FC<Props> = (props: any) => {
  const goToRoute = (route: string) => {
    if (route === 'SignOut') {
      console.log('Sign out ...')
      props.navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [
            { name: 'AuthNavigator' },
          ],
        }))
    } else {
      console.log('Go to: ' + route)
      props.navigation.navigate(route ? route : 'Home')
    }
  }
  React.useEffect(() => {
    if (Platform.OS  === 'android') {
      NativeModules.FullScreen.gray();
    }
  }, []);
  return (
    <>
      <StatusBar backgroundColor='#F4F4F7' barStyle='dark-content' />
      <SafeAreaView style={styles.containerSettingsHome}>
        <HeaderSecondary title='Configuración' onHome />
        <ScrollView style={styles.containerScrollSettingsElements}>
          <SettingsButtons goRoute={(route: string) => goToRoute(route)} />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  containerSettingsHome: {
    backgroundColor: '#F4F4F7',
    height: '100%',
    width: '100%',
  },
  containerScrollSettingsElements: {
    backgroundColor: '#F4F4F7',
    width: '100%',
  },
});

export default SettingsHome;
