// @ts-nocheck
import SettingsHome from './SettingsHome';
import SettingsHomeHOC from './SettingsHomeHOC';

export default SettingsHomeHOC(SettingsHome);
