// @ts-nocheck
import React from 'react';

export default (SettingsHome) => (props: any) => {
  const { navigation } = props;

  return (
    <SettingsHome
      navigation={navigation}
    />
  );
};
