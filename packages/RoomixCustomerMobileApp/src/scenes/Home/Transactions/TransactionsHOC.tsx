// @ts-nocheck
import React from 'react';
import {
  useFocusEffect,
} from '@react-navigation/native';
import { useServices } from 'src/data/providers/ServicesProvider';

export default (Transactions) => (props: any) => {
  const { navigation } = props;
  const [transactions, setTransactions] = React.useState([]);
  const [edit, setEdit] = React.useState({})
  const { stateService, recipient, services: { updateTransaction } } = useServices();
  const handleChange = (e, concept) => {
    console.log(e, concept)
    if (e) {
      setEdit({
        bodyRecipient: {
          ...edit.bodyRecipient,
          [concept]: e
        }
      })
    } else {
      setEdit({
        body: {
          ...edit.bodyRecipient,
          [concept]: ''
        }
      })
    }
  }
  const onPress = () => {
    console.log(edit)
    updateTransaction(edit)
    props.navigation.navigate('Succesful')
  }
 
  useFocusEffect(
    React.useCallback(() => {
      console.log('loading transactions...', recipient);
    }, [])
  );
  return (
    <Transactions
      navigation={navigation}
      stateService={stateService}
      transactions={transactions}
      onPress={onPress}
      handleChange={handleChange}
    />
  );
};
