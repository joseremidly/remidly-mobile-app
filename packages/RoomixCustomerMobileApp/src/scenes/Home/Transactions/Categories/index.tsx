// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import TopCategories from 'src/components/organisms/TopCategories'
import TransactionsList from 'src/components/organisms/TransactionsList'
import { getTransactions } from 'src/data/services/api'

export interface Props {
  navigation: Object;
}

const Categories: React.FC<Props> = (props: any) => {
  const [transactions, setTransactions] = React.useState([]);
  React.useEffect(() => {
    getTransactions().then((res: any) => {
      console.log('GET SERVER TRANSACTIONS')
      // console.log(res)
      setTransactions(res)
    })
  }, [])
  return (
    <>
      <StatusBar backgroundColor='#7b3eff' barStyle='light-content' />
      <SafeAreaView style={styles.sectionTransactionsContainer}>
        <TopCategories />
        <TransactionsList transactions={transactions}/>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  sectionTransactionsContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default Categories;
