// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderHome from 'src/components/molecules/HeaderHome';
import SendDataInformation from 'src/components/organisms/SendDataInformation';

export interface Props {
  navigation: Object;
  transactions: Array;
  stateService: String;
}

const Transactions: React.FC<Props> = (props: any) => {
  const searchTransactions = (input: string) => {
    console.log('Searching...')
    console.log(input)
  }
  return (
    <>
      <StatusBar backgroundColor='#7b3eff' barStyle='light-content' />
      <SafeAreaView style={styles.sectionTransactionsContainer}>
        <HeaderHome home />
        <View style={styles.sectionTransactionsList}>
          {props.stateService === 'loading'
            && (
              <View style={styles.containerLoading}>
                <ActivityIndicator size='large' color='#7b3eff' animating={true} />
              </View>
            )}
          {props.stateService !== 'loading'
            && (
              <ScrollView style={styles.scrollElementsAnalytics}>
                <SendDataInformation onPress={props.onPress} handleChange={props.handleChange}/>
              </ScrollView>
            )}
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  sectionTransactionsContainer: {
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  sectionTransactionsList: {
    top: -20,
    alignItems: 'center',
    backgroundColor: 'white',
    borderTopLeftRadius: 26,
    borderTopRightRadius: 26,
    width: '100%',
    paddingTop: 20,
  },
  containerLoading: {
    height: '80%',
    justifyContent: 'center',
  },
  containerWithoutTransactions: {
    alignItems: 'center',
    justifyContent: 'center',
    height: '80%',
  },
  textWithoutTransactions: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 20,
    color: '#b3b3b3',
  },
  scrollElementsAnalytics: {
    width: '90%',
    marginTop: 30,
    paddingBottom: 20,
    marginBottom: 80,
  },
});

export default Transactions;
