// @ts-nocheck
import Transactions from './Transactions';
import TransactionsHOC from './TransactionsHOC';

export default TransactionsHOC(Transactions);
