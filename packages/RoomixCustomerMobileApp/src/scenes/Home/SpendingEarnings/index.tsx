// @ts-nocheck
import SpendingEarnings from './SpendingEarnings';
import SpendingEarningsHOC from './SpendingEarningsHOC';

export default SpendingEarningsHOC(SpendingEarnings);
