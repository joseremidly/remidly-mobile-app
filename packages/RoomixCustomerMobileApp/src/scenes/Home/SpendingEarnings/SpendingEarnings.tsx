// @ts-nocheck
import React, { useEffect } from 'react';
import {
  StyleSheet,
	ScrollView,
	View,
  SafeAreaView,
  Dimensions,
  ActivityIndicator
} from 'react-native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderSpendingEarnings from 'src/components/organisms/HeaderSpendingEarnings';
import Graphs from 'src/components/molecules/GraphsContainer';

export interface Props {
  stateService: any;
  statsCircle: any;
  expenses: boolean;
  category: boolean;
  overall: Function;
  incomeExpenses: Function;
  body: any;
  month: any;
  spent: any;
  selectGeneral: any;
  selectCategory: any;
  onPressGraphGeneral: Function;
  indexPressTable: Function;
  spentCategory: any;
  categoryName: any;
}

const SpendingEarnings: React.FC<Props> = (props: any) => {
  const {height} = Dimensions.get('window');
  return (
    <>
      <StatusBar backgroundColor='#7b3eff' barStyle='light-content' />
      <SafeAreaView style={styles.sectionNewsFeedContainer}>
			<HeaderSpendingEarnings
        onPress={props.incomeExpenses}
        select={props.expenses}
        home
      />
        {props.stateService === 'loading'
          && (
            <View style={styles.containerLoading}>
              <ActivityIndicator size='large' color='#7b3eff' animating={true} />
            </View>
          )}
        {props.stateService !== 'loading'  && props.statsCircle !== '' && props.body !== '' 
          && (
            <ScrollView style={{zIndex: 2, marginHorizontal: 15, marginTop: height <= 680 ? 135 : 135}}>
              <Graphs
                overall={props.overall}
                backgroundGeneral={props.expenses === true ? '#FF2169' : '#00A99D'}
                month={props.month}
                spent={props.spent}
                categoryName={props.categoryName}
                spentCategory={props.spentCategory}
                category={props.category}
                circleData={props.statsCircle}
                body={props.body}
                onPressGeneral={props.onPressGraphGeneral}
                onPress={props.indexPressTable}
                fillSelectMonth={props.selectGeneral}
                fillSelectCategory={props.selectCategory}
              />
            </ScrollView>
          )}
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  sectionNewsFeedContainer: {
    backgroundColor: '#F4F4F7',
    height: '100%',
    width: '100%',
  },
  sectionContainer: {
    backgroundColor: '#F4F4F7',
    width: '100%',
  },
  containerLoading: {
    height: '80%',
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    zIndex: 2, 
    marginHorizontal: 15,
    marginTop: 135,
    borderRadius: 9
  },
});

export default SpendingEarnings;