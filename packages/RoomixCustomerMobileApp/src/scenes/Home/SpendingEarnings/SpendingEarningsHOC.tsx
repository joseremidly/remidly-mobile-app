// @ts-nocheck
import React from 'react';
import {
  useFocusEffect,
} from '@react-navigation/native';
import { useServices } from 'src/data/providers/ServicesProvider';
import months from 'src/utils/months'

export default (SpendingEarnings) => (props: any) => {
  const { stateService, services: { 
    getInflowStats,
    getOutflowStats,
    getExpensesStats,
    getIncomeStats} } = useServices();
  const [expenses, setExpenses] = React.useState(true);
  const [category, setCategory] = React.useState(true)
  const [body, setBody] = React.useState('')
  const [month, setMonth] = React.useState('');
  const [spent, setSpent] = React.useState('');
  const [selectGeneral, setSelectGeneral] = React.useState()
  const [selectCategory, setSelectCategory] = React.useState(0);
  const [categoryName, setCategoryName] = React.useState('');
  const [spentCategory, setSpentCategory] = React.useState('');
  const [dataCircle, setDataCircle] = React.useState('')

  const orderData = (data: any) => {
    let formBody = [];
    for (const property in data) {
      formBody.push({
        month: property,
        expenses: data[property],
        index: months[property].index
      });
    }
    return [...formBody].sort((a, b) =>  a.index > b.index ? 1 : a.index < b.index ? -1 : 0)
  }
  const getExpensesInformation = () => {
    getOutflowStats().then((res) => {
      if (res) {
        let sortedlist = orderData(res)
        setBody(sortedlist)
        setSelectGeneral(sortedlist.length - 1)
        getExpensesStats().then((res) => {
          if (res && res.length > 0) {
            setDataCircle(res)
          } 
        })
      }
    })
  }
  const getIncomeInformation = () => {
    getInflowStats().then((res) => {
      if (res) {
        let sortedlist = orderData(res)
        setBody(sortedlist)
        setSelectGeneral(sortedlist.length - 1)
        getIncomeStats().then((res) => {
          if (res && res.length > 0) {
            setDataCircle(res)
          } 
        })
      }
    })
  } 
  const incomeExpenses = (result: any) => {
    setExpenses(result)
	}
	const overallFunc = (result: any) => {
    setCategory(result)
  }
  const indexPressTable = (index: any, value: any) => {
    setCategoryName(value.category)
    setSpentCategory(value.expenses)
    setSelectCategory(index)
  }
	const onPressGraphGeneral = (index: any, value: any) => {
    setMonth(value.month)
    setSpent(value.expenses)
    setSelectGeneral(index)
  }
  useFocusEffect(
    React.useCallback(() => {
      console.log('loading transactions...');
      expenses ? getExpensesInformation() : getIncomeInformation()
      console.log('clear data')
      setMonth('')
      setSpent('')
      setCategoryName('')
      setSpentCategory('')
      setSelectCategory(0)
    }, [expenses])
  );
  return (
    <SpendingEarnings
      month={month}
      spent={spent}
      selectGeneral={selectGeneral}
      body={body}
      statsCircle={dataCircle}
      overall={overallFunc}
      incomeExpenses={incomeExpenses}
      expenses={expenses}
      category={category}
      stateService={stateService}
      indexPressTable={indexPressTable}
      onPressGraphGeneral={onPressGraphGeneral}
      categoryName={categoryName}
      spentCategory={spentCategory}
      selectCategory={selectCategory}
    />
  );
};
