import React from 'react';

import { StyleSheet, View, ActivityIndicator, ScrollView } from 'react-native';

import AdvanceSummary from 'src/components/organisms/AdvanceSummary';
import AdvanceDates from 'src/components/organisms/AdvanceDates';
import AdvanceSendCollect from 'src/components/organisms/AdvanceSendCollect';
import ButtonMid from 'src/components/atoms/ButtonMid';
import ButtonTransparent from 'src/components/atoms/ButtonTransparent';

export interface Props {
  navigation: Object;
  route: Object;
  stateService: string;
  depositDate: string | null;
  chargeDate: string | null;
  depositAccount: Object | null;
  chargeAccount: Object | null;
  availableChargeAccounts: Object | null;
  updateChargeAccount: Function;
  fee: number;
  type: string;
  amount: number;
}

const AdvanceConfirmation: React.FC<Props> = (props: any) => {
  return (
    <>
      <ScrollView>
        <AdvanceSummary
          type={props.type}
          fee={props.fee}
          amount={props.amount}
        />
        {props.stateService === 'loading' ? (
          <View style={styles.loadingView}>
            <ActivityIndicator size="large" color="#7b3eff" animating={true} />
          </View>
        ) : (
          <>
            <View style={styles.form}>
              <AdvanceDates
                dateDepositText={props.depositDate}
                dateChargeText={props.chargeDate}
              />
              <AdvanceSendCollect
                cardType="deposit"
                selectedAccount={props.depositAccount}
              />
              <AdvanceSendCollect
                cardType="charge"
                selectAccount={(newAccount) =>
                  props.updateChargeAccount(newAccount)
                }
                availableAccounts={props.availableChargeAccounts}
                selectedAccount={props.chargeAccount}
              />
              <View style={styles.buttonWrapper}>
                <ButtonMid label="Obtener el adelanto" />
              </View>
              <ButtonTransparent
                style={styles.tdc}
                label="Términos y condiciones"
              />
            </View>
          </>
        )}
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  loadingView: {
    marginTop: 30,
  },
  form: {
    marginHorizontal: 30,
    marginBottom: 20,
  },
  buttonWrapper: {
    marginTop: 40,
    alignItems: 'center',
    marginBottom: 16,
  },
});

export default AdvanceConfirmation;
