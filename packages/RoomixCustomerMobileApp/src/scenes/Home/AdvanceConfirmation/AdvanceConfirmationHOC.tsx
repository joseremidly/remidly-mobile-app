import React from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { useServices } from 'src/data/providers/ServicesProvider';

export default (AdvanceConfirmation) => (props: any) => {
  const {
    stateService,
    services: { getAdvanceConfirmationData },
  } = useServices();

  const params = props.route.params;
  const type = params.type;
  const fee = params.fee;
  const amount = params.amount;

  const [depositDate, setDepositDate] = React.useState(null);
  const [chargeDate, setChargeDate] = React.useState(null);
  const [depositAccount, setDepositAccount] = React.useState(null);
  const [availableChargeAccounts, setAvailableChargeAccounts] = React.useState(
    null,
  );
  const [chargeAccount, setChargeAccount] = React.useState(null);

  const getData = () => {
    getAdvanceConfirmationData(type, fee).then((data) => {
      setDepositDate(data.depositDate);
      setChargeDate(data.chargeDate);
      setDepositAccount(data.depositAccount);
      setAvailableChargeAccounts(data.chargeAccounts);
    });
  };

  const updateChargeAccount = (newAccount) => {
    console.log('Update');
    console.log(newAccount);
    setChargeAccount(newAccount);
  };

  useFocusEffect(
    React.useCallback(() => {
      getData();
    }, []),
  );

  return (
    <AdvanceConfirmation
      stateService={stateService}
      depositDate={depositDate}
      chargeDate={chargeDate}
      depositAccount={depositAccount}
      chargeAccount={chargeAccount}
      availableChargeAccounts={availableChargeAccounts}
      type={type}
      fee={fee}
      amount={amount}
      updateChargeAccount={updateChargeAccount}
      {...props}
    />
  );
};
