// @ts-nocheck
import AdvanceConfirmation from './AdvanceConfirmation';
import AdvanceConfirmationHOC from './AdvanceConfirmationHOC';

export default AdvanceConfirmationHOC(AdvanceConfirmation);
