// @ts-nocheck
import React from 'react';
import { useAuth } from 'src/data/providers/AuthProvider';
import io from 'socket.io-client'

export default (PalencaLinkAccount) => (props: any) => {
  const { navigation, route } = props;
  const [sandbox, setSandbox] = React.useState(false)
  const { dataUser } = useAuth();
  const accountAdded = (data: any) => {
    console.log('Account palenca added: ' + data)
    if (data === 'error') {
      navigation.navigate('Home')
    } else if (data === 'cancel') {
      if (route.params && route.params.nextStep) {
        navigation.navigate(route.params.nextStep, { jobType: route.params.jobType })
      } else {
        navigation.navigate('Home')
      }
    } else {
      if (route.params && route.params.nextStep) {
        navigation.navigate(route.params.nextStep, { jobType: route.params.jobType })
      } else {
        navigation.navigate('Home')
      }
    }
  }
  React.useEffect(() => {
    const socket = io('https://socket.palenca.com/')
    socket.on("connect", () => {
      console.log('connected: ....')
    })
    socket.onAny((event, ...args) => {
      // console.log('Input message: ')
      // console.log(args)
      if (args[0].user ===  dataUser.user_id && args[0].type === 'USER_CONECTED') {
        {route.params && route.params.nextStep === 'home' ? navigation.navigate('Home') : navigation.navigate(sandbox ? 'CreditOnboardingAccount' : route.params.nextStep, { jobType: route.params.jobType })}
      }
    })
  }, [])
  return (
    <PalencaLinkAccount
      url='https://www.palenca.com/form/bc62209e'
      isSandbox={sandbox}
      onAddedAccount={(data: any) => accountAdded(data)}
      userId={dataUser.user_id}
      home={route.params && route.params.nextStep === 'home'}
    />
  );
};
