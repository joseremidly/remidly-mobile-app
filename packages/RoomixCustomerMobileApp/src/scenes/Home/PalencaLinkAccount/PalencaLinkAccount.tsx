// @ts-nocheck
import React from 'react';
import { WebView } from 'react-native-webview';
import {
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import CustomStatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderSecondary from 'src/components/organisms/HeaderSecondary'

export interface Props {
  onAddedAccount: Function;
  url: String;
  userId: String;
  isSandbox: Boolean;
  home: Boolean;
}

const PalencaLinkAccount: React.FC<Props> = (props: any) => {
  return (
    <>
      <CustomStatusBar backgroundColor='#7b3eff' barStyle='light-content' />
      <SafeAreaView style={styles.sectionPalencaLinkAccountContainer}>
        <HeaderSecondary title='Conectar cuenta' backgroundColor='#7b3eff' arrowColor='white' textColor='#ffffff' onHome={props.home}/>
        <WebView
          source={{
            uri: `${props.url}/${props.isSandbox ? props.userId + '/?is_sandbox=true' : props.userId}`
          }}
          onMessage={(event) => props.onAddedAccount(event.nativeEvent.data)}
        />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  sectionPalencaLinkAccountContainer: {
    backgroundColor: '#e6e6e6',
    flex: 1,
  },
});

export default PalencaLinkAccount;
