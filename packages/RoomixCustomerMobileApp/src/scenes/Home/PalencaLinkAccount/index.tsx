// @ts-nocheck
import PalencaLinkAccount from './PalencaLinkAccount';
import PalencaLinkAccountHOC from './PalencaLinkAccountHOC';

export default PalencaLinkAccountHOC(PalencaLinkAccount);
