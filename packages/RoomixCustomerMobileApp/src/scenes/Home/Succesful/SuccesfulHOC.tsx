// @ts-nocheck
import React from 'react';
import {
  useFocusEffect,
} from '@react-navigation/native';
import { useServices } from 'src/data/providers/ServicesProvider';

export default (FinancialEducation) => (props: any) => {
  const { stateService, recipient, remitent, calculator} = useServices();
  const goToRoute = (route: string) => {
    props.navigation.navigate(route)
  }
  useFocusEffect(
    React.useCallback(() => {
      console.log('loading courses...', calculator, recipient, remitent)
    }, [])
  );
  return (
    <FinancialEducation
      stateService={stateService}
      goRoute={goToRoute}
      calculator={calculator}
      recipient={recipient}
      remitent={remitent}
    />
  );
};
