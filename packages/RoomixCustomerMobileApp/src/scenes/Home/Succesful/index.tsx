// @ts-nocheck
import Succesful from './Succesful';
import SuccesfulHOC from './SuccesfulHOC';

export default SuccesfulHOC(Succesful);
