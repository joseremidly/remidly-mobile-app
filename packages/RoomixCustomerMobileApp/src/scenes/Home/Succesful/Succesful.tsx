// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Image,
  Text
} from 'react-native';
import CustomStatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderHome from 'src/components/molecules/HeaderHome';
import EducationCard from 'src/components/molecules/EducationCard';

export interface Props {
  stateService: String;
  calculator: any;
  recipient: any;
  remitent: any;
  goRoute: Function;
}

const FinancialEducation: React.FC<Props> = (props: any) => {
  console.log(props.recipient.bodyRecipient.name)
  return (
    <>
      <CustomStatusBar backgroundColor='#7b3eff' barStyle='light-content' />
      <SafeAreaView style={styles.containerFinancialEducation}>
        <HeaderHome home />
        <View style={styles.containerElementsFinancialEducation}>
          <Image style={styles.imgSuccesful} source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/v1617096348/app/Crecy%20icons%20new%20dimensions/check_1_ji8cmo.png'}}></Image>
          <Text style={styles.regularText}>Envio Completo</Text>
          <View style={{marginVertical: 30}}>
            <Text style={styles.regularText2}>to: {props.recipient.bodyRecipient.name} {props.recipient.bodyRecipient.lastName}</Text>
            <Text style={styles.regularText2}>for: {props.calculator}MXN(Pesos Mexicanos)</Text>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  containerFinancialEducation: {
    backgroundColor: '#F4F4F7',
    height: '100%',
    width: '100%',
    alignItems: 'center',
  },
  containerElementsFinancialEducation: {
    marginTop: 40,
    alignItems: 'center',
    width: '90%',
    paddingTop: 20,
    justifyContent: 'center',
  },
  imgSuccesful: {
    width: 100,
    height: 100,
  },
  regularText: {
    fontSize: 24,
    fontFamily: "Baloo2-SemiBold",
    color: "#6E7C95",
  },
  regularText2: {
    fontSize: 18,
    fontFamily: "Baloo2-SemiBold",
    color: "#6E7C95",
  },
});

export default FinancialEducation;
