// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderSecondary from 'src/components/organisms/HeaderSecondary';
import EditFieldOrganism from 'src/components/organisms/EditFieldOrganism';

export interface Props {
  stateService: String;
  onClickSaveEmail: Function;
  email: String;
}

const EditEmail: React.FC<Props> = (props: any) => {
  return (
    <>
      <StatusBar backgroundColor='#F4F4F7' barStyle='dark-content' />
      <SafeAreaView style={styles.containerEditEmail}>
        <HeaderSecondary title='Editar correo'/>
        <EditFieldOrganism placeholder='Correo electrónico' stateService={props.stateService} onChangeField={props.onClickSaveEmail} content={props.email} />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  containerEditEmail: {
    backgroundColor: '#F4F4F7',
    height: '100%',
    width: '100%',
  },
});

export default EditEmail;
