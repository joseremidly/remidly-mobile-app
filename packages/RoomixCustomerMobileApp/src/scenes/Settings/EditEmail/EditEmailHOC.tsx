// @ts-nocheck
import React from 'react';
import { useServices } from 'src/data/providers/ServicesProvider';

export default (EditEmail) => (props: any) => {
  const { navigation } = props;
  const { stateService, services: { getDataUser, editDataUser } } = useServices();
  const [dataUser, setDataUser] = React.useState('')

  const updateEmail = (email: string) => {
    editDataUser({ email: email }).then((res) => {
      if (res) {
        navigation.navigate('EditProfile')
      }
    })
  }
  const getAllDataUser = () => {
    getDataUser().then((data: any) => {
      setDataUser(data);
    })
  }
  React.useEffect(() => {
    getAllDataUser();
  }, [])
  return (
    <>
      {dataUser !== ''
        && (
          <EditEmail
            stateService={stateService}
            onClickSaveEmail={(email: string) => updateEmail(email.toLowerCase())}
            email={dataUser.email}
          />
        )}
    </>
  );
};
