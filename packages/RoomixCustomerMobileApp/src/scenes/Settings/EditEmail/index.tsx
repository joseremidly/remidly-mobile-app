// @ts-nocheck
import EditEmail from './EditEmail';
import EditEmailHOC from './EditEmailHOC';

export default EditEmailHOC(EditEmail);
