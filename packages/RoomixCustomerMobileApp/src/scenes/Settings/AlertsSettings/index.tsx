// @ts-nocheck
import AlertsSettings from './AlertsSettings';
import AlertsSettingsHOC from './AlertsSettingsHOC';

export default AlertsSettingsHOC(AlertsSettings);
