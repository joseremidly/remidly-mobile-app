// @ts-nocheck
import React, { useState } from 'react';

import { useFocusEffect } from '@react-navigation/native';
import { useServices } from 'src/data/providers/ServicesProvider';
import LoadingView from 'src/components/organisms/LoadingView';

export default (AlertsSettings) => (props: any) => {
  const {
    alertsSettings,
    stateService,
    services: { getAlertsSettings, setAlertSettingsValue },
  } = useServices();

  const [firstLoad, setFirstLoad] = useState(true);

  const getAllAlertsSettings = () => {
    getAlertsSettings().then(() => setFirstLoad(false));
  };

  useFocusEffect(
    React.useCallback(() => {
      getAllAlertsSettings();
    }, []),
  );

  return (
    <AlertsSettings
      alertsSettings={alertsSettings}
      stateService={stateService}
      firstLoad={firstLoad}
      setAlertSettingsValue={setAlertSettingsValue}
    />
  );
};
