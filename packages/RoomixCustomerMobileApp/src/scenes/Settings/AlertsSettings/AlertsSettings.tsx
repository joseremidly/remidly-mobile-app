// @ts-nocheck
import React, { useState } from 'react';
import {
  StyleSheet,
  SafeAreaView
} from 'react-native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import LoadingView from 'src/components/organisms/LoadingView';
import HeaderSecondary from 'src/components/organisms/HeaderSecondary';
import AlertsSettingsOrganism from 'src/components/organisms/AlertsSettings';

export interface Props {
  alertsSettings: any;
  stateService: string;
  firstLoad: boolean;
  setAlertSettingsValue: Function;
}

const AlertsSettings: React.FC<Props> = (props: any) => {
  return (
    <>
      <StatusBar backgroundColor='#F4F4F7' barStyle='dark-content' />
      <SafeAreaView style={styles.containerAlertSettings}>
        <HeaderSecondary title="Alertas" />
        {props.firstLoad & (props.stateService === 'loading') ? (
          <LoadingView />
        ) : (
          <AlertsSettingsOrganism
            alertsSettings={props.alertsSettings}
            stateService={props.stateService}
            setAlertSettingsValue={props.setAlertSettingsValue}
          />
        )}
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  containerAlertSettings: {
    backgroundColor: '#F4F4F7',
    height: '100%',
    width: '100%',
  },
});

export default AlertsSettings;
