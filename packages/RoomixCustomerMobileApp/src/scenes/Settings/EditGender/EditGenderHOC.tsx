// @ts-nocheck
import React from 'react';
import { useServices } from 'src/data/providers/ServicesProvider';

export default (EditGender) => (props: any) => {
  const { navigation } = props;
  const { stateService, services: { editDataUser } } = useServices();
  const updateGender = (gender: string) => {
    editDataUser({ gender: gender }).then((res) => {
      if (res) {
        navigation.navigate('EditProfile')
      }
    })
  }
  return (
    <EditGender
      stateService={stateService}
      onClickSaveGender={(gender: string) => updateGender(gender)}
    />
  );
};
