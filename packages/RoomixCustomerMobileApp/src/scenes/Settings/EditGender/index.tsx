// @ts-nocheck
import EditGender from './EditGender';
import EditGenderHOC from './EditGenderHOC';

export default EditGenderHOC(EditGender);
