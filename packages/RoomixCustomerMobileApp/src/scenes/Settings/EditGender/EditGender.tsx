// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderSecondary from 'src/components/organisms/HeaderSecondary';
import EditGenderOrganism from 'src/components/organisms/EditGenderOrganism';

export interface Props {
  stateService: String;
  onClickSaveGender: Function;
}

const EditGender: React.FC<Props> = (props: any) => {
  return (
    <>
      <StatusBar backgroundColor='#F4F4F7' barStyle='dark-content' />
      <SafeAreaView style={styles.containerEditGender}>
        <HeaderSecondary title='Editar género'/>
        <EditGenderOrganism stateService={props.stateService} onChangeGender={props.onClickSaveGender}/>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  containerEditGender: {
    backgroundColor: '#F4F4F7',
    height: '100%',
    width: '100%',
  },
});

export default EditGender;
