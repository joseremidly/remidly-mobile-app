// @ts-nocheck
import React from 'react';
import {
  useFocusEffect,
} from '@react-navigation/native';
import { useServices } from 'src/data/providers/ServicesProvider';

export default (EditProfile) => (props: any) => {
  const { navigation } = props;
  const [dataUser, setDataUser] = React.useState({})

  const { stateService, services: { getDataUser } } = useServices();

  const getAllDataUser = () => {
    getDataUser().then((data: any) => {
      setDataUser(data);
    })
  }
  useFocusEffect(
    React.useCallback(() => {
      getAllDataUser();
    }, [])
  );
  return (
    <EditProfile
      navigation={navigation}
      stateService={stateService}
      dataUser={dataUser}
    />
  );
};
