// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderSecondary from 'src/components/organisms/HeaderSecondary';
import EditProfileButtons from 'src/components/organisms/EditProfileButtons';

export interface Props {
  navigation: Object;
  datauser: Object;
  stateService: String;
}

const EditProfile: React.FC<Props> = (props: any) => {
  const goToRoute = (route: string) => {
    if (route === 'deleteAccount') {
      console.log('Delete account ...')
      // props.navigation.navigate('Login')
    } else {
      console.log('Go to: ' + route)
      props.navigation.navigate(route)
    }
  }
  return (
    <>
      <StatusBar backgroundColor='#F4F4F7' barStyle='dark-content' />
      <SafeAreaView style={styles.containerEditProfile}>
        <HeaderSecondary title='Editar Perfil'/>
        <ScrollView style={styles.containerScrollEditProfileElements}>
          <EditProfileButtons goRoute={(route: string) => goToRoute(route)} dataUser={props.dataUser} stateService={props.stateService} />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  containerEditProfile: {
    backgroundColor: '#F4F4F7',
    height: '100%',
    width: '100%',
  },
  containerScrollEditProfileElements: {
    backgroundColor: '#F4F4F7',
    width: '100%',
  },
});

export default EditProfile;
