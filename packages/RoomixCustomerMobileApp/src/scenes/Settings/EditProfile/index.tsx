// @ts-nocheck
import EditProfile from './EditProfile';
import EditProfileHOC from './EditProfileHOC';

export default EditProfileHOC(EditProfile);
