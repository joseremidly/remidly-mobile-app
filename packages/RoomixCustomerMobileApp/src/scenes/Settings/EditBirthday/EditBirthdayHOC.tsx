// @ts-nocheck
import React from 'react';
import { useServices } from 'src/data/providers/ServicesProvider';

export default (EditBirthday) => (props: any) => {
  const { navigation } = props;
  const { stateService, services: { getDataUser, editDataUser } } = useServices();
  const [dataUser, setDataUser] = React.useState('')

  const updateBirthday = (birthday: string) => {
    console.log('Before to save')
    console.log(birthday)
    editDataUser({ birthday: birthday }).then((res) => {
      if (res) {
        navigation.navigate('EditProfile')
      }
    })
  }
  const getAllDataUser = () => {
    getDataUser().then((data: any) => {
      setDataUser(data);
    })
  }
  React.useEffect(() => {
    getAllDataUser();
  }, [])
  return (
    <>
      {dataUser !== ''
        && (
          <EditBirthday
            stateService={stateService}
            birthday={dataUser.birthday}
            onClickSaveBirthday={(birthday: any) => updateBirthday(birthday)}
          />
        )}
    </>
  );
};
