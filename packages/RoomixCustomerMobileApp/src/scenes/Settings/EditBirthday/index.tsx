// @ts-nocheck
import EditBirthday from './EditBirthday';
import EditBirthdayHOC from './EditBirthdayHOC';

export default EditBirthdayHOC(EditBirthday);
