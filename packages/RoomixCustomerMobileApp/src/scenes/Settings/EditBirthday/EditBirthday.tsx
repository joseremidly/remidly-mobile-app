// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderSecondary from 'src/components/organisms/HeaderSecondary';
import EditBirthdayOrganism from 'src/components/organisms/EditBirthdayOrganism';

export interface Props {
  stateService: String;
  onClickSaveBirthday: Function;
  birthday: Date;
}

const EditBirthday: React.FC<Props> = (props: any) => {
  return (
    <>
      <StatusBar backgroundColor='#F4F4F7' barStyle='dark-content' />
      <SafeAreaView style={styles.containerEditBirthday}>
        <HeaderSecondary title='Fecha de nacimiento'/>
        <EditBirthdayOrganism stateService={props.stateService} onChangeBirthday={props.onClickSaveBirthday} date={props.birthday} />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  containerEditBirthday: {
    backgroundColor: '#F4F4F7',
    height: '100%',
    width: '100%',
  },
});

export default EditBirthday;
