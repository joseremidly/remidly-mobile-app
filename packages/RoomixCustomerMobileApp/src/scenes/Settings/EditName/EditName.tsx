// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import StatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderSecondary from 'src/components/organisms/HeaderSecondary';
import EditNameOrganism from 'src/components/organisms/EditNameOrganism';

export interface Props {
  stateService: String;
  onClickCSaveName: Function;
  name: String;
  lastName: String;
}

const EditName: React.FC<Props> = (props: any) => {
  return (
    <>
      <StatusBar backgroundColor='#F4F4F7' barStyle='dark-content' />
      <SafeAreaView style={styles.containerEditName}>
        <HeaderSecondary title='Editar nombre'/>
        <EditNameOrganism stateService={props.stateService} onChangeField={props.onClickCSaveName} name={props.name !== '' ? props.name : 'Tony'} lastName={props.lastName !== '' ? props.lastName : 'Starkowich'} />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  containerEditName: {
    backgroundColor: '#F4F4F7',
    height: '100%',
    width: '100%',
  },
});

export default EditName;
