// @ts-nocheck
import React from 'react';
import { useServices } from 'src/data/providers/ServicesProvider';

export default (EditName) => (props: any) => {
  const { navigation } = props;
  const { stateService, services: { getDataUser, editDataUser } } = useServices();
  const [dataUser, setDataUser] = React.useState('')

  const updateName = (name: Object) => {
    editDataUser(name).then((res) => {
      if (res) {
        navigation.navigate('EditProfile')
      }
    })
  }
  const getAllDataUser = () => {
    getDataUser().then((data: any) => {
      setDataUser(data);
    })
  }
  React.useEffect(() => {
    getAllDataUser();
  }, [])
  return (
    <>
      {dataUser !== ''
        && (
          <EditName
            stateService={stateService}
            onClickCSaveName={(name: string) => updateName(name)}
            name={dataUser.name}
            lastName={dataUser.last_name}
          />
        )}
    </>
  );
};
