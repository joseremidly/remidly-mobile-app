// @ts-nocheck
import EditName from './EditName';
import EditNameHOC from './EditNameHOC';

export default EditNameHOC(EditName);
