// @ts-nocheck
import LinkedAccounts from './LinkedAccounts';
import LinkedAccountsHOC from './LinkedAccountsHOC';

export default LinkedAccountsHOC(LinkedAccounts);
