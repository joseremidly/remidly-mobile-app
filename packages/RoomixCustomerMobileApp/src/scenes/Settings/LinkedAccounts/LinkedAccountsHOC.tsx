// @ts-nocheck
import React from 'react';
import {
  useFocusEffect,
} from '@react-navigation/native';
import { useServices } from 'src/data/providers/ServicesProvider';

export default (LinkedAccounts) => (props: any) => {
  const [accounts, setAccounts] = React.useState([]);
  const { stateService, services: { getAllAccounts, removeAccount } } = useServices();
  const { navigation } = props;

  const goToRoute = (route: string) => {
    console.log('Go to: ' + route)
    navigation.navigate(route)
  }
  const getAllAccountsUser = () => {
    getAllAccounts().then((data: any) => {
      setAccounts(data.accounts);
    })
  }
  const removeAccounts = (accountId) => {
    removeAccount(accountId)
      .then(res => {
        console.log('Removed account' + accountId)
        console.log(res)
        navigation.navigate('SettingsHome')
      })
  }
  useFocusEffect(
    React.useCallback(() => {
      getAllAccountsUser();
    }, [])
  );
  return (
    <LinkedAccounts
      stateService={stateService}
      accounts={accounts}
      onRemoveAccount={(accountId: any) => removeAccounts(accountId)}
      goToRoute={(route: string) => goToRoute(route)} onSelectAccount={() => goToRoute('Home')}
    />
  );
};
