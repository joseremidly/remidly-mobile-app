// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  ScrollView,
  SafeAreaView,
  View,
  Text,
} from 'react-native';
import CustomStatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderSecondary from 'src/components/organisms/HeaderSecondary';
import LinkedAccountsSettings from 'src/components/organisms/LinkedAccountsSettings';
import LoadingView from 'src/components/organisms/LoadingView';

export interface Props {
  stateService: String;
  accounts: Array;
  onRemoveAccount: Function;
  goRoute: Function;
}

const LinkedAccounts: React.FC<Props> = (props: any) => {
  return (
    <>
      <CustomStatusBar backgroundColor='#F4F4F7' barStyle='dark-content' />
      <SafeAreaView style={styles.containerLinkedAccounts}>
        <HeaderSecondary title='Cuentas conectadas'/>
        {props.stateService === 'loading'
          && (
            <LoadingView />
          )}
        {props.stateService === 'success'
          && (
            <>
              {props.accounts === null
                ? (
                  <View style={styles.containerEmptyAccounts}>
                    <Text style={styles.textEmptyAccounts}>Aún no tienes cuentas conectadas  💳</Text>
                  </View>
                ) : (
                  <ScrollView style={styles.containerScrollLinkedAccountsElements}>
                    <LinkedAccountsSettings accounts={props.accounts} onRemoveAccount={props.onRemoveAccount} goRoute={props.goRoute}/>
                  </ScrollView>
                )
              }
            </>
          )}
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  containerLinkedAccounts: {
    backgroundColor: '#F4F4F7',
    height: '100%',
    width: '100%',
  },
  containerScrollLinkedAccountsElements: {
    backgroundColor: '#F4F4F7',
    width: '100%',
  },
  containerEmptyAccounts: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 50,
  },
  textEmptyAccounts: {
    fontFamily: 'Baloo2-Bold',
    color: '#b3b3b3',
    fontSize: 25,
    lineHeight: 32,
    textAlign: 'center',
    marginTop: 100,
  }
});

export default LinkedAccounts;
