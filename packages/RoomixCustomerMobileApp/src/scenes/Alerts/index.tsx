// @ts-nocheck
import Alerts from './Alerts';
import AlertsHOC from './AlertsHOC';

export default AlertsHOC(Alerts);