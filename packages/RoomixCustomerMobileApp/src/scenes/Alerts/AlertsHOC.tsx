// @ts-nocheck
import React from 'react';
import {
  useFocusEffect,
} from '@react-navigation/native';
import { useAuth } from 'src/data/providers/AuthProvider';
import { useServices } from 'src/data/providers/ServicesProvider';

export default (Alerts) => (props: any) => {
  const { navigation, route } = props;
  const { saveDataUser } = useAuth();
  const { stateService, services: { } } = useServices();

  const onPinSecurity = (data: any) => {
    setKycOpt(data)
      .then(data => {
        if (data) {
          console.log(data)
          navigation.navigate('UploadFrontID', { jobType: route.params.jobType, accountNumber: route.params.accountNumber })
        }
      })
  };
  useFocusEffect(
    React.useCallback(() => {
      console.log('Opt...')
    }, [])
  );
  return (
    <Alerts
      stateService={stateService}
      navigation={navigation}
      onPinSecurity={onPinSecurity}
    />
  );
};