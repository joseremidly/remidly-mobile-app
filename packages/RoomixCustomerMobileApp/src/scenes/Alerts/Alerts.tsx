// @ts-nocheck
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Image,
  Text,
  View
} from 'react-native';
import CustomStatusBar from 'src/components/atoms/CustomStatusBar';
import HeaderAlerts from 'src/components/organisms/HeaderAlerts';

export interface Props {
  navigation: Object;
  stateService: String;
  onPinSecurity: Function;
}

const Alerts: React.FC<Props> = (props: any) => {
  return (
    <>
      <CustomStatusBar backgroundColor='#7b3eff' barStyle='light-content' />
      <SafeAreaView style={styles.sectionOtpContainer}>
        <HeaderAlerts  />
        <View style={styles.containerAlert}>
          <Text style={styles.textAlert}>Aquí aparecerán tus alertas ... </Text>
          <Image style={styles.emoji} resizeMode='contain' source={{uri: 'https://res.cloudinary.com/crecy-io/image/upload/f_auto,q_auto/v1617655780/app/Crecy%20icons%20new%20dimensions/Emoji_Smiley-04_dyzd0w.png'}}></Image>
        </View>
        
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  sectionOtpContainer: {
    backgroundColor: '#F4F4F7',
    height: '100%',
    width: '100%',
  },
  containerAlert: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 30,
  },
  emoji: {
    width: 30,
    height: 30
  },
  textAlert: {
    fontFamily: 'Avenir-Black',
    fontSize: 17,
    color: '#b3b3b3',
    textAlign: 'center',
  }
});

export default Alerts;