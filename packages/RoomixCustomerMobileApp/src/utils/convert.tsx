function convert(value: any) {
    if (value === 0 || value === null) {
      return value;
    } else {
      let n = value.toString()
      while (true) {
        var n2 = n.replace(/(\d)(\d{3})($|,|\.)/g, '$1,$2$3')
        if (n == n2) break
        n = n2
      }
      return n
    }
  }
  
  export default convert;