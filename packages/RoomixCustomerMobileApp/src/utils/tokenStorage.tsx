// @ts-nocheck
import SInfo from 'react-native-sensitive-info';
const TOKEN = 'token';
const TOKEN_REGISTER = 'tokenPreRegister';
const LOGIN_SAVE = 'loginSaved';
// const SHARED_PERFS = 'ObytesSharedPerfs';
// const KEYCHAIN_SERVICE = 'ObytesKeychain';
const SHARED_PERFS = 'shared_preferences';
const KEYCHAIN_SERVICE = 'app';
const keyChainOptions = {
  sharedPreferencesName: SHARED_PERFS,
  keychainService: KEYCHAIN_SERVICE,
};

export async function getItem<T>(key: string): Promise<T | null> {
  const value = await SInfo.getItem(key, keyChainOptions);
  return value ? JSON.parse(value)?.[key] || null : null;
}

export async function setItem<T>(key: string, value: T): Promise<void> {
  SInfo.setItem(key, JSON.stringify({[key]: value}), keyChainOptions);
}

export async function removeItem(key: string): Promise<void> {
  SInfo.deleteItem(key, keyChainOptions);
}

export const getToken = () => getItem<string>(TOKEN);
export const getTokenRegister = () => getItem<string>(TOKEN_REGISTER);
export const getFirstTimeLogin = () => getItem<string>(LOGIN_SAVE);
export const removeToken = () => removeItem(TOKEN);
export const removeTokenRegister = () => removeItem(TOKEN_REGISTER);
export const removeFirstTimeLogin = () => removeItem(LOGIN_SAVE);
export const setTokenRegister = (value: string) => setItem<string>(TOKEN_REGISTER, value);
export const setToken = (value: string) => setItem<string>(TOKEN, value);
export const saveFirstTimeLogin = (phone: string) => setItem<string>(LOGIN_SAVE, phone);