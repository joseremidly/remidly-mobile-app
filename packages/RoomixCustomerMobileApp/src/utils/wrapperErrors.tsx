// @ts-nocheck
const DEFAULT_ERROR = {
  message: 'Error',
  description: 'Error inesperado, intenta más tarde',
  type: 'danger',
}

export const loginError = (error: any) => {
  switch (error.response.status) {
    case 400:
      return {
        message: 'Error',
        description: 'Pin incorrecto',
        type: 'danger',
      }
      default:
        return DEFAULT_ERROR
  }
};

export const saveTokenError = (error: any) => {
  switch (error.response.status) {
    case 400:
      return {
        message: 'Alerta',
        description: 'Notificaciones no activas',
        type: 'warning',
      }
      default:
        return DEFAULT_ERROR
  }
};

export const accountsError = (error: any) => {
  switch (error.response.status) {
    case 400:
      return {
        message: 'Error',
        description: 'Error al eliminar cuenta',
        type: 'danger',
      }
      default:
        return DEFAULT_ERROR
  }
};

export const preRegisterError = (error: any) => {
  switch (error.response.status) {
    case 400:
      return {
        message: 'Error',
        description: 'Este número ya se encuentra registrado',
        type: 'warning',
      }
    default:
      return DEFAULT_ERROR
  }
};

export const verifyUserError = (error: any) => {
  switch (error.response.status) {
    case 400:
      return {
        message: 'Error',
        description: 'Código incorrecto',
        type: 'warning',
      }
    default:
      return DEFAULT_ERROR
  }
};

export const getDataError = (error: any) => {
  switch (error.response.status) {
    case 401:
      return {
        message: 'Error',
        description: 'El usuario no existe',
        type: 'danger',
      }
    default:
      return DEFAULT_ERROR
  }
};

export const getAccountsError = (error: any) => {
  switch (error.response.status) {
    case 401:
      return {
        message: 'Error',
        description: 'Error al obtener cuentas',
        type: 'danger',
      }
    default:
      return DEFAULT_ERROR
  }
};

export const getAlertsError = (error: any) => {
  switch (error.response.status) {
    case 401:
      return {
        message: 'Error',
        description: 'Error al obtener alertas',
        type: 'danger',
      }
    default:
      return DEFAULT_ERROR
  }
};

export const editProfileError = (error: any) => {
  switch (error.response.status) {
    case 401:
      return {
        message: 'Error',
        description: 'Ocurrió un error al actualizar los datos de usuario',
        type: 'danger',
      }
    default:
      return DEFAULT_ERROR
  }
};

export const getAlertsSettingsError = (error: any) => {
  switch (error.response.status) {
    case 401:
      return {
        message: 'Error',
        description: 'Ocurrió un error al cargar las preferencias de las alertas',
        type: 'danger',
      }
    default:
      return DEFAULT_ERROR
  }
};

export const setAlertSettingsValueError = (error: any) => {
  switch (error.response.status) {
    case 401:
      return {
        message: 'Error',
        description: 'Ocurrió un error al actualizar las preferencias de la alertas',
        type: 'danger',
      }
    default:
      return DEFAULT_ERROR
  }
};

export const setCardsValueError = (error: any) => {
  switch (error.response.status) {
    case 400:
      return {
        message: 'Error',
        description: 'Ocurrió un error al cargar la tarjeta',
        type: 'danger',
      }
    default:
      return DEFAULT_ERROR
  }
};

export const uploadIDError = (error: any) => {
  switch (error.response.status) {
    case 500:
      return {
        message: 'Error',
        description: 'No se encontró una identificación',
        type: 'danger',
      }
    default:
      return DEFAULT_ERROR
  }
};

export const uploadSelfieError = (error: any) => {
  switch (error.response.status) {
    case 500:
      return {
        message: 'Error',
        description: 'Error al subir la imagen',
        type: 'danger',
      }
    default:
      return DEFAULT_ERROR
  }
};

export const validateInvitationCodeError = (error: any) => {
  switch (error.response.status) {
    case 404:
      return {
        message: 'Código incorrecto',
        description: 'Verifique su código',
        type: 'danger',
      }
    default:
      return DEFAULT_ERROR
  }
};

export const oxxoPayError = (error: any) => {
  switch (error.response.status) {
    case 404:
      return {
        message: 'Error',
        description: 'Error al generar referencia de Oxxo Pay',
        type: 'danger',
      }
    default:
      return DEFAULT_ERROR
  }
};

export const getBalanceError = (error: any) => {
  switch (error.response.status) {
    case 404:
      return {
        message: 'Error',
        description: 'Error al obtener balance de gastos e ingresos',
        type: 'danger',
      }
    default:
      return DEFAULT_ERROR
  }
};