// @ts-nocheck
import React, { useContext, useState, useEffect } from 'react';
import axios, { AxiosRequestConfig, AxiosInstance } from 'axios';
import config from 'src/data/services/config';
import createServices from 'src/data/services/server';
import { 
  getDataError,
} from 'src/utils/wrapperErrors';
import { useAuth } from 'src/data/providers/AuthProvider';
import { showMessage, hideMessage } from "react-native-flash-message";

type ContextValue = undefined
const Context = React.createContext<ContextValue>(undefined);

export type Props = {
  children: React.ReactNode;
};

const ServicesProvider = (props: Props) => {
  const { children } = props;
  const [stateService, setStateService] = useState('');
  // alert configurations are unkonwn by default
  const [alertsSettings, setAlertsSettings] = useState({});
  const [recipient, setRecipient] = useState({});
  const [remitent, setRemitent] = useState({});
  const [calculator, setCalculator] = useState('');
  const [readyServices, setReadyServices] = useState(false);
  const [creditCode, setCreditCode] = useState('');
  const [skipAddAccount, setSkipAddAccount] = useState(false);
  const { status, userToken, registerToken, removePreRegister } = useAuth();
  const conf: AxiosRequestConfig = {
    baseURL: config.baseURL
  }
  const ApiRoomixServices: AxiosInstance = axios.create(conf);

  ApiRoomixServices.interceptors.request.use(
    (config: AxiosRequestConfig) =>{
      // if (status === 'signIn' && userToken) {
      //   config.headers['Authorization'] = 'Bearer ' + userToken;
      // } else if (status === 'preRegister' && registerToken) {
      //   config.headers['Authorization'] = 'Bearer ' + registerToken;
      // }
      config.headers['Origin'] = 'http://localhost:3000';
      return config;
    },
    error => {
      Promise.reject(error);
    }
  );
  ApiRoomixServices.interceptors.request.use((config: AxiosRequestConfig) => config);
  const Services = createServices(ApiRoomixServices)
  
  const getPrices = async () => {
    setStateService('loading')
    console.log('services provider')
    return Services.getPrices()
      .then(dataUser => {
        setStateService('success')
        return dataUser
      })
      .catch(err => {
        showMessage(getDataError(err));
        setStateService('error')
      })
  }
  const postSendMoney = async (e) => {
    setStateService('loading')
    console.log(e)
    return Services.postSendMoney(e)
      .then(dataUser => {
        setStateService('success')
        return dataUser
      })
      .catch(err => {
        showMessage(getDataError(err));
        setStateService('error')
      })
  }
  const updateCalculator = (e) => {
    setStateService('loading')
    setCalculator(e)
    setStateService('success')
  }
  const updateRecipient = (e) => {
    setStateService('loading')
    setRecipient(e)
    setStateService('success')
  }
  const updateTransaction = (e) => {
    setStateService('loading')
    console.log(e)
    setRemitent(e)
    setStateService('success')
  }
  useEffect(() => {
    setReadyServices(true)
  }, []);
  const value = {
    readyServices,
    stateService,
    alertsSettings,
    calculator,
    skipAddAccount,
    recipient,
    remitent,
    services: {
      getPrices,
      postSendMoney,
      updateCalculator,
      updateRecipient,
      updateTransaction
    }
  }
  return <Context.Provider value={value}>{children}</Context.Provider>;
}

const useServices = () => {
  const context = useContext(Context);
  if (context === undefined) {
    throw new Error('useServices can only be used inside ServiceProvider')
  }
  return context
}

export { ServicesProvider, useServices };