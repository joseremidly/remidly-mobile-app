// @ts-nocheck
import React from 'react';
import {
  getTokenRegister,
  getFirstTimeLogin,
  setTokenRegister,
  saveFirstTimeLogin,
  removeToken,
  removeTokenRegister,
  removeFirstTimeLogin,
} from 'src/utils/tokenStorage.tsx';

interface AuthState {
  userToken: string | undefined | null
  registerToken: string | undefined | null
  stateLoginSaved: string | undefined | null
  dataUser: any | undefined | null
  status: 'idle' | 'signOut' | 'signIn' | 'preRegister' | 'loginSaved'
}

type AuthAction = { type: 'SIGN_IN'; token: string } | { type: 'SAVE_DATA'; data: any } | { type: 'LOGIN_SAVED'; phone: string } | { type: 'PRE_REGISTER'; token: string } | { type: 'SIGN_OUT' }
type AuthPayload = string

interface AuthContextActions {
  saveLogin: (data: AuthPayload) => void
  saveTemporalPhone: (data: AuthPayload) => void
  preRegister: (data: AuthPayload) => void
  signIn: (data: AuthPayload) => void
  signOut: () => void
  removePreRegister: () => void
}

interface AuthContextType extends AuthState, AuthContextActions {}
const AuthContext = React.createContext<AuthContextType>({
  authReady: false,
  status: 'idle',
  userToken: null,
  registerToken: null,
  phoneSaved: null,
  dataUser: null,
  saveLogin: () => {},
  saveDataUser: () => {},
  saveTemporalPhone: () => {},
  preRegister: () => {},
  signIn: () => {},
  signOut: () => {},
  removePreRegister: () => {},
})
export const AuthProvider = ({ children }: { children: React.ReactNode }) => {
  const [state, dispatch] = React.useReducer(AuthReducer, {
    authReady: false,
    status: 'idle',
    userToken: null,
    dataUser: null,
    registerToken: null,
    phoneSaved: null,
  })

  React.useEffect(() => {
    const initState = async () => {
      try {
        const registerToken = await getTokenRegister()
        const loginPhone = await getFirstTimeLogin()
        // -- Uncomment 3 these lines to remove the first time login in app
        await removeToken()
        await removeTokenRegister()
        await removeFirstTimeLogin()
        if (registerToken !== null) {
          dispatch({ type: 'PRE_REGISTER', token: registerToken })
        } else {
          dispatch({ type: 'SIGN_OUT' })
        }
        if (loginPhone !== null) {
          dispatch({ type: 'LOGIN_SAVED', phone: loginPhone })
        }
      } catch (e) {
        dispatch({ type: 'SIGN_OUT' })
      }
      dispatch({ type: 'AUTH_READY' })
    }
    initState()
  }, [])
  const authActions: AuthContextActions = React.useMemo(
    () => ({
      saveTemporalPhone: async (phone: string) => {
        dispatch({ type: 'TEMP_PHONE', phone })
      },
      saveLogin: async (phone: string) => {
        dispatch({ type: 'LOGIN_SAVED', phone })
        await saveFirstTimeLogin(phone)
      },
      preRegister: async (token: string) => {
        dispatch({ type: 'PRE_REGISTER', token })
        await setTokenRegister(token)
      },
      signIn: async (token: string) => {
        dispatch({ type: 'SIGN_IN', token })
        // await setToken(token)
      },
      saveDataUser: async (data: string) => {
        dispatch({ type: 'SAVE_DATA', data })
      },
      signOut: async () => {
        await removeToken()
        await removeTokenRegister()
        await removeFirstTimeLogin()
        dispatch({ type: 'SIGN_OUT' })
      },
      removePreRegister: async () => {
        await removeTokenRegister()
      },
    }),
    []
  )
  return (
    <AuthContext.Provider value={{ ...state, ...authActions }}>{children}</AuthContext.Provider>
  )
}
const AuthReducer = (prevState: AuthState, action: AuthAction): AuthState => {
  switch (action.type) {
    case 'AUTH_READY':
      return {
        ...prevState,
        authReady: true,
      }
    case 'LOGIN_SAVED':
      return {
        ...prevState,
        status: 'loginSaved',
        phoneSaved: action.phone,
      }
    case 'TEMP_PHONE':
      return {
        ...prevState,
        status: 'preRegister',
        phoneSaved: action.phone,
      }
    case 'PRE_REGISTER':
      return {
        ...prevState,
        status: 'preRegister',
        registerToken: action.token,
      }
    case 'SIGN_IN':
      return {
        ...prevState,
        status: 'signIn',
        userToken: action.token,
      }
    case 'SAVE_DATA':
      return {
        ...prevState,
        status: 'signIn',
        dataUser: action.data,
      }
    case 'SIGN_OUT':
      return {
        ...prevState,
        status: 'signOut',
        phoneSaved: null,
        userToken: null,
      }
  }
}

const useAuth = (): AuthContextType => {
  const context = React.useContext(AuthContext)
  if (!context) {
    throw new Error('useAuth must be inside an AuthProvider with a value')
  }
  /*
    const isLoggedIn  = context.status ==== 'signIn'
    return ({ ...context, isloggedIn})
  */
  return context
}

export { AuthProvider, useAuth };