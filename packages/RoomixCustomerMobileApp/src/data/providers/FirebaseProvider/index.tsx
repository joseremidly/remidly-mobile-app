// @ts-nocheck
import React, { useEffect } from 'react';
import { AppRegistry, Alert, Platform } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import { useAuth } from 'src/data/providers/AuthProvider';
import { useServices } from 'src/data/providers/ServicesProvider';

const saveTokenToDatabase = async (token: any, dataUser: any) => {
  // instead, save token to server?
  if (dataUser == null){
    console.log("can't register an anonymous user")
    return
  }
  // Add the token to the users datastore
  await firestore()
    .collection('users')
    .doc(dataUser.user_id)
    .update({
      tokens: firestore.FieldValue.arrayUnion(token),
    });
}

const removeTokenFromDatabase = async (dataUser: any) => {
  // instead, save token to server?
  if (dataUser == null){
    console.log("can't remove a token for an anonymous user")
    return
  }
  // removeToken from database
  console.log('removing token from user', dataUser.user_id)
  // Add the token to the users datastore
  await firestore()
    .collection('users')
    .doc(dataUser.user_id)
    .update({
      tokens: firestore.FieldValue.delete(),
    });
}

const FirebaseProvider = ({ children }: { children: React.ReactNode }) => {
  const { status, dataUser, saveDataUser } = useAuth();
  const { services: { getDataUser, saveDeviceToken } } = useServices();
  const requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    if (enabled) {
      console.log('Authorization notification status:', authStatus);
    }
  }
  useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      console.log('Notification handled in app');
      Alert.alert('Notification received:', JSON.stringify(remoteMessage));
    });
    return unsubscribe;
  }, []);

  useEffect(() => {
    // Listen to whether the token changes
    return messaging().onTokenRefresh(token => {
      saveTokenToDatabase(token, dataUser);
    });
  }, [])

  useEffect(() => {
    // listen the change in Auth status
    if (status === 'signOut') {
      // remove the user's token from firestore
      removeTokenFromDatabase(dataUser);
    } else if (status === 'signIn') {
      getDataUser().then((data: any) => {
        if (data) {
          requestUserPermission()
            .then(res => {
              saveDataUser(data)
              console.log('User logged: ' + data.user_id + '-- get and update token in firestore')
              messaging()
                .getToken()
                .then(token => {
                  console.log('Token de prueba: ' + token)
                  // Register background handler
                  messaging().setBackgroundMessageHandler(async remoteMessage => {
                    console.log('Message handled in the background!', remoteMessage);
                  });
                  AppRegistry.registerComponent('app', () => App);
                  // store in firestore
                  // return saveTokenToDatabase(token, data);
                  saveDeviceToken(Platform.OS, token)
                    .then(res => {
                      console.log('Token saved')
                      console.log(res)
                      return saveTokenToDatabase(token, data);
                    })
                })
                .catch(err => {
                  console.log('Fallo al obtener token')
                  console.log(err)
                })
            })
            .catch(err => {
              console.log('Error al obtener permisos')
              console.log(err)
            })
        } else {
          console.log('error updating the user token -- no such user')
        }
      })
    }
  }, [status]);
  return <>{children}</>;
}

export { FirebaseProvider };
