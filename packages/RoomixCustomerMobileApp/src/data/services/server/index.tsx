// @ts-nocheck
interface ApiConfig {
  ApiRoomixServices?: any;
}
const createServices = (ApiRoomixServices: ApiConfig) => {
  const RoomixServices = {}
  RoomixServices.getPrices = () => {
    console.log('entrando a la peticion')
    return ApiRoomixServices
      .get('calculator/prices')
      .then((response: any) => response.data)
  };
  RoomixServices.postSendMoney = (e) => {
    console.log('entrando a la peticion')
    return ApiRoomixServices
      .post('calculator/calculate',{'amount':e})
      .then((response: any) => response.data)
  };
  return RoomixServices;
};
export default createServices;
