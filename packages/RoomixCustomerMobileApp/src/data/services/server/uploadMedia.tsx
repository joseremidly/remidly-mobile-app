// @ts-nocheck
interface ApiConfig {
  ApiRoomixServices?: any;
}
const createUploadServices = (ApiRoomixServices: ApiConfig) => {
  const RoomixServices = {}
  RoomixServices.uploadID = (data: any) => {
    console.log('Previous to upload :')
    console.log(data)
    const options = {
      headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data"
      }
    };
    return ApiRoomixServices
      .post('/kyc/upload-id', data, options)
      .then((response: any) => response)
  };
  RoomixServices.uploadSelfie = (data: any) => {
    console.log('Previous to upload selfie:')
    console.log(data)
    const options = {
      headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data"
      }
    };
    return ApiRoomixServices
      .post('/kyc/upload-selfie', data, options)
      .then((response: any) => response)
  };
  return RoomixServices;
};
export default createUploadServices;
