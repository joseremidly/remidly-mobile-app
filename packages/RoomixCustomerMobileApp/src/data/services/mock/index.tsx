// @ts-nocheck
import Transactions from 'src/data/services/mock/data/transactions';
import Accounts from 'src/data/services/mock/data/accounts';
import Token from 'src/data/services/mock/data/token';
import userData from 'src/data/services/mock/data/userData';
import advanceConfirmationData from 'src/data/services/mock/data/advanceConfirmationData';
import AlertsSettings from 'src/data/services/mock/data/alertsSettings';
import coursesData from 'src/data/services/mock/data/coursesData';
import storiesData from 'src/data/services/mock/data/storiesData';

// Simulate API call to CrecyServices
const fetch = (mockData: any, time: number) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(mockData)
      // reject({response: {status: 401}})
    }, time)
  })
}

export const getAccountsMock = (token: any) => {
  return fetch(Accounts, 1000)
}

export const preRegisterMock = (data: any) => {
  return fetch(Token, 1000)
}

export const getTransactionsMock = (token: any) => {
  return fetch(Transactions, 1000)
}

export const loginMock = (token: any) => {
  return fetch(Token, 1000)
}

export const userDataMock = (token: any) => {
  return fetch(userData, 1000)
}


export const getAdvanceConfirmationDataMock = (advanceType: string, advanceFee: number) => {
  return fetch(advanceConfirmationData, 1000)
}

export const getAlertsSettingsMock = (token: any) => {
  return fetch(AlertsSettings, 1000)
}

export const setAlertSettingsValueMock = (code, value) => {  
  return fetch(null, 300)
}

export const getCoursesDataMock = () => {  
  return fetch(coursesData, 250)
}

export const setGoalMock = (newGoal) => {
  coursesData.data.goalsTotal = newGoal;
  return fetch(null, 250)
}

export const getStoriesDataMock = () => {  
  return fetch(storiesData, 250)
}

export const setLessonDoneMock = () => {
  coursesData.data.goalsCompleted += 1;
  return fetch(null, 250)
}

