// @ts-nocheck
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { AuthStackNavigator } from './auth-navigator';
import { HomeStackScreen } from './app-tab-navigator';
import { SettingsStackNavigator } from './settings-navigator';
import { FinancialEducationStackNavigator } from './financial-education-navigator';
import { KycStackNavigator } from './kyc-navigator';
import { ExpensesTrackingStackNavigator } from './expenses-tracking-navigator';
import { CreditOnboardingStackNavigator } from './credit-onboarding-navigator';
import { CreditLineStackNavigator } from './credit-line-navigator';

import BelvoLinkAccount from 'src/scenes/Home/BelvoLinkAccount';
import PalencaLinkAccount from 'src/scenes/Home/PalencaLinkAccount';

let headerDefaultNavigationConfig = {
  headerStyle: {
    backgroundColor: '#ffffff',
  },
  cardStyle: {
    backgroundColor: '#ffffff',
  },
  headerTintColor: "#fff"
};

const Stack = createStackNavigator();

export const AppNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="AuthNavigator"
      screenOptions={headerDefaultNavigationConfig}
      unmountInactiveRoutes={true}
    >
      <Stack.Screen
        name="AuthNavigator"
        component= {AuthStackNavigator}
        options={{
          headerShown: false,
        }}
      />
      {/* <Stack.Screen
        name="PalencaLinkAccount"
        component={PalencaLinkAccount}
        options={{
          headerShown: false,
        }}
      /> */}
      <Stack.Screen
        name="Home"
        component= {HomeStackScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="SettingsHome"
        component= {SettingsStackNavigator}
        options={{
          headerShown: false,
        }}
      />
      {/* <Stack.Screen
        name="FinancialEducationHome"
        component= {FinancialEducationStackNavigator}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="ExpensesTrackingHome"
        component= {ExpensesTrackingStackNavigator}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="KycFlowStart"
        component= {KycStackNavigator}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="CreditOnboardingStart"
        component= {CreditOnboardingStackNavigator}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="CreditLine"
        component= {CreditLineStackNavigator}
        options={{
          headerShown: false,
        }} */}
      
    </Stack.Navigator>
  )
}