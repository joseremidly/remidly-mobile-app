// @ts-nocheck
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SettingsHome from 'src/scenes/Home/SettingsHome';
import AlertsSettings from 'src/scenes/Settings/AlertsSettings';
import EditProfile from 'src/scenes/Settings/EditProfile';
import EditName from 'src/scenes/Settings/EditName';
import EditEmail from 'src/scenes/Settings/EditEmail';
import EditGender from 'src/scenes/Settings/EditGender';
import EditBirthday from 'src/scenes/Settings/EditBirthday';
import LinkedAccounts from 'src/scenes/Settings/LinkedAccounts';

let headerDefaultNavigationConfig = {
  headerStyle: {
    backgroundColor: '#ffffff',
  },
  cardStyle: {
    backgroundColor: '#ffffff',
  },
  headerTintColor: "#ffffff"
};

const Stack = createStackNavigator();

export const SettingsStackNavigator = () => {
  return (
    <>
      <Stack.Navigator
        initialRouteName='SettingsHome'
        unmountInactiveRoutes={true}
        screenOptions={headerDefaultNavigationConfig}
      >
        <Stack.Screen
          name="SettingsHome"
          component={SettingsHome}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="AlertsSettings"
          component={AlertsSettings}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="EditProfile"
          component={EditProfile}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="EditName"
          component={EditName}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="EditEmail"
          component={EditEmail}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="EditGender"
          component={EditGender}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="EditBirthday"
          component={EditBirthday}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="LinkedAccounts"
          component={LinkedAccounts}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </>
  )
}