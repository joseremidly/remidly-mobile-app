// @ts-nocheck
import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Platform, NativeModules } from 'react-native';

import IconNavigationBar from 'src/components/atoms/IconNavigationBar'
import Calculator from 'src/scenes/Home/Calculator';
import Recipient from 'src/scenes/Home/Recipient';
import Transactions from 'src/scenes/Home/Transactions';
import Succesful from 'src/scenes/Home/Succesful';
import Alerts from 'src/scenes/Alerts'

let tabDefaultNavigationConfig = {
  showLabel: false,
  animationEnabled: true,
  keyboardHidesTabBar: true,
  safeAreaInsets: {
    bottom: Platform.select({
      ios: 17,
      android: 0,
    }),
  },
  cardStyle: {
    backgroundColor: 'white',
  },
  style: {
    backgroundColor: '#7b3eff',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    height: Platform.select({
      ios: '9%',
      android: 55,
    }),
  }
};

const Tab = createBottomTabNavigator();

export const  HomeStackScreen = () => {
  React.useEffect(() => {
    if (Platform.OS  === 'android') {
      NativeModules.FullScreen.enable();
    }
  }, []);
  return (
    <Tab.Navigator
      initialRouteName="Calculator"
      tabBarOptions={tabDefaultNavigationConfig}
    >
      <Tab.Screen
        name="Calculator"
        component={Calculator}
        options={{
          tabBarIcon: ({ focused }) => (
            <IconNavigationBar size={22} selected={focused} src={require('assets/images/crecy_icons_bottom_bar_home.png')} />
          ),
          unmountOnBlur: true
        }}
      />
      <Tab.Screen
        name="Recipient"
        component={Recipient}
        options={{
          tabBarIcon: ({ focused }) => (
            <IconNavigationBar size={21} selected={focused} src={require('assets/images/crecy_icons_bottom_bar_analytics.png')} />
          ),
          unmountOnBlur: true
        }}
      />
      <Tab.Screen
        name="Transactions"
        component={Transactions}
        options={{
          tabBarIcon: ({ focused }) => (
            <IconNavigationBar size={21} selected={focused} src={require('assets/images/crecy_icons_bottom_bar_transaction.png')} />
          ),
          unmountOnBlur: true
        }}
      />
      <Tab.Screen
        name="Succesful"
        component={Succesful}
        options={{
          tabBarIcon: ({ focused }) => (
            <IconNavigationBar size={26} selected={focused} src={require('assets/images/crecy_icons_bottom_bar_financial_education.png')} />
          ),
          unmountOnBlur: true
        }}
      />
      <Tab.Screen
        name="Alerts"
        component={Alerts}
        options={{
          tabBarIcon: ({ focused }) => (
            <IconNavigationBar size={22} selected={focused} src={require('assets/images/crecy_icons_bottom_bar_alerts.png')} />
          ),
          unmountOnBlur: true
        }}
      />
    </Tab.Navigator>
  );
}