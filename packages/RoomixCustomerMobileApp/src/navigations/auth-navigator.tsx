// @ts-nocheck
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { useAuth } from 'src/data/providers/AuthProvider';

import Login from 'src/scenes/Login';
import PhoneForm from 'src/scenes/CreateAccount/PhoneForm';
import CodeForm from 'src/scenes/CreateAccount/CodeForm';
import OneTimePassword from 'src/scenes/Login/OneTimePassword';
import SuccessfulCode from 'src/scenes/CreateAccount/SuccessfulCode';
import StartRegisterMessage from 'src/scenes/CreateAccount/StartRegisterMessage';
import EmailForm from 'src/scenes/CreateAccount/EmailForm';
import PinForm from 'src/scenes/CreateAccount/PinForm';
import ConfirmPinForm from 'src/scenes/CreateAccount/ConfirmPinForm';
import EnableTouchID from 'src/scenes/CreateAccount/EnableTouchID';
import ScanTouchID from 'src/scenes/CreateAccount/ScanTouchID';
import LoginForm from 'src/scenes/Login/LoginForm';
import LoginPinForm from 'src/scenes/Login/LoginPinForm';
import ResetPin from 'src/scenes/Login/ResetPin';

let headerDefaultNavigationConfig = {
  headerStyle: {
    backgroundColor: '#ffffff',
  },
  cardStyle: {
    backgroundColor: '#ffffff',
  },
  headerTintColor: "#fff"
};

const Stack = createStackNavigator();

export const AuthStackNavigator = () => {
  const { authReady, status } = useAuth();
  return (
    <>
      {authReady
        && (
          <Stack.Navigator
            initialRouteName={status === 'loginSaved' ? 'LoginPinForm' : (status === 'preRegister' ? 'CodeForm' : 'Login')}
            // initialRouteName='NameForm'
            unmountInactiveRoutes={true}
            screenOptions={headerDefaultNavigationConfig}
          >
            <Stack.Screen
              name="Login"
              component={Login}
              options={{
                headerShown: false,
              }}
            />
            {/* <Stack.Screen
              name="LoginForm"
              component={LoginForm}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="LoginPinForm"
              component={LoginPinForm}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="CodeForm"
              component={CodeForm}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="SuccessfulCode"
              component={SuccessfulCode}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="StartRegisterMessage"
              component={StartRegisterMessage}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="EmailForm"
              component={EmailForm}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="PinForm"
              component={PinForm}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="ConfirmPinForm"
              component={ConfirmPinForm}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="ResetPin"
              component={ResetPin}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="OneTimePassword"
              component={OneTimePassword}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="EnableTouchID"
              component={EnableTouchID}
              options={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="ScanTouchID"
              component={ScanTouchID}
              options={{
                headerShown: false,
              }}
            /> */}
          </Stack.Navigator>
        )}
    </>
  )
}