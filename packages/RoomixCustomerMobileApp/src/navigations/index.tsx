// @ts-nocheck
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { ServicesProvider } from 'src/data/providers/ServicesProvider';
import { AuthProvider } from 'src/data/providers/AuthProvider';
import { FirebaseProvider } from 'src/data/providers/FirebaseProvider';
import { AppNavigator } from './app-navigator';
import FlashMessage from "react-native-flash-message";

const Navigator = () => {
  return (
    <AuthProvider>
      <ServicesProvider>
        <FirebaseProvider>
          <NavigationContainer>
            <AppNavigator />
            <FlashMessage position="top" animationDuration={500} />
          </NavigationContainer>
        </FirebaseProvider>
      </ServicesProvider>
    </AuthProvider>
  );
};

export default Navigator;