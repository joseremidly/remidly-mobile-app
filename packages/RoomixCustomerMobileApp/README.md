![Image of Yaktocat](https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2020-09-03/1339372310645_4588ca7993ec777d3d0a_132.png)

# Customer's Mobile

A react native mobile app to allow our customer have a better financial life.


## Installation

We simply need to send the app to a phone/emulator by running (on the project root):

```bash
npx lerna --scope=CrecyCustomerMobileApp run android/ios
```

For android sync
```bash
adb reverse tcp:8081 tcp:8081
```

*Note: in case the previous commnad doesn't start the react native server, simply run (inside the react native project):

```bash
yarn start
```