![Image of Yaktocat](https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2020-09-03/1339372310645_4588ca7993ec777d3d0a_132.png)

# Core Package

The idea of this package is to have all the business logic isolated, so we can reuse it in many frontends.


## Installation

We simply need to transpile from typescript to vanilla js by running (on the project root):

```bash
npx lerna npx lerna --scope=@crecy/core run build
```

*Note: in case you have a write error, please simply remove your dist folder, I'll fix this later