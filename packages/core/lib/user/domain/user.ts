export class User {
    token: string;
    phone: string;

    constructor(phone: string) {
        this.phone = phone;
    }
}