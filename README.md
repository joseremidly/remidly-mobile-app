![Image of Yaktocat](https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2020-09-03/1339372310645_4588ca7993ec777d3d0a_132.png)
# Frontends Monorepo

The idea behind this monorepo is to have all the posible frontend applications in the same place
so we can share code between them and move faster.

## Packages

We are currently working on to different packages for crecy:
 * Core: this module has the responsibility to handle all the reusable business logic.
 * CrecyCustomerMobileApp: Our customers react native app.
## Installation

We use [lerna](https://github.com/lerna/lerna) to correctly manage the monorepo.

To run the project we first need to run

If you are sure the module exists, try these steps:
 * Clear watchman watches: watchman watch-del-all
 * Delete node_modules: rm -rf node_modules and run yarn install
 * Reset Metro's cache: yarn start --reset-cache
 * Remove the cache: rm -rf /tmp/metro-*
 
```bash
rm -rf ./packages/*/dist

npx lerna bootstrap
npx lerna --scope=@crecy/core run build
npx lerna --scope=CrecyCustomerMobileApp run ios

npx lerna --scope=CrecyCustomerMobileApp run android
adb reverse tcp:8081 tcp:8081
```
## Generate Debug
```bash
react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res

./gradlew assembleDebug
```
## Generate Release
```bash
react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/build/intermediates/res/merged/release/ && rm -rf android/app/src/main/res/drawable-* && rm -rf android/app/src/main/res/raw/*

./gradlew bundleRelease
```
## Upload to app distribution
```bash
firebase appdistribution:distribute app-arm64-v8a-debug.apk  \
    --app 1:639612340731:android:9b7e1c3a87adaf6d7e635e  \
    --release-notes "Description release note"
```

And the follow the instructions on every package.

## Contributing

We are using clean architecture and clean code so please make sure to follow those rules!